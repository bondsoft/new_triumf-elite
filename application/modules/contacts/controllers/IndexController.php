<?php
/**
 * 
 * 
 * 
 * 
 */
class Contacts_IndexController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
	    $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('contacts');	
    } 
  
    function indexAction()
    {	
		$contacts_model = new Default_Model_DbTable_Contacts();
		$page_contacts = $contacts_model ->fetchAll();
		$this->view->page = $page_contacts;
		
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
       
    }
    
    function contactAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $form = new Default_Form_Contact_Contact(); 
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
                  
                $name       = $form->getValue('name'); 
                $email      = $form->getValue('email');
                $phone      = $form->getValue('phone');
                $message      = $form->getValue('message');
				
				if(!$phone) $phone = '';
			//	if(!$email) $email = '';
                $model = new Default_Model_DbTable_Messages();
                if( @$_POST['check'] != NULL ) { 
					$model->addMessage(
											$name,
											$email,
											$phone,
											$message
										);
             
				require_once('idna_convert.class.php');
				$idn = new idna_convert(array('idn_version'=>2008));
				$punycode=isset($_SERVER['SERVER_NAME']) ? stripslashes($_SERVER['SERVER_NAME']) : '';
				$punycode=(stripos($punycode, 'xn--')!==false) ? $idn->decode($punycode) : $idn->encode($punycode);
         
         //   $addr = "kovalchyk20101@gmail.com";
			
				$addr = Zend_Registry::get('Email');
				$sub = "Сообщение с сайта $punycode";
					
					$mes = "\nИмя: $name \nТелефон: $phone \nE-mail: $email \n Сообщение: $message";
					mail ($addr,$sub,$mes);
					
					header('Content-Type: application/json');
					echo Zend_Json::encode(array('success' => true));
					exit; 
				}
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
        echo Zend_Json::encode($json);
        exit;
    
    }
	
	function sendrequestAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $form = new Default_Form_Contact_Request(); 
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
                  
                $user_name       = $form->getValue('user_name'); 
                $user_email      = $form->getValue('user_email');
                $user_phone      = $form->getValue('user_phone');
                
				$length = $_POST['length'];
				$widthsize = $_POST['widthsize'];
				$product_id = $_POST['product_id'];
				$product_model = $_POST['product_model'];
				
				$order_model = new Default_Model_DbTable_Order();
				$order_product_model = new Default_Model_DbTable_Orderproduct();
                
                $order_model->addOrder(
											$user_name,
											$user_phone,
											$user_email
										);
				$order_id = $order_model->getLastInsertId();
				$this->view->order_id = $order_id;
				
				$order_product_model->addProductOrder(
														$order_id,
														$product_id,
														$length,
														$widthsize
													);
             
			$site_url = $_SERVER['SERVER_NAME']; //Zend_Debug::dump($site_url); exit;
        //    $addr = "kovalchyk20101@gmail.com";
            $addr = Zend_Registry::get('Email'); //Zend_Debug::dump($addr); //exit;
           
            $subject = "Ваш заказ в магазине $site_url";
            
            $comment = 'Номер вашего заказа: '.$order_id.'<br><br>'; 
			
			$comment .= '<br>'.$product_model.'&nbsp;&nbsp;&nbsp;  Длина: '.$length. '.  &nbsp;&nbsp;&nbsp;  Ширина: '.$widthsize.'<br>';
			
			$mail = new Zend_Mail('UTF-8');
			$mail->setBodyHtml($comment);
			$mail->setFrom($addr, $site_url);
			$mail->addTo($user_email, $user_name);
			$mail->setSubject($subject);
			$mail->send();
			
			$mail = new Zend_Mail('UTF-8');
			$mail->setBodyHtml($comment);
			$mail->setFrom($addr, $site_url);
			$mail->addTo($addr, $site_url);
			$mail->setSubject($subject);
			$mail->send();
            
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
        echo Zend_Json::encode($json);
        exit;
    
    }
	
	function orderAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $form = new Default_Form_Contact_Order(); 
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
                  
                $name       = $form->getValue('name'); 
                $email      = $form->getValue('email');
                $phone      = $form->getValue('phone');
                $message      = $form->getValue('message');
				
				if(!$name) $name = '';
				if(!$email) $email = '';
                $model = new Default_Model_DbTable_Messages();
                
                $model->addMessage(
                                        $name,
										$email,
                                        $phone,
                                        $message
                                    );
             
			$site_url = $_SERVER['SERVER_NAME']; //Zend_Debug::dump($site_url); exit;
        //    $addr = "kovalchyk20101@gmail.com";
            $addr = Zend_Registry::get('Email'); //Zend_Debug::dump($addr); //exit;
            $sub = "Заказ с сайта $site_url"; 
            
            $mes = "\nНеобходимая занрузка: $message \nТелефон: $phone";
            mail ($addr,$sub,$mes);
            
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
        echo Zend_Json::encode($json);
        exit;
    
    }
    
    function getpricevalidateAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $form = new Default_Form_Contact_Getprice(); 
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
                
			$name       = $form->getValue('name'); 
			$email      = $form->getValue('email');
			$phone      = $form->getValue('phone');
			$message      = $form->getValue('message');
				
				if(!$email) $email = '';
				if(!$message) $message = 'Прошу связаться со мной для получения прайса';
                $model = new Default_Model_DbTable_Messages();
                
                $model->addMessage(
                                        $name,
										$email,
                                        $phone,
                                        $message
                                    );
									
			require_once('idna_convert.class.php');
			$idn = new idna_convert(array('idn_version'=>2008));
			$punycode=isset($_SERVER['SERVER_NAME']) ? stripslashes($_SERVER['SERVER_NAME']) : '';
			$punycode=(stripos($punycode, 'xn--')!==false) ? $idn->decode($punycode) : $idn->encode($punycode);
         
         //   $addr = "kovalchyk20101@gmail.com";
			
            $addr = Zend_Registry::get('Email');
            $sub = "Сообщение с сайта $punycode";
            
            $mes = "\nИмя: $name \nТелефон: $phone \nE-mail: $email \n Сообщение: $message";
            mail ($addr,$sub,$mes);
            
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
        echo Zend_Json::encode($json);
        exit;
    
    }
	
	function callbackvalidateAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $form = new Default_Form_Contact_Callback(); 
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
                
			$name       = $form->getValue('callback_name'); 
			$email      = $form->getValue('callback_email');
			$phone      = $form->getValue('phone');
			$message      = $form->getValue('message');
				
				if(!$email) $email = '';
				if(!$phone) $phone = '';
				if(!$message) $message = 'Прошу связаться со мной для получения прайса';
                $model = new Default_Model_DbTable_Messages();
                
                $model->addMessage(
                                        $name,
										$email,
                                        $phone,
                                        $message
                                    );
									
			require_once('idna_convert.class.php');
			$idn = new idna_convert(array('idn_version'=>2008));
			$punycode=isset($_SERVER['SERVER_NAME']) ? stripslashes($_SERVER['SERVER_NAME']) : '';
			$punycode=(stripos($punycode, 'xn--')!==false) ? $idn->decode($punycode) : $idn->encode($punycode);
         
         //   $addr = "kovalchyk20101@gmail.com";
			
            $addr = Zend_Registry::get('Email');
            $sub = "Сообщение с сайта $punycode";
            
            $mes = "\nИмя: $name \nE-mail: $email \n Сообщение: $message";
            mail ($addr,$sub,$mes);
            
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
        echo Zend_Json::encode($json);
        exit;
    
    }
	
	function requestAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $form = new Default_Form_Contact_Contact(); 
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
                
			$name       = $form->getValue('name'); 
			$email      = $form->getValue('email');
			$phone      = $form->getValue('phone');
			$message      = $form->getValue('message');
				
				if(!$email) $email = '';
				if(!$message) $message = 'Прошу связаться со мной для сотрудничества';
                $model = new Default_Model_DbTable_Messages();
                
                $model->addMessage(
                                        $name,
										$email,
                                        $phone,
                                        $message
                                    );
         
         //   $addr = "kovalchyk20101@gmail.com";
			$site_url = $_SERVER['SERVER_NAME'];
            $addr = Zend_Registry::get('Email');
            $sub = "Сообщение с сайта $site_url";
            
            $mes = "\nИмя: $name \nТелефон: $phone \n Сообщение: $message";
            mail ($addr,$sub,$mes);
            
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
        echo Zend_Json::encode($json);
        exit;
    
    }
   
}