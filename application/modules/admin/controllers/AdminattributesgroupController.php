<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminattributesgroupController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    function indexAction()
    {
        $attributesgroup_model = new Default_Model_DbTable_Attributesgroup();

        $attributesgroup = $attributesgroup_model->getAllAttributesgroupAdmin(); //Zend_Debug::dump($categories_01);exit;
     
        $paginator = Zend_Paginator::factory($attributesgroup);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);
       
    }
   
    public function addattributesgroupAction()
    {
        $form = new Default_Form_Attributesgroup_Attributesgroup();
        $this->view->form = $form;
      //  Zend_Debug::dump($form);exit;
        $attributesgroup_model = new Default_Model_DbTable_Attributesgroup();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $sort_order = $form->getValue('sort_order');
                $attribute_group_name = $form->getValue('attribute_group_name');    //Zend_Debug::dump($menu_first_level_name_ru);exit;
                
                $attributesgroup_model->addAttributesgroup(
                                                            $attribute_group_name,
                                                            $sort_order                                                           
                                                        );
                
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }
   
   
    public function editattributesgroupAction()
    {
        $attribute_group_id = $this->getRequest()->getParam('attribute_group_id');
        $form = new Default_Form_Attributesgroup_Attributesgroup();
        $this->view->form = $form;
      //  Zend_Debug::dump($category_id);exit;
        $attributesgroup_model = new Default_Model_DbTable_Attributesgroup();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {  
                $attribute_group_id = $form->getValue('attribute_group_id');
                $sort_order = $form->getValue('sort_order');
                $attribute_group_name = $form->getValue('attribute_group_name'); 
                
 
                $attributesgroup_model->updateAttributesgroup(
                                                                $attribute_group_id, 
                                                                $attribute_group_name,
                                                                $sort_order 
                                                            );
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $attribute_group_id = $this->_getParam('attribute_group_id', 0);
            if ($attribute_group_id > 0)
            {
                $form->populate($attributesgroup_model->getAttributesgroup($attribute_group_id));
                $attributesgroup_object = $attributesgroup_model->getAttributesgroup($attribute_group_id);
                $this->view->attributesgroup_name = $attributesgroup_object['attribute_group_name'];
            }
        }
    }
    
    public function deleteattributesgroupAction()
    {
        $attributesgroup_model = new Default_Model_DbTable_Attributesgroup();
        $attribute_group_id = $this->getRequest()->getParam('attribute_group_id');
        $page = $this->getRequest()->getPost('page');
        if ($this->getRequest()->isPost())
        {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
            {
                $attributes_model = new Default_Model_DbTable_Attributes();
                
                $first_attribute_id = $attributes_model->getFirst_Attribute($attribute_group_id);
             // Zend_Debug::dump($first_attribute_id);exit;
                if($first_attribute_id != NULL){
                    $this->view->errMessage = "Эта группа атрибутов активна. Удалить нельзя!";
                    return;
                } else {
                    $attributesgroup_model->deleteAttributesgroup($attribute_group_id);
                   
                } 
               
            }
            $this->_helper->redirector('index', 'adminattributesgroup', 'admin', array('page' => $page));
        } else {
            $attribute_group_id = $this->_getParam('attribute_group_id');
            $attributesgroup = $attributesgroup_model->getAttributesgroup($attribute_group_id);
            
            $this->view->attributesgroup = $attributesgroup;
        }
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $categories_model = new Default_Model_DbTable_Categories();
        $category_id = $this->getRequest()->getParam('category_id');
        
        $old_photo_file = $categories_model->getCategoryImage($category_id);
        $directory = "media/photos/categories";
        
        if ($this->getRequest()->isPost())
        {
            $category_id = $this->getRequest()->getParam('category_id');
            $main_photo = '';
           
            $categories_model->deleteCategoryImage(
                                                    $category_id,  
                                                    $main_photo
                                                );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
}