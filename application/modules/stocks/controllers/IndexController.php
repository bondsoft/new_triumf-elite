<?php
/**
 *
 * Default controller for this application.
 * 
 */
class Stocks_IndexController extends Zend_Controller_Action
{  
    public function init()
    {
        /* Initialize action controller here */ 
    }
	
    public function indexAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('stocks');
		
        $stocksPerPage = 10;
        $stocks_model = new Default_Model_DbTable_Stocks();
		
		$stocks = $stocks_model->getAllStocksFront(); //Zend_Debug::dump($stocks);exit;
	  
		$paginator = Zend_Paginator::factory($stocks);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage($stocksPerPage);
        $stocksArray = $stocks->toArray();
        $maxNumberOfStocks = count($stocksArray);
        $maxNumberOfPages = ceil($maxNumberOfStocks / $stocksPerPage);

        if($this->_getParam('page', 1) > $maxNumberOfPages) {
            $this->getResponse()->setHttpResponseCode(404);
            $layout->setLayout('page_not_found_layout');
        }
       
        $stocks = new Default_Model_DbTable_Stocksseo();
        $stocks_name = $stocks->fetchAll();
        
        $page = Zend_Paginator::factory($stocks_name);

        $this->view->page = $page;

        $sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
    }
	
	public function stockmenuAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $stocks_model = new Default_Model_DbTable_Stocks();
        
        $stocks = $stocks_model->getAllStocksFront();
		//Zend_Debug::dump($config_telephone);exit;
        
        $this->view->stocks = $stocks;   
    }
	
	public function recommendationsAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $stocks_model = new Default_Model_DbTable_Stocks();
        
        $intro_text = $stocks_model->getStockIntroText('rekomendacii-po-ustanovke-karniza'); //Zend_Debug::dump($page);exit;
        $this->view->intro_text = $intro_text; 
    }

	function stockAction()
	{  
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('stock_item');
	//	$form = new Default_Form_Reviews_AddReviewFront(); 
	//	$this->view->form = $form;
		
		$stocks_model = new Default_Model_DbTable_Stocks();
     
        $url_seo = $this->getRequest()->getParam('url_seo'); // Zend_Debug::dump($stock_id);exit;
        
        $stock_title = $stocks_model->getStockTitle($url_seo); //Zend_Debug::dump($page);exit;
        $this->view->stock_title = $stock_title;
		
		$page = $stocks_model->getStockPage($url_seo); //Zend_Debug::dump($page);exit;
        $this->view->page = $page;
        $paginator = Zend_Paginator::factory($page);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(1);
        
        $photo_model = new Default_Model_DbTable_Stocksphoto();
        
        $stock_id = $stocks_model->getStockIdByUrl($url_seo);
        $this->view->stock_id = $stock_id;
        $photos = $photo_model->getAllPhotosForObject($stock_id); //Zend_Debug::dump($photos);exit;
        
        $this->view->photos = $photos;
	//	$reviews_model = new Default_Model_DbTable_Reviews();
	//	$reviews = $reviews_model->getRatingForIndex($stock_id);
	//	Zend_Debug::dump($reviews);exit;
	//	$paginator_reviews = Zend_Paginator::factory($reviews);
     //   $paginator_reviews->setCurrentPageNumber($this->_getParam('page', 1));
    //    $this->view->paginator_reviews = $paginator_reviews;
    //    $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(7); 
		
	/*	foreach ($reviews as $key => $value) {
			$answers[$key] = $reviews_model->getAnswersForIndex($reviews[$key]['review_id']);
		} */
	//	$this->view->answers = @$answers;
		//Zend_Debug::dump($answers[1][0]['text']);exit;
	}	
	
	public function stocksfrontAction()
    {
        $stocks_model = new Default_Model_DbTable_Stocks();
		
		$stocks = $stocks_model->getAllStocksRanrom(); //Zend_Debug::dump($stocks);exit;
	  
        $this->view->stocks = $stocks;
        
    }
	
	function sendreviewAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        
        $form = new Default_Form_Reviews_AddReviewFront(); 
        $form->isValid($this->_getAllParams());
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();//Zend_Debug::dump($userdata);exit;
	    @$user_id = $userdata->user_id; 
        @$realname = $userdata->realname;
		@$surname = $userdata->surname;
		@$user_name = $realname.' '.$surname;
        if ($form->isValid($request->getPost())) {
                 
				$parent_id = @$_POST['rev_id']; //Zend_Debug::dump($rev_id);exit;
				$stock_id = $_POST['stock_id'];
				$user_id       = $user_id; 
                $author       = $user_name; 
                $text      = $form->getValue('text');
				$status = 1;
           
                $reviews_model = new Default_Model_DbTable_Reviews();
                if($parent_id) {
					$reviews_model->addAnswer(
												$stock_id,
												$parent_id,
												$user_id,
												$author,
												$text,
												$status
											);
				
				} else {
					$reviews_model->addReview(
												$stock_id,
												$user_id,
												$author,
												$text,
												$status
											);
				}
										
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
     //   echo Zend_Json::encode($json);
     //   exit;
       
        echo Zend_Json::encode($json);
        exit;
    
    }
}
