<?php
/**
 * 
 *
 *
 */
class Default_Form_Members_Account extends Zend_Form
{

  public function init()
  {     
        $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
		$this->addElement('Hidden', 'user_role', array(
          'value' => 'member'
        ));
		
		// Element: username
    /*    $this->addElement('Text', 'username', array(
          'label' => 'Имя:',
		  'size'  => '30', 
		  'class'  => 'text',
          'required' => true,
          'filters'    => array('StringTrim'),
		  'validators' => array(
            array('NotEmpty', true),
			array('Db_NoRecordExists', false, array('zend_users', 'username'))
          ),
        )); */
        
    /*    $this->addElement('Text', 'username', array(
            'label' => '',
            'value' => '',
            'size'  => '30', 
            'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('NotEmpty', true),
                array('Db_NoRecordExists', false, array('zend_users', 'username'))
            ),
            'attribs' => array('class' => 'valid', 'placeholder' => 'Логин'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        ));   
        
        $element = $this->getElement('username');
        $element->removeDecorator('Label'); */
        
    /*    $username = new Zend_Form_Element_Text('username');
        $username->setLabel("SEO Url:")
            ->setAttrib('class', 'span10')
			->removeDecorator('label')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
            ->addValidator('Db_NoRecordExists', false, array('zend_users', 'username')); */

        $this->addElement('Text', 'realname', array(
			'label' => 'Имя:',
		//	'size'  => '30',
		//	'class'  => 'text',		  
			'required' => true,
			'filters'    => array('StringTrim'),
			'validators' => array(
				array('NotEmpty', true)
			),
			'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signup-username', 'placeholder' => 'Имя'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        )); 
        $element = $this->getElement('realname');
        $element->removeDecorator('Label'); 

        $this->addElement('Text', 'surname', array(
			'label' => 'Фамилия:',
		//	'size'  => '30', 
		//	'class'  => 'text',
			'required' => true,
			'filters'    => array('StringTrim'),
			'validators' => array(
				array('NotEmpty', true)
			),
			'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signup-userlastname', 'placeholder' => 'Фамилия'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        ));
        $element = $this->getElement('surname');
        $element->removeDecorator('Label'); 
        
        $this->addElement('Text', 'town', array(
			'label' => 'Город:',
		//	'size'  => '30', 
		//	'class'  => 'text',
			'required' => true,
			'filters'    => array('StringTrim'),
			'validators' => array(
				array('NotEmpty', true)
			),
			'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signup-usercity', 'placeholder' => 'Город'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        ));
        $element = $this->getElement('town');
        $element->removeDecorator('Label'); 
    	
	
		// Element: email
        $this->addElement('Text', 'email', array(
			'label' => 'Email Address:',
			'size'  => '30', 
		//	'class'  => 'text',
			'required' => true,
			'allowEmpty' => false,
			'validators' => array(
				array('NotEmpty', true),
				array('EmailAddress', true),
				array('Db_NoRecordExists', false, array('zend_users', 'email'))
			),
			'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signup-email', 'placeholder' => 'Электронная почта'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
		));
        $element = $this->getElement('email');
        $element->removeDecorator('Label'); 
		 
		
        // Init password
		$this->addElement('Password', 'password', array(
			'label' => $translate->translate('Password:'),
		//	'size'  => '30',
		//	'class'  => 'text',
		//	'description' => $translate->translate('Passwords must be at least 6 characters in length.'),
			'required' => true,
			'allowEmpty' => false,
			'validators' => array(
			array('NotEmpty', true),
			array('StringLength', false, array(6, 32)),
			),
			'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signup-password', 'placeholder' => 'Пароль'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
			
	));
		$element = $this->getElement('password');
        $element->removeDecorator('Label'); 
    
      // Init confirm password
	$this->addElement('Password', 'passconf', array(
        'label' => $translate->translate('Password Again:'),
	//	'size'  => '30',
	//	'class'  => 'text',
     //   'description' => $translate->translate('Enter your password again for confirmation.'),
        'required' => true,
        'validators' => array(
          array('NotEmpty', true),
		  array('identical', false, array('token' => 'password'))
        ),
        'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signup-passwordconf', 'placeholder' => 'Повторите пароль'),
        'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
	));
        $element = $this->getElement('passconf');
        $element->removeDecorator('Label'); 
 
	/*	$agreement_validation = new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::ZERO);
        $agreement_validation->setMessage('Для регистрации необходимо принять Условие', Zend_Validate_NotEmpty::IS_EMPTY);

        $agreement = new Zend_Form_Element_Checkbox('agreement');
        $agreement->setLabel('Соглашаюсь не размещать уроки на других сайтах')
		          ->addValidator($agreement_validation);
        $this->addElement($agreement); 	*/
		
	  // Add a captcha
        $this->addElement('captcha', 'captcha', array(
			'label'      => $translate->translate('Enter the simbols:'),
			'size'  => '30',
			'class'  => 'capcha',		  
			'description' => '',
			'captcha' => 'image',
			'required' => true,
			'tabindex' => 3,
			'captchaOptions' => array(
				'wordLen' => 4,
				'fontSize' => '30',
				'timeout' => 300,
				'imgDir' =>  APPLICATION_PATH .'/../public/images/captcha/',
				'imgUrl' => $this->getView()->baseUrl() .'/images/captcha',
				'font' =>  APPLICATION_PATH .'/../public/fonts/arial.ttf',
				'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                )
			),
			
        ));
        $element = $this->getElement('captcha');
        $element->removeDecorator('Label');
      	
		// Init submit
        
         $this->addElement('Button', 'submit', array(
            'label' => 'Зарегистрироваться',
            'type' => 'submit',
            'ignore' => true,
            'attribs' => array('class' => 'submit_account'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
                            
           // 'class'  => 'ajax-validate'
        ));
        $element = $this->getElement('submit');
        $element->removeDecorator('Label'); 
				
        // And finally add some CSRF protection
      /*  $this->addElement('hash', 'csrf', array(
            'ignore' => true,
        )); */
        
       
        
    }
  
}
