<?php

class Admin_AdmintownsController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');		
    }

    public function indexAction()
    {
		$towns_object = new Default_Model_DbTable_Towns();
		$towns = $towns_object->fetchAll();
		$paginator = Zend_Paginator::factory($towns);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
		$count_per_page = Zend_Paginator::setDefaultItemCountPerPage(50);
    }
	
	public function addtownAction()
    {
        $form = new Default_Form_Towns_Towns();
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
           
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
			{
                $town_object = new Default_Model_DbTable_Towns($form->getValues());
				$data = array(
                    'town_name'       => $form->getValue('town_name')
                );
				$town_object->insert($data);
                  
                $this->_helper->redirector('index', 'admintowns', 'admin');
            } else {
                
                $form->populate($formData);
            }
        }
    }
	
	public function edittownAction()
    {
        $form = new Default_Form_Towns_Towns();    
        $this->view->form = $form;
        $town_id = $this->getRequest()->getParam('town_id'); //Zend_Debug::dump($user_id);exit;
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
			{
				$town_name = $form->getValue('town_name');
               
                $town_object = new Default_Model_DbTable_Towns();
				
                $town_object->updateTown(	
											$town_id,
											$town_name
										);
                
                $this->_helper->redirector('index', 'admintowns', 'admin');
            } else {
                $form->populate($formData);
            }
        } else {
           
            $town_id = $this->_getParam('town_id', 0);
            if ($town_id > 0){
                $town_object = new Default_Model_DbTable_Towns();   
                $form->populate($town_object->getTown($town_id));
            }
        }
    }
	
	public function deletetownAction()
    {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $town_id = $this->getRequest()->getPost('town_id');
                
                $town_object = new Default_Model_DbTable_Towns();
                
                $town_object->deleteTown($town_id);		
            }
            
            $this->_helper->redirector('index');
		   
        } else {
            $town_id = $this->_getParam('town_id');
            $town_object = new Default_Model_DbTable_Towns();
            $this->view->town = $town_object->getTown($town_id);
        }
    }
    

}

