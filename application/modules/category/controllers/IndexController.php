<?php
/**
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
 
class Category_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('products_of_category');

    }
   
    public function indexAction()
    {
       
        $url = explode("/",$_SERVER['REQUEST_URI']);
        Zend_Debug::dump($url);exit;
      //  if (preg_match("/\ben\b/i", $_SERVER['REQUEST_URI'])) {
    
        //    $category_seo = $url[2]; //
       // } else {
            $url_seo = $url[2];
			Zend_Debug::dump($url_seo);exit;
       // }    
        
        $categores_model = new Default_Model_DbTable_Categores();

        $category = $categores_model->getСertainCategory($url_seo);
        $this->view->category = $category; //Zend_Debug::dump($category);exit;
       
    }
	
	public function categoryAction()
    {   
		//$url = explode("/",$_SERVER['REQUEST_URI']);
    	// BS  - замена, чтобы не учитывались GET-параметры (utm-метки)
    	$bsUrlParts = explode("?", $_SERVER['REQUEST_URI']);
    	$url = explode("/", $bsUrlParts[0]);
 
        $url_seo = $url[2];
		
        $categories_model = new Default_Model_DbTable_Categories();

        $category = $categories_model->getСertainCategory($url_seo);
        $this->view->category = $category; //Zend_Debug::dump($category);exit;
        foreach ($category as $key => $value) {
            $parent_category_id = $value->parent_id;
            if($parent_category_id != 0) {
                $parent_categories[] = $categories_model->getCategoryItemForNav($parent_category_id);
                if($parent_categories[0]['parent_id'] !=  '1' && $parent_categories[0]['parent_id'] != '8' && $parent_categories[0]['parent_id'] != '0') {
                    $parent_category_lvl = $categories_model->getCategoryItemForNav($parent_categories[0]['parent_id']);
                }
                rsort($parent_categories);
                //Zend_Debug::dump($parent_categories);exit;
                $this->view->parent_info = $parent_categories;
            }
        }
		
		$category_id = $categories_model->getCategoryIdByUrl($url_seo);
		
		$categoryphoto_model = new Default_Model_DbTable_Categoryphoto();
		$categoryphoto = $categoryphoto_model->getAllPhotosForObject($category_id);
        $this->view->categoryphoto = $categoryphoto; 
	//	Zend_Debug::dump($categoryphoto);exit;
	
		$slider_model = new Default_Model_DbTable_Sliderthree();
        $sliders = $slider_model->getAllSliderAdmin();
		$this->view->sliders = $sliders; 
		
		$video_model = new Default_Model_DbTable_Videolessons();
        $videos = $video_model->getAllVideoFront();
	//	Zend_Debug::dump($videos);exit;
		
        $this->view->videos = $videos;
		
		$cat_object = $categories_model->getCatItemMeta($category_id);
		$this->view->page = $cat_object;
    }
	
	public function videoAction()
    {   
		
		$slider_model = new Default_Model_DbTable_Sliderthree();
        $sliders = $slider_model->getAllSliderAdmin();
		$this->view->sliders = $sliders; 
		
		$video_model = new Default_Model_DbTable_Videolessons();
        $videos = $video_model->getAllVideoFront();
	//	Zend_Debug::dump($videos);exit;
		
        $this->view->videos = $videos;
    }

    public function videopageAction()
    {   
        
        $slider_model = new Default_Model_DbTable_Sliderthree();
        $sliders = $slider_model->getAllSliderAdmin();
        $this->view->sliders = $sliders; 
        
        $video_model = new Default_Model_DbTable_Videolessons();
        $videos = $video_model->getAllVideoFront();
    //  Zend_Debug::dump($videos);exit;
        
        $this->view->videos = $videos;
    }
}
