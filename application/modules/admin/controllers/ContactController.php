<?php

class Admin_ContactController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
	
	public function indexAction()
    {
	    $contact_object = new Default_Model_DbTable_Contact();
		$cont = $contact_object->fetchAll(); // Zend_Debug::dump($page_text);exit;
		
        $this->view->cont = $cont;
    }
	
    public function editAction()
    {
        $form = new Default_Form_Contact_Tel();     
        $this->view->form = $form;
        
        // ���� � ��� ��� Post ������
        if ($this->getRequest()->isPost()) {
            // ��������� ���
            $formData = $this->getRequest()->getPost();
          //   Zend_Debug::dump($formData);exit;
            // ���� ����� ��������� �����
            if ($form->isValid($formData)) {
                // ��������� id
                $contact_id = (int)$form->getValue('contact_id');  // Zend_Debug::dump($contact_id);exit;
                $contact_tel = $form->getValue('contact_tel');// Zend_Debug::dump($contact_tel);exit;
				$contact_email = $form->getValue('contact_email');
               
                // ������ ������ ������
                $contact_object = new Default_Model_DbTable_Contact();
				
                $contact_object->updateContact($contact_id, $contact_tel, $contact_email);
                
                // ���������� ������������ helper ��� ��������� �� action = index
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            // ���� �� ������� �����, �� �������� id ������, ������� ����� ��������
            $contact_id = $this->_getParam('contact_id', 0);
            if ($contact_id > 0) {
                // ������ ������ ������
                $contact_object = new Default_Model_DbTable_Contact();
                
                // ��������� ����� ����������� ��� ������ ������ populate
                $form->populate($contact_object->getContact($contact_id));
            }
        }
    }
	
}

