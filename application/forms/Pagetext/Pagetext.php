<?php

/**
 * 
 *  
 * 
 */
class Default_Form_Pagetext_Pagetext extends Zend_Form
{
    public function init()
    {	
	    
		
	    
        // Set the method for the display form to POST
        $this->setMethod('post');
		
	    // указываем имя формы
        $this->setName('realestate');
		$realestate_id = new Zend_Form_Element_Hidden('realestate_id');
        $realestate_id->addFilter('Int');
        
        $publish = new Zend_Form_Element_Select('publish');
        $publish ->setLabel("Статус:")
            ->addMultiOptions(array(
                    '0' => "Не публиковать", 
                    '1' => "Опубликовать"       
                    )); 
					
		// prepare categories
		$category_object = new Default_Model_DbTable_Category();
		$categories = $category_object->fetchAll(); 
      
        if (count($categories)!=0)
		{
            $categories_prepared[0]= "не указана";
            foreach ($categories as $category)
			{
                $categories_prepared[$category->category_id]= $category->category_name_ru;
            }

		    $category_id = new Zend_Form_Element_Select('category_id');
		    $category_id ->setLabel("Вид недвижимости:")
                ->addMultiOptions($categories_prepared)
				->setRequired(true); 
        }
			
		$content_list_intro_lv = new Zend_Form_Element_Textarea('content_list_intro_lv');
        $content_list_intro_lv->setLabel('Вступление: (lv)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_intro_lv')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		$content_list_intro_ru = new Zend_Form_Element_Textarea('content_list_intro_ru');
        $content_list_intro_ru->setLabel('Вступление: (ru)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_intro_ru')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		$content_list_intro_en = new Zend_Form_Element_Textarea('content_list_intro_en');
        $content_list_intro_en->setLabel('Вступление: (en)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_intro_en')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		$content_list_intro_ch = new Zend_Form_Element_Textarea('content_list_intro_ch');
        $content_list_intro_ch->setLabel('Вступление: (ch)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_intro_ch')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		$content_list_text_lv = new Zend_Form_Element_Textarea('content_list_text_lv');
        $content_list_text_lv->setLabel('Основной текст: (lv)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_text_lv')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		$content_list_text_ru = new Zend_Form_Element_Textarea('content_list_text_ru');
        $content_list_text_ru->setLabel('Основной текст: (ru)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_text_ru')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		$content_list_text_en = new Zend_Form_Element_Textarea('content_list_text_en');
        $content_list_text_en->setLabel('Основной текст: (en)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_text_en')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		$content_list_text_ch = new Zend_Form_Element_Textarea('content_list_text_ch');
        $content_list_text_ch->setLabel('Основной текст: (ch)')
		  //  ->setRequired(false)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 15)
            ->setAttrib('id', 'content_list_text_ch')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(0,65534));
			
		
        $this->addElements(array(
									$publish,
									$category_id,
									$content_list_intro_lv,
									$content_list_intro_ru,
									$content_list_intro_en,
									$content_list_intro_ch,
									$content_list_text_lv,
									$content_list_text_ru,
									$content_list_text_en,
									$content_list_text_ch
							    )
							);
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'class' => 'btn btn-midle',
          'type' => 'submit',
          'ignore' => true,
        ));	 
    }
}

