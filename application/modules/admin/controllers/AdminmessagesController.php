<?php

class Admin_AdminmessagesController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');		
    }

    public function indexAction()
    {
        // action body
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('messages/index.phtml');
		$messages = new Default_Model_DbTable_Messages();
		$mes = $messages->getDescMessage();
		$paginator = Zend_Paginator::factory($mes);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
		$count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);
    }
	
	public function deleteAction()
    {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $message_id = $this->getRequest()->getPost('message_id');
                
                $messages = new Default_Model_DbTable_Messages();
                
                $messages->deleteMessage($message_id);		
            }
            $this->_helper->redirector('index');
		   
        } else {
            $message_id = $this->_getParam('message_id');
            
            $messages = new Default_Model_DbTable_Messages();
            
            $this->view->message = $messages->getMessage($message_id);
        }
    }
    

}

