<?php
/**
 * Contact controller
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class Payment_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('page');
    }
	
    public function indexAction()
    {	
		$payment_model = new Default_Model_DbTable_Payment();
		$page_payment = $payment_model ->fetchAll();
		$this->view->page = $page_payment;
    }
}