<?php
/**
 *
 * Default controller for this application.
 * 
 */
class News_IndexController extends Zend_Controller_Action
{  
    public function init()
    {
        /* Initialize action controller here */ 
    }
	
    public function indexAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('news');
		
        $news_model = new Default_Model_DbTable_News();
		
		$news = $news_model->getAllNewsFront();
	  
		$paginator = Zend_Paginator::factory($news);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(5);
        
        $articles = new Default_Model_DbTable_Newsseo();
        $articles_name = $articles->fetchAll();
        
        $page = Zend_Paginator::factory($articles_name);
        
        $this->view->page = $page;
		
    }

	function newsAction()
	{  
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('news_single');
		
		$news_model = new Default_Model_DbTable_News();
     
        $url_seo = $this->getRequest()->getParam('url_seo'); // Zend_Debug::dump($article_id);exit;
        
        $news_title = $news_model->getNewsTitle($url_seo); //Zend_Debug::dump($page);exit;
        $this->view->news_title = $news_title;
		
		$page = $news_model->getNewspage($url_seo); //Zend_Debug::dump($page);exit;
        $this->view->page = $page;
        $paginator = Zend_Paginator::factory($page);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(1);
	}	
}
