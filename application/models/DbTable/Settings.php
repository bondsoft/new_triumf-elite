<?php

/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Settings extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_setting';
	
	public function getSettings($setting_id)
    {
        $setting_id = (int)$setting_id;
        $row = $this->fetchRow('setting_id = ' . $setting_id);
        if(!$row) {
            throw new Exception("Нет записи с setting_id - $setting_id");
        }
        return $row->toArray();
    }
	
	public function getSettingsAdmin()
	{
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->order('setting_id');
     
		$articles = $this->fetchAll($select);
        return $articles;
    }

	
// variant	
/*	public  function updateHomepage($id, $text)
    {   $homepage = new Default_Model_DbTable_Homepage();
        $data = array(
            'text' => $text,
        );
        $where = $homepage->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateSettings(
                                        $setting_id,
                                        $setting_value
                                    )
    {
        $data = array(
            'setting_value' => $setting_value
        );
        
        $this->update($data, 'setting_id = ' . (int)$setting_id);
    }
    
    public function getConfigTelephone_01()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_telephone_01');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $setting_rec = $this->fetchRow($select);    
        $setting = $setting_rec["setting_value"];     
        return $setting;
        
    }
    
    public function getConfigTelephone_02()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_telephone_02');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $setting_rec = $this->fetchRow($select);    
        $setting = $setting_rec["setting_value"];     
        return $setting;
        
    }
    
    public function getConfigTelephone_03()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_telephone_03');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $setting_rec = $this->fetchRow($select);    
        $setting = $setting_rec["setting_value"];     
        return $setting;
        
    }
	
	public function getConfigTelephone_04()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_telephone_04');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $setting_rec = $this->fetchRow($select);    
        $setting = $setting_rec["setting_value"];     
        return $setting;
        
    }
    
    public function getConfigWorkingHours()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_working_hours');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $config_working_hours_object = $this->fetchRow($select);    
        $config_working_hours = $config_working_hours_object["setting_value"];     
        return $config_working_hours;
        
    }
    
    public function getConfigScheduleOfWork()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_schedule_of_work');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $config_schedule_of_work_object = $this->fetchRow($select);    
        $config_schedule_of_work = $config_schedule_of_work_object["setting_value"];     
        return $config_schedule_of_work;
        
    }
    
    public function getConfigEmail()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_email');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec["setting_value"];     
        return $main_photo_file_name;
        
    }
    
    public function getConfigAddress()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_address');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec["setting_value"];     
        return $main_photo_file_name;
        
    }
	
	public function getConfigMap()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_map');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $rec = $this->fetchRow($select);    
        $rec = $rec["setting_value"];     
        return $rec;
        
    }
	
	public function getConfigYandexMetrika()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_yandex_metrika');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $rec = $this->fetchRow($select);    
        $rec = $rec["setting_value"];     
        return $rec;
        
    }
	
	public function getConfigInstagram()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_social_instagram');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $rec = $this->fetchRow($select);    
        $rec = $rec["setting_value"];     
        return $rec;
        
    }
	
	public function getConfigVk()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_social_vk');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $rec = $this->fetchRow($select);    
        $rec = $rec["setting_value"];     
        return $rec;
        
    }
	
	public function getConfigFacebook()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_social_facebook');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $rec = $this->fetchRow($select);    
        $rec = $rec["setting_value"];     
        return $rec;
        
    }
	
	public function getConfigYoutube()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_social_youtube');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $rec = $this->fetchRow($select);    
        $rec = $rec["setting_value"];     
        return $rec;
        
    }
	
	public function getConfigOk()
    {
        $_name = 'zend_setting';
        $select = $this->select()
                    ->from($_name)
                    ->where('setting_key = ?', 'config_social_ok');
       //  echo $select; 
        // Zend_Debug::dump($select->__toString()); exit;  
        $rec = $this->fetchRow($select);    
        $rec = $rec["setting_value"];     
        return $rec;
        
    }
	
}
