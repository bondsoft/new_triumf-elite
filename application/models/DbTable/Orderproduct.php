<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Orderproduct extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_order_product';
	
	public function getOrder($order_id)
    {
        $order_id = (int)$order_id;
        $row = $this->fetchRow('order_id = ' . $order_id);
        if(!$row) {
            throw new Exception("Нет записи с order_id - $order_id");
        }
        return $row->toArray();
    }
	
	public function deleteOrderProduct($order_id)
    {
        $this->delete('order_id = ' . (int)$order_id);
    }
	
	public function getDescOrder()
	{
        $_name = 'zend_order';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->order('order_id DESC');
     
		$order = $this->fetchAll($select);
        return $order;
    }
	
	
	public function addOrder(
                                $name,
                                $phone,
                                $email,
								$sum_cart
                                 
                               )
    {
        $data = array(
            'name' => $name,
            'phone' => $phone,
            'email' => $email,
			'sum_cart' => $sum_cart,
            'creation_date' => date('Y-m-d H:i:s')
        );
        $this->insert($data);
    } 
	
	public  function getLastInsertId()
    {
        $_name = 'zend_order_product';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('order_product_id DESC')
                    ->limit(1);
                     
        $LastInsertId_rec = $this->fetchRow($select);	
        $LastInsertId = $LastInsertId_rec["order_product_id"];		
        return $LastInsertId;
    }
	
	public  function addProductOrder( 
                                        $order_id,
                                        $product_id,
										$product_model,
										$image,
										$quantity,
										$price,
										$total
                                    )
    {
        $data = array(
			'order_id' => $order_id,
            'product_id' => $product_id,
			'product_model' => $product_model,
			'image' => $image,
			'quantity' => $quantity,
			'price' => $price,
			'total' => $total
            
        );
        
        $this->insert($data);
    }
	
	public  function deleteProductOrderAjax(
                                                $order_product_id                
                                            )
    {
		
        $this->delete(array(
			'order_product_id = ?' => $order_product_id
        ));
    } 
	
	public  function addProductOrderAjax( 
											$order_id,
											$product_id,
											$quantity
										)
    {
        $data = array(
			'order_id' => $order_id,
            'product_id' => $product_id,
			'quantity' => $quantity
            
        );
        
        $this->insert($data);
    }
	
	public function checkOrderProduct(
										$order_id,     
										$product_id	
									)
    {
        $_name_01 = 'zend_order_product';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('order_id = ?', $order_id)
					->where('product_id = ?', $product_id)
                    ->limit(1);

        $product_order_object = $this->fetchRow($select);    
        $product_order_model = $product_order_object["product_id"];     
        return $product_order_model;
    }
	
	public function getOrderProductQuantity(
												$order_id,     
												$product_id	
											)
    {
        $_name_01 = 'zend_order_product';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('order_id = ?', $order_id)
					->where('product_id = ?', $product_id)
                    ->limit(1);

        $product_order_object = $this->fetchRow($select);    
        $product_order_model = $product_order_object["quantity"];     
        return $product_order_model;
    }
	public  function updateProductOrderAjax(
												$order_id,     
												$product_id,
												$quantity
											)
    {
        $data = array(
            'quantity' => $quantity
        );
        
        $this->update($data, array('order_id = ' . (int)$order_id, 'product_id = ' . (int)$product_id));
    }
}
