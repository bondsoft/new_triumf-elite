<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Order extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_order';
	
	public function getOrder($order_id)
    {
        $order_id = (int)$order_id;
        $row = $this->fetchRow('order_id = ' . $order_id);
        if(!$row) {
            throw new Exception("Нет записи с order_id - $order_id");
        }
        return $row->toArray();
    }
	
	public function getOrderIdByToken( $token )
	{
        $_name = 'zend_order';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.token = ?', $token);
     
		$object_rec = $this->fetchRow($select);	
        $order_id = $object_rec['order_id'];		
        return $order_id;
    }
	
	public function deleteOrder($order_id)
    {
        $this->delete('order_id = ' . (int)$order_id);
    }
	
	public function getDescOrder()
	{
        $_name = 'zend_order';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->order('order_id DESC');
     
		$order = $this->fetchAll($select);
        return $order;
    }
	
	public function getUserOrders( $email )
	{
        $_name = 'zend_order';
        $select = $this->select()
                //    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('user_email = ?', $email)
					->order('order_id DESC');
     
		$order = $this->fetchAll($select);
        return $order;
    }
	
	
	public function addOrder(
								$token,
                                $user_name,
								$user_phone,
								$user_email,
								$sum_cart,
							//	$delivery,
							//	$address,
							//	$samovivoz_city,
								$city,
								$street_delivery,
								$house_delivery,
								$room_delivery
                                 
							)
    {
        $data = array(
            'token' => $token,
			'user_name' => $user_name,
            'user_phone' => $user_phone,
            'user_email' => $user_email,
			'sum_cart' => $sum_cart,
		//	'delivery' => $delivery,
		//	'address' => $address,
		//	'samovivoz_city' => $samovivoz_city,
			'city' => $city,
			'street_delivery' => $street_delivery,
			'house_delivery' => $house_delivery,
			'room_delivery' => $room_delivery,
		//	'payment' => $payment,
            'creation_date' => date('Y-m-d H:i:s')
        );
        $this->insert($data);
    } 
	
	public  function getLastInsertId()
    {
        $_name = 'zend_order';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('order_id DESC')
                    ->limit(1);
                     
        $LastInsertId_rec = $this->fetchRow($select);	
        $LastInsertId = $LastInsertId_rec["order_id"];		
        return $LastInsertId;
    }
    
    public function getAllOrders()
    {
        $_name = 'zend_order';
        $_name_01 = 'zend_order_product';
        $_name_02 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->joinLeft(array('name_01' => $_name_01),'name.order_id = name_01.order_id')
                    ->joinLeft(array('name_02' => $_name_02),'name_01.product_id = name_02.product_id', array('product_model', 'price_rub'))
                    ->group('name.order_id')
                    ->order('name.order_id DESC');
                    
        $all_orders = $this->fetchAll($select);
        return $all_orders;
    }
    
    public function getOrderDetails($order_id)
    {
        $_name = 'zend_order';
        $_name_01 = 'zend_order_product';
        $_name_02 = 'zend_order_option';
		$_name_03 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.order_id = ?', $order_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.order_id = name_01.order_id')
                    ->joinLeft(array('name_02' => $_name_02),'name_01.order_product_id = name_02.order_product_id')
                    ->order('name.order_id DESC');
                    
        $all_orders = $this->fetchAll($select);
        return $all_orders;
    }
	
	public function getOrderDetailsByToken($token)
    {
        $_name = 'zend_order';
        $_name_01 = 'zend_order_product';
        $_name_02 = 'zend_order_option';
		$_name_03 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.token = ?', $token)
                    ->joinLeft(array('name_01' => $_name_01),'name.order_id = name_01.order_id')
                    ->joinLeft(array('name_02' => $_name_02),'name_01.order_product_id = name_02.order_product_id')
                    ->order('name.order_id DESC');
                    
        $all_orders = $this->fetchAll($select);
        return $all_orders;
    }
    
    public  function updateOrderSum( 
										$order_id,
										$sum_cart   
									)
    {
        $data = array(
            'sum_cart' => $sum_cart
        );
        
        $this->update($data, 'order_id = ' . (int)$order_id);
    }
	
	
}
