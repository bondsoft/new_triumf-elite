<?php

/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Photos extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'realestate_photos';
	
	public function getPhoto($photo_id)
    {
        $photo_id = (int)$photo_id;
        $row = $this->fetchRow('photo_id = ' . $photo_id);
        if(!$row) {
            throw new Exception("no photo_id - $photo_id");
        }
        return $row->toArray();
    }
	
	public function updatePhoto_file($photo_id, $photo_file)
    {
		$data = array(  
            'photo_file' => $photo_file,			
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }
	
	public  function updatePhoto($photo_id, $photo_text)
    {
        $data = array(
            'photo_text' => $photo_text,		
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }

	public function getPhoto_file($photo_id)
	{
	    $_name = 'realestate_photos';
        $select = $this->select()
                    ->from($_name)
					->where('photo_id = ?', $photo_id);

		$photo_file_rec = $this->fetchRow($select);	
        $photo_file_name = $photo_file_rec["photo_file"];		
        return $photo_file_name;
    }
	
	public function addPhoto($realestate_id, $photo_file)
    {
        $data = array(
		    'realestate_id' => $realestate_id,
            'photo_file' => $photo_file
        );
        $this->insert($data);
    } 
	
	public function deletePoetrytext($poetrytext_id)
    {
        $this->delete('poetrytext_id = ' . (int)$poetrytext_id);
    }
	
	public function getPhoto_galary($realestate_id)
	{
        $_name = 'realestate_photos';
        $select = $this->select()
                    ->from($_name)
                    ->where('realestate_id = ?', (int)$realestate_id);
     
		$photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function deletePhoto($photo_id)
    {
	 //  $_name = 'galary_photos';
	 //  $condition = array(
     //    'photo_id = ?' => $photo_id
     //  );
     //   $this->delete($_name, $condition); 
        $this->delete('photo_id = ' . (int)$photo_id);
    }
	
	public function getPhoto_fileFromEachAlbum($galary_id)
	{
	    $_name = 'realestate_photos';
        $select = $this->select()
                    ->from($_name)
					->where('galary_id = ?', $galary_id);

		$photo_file_rec = $this->fetchAll($select);	
        return $photo_file_rec;
    }
	
	public function deleteAllPhoto($galary_id)
    {
       $this->delete('galary_id = ' . (int)$galary_id);
    }
    
    public function deleteAll_photos($realestate_id)
    {
        $this->delete('realestate_id = ' . (int)$realestate_id);
    }

}
