<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Stocks extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_stocks';
	
	public function getStock($stock_id)
    {
        $stock_id = (int)$stock_id;
        $row = $this->fetchRow('stock_id = ' . $stock_id);
        if(!$row) {
            throw new Exception("Нет записи с stock_id - $stock_id");
        }
        return $row->toArray();
    }
	
	public function addStock(
									$stock_title,
									$url_seo,
									$intro_text,
									$full_text,
									$creation_date,
									$main_photo,
                                    $alt,
                                    $show_photo_inside,
									$header_tags_title,
									$header_tags_description,
									$header_tags_keywords
								)
    {
        $data = array(
            'stock_title' => $stock_title,
            'url_seo' => $url_seo,
            'intro_text' => $intro_text,
            'full_text' => $full_text,
            'creation_date' => $creation_date,
			'main_photo' => $main_photo,
            'alt' => $alt,
			'show_photo_inside' => $show_photo_inside,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        $this->insert($data);
    }
	
	public  function updateStock(
										$stock_id,
										$stock_title,
										$url_seo,
										$intro_text,
										$full_text,
										$creation_date,
										$main_photo,
										$alt,
                                        $show_photo_inside,
										$header_tags_title,
										$header_tags_description,
										$header_tags_keywords
									)
    {
        $data = array( 
            'stock_title' => $stock_title,
            'url_seo' => $url_seo,
            'intro_text' => $intro_text,
            'full_text' => $full_text,
            'creation_date' => $creation_date,
			'main_photo' => $main_photo,
			'alt' => $alt,
            'show_photo_inside' => $show_photo_inside,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords	
        );
        
        $this->update($data, 'stock_id = ' . (int)$stock_id);
    }
	
	public function getAllStocksAdmin()
	{
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->order('stock_id DESC');
     
		$stocks = $this->fetchAll($select);
        return $stocks;
    }
	
	public function getAllStocksFront()
	{
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->where('status = ?', '1')
					->order('creation_date DESC');
     
		$stocks = $this->fetchAll($select);
        return $stocks;
    }
	
	public function getAllStocksRanrom()
	{
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->where('status = ?', '1')
					->order('RAND()')
					->limit(10);
     
		$stocks = $this->fetchAll($select);
        return $stocks;
    }
    
	public function getLastStocks()
	{
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
					->where('prime = ?', '1')
                    ->order('creation_date DESC')
					->limit(2);
     
		$laststocks = $this->fetchAll($select);
        return $laststocks;
    }
   
	public function getMainPhotoFile($stock_id)
	{
	    $_name = 'zend_stocks';
        $select = $this->select()
                    ->from($_name)
					->where('stock_id = ?', $stock_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec['main_photo'];		
        return $main_photo_file_name;
    }
	
	public function getStockTitle($url_seo)
	{
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
		$stock_title_rec = $this->fetchRow($select);	
        $stock_title = $stock_title_rec['stock_title'];		
        return $stock_title;
    }
    
	public function getStockIntroText($url_seo)
	{
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
		$stock_rec = $this->fetchRow($select);	
        $full_text = $stock_rec['intro_text'];		
        return $full_text;
    }
	
    public function getStockIdByUrl($url_seo)
	{
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
		$stock_title_rec = $this->fetchRow($select);	
        $stock_id = $stock_title_rec['stock_id'];		
        return $stock_id;
    }
	
	public function deleteStock($stock_id)
    {
        $this->delete('stock_id = ' . (int)$stock_id);
    }
	
	public  function updateMainphoto(
										$stock_id, 
										$main_photo
	                                )
    {
        $data = array(
			'main_photo' => $main_photo,
        );
        
        $this->update($data, 'stock_id = ' . (int)$stock_id);
    }
    
    public function getStockPage($url_seo)
    {
        $_name = 'zend_stocks';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
        $news = $this->fetchAll($select);
        return $news;
    }
    
    public  function updateStockStatus(
											$stock_id,
											$status
										)
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'stock_id = ' . (int)$stock_id);
    }
    
    public  function updatePrimeStock(
											$stock_id,
											$prime
										)
    {
        $data = array( 
            'prime' => $prime  
        );
        
        $this->update($data, 'stock_id = ' . (int)$stock_id);
    }
	
	public function getStockImage($stock_id)
	{
	    $_name = 'zend_stocks';
        $select = $this->select()
                    ->from($_name)
					->where('stock_id = ?', $stock_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["main_photo"];		
        return $main_photo_file_name;
    }
	
	public  function deleteStockImage(
											$stock_id,  
											$main_photo
										)
    {
        $data = array(
            'main_photo' => $main_photo,
        );
        
        $this->update($data, 'stock_id = ' . (int)$stock_id);
    }
}
