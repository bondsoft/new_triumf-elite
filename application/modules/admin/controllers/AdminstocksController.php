<?php
/**
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminstocksController extends Zend_Controller_Action
{
	public function init()
	{
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
	}
	
	function indexAction()
	{
        $stocks_model = new Default_Model_DbTable_Stocks();
		$stocks = $stocks_model->getAllStocksAdmin();
		
		$paginator = Zend_Paginator::factory($stocks);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);	   
	}
	
	public function additemAction()
    {
		include( 'SimpleImage.php' );
        $form = new Default_Form_Stocks_AddStock();     
        $this->view->form = $form;
        $stocks_model = new Default_Model_DbTable_Stocks();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
				$stock_title = $form->getValue('stock_title');
				$url_seo = $form->getValue('url_seo');
                $intro_text = $form->getValue('intro_text');
                $full_text = $form->getValue('full_text');
                if(!$creation_date = $form->getValue('creation_date')) {
                    $creation_date = date('Y-m-d H:i:s');
                }
				$header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
				$alt = $form->getValue('alt');
                $show_photo_inside = $form->getValue('show_photo_inside');
				$data = $_FILES['main_photo']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['main_photo']['tmp_name']; 
				
				if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/stocks/main_photos/big_".$newname;
                    $name_small = "media/photos/stocks/main_photos/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
					$image_big->load($uploadedfile);
				//	$image_big->maxareafill(1020, 450, 241, 241, 247); 
							
					$image_big->save($name_big);
									
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->resizeToWidth(450);
					
					$image_small->save($name_small);
                } 
				 
                $stocks_model->addStock(
												$stock_title,
												$url_seo,
												$intro_text,
												$full_text,
												$creation_date,
												$newname,
												$alt,
                                                $show_photo_inside,
												$header_tags_title,
												$header_tags_description,
												$header_tags_keywords
											);
                unlink($_FILES['main_photo']['tmp_name']); 
                $this->_helper->redirector('index');
            } else { 
                $form->populate($formData);
            }
        }
    }
   
    public function edititemAction()
    {
		include( 'SimpleImage.php' );
        $form = new Default_Form_Stocks_EditStock();     
        $this->view->form = $form;
        $stocks_model = new Default_Model_DbTable_Stocks();
		
		$stock_id = $this->getRequest()->getParam('stock_id');
        $stock_image = $stocks_model->getStockImage($stock_id);
        $this->view->stock_image = $stock_image; 
        $this->view->stock_id = $stock_id; 
        
		$old_photo_file = $stocks_model->getStockImage($stock_id);
        $directory = "media/photos/stocks/main_photos";
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
			{
                $stock_id = (int)$form->getValue('stock_id');
                $stock_title = $form->getValue('stock_title');
				$url_seo = $form->getValue('url_seo');
                $intro_text = $form->getValue('intro_text');
                $full_text = $form->getValue('full_text');
                if(!$creation_date = $form->getValue('creation_date')) {
                    $creation_date = date('Y-m-d H:i:s');
                }
				$header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
				$alt = $form->getValue('alt');
                $show_photo_inside = $form->getValue('show_photo_inside');
				$main_photo = $form->getValue('main_photo');
				$data = $_FILES['main_photo'];  // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['main_photo']['tmp_name']; 
				
				if($main_photo)
                {       
                    if(is_file("$directory/small_$old_photo_file"))
                    {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                    }
                }
				
				if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
					//Zend_Debug::dump($newname);exit;
                    $name_big = "media/photos/stocks/main_photos/big_".$newname;
                    $name_small = "media/photos/stocks/main_photos/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
					$image_big->load($uploadedfile);
				//	$image_big->maxareafill(1020, 450, 241, 241, 247); 
							
					$image_big->save($name_big);
									
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->resizeToWidth(450);
					
					$image_small->save($name_small);
                } 
                
                if(!$main_photo)
                {      
                    $stocks_object = $stocks_model->getStock($stock_id);
                    $newname = $stocks_object['main_photo'];
                }
 
                $stocks_model->updateStock(
												$stock_id, 
												$stock_title,
												$url_seo,
												$intro_text,
												$full_text,
												$creation_date,
												$newname,
												$alt,
                                                $show_photo_inside,
												$header_tags_title,
												$header_tags_description,
												$header_tags_keywords
											);
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $stock_id = $this->_getParam('stock_id', 0);
            if ($stock_id > 0)
			{
                $form->populate($stocks_model->getStock($stock_id));
            }
        }
    }
	
	public function deleteitemAction()
    {
		$stocks_model = new Default_Model_DbTable_Stocks(); //  Zend_Debug::dump($stock);exit;
	//	$newstext_model = new Default_Model_DbTable_Newstext();
		$photo_model = new Default_Model_DbTable_Stocksphotoforpage();
        if ($this->getRequest()->isPost())
		{
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
			{
                $stock_id = $this->getRequest()->getPost('stock_id');
                
                $all_photos = $photo_model->getAllPhotosForStock($stock_id); //Zend_Debug::dump($all_photos);exit;
                
                $directory_photos = "media/photos/stocks/pages";
                
                foreach($all_photos as $photo)
                {
                    unlink($directory_photos .'/big_'.$photo["photo_file"]);
                    unlink($directory_photos .'/small_'.$photo["photo_file"]); 
                }
                 
                $directory = "media/photos/stocks/main_photos";
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
				{
					unlink("$directory/big_$old_photo_file");
					unlink("$directory/small_$old_photo_file");
				}  
                
                 // получаем имя удаляемого файла
                $old_photo_file = $stocks_model->getMainphotoFile($stock_id);//Zend_Debug::dump($old_photo_file);exit;
                $main_photo == NULL;
                
            //  Zend_Debug::dump($old_photo_file);exit;
            
                $directory = "media/photos/stocks/main_photos";
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
                
                
                $photo_model->deleteAllPhotosForStock(
															$stock_id
														);
            //    $newstext_model->deleteAllPagesForNews($news_id);
                $stocks_model->deleteStock($stock_id);
            }
            $this->_helper->redirector('index', 'adminstocks', 'admin');
        } else {
            $stock_id = $this->_getParam('stock_id');
            
            $this->view->stocks = $stocks_model->getStock($stock_id);
        }
    }
    
    function seoAction()
    {
        $seo_model = new Default_Model_DbTable_Stocksseo();
        $seo = $seo_model->fetchAll();
        
        $paginator = Zend_Paginator::factory($seo);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);   
       
    }
    
    public function seoeditAction()
    {
        $form = new Default_Form_Stocks_Seo();     
        $this->view->form = $form;
        $seo_model = new Default_Model_DbTable_Stocksseo();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {
                $id = (int)$form->getValue('id'); 
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
 
                $seo_model->updateSeo(
                                        $id,  
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                    );
                
                $this->_helper->redirector('seo', 'adminstocks', 'admin');
            } else {
                $form->populate($formData);
            }
        } else {   
            $id = $this->_getParam('id', 0);
            if ($id > 0)
            {
                $form->populate($seo_model->getSeo($id));
            }
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $stocks_model = new Default_Model_DbTable_Stocks();
        
        if ($this->getRequest()->isPost())
        {
            $stock_id = $this->getRequest()->getParam('stock_id');
            $status = '1';
            $stocks_model->updateStockStatus(
													$stock_id,  
													$status
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $stocks_model = new Default_Model_DbTable_Stocks();
        
        if ($this->getRequest()->isPost())
        {
            $stock_id = $this->getRequest()->getParam('stock_id');
            $status = '0';
            $stocks_model->updateStockStatus(
													$stock_id,  
													$status
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $stocks_model = new Default_Model_DbTable_Stocks();
        $stock_id = $this->getRequest()->getParam('stock_id');
        
        $old_photo_file = $stocks_model->getStockImage($stock_id);
        $directory = "media/photos/stocks/main_photos";
        
        if ($this->getRequest()->isPost())
        {
            $stock_id = $this->getRequest()->getParam('stock_id');
            $main_photo = '';
           
            $stocks_model->deleteStockImage(
                                                    $stock_id,  
                                                    $main_photo
                                                );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function primeonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $stocks_model = new Default_Model_DbTable_Stocks();
        
        if ($this->getRequest()->isPost())
        {
            $stock_id = $this->getRequest()->getParam('stock_id');
            $prime = '1';
            $stocks_model->updatePrimeStock(
													$stock_id,  
													$prime
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function primeoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $stocks_model = new Default_Model_DbTable_Stocks();
        
        if ($this->getRequest()->isPost())
        {
            $stock_id = $this->getRequest()->getParam('stock_id');
            $prime = '0';
            $stocks_model->updatePrimeStock(
													$stock_id,  
													$prime
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
}