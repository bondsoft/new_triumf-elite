<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Request extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_request';
    
    public function getRequest($request_id)
    {
        $request_id = (int)$request_id;
        $row = $this->fetchRow('request_id = ' . $request_id);
        if(!$row) {
            throw new Exception("Нет записи с request_id - $request_id");
        }
        return $row->toArray();
    }
	
	public function getRequestpage_text($lang)
	{
        $_name = 'zend_request';
        $select = $this->select()
                    ->from($_name)
                    ->where('lang = ?', $lang);
     
		$text = $this->fetchAll($select);
        return $text;
    }
   
    public  function updateRequest(
										$request_id,
										$header_tags_title,
										$header_tags_description,
										$header_tags_keywords
									)
    {
        $data = array(
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'request_id = ' . (int)$request_id);
    }
}
