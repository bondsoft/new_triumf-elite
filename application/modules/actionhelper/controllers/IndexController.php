<?php
/**
 *
 * Default controller for this application.
 * 
 */
class Actionhelper_IndexController extends Zend_Controller_Action
{  
    public function init()
    {
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('articles');
        /* Initialize action controller here */ 
    }
	
    public function indexAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('articles');
		
        $articles_model = new Default_Model_DbTable_Articles();
		
		$articles = $articles_model->getAllArticlesFront(); //Zend_Debug::dump($articles);exit;
	  
		$paginator = Zend_Paginator::factory($articles);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(3);
        
        $articles = new Default_Model_DbTable_Articlesseo();
        $articles_name = $articles->fetchAll();
        
      //  $page = Zend_Paginator::factory($articles_name);
        
      //  $this->view->page = $page;
        
        $sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
    }
	
	public function articlescategoriesAction()
    {
        $categories_model = new Default_Model_DbTable_Articlescategories();

        $categories_01 = $categories_model->getAllFirstLevelCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
			$children_data = array();
			$children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
                            'url_seo'  => $child['url_seo']
                        );
				$children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  //Zend_Debug::dump($children_lv3); exit;
				foreach ($children_lv3 as $child_lv3) {
					$children_lv3_data[] = array(
								'category_name'  => $child_lv3['category_name'],
								'url_seo'  => $child_lv3['url_seo']
							);
					
				}
            }
			if(@$children_lv3)
			{
				
				
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'children_lv3' => $children_lv3_data,
						'url_seo'     => $category['url_seo']
					);
			} else {
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'url_seo'     => $category['url_seo']
					);
			}
			
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
    }
	
	public function latestprojectsAction()
    {
        $articles_model = new Default_Model_DbTable_Articles();
		
		$articles = $articles_model->getLatestArticlesFront(); 
	//	Zend_Debug::dump($articles);exit;
        $this->view->articles = $articles;
        
    }

	public function headAction()
    {   
        
    } 
	
	public function headerAction()
    {   
        
    } 
	
	public function footerAction()
    {   
        
    }

	function sendmessageAction()
    {   
        $form = new Default_Form_Contact_Contact(); 
        $this->view->form = $form; 
       
    }
	
}
