

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));


$(document).ready(function(){

  show_banner=true;
  s_date=30;
  date = new Date();
  if ($.cookie("banner_time") == null )
  {
    $.cookie('banner_time',date.getTime());
  } else
  {
    old_date=$.cookie('banner_time');
    date=date.getTime();
    s_date=(date-old_date)/60000;
  }
  if (s_date<30)
  {
    show_banner=false;
  }
  if (show_banner)
  {
    console.log('Показывать баннер');
  } else
  {
    console.log('Не показывать баннер');
  }

  var mobileMenuObj = $("#for_mobile_meny ul").clone(); // код на главной странице;

  mobileMenuObj = $("<div id='my-menu'></div>").append(mobileMenuObj);
  $("#body-inner").append(mobileMenuObj);

  var phnMenuWrapper = $('<div class="contact-small"></div>');
  var phnMenu1 = $('<a />', {
    class: 'citi-phone',
    href: "tel:+78632310303",
    text: "+7 (863) 231-03-03"
  });
  phnMenuWrapper.append(phnMenu1);

  var $menu = $('#my-menu');
  $menu.mmenu({
    drag : {
      menu : {
        open: ($.mmenu.support.touch?true:false),
        width: {perc:0.8}
      }
    },
    navbar : {
      title : "<a href=\"/\" class=\"mLogo\">Меню</a>",

    },
    "navbars": [  {
        "position": "bottom",
        "content": [
           phnMenuWrapper
        ],
        "height": 1
      }
    ],
    "extensions": [
        "fx-menu-slide",
        "fx-panels-zoom"
    ]
  });
  var api = $("#my-menu").data( "mmenu" );
  $("#open_meny, .mobile-menu__button").click(function(e) {
    api.open();
    e.preventDefault();
  });
  $("#open_meny, .mobile-menu__button").click(function(e) {
    api.close();
    e.preventDefault();
  });
  api.bind('open:finish', function () {
    $('html').addClass('slideout-open');
  });
  api.bind('close:before', function () {
    $('html').removeClass('slideout-open');
  });

});


/* services */
(function($) {
  $(function() {

    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
      $(this)
        .addClass('active').siblings().removeClass('active')
        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });

  });
})(jQuery);

function click_accordions() {

	$('.accordion-content').each(function() {

		$( this ).removeClass('is-collapsed').addClass('is-expanded').addClass('animateIn');
		$( this ).attr('aria-hidden','false');


	});

	$('.butr_e').html('Закрыть всё');
	$('.butr_e').attr('onclick','close_accordions()');

}

function close_accordions() {

	$('.accordion-content').each(function() {

		$( this ).addClass('is-collapsed').removeClass('is-expanded').removeClass('animateIn');
		$( this ).attr('aria-hidden','true');


	});

	$('.butr_e').html('Раскрыть всё');
	$('.butr_e').attr('onclick','click_accordions()');

}


$(function(){
      (function(){
        var d = document,
        accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
        setAria,
        setAccordionAria,
        switchAccordion,
        touchSupported = ('ontouchstart' in window),
        pointerSupported = ('pointerdown' in window);

        skipClickDelay = function(e){

			var ab = true; // Контроль скролла, если есть скролл - раскрытия таба не будет.
			setTimeout(function() {

				if( document.body.offsetHeight > document.documentElement.clientHeight )
				ab = false;
				if( ab === true ) {
					e.preventDefault();
					e.target.click();
				}
			}, 10);

        }

          setAriaAttr = function(el, ariaType, newProperty){
          el.setAttribute(ariaType, newProperty);
        };
        setAccordionAria = function(el1, el2, expanded){
          switch(expanded) {
            case "true":
              setAriaAttr(el1, 'aria-expanded', 'true');
              setAriaAttr(el2, 'aria-hidden', 'false');
              break;
            case "false":
              setAriaAttr(el1, 'aria-expanded', 'false');
              setAriaAttr(el2, 'aria-hidden', 'true');
              break;
            default:
              break;
          }
        };
      //function
      switchAccordion = function(e) {
        //console.log("triggered");
        e.preventDefault();
        var thisAnswer = e.target.parentNode.nextElementSibling;
        var thisQuestion = e.target;
        if(thisAnswer.classList.contains('is-collapsed')) {
          setAccordionAria(thisQuestion, thisAnswer, 'true');
        } else {
          setAccordionAria(thisQuestion, thisAnswer, 'false');
        }
          thisQuestion.classList.toggle('is-collapsed');
          thisQuestion.classList.toggle('is-expanded');
          thisAnswer.classList.toggle('is-collapsed');
          thisAnswer.classList.toggle('is-expanded');

          thisAnswer.classList.toggle('animateIn');
        };
        for (var i=0,len=accordionToggles.length; i<len; i++) {
          if(touchSupported) {
            accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
          }
          if(pointerSupported){
            accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
          }
          accordionToggles[i].addEventListener('click', switchAccordion, false);
        }
      })();

    	$("#footer").on("click","a.scroll", function (event) {
    		event.preventDefault();
    		var id  = $(this).attr('href'),
   			top = $(id).offset().top;
    		$('body,html').animate({scrollTop: top}, 1500);
    	});
    });



/* pop-up */
        $(function() {

			var on_blue = true;
			// $(window).scroll(function () {

			// 	if( $(".ab").hasClass("blue") ) {

			// 		var scrollTop = $(window).scrollTop();
			// 		var offset_blue = $('.ab.blue').offset();
			// 		var top_blue = offset_blue.top - ( ( screen.height - $('.ab.blue').height()  ) / 2 );

			// 		if( scrollTop > top_blue && on_blue === true ) {

			// 			$('#zb1 h2').text('Записаться на приём');
			// 			$('#zb1 .submit-button').val('Записаться');
			// 			$('#zb1 .submit-button').attr('onclick','');
			// 			$("#add_pc").remove();
			// 			$('#zb1 .formbox').prepend('<p id="add_pc" style="text-align: center; color: #fff; font-size: 1.3em; line-height: 1.3em;">Получите скидку 3% на первый приём, записавшись через форму</p>');
			// 			$('#zb1 label + #comment, #zb1 #comment-label').show();
			// 			$('#zb1').fadeIn(0).removeClass('zoomOut').addClass('zoomIn2').addClass('animated');
			// 			$('.wrapper').fadeIn(300);
			// 			$('body').css('overflow','hidden');
			// 			on_blue = false;

			// 		}

			// 	}

      // });
      if(( $(".ab").hasClass("blue") ) && (show_banner) ){
        setTimeout(()=>{showModal()}, 25000);
      }


            $('.but-callback').click(function(){
                $('#zb1 h2').text('Записаться на приём');
                $('#zb1 .submit-button').val('Записаться');
				$('#zb1 .submit-button').attr('onclick','');
				$("#add_pc").remove();
				$('#zb1 .formbox').prepend('<p id="add_pc" style="text-align: center; color: #fff; font-size: 1.3em; line-height: 1.3em;">Получите скидку 3% на первый приём, записавшись через форму</p>');
                $('#zb1 label + #comment, #zb1 #comment-label').show();
                $('#zb1').fadeIn(0).removeClass('zoomOut').addClass('zoomIn2').addClass('animated');
                $('.wrapper').fadeIn(300);
                $('body').css('overflow','hidden');
            });




            $('.but-callback-2').click(function(){
                $('#zb1 h2').text('Заказать обратный звонок');
                $('#zb1 .submit-button').val('Заказать');
				$('#zb1 .submit-button').attr('onclick','yaCounter46748658.reachGoal(\'recall\'); return true;');
				$("#add_pc").remove();
                $('#zb1 label + #comment, #zb1 #comment-label').hide();
                $('#zb1').fadeIn(0).removeClass('zoomOut').addClass('zoomIn2').addClass('animated');
                $('.wrapper').fadeIn(300);
                $('body').css('overflow','hidden');
                return false;
            });
        });



        $(function() {
            $('.wrapper, .close').click(function(){
                $('.wrapper').fadeOut(300);
                $('.z-box, .thnx, .thnx2').removeClass('zoomIn2').addClass('zoomOut').addClass('animated').fadeOut(300);
                $('body').css('overflow','auto');
            });
        });

/* form */
/*
        $(function() {
            $('#form1').validate(
                {
                    rules: {
                        "name": {
                            required:true
                        },
                        "phone":{
                            required:true
                        },
                        "email":{
                            required:true
                        }
                    },
                    messages: {
                        "name": {
                            required: ""
                        },
                        "phone":{
                            required:""
                        },
                        "email":{
                            required:""
                        }
                    }
                });
        });
*/
$(function() {
  $(document).ready(function () {
    $('input[name="phn"]').inputmask('+7 (999) 999-99-99');
  });
  $('#form1').validate({
    errorPlacement: function(error, element) {
    // Append error within linked label
      $( element )
      .closest( "form" )
      .find( "div#error__"+element.attr( "name" )+"" )
      .append( error );
    },
    errorElement: "div",
    rules: {
      name: {
        required: true,
        minlength: 2
      },
      phn: {
        required: true,
        minlength: 18
      },
    },
    messages: {
      name: {
        required: "(это обязательное поле)",
        minlength: "(введите минимум 2 символа)"
      },
      phn: {
        required: "(это обязательное поле)",
        minlength: "(введите номер полностью)"
      },
    },
    submitHandler: function(form) {
      //Отправка формы
      var formData = $(form).serialize(),
      action = '/ajax/send.php';
      $.ajax({
        method:'POST',
        data:formData,
        dataType: 'json',
        url: action,
        success: function(data){
          console.log(data);
          if(data.success == 1) {
            //цели
            gtag('event', 'send', {
             'event_category': 'conversion',
             'event_action': 'send'
            });
            yaCounter46748658.reachGoal('send');
            $('.wrapper').fadeIn(300);
            $('.thnx').fadeIn(0).removeClass('zoomOut').addClass('zoomIn2').addClass('animated');
            $('.z-box').removeClass('zoomIn2').addClass('zoomOut').addClass('animated').fadeOut(300);
            $('body').css('overflow','hidden');
            $('#form1')[0].reset();
          } else {
            console.log('error');
          }
        },
        error: function(error){
          console.log(error);
        }
      });
      return false;
    }
  });
});
/* thnx */

        /*$(function() {
            $('#form1').ajaxForm(function() {
                $('.wrapper').fadeIn(300);
                $('.thnx').fadeIn(0).removeClass('zoomOut').addClass('zoomIn2').addClass('animated');
                $('.z-box').removeClass('zoomIn2').addClass('zoomOut').addClass('animated').fadeOut(300);
                $('body').css('overflow','hidden');
                $('#form1')[0].reset();
            });
        });*/

/* gallery */
$(function() {

  $(".fancybox").fancybox();

  $('[data-fancybox]').fancybox({
	youtube : {
		controls : 0,
		showinfo : 0,
        autoplay : 0
	},

    media : {
    	youtube : {
    		params : {
            			autoplay : 0
            		}
            	}
        }
  });

  $('[data-fancybox="reviews-video"]').fancybox({

  afterLoad : function( instance, current ) {

     // Remove scrollbars and change background
    current.$content.css({
			overflow   : 'visible',
      background : '#000'
		});

  },
  onUpdate : function( instance, current ) {
    var width,
        height,
        ratio = 16 / 9,
        video = current.$content;

    if ( video ) {
      video.hide();

      width  = current.$slide.width();
      height = current.$slide.height() - 100;

      if ( height * ratio > width ) {
        height = width / ratio;
      } else {
        width = height * ratio;
      }

      video.css({
        width  : width,
        height : height
      }).show();

    }
  }
});

});


/* sliders */
           $(document).ready(function(){
                $('.banner').slick({
                  dots: true,
                  arrows: false,
                  autoplay: true,
                  infinite: true,
                  speed: 900,
                  slidesToShow: 1,
                  slidesToScroll: 1
                });
            });


            $(document).ready(function(){
                $('.team').slick({
                  dots: false,
                  arrows: true,
                  autoplay: true,
                  speed: 600,
                  infinite: true,
                  slidesToShow: 4,
                  slidesToScroll: 1,
                  responsive: [
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 992,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 620,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
                });
            });


            $(document).ready(function(){
                $('.stock-news').slick({
                  dots: false,
                  arrows: true,
                  autoplay: true,
                  speed: 600,
                  infinite: true,
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  responsive: [
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
                }).on('setPosition', function (event, slick) {
                    slick.$slides.css('height', slick.$slideTrack.height() + 'px');
                });
            });


            $(document).ready(function(){
                $('.video').slick({
                  dots: false,
                  arrows: true,
                  autoplay: true,
                  speed: 600,
                  infinite: true,
                  slidesToShow: 4,
                  slidesToScroll: 1,
                  responsive: [
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 992,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
                });
            });


            $(document).ready(function(){
                $('.inner-top-slider').slick({
                  dots: false,
                  arrows: true,
                  autoplay: true,
                  speed: 600,
                  infinite: true,
                  slidesToShow: 6,
                  slidesToScroll: 1,
                  responsive: [
                    {
                      breakpoint: 1400,
                      settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 1050,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 700,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 550,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
                });
            });
var eventMenuClick = new Event('menuClick');
function garm(clk, ParentElement, FindElement, FindElement2){
  if( ParentElement.hasClass('nav-open')){
    FindElement.animate({'height':0},400)
    FindElement2.animate({'opacity':0},250)
    ParentElement.removeClass("nav-open");
    ParentElement.addClass("nav-close");
  } else{
    FindElement.removeAttr('style');
    FindElement2.css({'opacity':1})
    ParentElement.removeClass("nav-close");
    ParentElement.addClass("nav-open");
    heightChildUl = FindElement.height();
    ParentElement.removeClass("nav-open");
    ParentElement.addClass("nav-close");
    FindElement.css({'height':0})
    FindElement2.css({'opacity':0})
    FindElement.animate({'height':heightChildUl+"px"},250);
    FindElement2.animate({'opacity':1},400)
    ParentElement.removeClass("nav-close");
    ParentElement.addClass("nav-open");
  }
}
jQuery(document).ready(function() {
  $(".sb_showchild").on('click',  function() {
    ParentElement = $(this).parents("li");
    FindElement = ParentElement.find("ul").first();
    FindElement2 = ParentElement.find("ul li").first();

    window.dispatchEvent(eventMenuClick);
    garm($(this), ParentElement, FindElement, FindElement2);
  });

  //Прокрутка к верху страниицы
  var top_show = 700; // В каком положении полосы прокрутки начинать показ кнопки "Наверх"
  var delay = 1000; // Задержка прокрутки
  $(window).scroll(function () { // При прокрутке попадаем в эту функцию
    /* В зависимости от положения полосы прокрукти и значения top_show, скрываем или открываем кнопку "Наверх" */
    if ($(this).scrollTop() > top_show) $('#top').fadeIn();
    else $('#top').fadeOut();
  });
  $('#top').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
    /* Плавная прокрутка наверх */
    $('body, html').animate({
      scrollTop: 0
    }, delay);
  });
  let urlArr= window.location.href.split('/');
  console.log(urlArr[urlArr.length-1]);
 if($('.accordion')&&urlArr[urlArr.length-1]!="prices")
 {
     let html=`\
     <div class="menu-bar">
         <div class="menu-bar-item menu-bar-item-active">Описание</div>\
         <div class="menu-bar-item">Цены</div>
     </div>
     `;
     $('.accordion dd').attr({'class':"accordion-content accordionItem is-expanded animateIn", 'aria-hidden':"false"});
     $('.accordion').wrapAll('<div class="prices"></div>');
     $('.prices').siblings().wrapAll('<div class="information"></div>');
     $('.prices').eq(0).before(html);
     $('.prices').hide();
     $(document).on('click','.menu-bar-item',function(){
         if(!$(this).hasClass('menu-bar-item-active'))
         {
             $('.menu-bar-item').removeClass('menu-bar-item-active');
             $(this).addClass('menu-bar-item-active');
             $('.prices').toggle();
             $('.information').toggle();
         }
     });
 }
});
function showModal()
      {
        $('#zb1 h2').text('Записаться на приём');
						$('#zb1 .submit-button').val('Записаться');
						$('#zb1 .submit-button').attr('onclick','');
						$("#add_pc").remove();
						$('#zb1 .formbox').prepend('<p id="add_pc" style="text-align: center; color: #fff; font-size: 1.3em; line-height: 1.3em;">Получите скидку 3% на первый приём, записавшись через форму</p>');
						$('#zb1 label + #comment, #zb1 #comment-label').show();
						$('#zb1').fadeIn(0).removeClass('zoomOut').addClass('zoomIn2').addClass('animated');
						$('.wrapper').fadeIn(300);
						$('body').css('overflow','hidden');
      }
