<?php

class Admin_AdminproductsController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
		
		$this->view->addHelperPath(
                'ZendX/JQuery/View/Helper'
                ,'ZendX_JQuery_View_Helper');
    }
    
    public function indexAction()
    {
		$form = new Default_Form_Search_SearchAdmin();
		$this->view->form = $form;
        $products_model = new Default_Model_DbTable_Products();
        $products = $products_model->getAllProductsAdmin(); //Zend_Debug::dump($products);exit;
		$page = $this->getRequest()->getParam('page');
		$this->view->page = $page;
		
        $paginator = Zend_Paginator::factory($products);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);   
    
    }
    
    public function addproductAction()
    {
        include( 'SimpleImage.php' );
		
		// Использование Web-сервиса 
        // "Currency Exchange Rate" от xmethods.com 
        // Создание SOAP-клиента по WSDL-документу
    //    $client = new SoapClient('http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL'); 
     //   if (!isset($date)) $date = date('Y-m-d', strtotime('+1 day')); 
    //    $curs = $client->GetCursOnDate(array('On_date' => $date));
     //   $rates = new SimpleXMLElement($curs->GetCursOnDateResult->any);//var_dump($rates);
    //    $code = 978;    // евро
        //  $code = 840;  // доллар
    //    $result = $rates->xpath('ValuteData/ValuteCursOnDate/Vcode[.=' . $code . ']/parent::*'); //var_dump($result);
        
      //  $vc = (float)$result[0]->Vcurs;
        //        $vn = (int)$result[0]->Vnom;
		$vc = 1;		
        $form = new Default_Form_Products_AddProduct(); 
        $this->view->form = $form;
		$option_form = new Default_Form_Search_OptionAdmin();
		$this->view->option_form = $option_form;
        $products_model = new Default_Model_DbTable_Products();
        $attributes_model = new Default_Model_DbTable_Attributes();
        $attributes = $attributes_model->getAllAttributesAdmin(); //Zend_Debug::dump($attributes);exit;
        $this->view->attributes = $attributes; 
		$productsattributes_model = new Default_Model_DbTable_Productsattributes();
		$categories_model = new Default_Model_DbTable_Categories();
		
		$options_model = new Default_Model_DbTable_Option();
		$options = $options_model->getAllOptionsAdmin();
	//	Zend_Debug::dump($products);exit;
		
		$this->view->options = $options;
    
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
		
				$category_id = $_POST['category_id'];
				$parent_id = $categories_model->getParentCategory($category_id);
                $manufacturer_id = $_POST['manufacturer_id'];
                $sort_order = $_POST['sort_order'];
                $product_model = $_POST['product_model']; 
				$vendor_code = $_POST['vendor_code']; 
				//Zend_Debug::dump($product_model);exit;
                $status_in_out_of_stock = $_POST['status_in_out_of_stock'];
				$stock_availability_01 = $_POST['stock_availability_01'];
				$stock_availability_02 = $_POST['stock_availability_02'];
				$latest = $_POST['latest'];
				$special = $_POST['special'];
				$recommended = $_POST['recommended'];
                $price = $_POST['price'];
				$price_unit = $_POST['price_unit'];
				$price_per_unit = $_POST['price_per_unit'];
				$amount = $_POST['amount'];
                $intro_text = $_POST['intro_text'];
                $product_description = $_POST['product_description'];
                $url_seo = $_POST['url_seo']; //Zend_Debug::dump($menu_first_level_seo);exit;
                $header_tags_title = $_POST['header_tags_title'];
                $header_tags_description = $_POST['header_tags_description'];
				$product_characteristics = $_POST['product_characteristics'];
				$product_configurations = $_POST['product_configurations'];
				$product_overlook = $_POST['product_overlook'];
                $header_tags_keywords = $_POST['header_tags_keywords'];
				$alt = $_POST['alt'];
             //   $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
			//	$file_name = $_FILES['image']['name']; //Zend_Debug::dump($file_name);exit;
			//	$file_size = $_FILES['image']['size']; //Zend_Debug::dump($file_size);exit;
			//	$file_tmp = $_FILES['image']['tmp_name']; Zend_Debug::dump($file_tmp);exit;
			//	$file_type = $_FILES['image']['type']; Zend_Debug::dump($file_name);exit;
				$image = $_POST['image'];
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/products/big_".$newname;
                    $name_small = "media/photos/products/small_".$newname;
					$name_extra = "media/photos/products/extra_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
				//	$image_big->cutFromCenter(371, 373);
				//	$image_big->maxareafill(371, 373, 255, 255, 255); 
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                //    $image_small->resizeToWidth(300);
				//	$image_small->cutFromCenter(600, 450);
					$image_small->maxareafill(160, 110, 241, 241, 241); 
                    $image_small->save($name_small); 

					$image_extra = new SimpleImage();
                    $image_extra->load($uploadedfile);
					$image_extra->maxareafill(160, 110, 242, 194, 0); 
                    $image_extra->save($name_extra);  
                }  
                
                $products_model->addProduct(
                                                $category_id,
												$parent_id,
                                                $manufacturer_id,
                                                $sort_order,
                                                $product_model,
												$vendor_code,
                                                $price,
												$price_per_unit,
												$price_unit,
												$amount,
                                                $newname,
												$status_in_out_of_stock,
												$stock_availability_01,
												$stock_availability_02,
												$latest,
												$special,
												$recommended,
                                                $intro_text,
                                                $product_description,
												$product_characteristics,
												$product_configurations,
												$product_overlook,
                                                $url_seo,
                                                $header_tags_title,
                                                $header_tags_description,
                                                $header_tags_keywords,
												$alt
                                            );
				$product_id = $products_model->getLastInsertId();
				$this->view->product_id = $product_id;
			//	Zend_Debug::dump($product_id);exit;
			
				$attributes_array = array();
				$attributes_array = $_POST['product_attribute']; //Zend_Debug::dump($attributes_array);exit;
				
				
				foreach ($attributes_array as $product_attribute)  // Zend_Debug::dump($value);exit;
				{   
					$productsattributes_model->deleteAttributeAjax(
																		$product_id,
																		$product_attribute['attribute_id']
																	   ); 
				
					$productsattributes_model->addProductsAttributes(
																		$product_id,
																		$product_attribute['attribute_id'],
																		$product_attribute['text'] 
																	);
				} 
				
				$product_option_array = array();
				$product_option_array = $_POST['product_option']; 
			//	Zend_Debug::dump($product_option_array);exit;
				
				$product_option_model = new Default_Model_DbTable_Productoption();
				$product_option_value_model = new Default_Model_DbTable_Productoptionvalue();
			
				foreach ($product_option_array as $product_option) 
				{
					$product_option_model->addProductOption(
																$product_id,
																$product_option['option_id']
															);
																
						
					$product_option_id = $product_option_model->getLastInsertId();
					$this->view->product_option_id = $product_option_id;
					
					if (isset($product_option['product_option_value']) && count($product_option['product_option_value']) > 0 ) {
						foreach ($product_option['product_option_value'] as $product_option_value) {
						
							
							$product_option_value_model->addProductOptionValue(
																				$product_option_id,
																				$product_id,
																				$product_option['option_id'],
																				$product_option_value['option_value_id'],
																				$product_option_value['price_prefix'],
																				$product_option_value['price'],
																				$product_option_value['sort_order']
																			);
							
						}
					} else {
						$product_option_model->deleteProductOption(
																		$product_option_id
																	);
					}
				}
				                          
                unlink($_FILES['image']['tmp_name']);                            
                    
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editproductAction()
    {
        include( 'SimpleImage.php' );
		
		// Использование Web-сервиса 
        // "Currency Exchange Rate" от xmethods.com 
        // Создание SOAP-клиента по WSDL-документу
     //   $client = new SoapClient('http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL'); 
     //   if (!isset($date)) $date = date('Y-m-d', strtotime('+1 day')); 
     //   $curs = $client->GetCursOnDate(array('On_date' => $date));
      //  $rates = new SimpleXMLElement($curs->GetCursOnDateResult->any);//var_dump($rates);
      //  $code = 978;    // евро
        //  $code = 840;  // доллар
      //  $result = $rates->xpath('ValuteData/ValuteCursOnDate/Vcode[.=' . $code . ']/parent::*'); //var_dump($result);
        
     //   $vc = (float)$result[0]->Vcurs;
       //         $vn = (int)$result[0]->Vnom;
		$vc = 1;		
        $form = new Default_Form_Products_EditProduct(); 
        $this->view->form = $form;
        $products_model = new Default_Model_DbTable_Products();
        
        $product_id = $this->getRequest()->getParam('product_id');
		$page = $this->getRequest()->getParam('page');
        $product_image = $products_model->getProductImage($product_id);
        $this->view->product_image = $product_image; 
        $this->view->product_id = $product_id; 
        
        $products_attributes = $products_model->getProductsAttributesAdmin($product_id); //Zend_Debug::dump($products_attributes);exit;
        $this->view->products_attributes = $products_attributes; //Zend_Debug::dump($products_attributes['category_id']);exit;
        
        $product = $products_model->getProduct($product_id); //Zend_Debug::dump($products_attributes);exit;
        $this->view->product = $product; //Zend_Debug::dump($product['manufacturer_id']);exit;
		
		$categories_model = new Default_Model_DbTable_Categories();
        $categories = $categories_model->fetchAll(); //Zend_Debug::dump($categories);exit;
		$this->view->categories = $categories;
        
        $attributes_model = new Default_Model_DbTable_Attributes();
        $attributes = $attributes_model->getAllAttributesAdmin(); //Zend_Debug::dump($attributes);exit;
        $this->view->attributes = $attributes; 
		
		$options_model = new Default_Model_DbTable_Option();
		$options = $options_model->getAllOptionsAdmin();
	//	Zend_Debug::dump($products);exit;
		
		$this->view->options = $options;
		
		$product_options = $products_model->getProductOptionsFront($product_id); 
	//	Zend_Debug::dump($product_options);exit;
        $this->view->product_options = $product_options; 
		
		$product_options_values['option_values'] = array();
		
		$option_value_model = new Default_Model_DbTable_Optionvalue();
		
		foreach ($product_options as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				if (!isset($product_options_values['option_values'][$product_option['option_id']])) {
					$product_options_values['option_values'][$product_option['option_id']] = $option_value_model->getOptionValues($product_option['option_id']);
				}
			}
		}
	//	Zend_Debug::dump($product_options1['option_values']);exit;
		$this->view->option_values = $product_options_values['option_values']; 
	//	$this->view->product_options['option_values'][$product_option['option_id']] = $product_options['option_values'][$product_option['option_id']]; 
		
		//Zend_Debug::dump($products_attributes['category_id']);exit;
	/*	$option_id = $products_options[0]['option_id'];
	
		$option_model = new Default_Model_DbTable_Option();
		if($option_id) {
			$option_value_description = $option_model->getOptionValueDescriptionAdmin($option_id); 
	
			$this->view->option_value_description = $option_value_description; 
		}*/
		
		$manufacturers_model = new Default_Model_DbTable_Manufacturers();
        $manufacturers = $manufacturers_model->getAllManufacturersAdmin();
		$this->view->manufacturers = $manufacturers;
		
		$productsattributes_model = new Default_Model_DbTable_Productsattributes();
        
        $old_photo_file = $products_model->getProductImage($product_id);
        $directory = "media/photos/products";
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $category_id = $_POST['category_id'];
				$parent_id = $categories_model->getParentCategory($category_id);
                $manufacturer_id = $_POST['manufacturer_id'];
				$option_id = $_POST['option_id'];
				$product_option_id = $_POST['product_option_id'];
                $sort_order = $_POST['sort_order'];
                $product_model = $_POST['product_model']; 
				$vendor_code = $_POST['vendor_code']; 
				//Zend_Debug::dump($product_model);exit;
                $status_in_out_of_stock = $_POST['status_in_out_of_stock'];
				$stock_availability_01 = $_POST['stock_availability_01'];
				$stock_availability_02 = $_POST['stock_availability_02'];
				$latest = $_POST['latest'];
				$special = $_POST['special'];
				$recommended = $_POST['recommended'];
                $price = $_POST['price'];
				$price_unit = $_POST['price_unit'];
				$price_per_unit = $_POST['price_per_unit'];
				$amount = $_POST['amount'];
                $intro_text = $_POST['intro_text'];
                $product_description = $_POST['product_description'];
				$product_characteristics = $_POST['product_characteristics'];
				$product_configurations = $_POST['product_configurations'];
				$product_overlook = $_POST['product_overlook'];
                $url_seo = $_POST['url_seo']; //Zend_Debug::dump($menu_first_level_seo);exit;
                $header_tags_title = $_POST['header_tags_title'];
                $header_tags_description = $_POST['header_tags_description'];
                $header_tags_keywords = $_POST['header_tags_keywords'];
				$alt = $_POST['alt'];
				$image = $form->getValue('image');
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                        //  Zend_Debug::dump($uploadedfile);exit;
                if($image)
                {       
                    if(is_file("$directory/small_$old_photo_file"))
                    {
						unlink("$directory/big_$old_photo_file");
						unlink("$directory/small_$old_photo_file");
						unlink("$directory/extra_$old_photo_file");
                    }
                }
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
					//Zend_Debug::dump($newname);exit;
                    $name_big = "media/photos/products/big_".$newname;
                    $name_small = "media/photos/products/small_".$newname;
					$name_extra = "media/photos/products/extra_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
				//	$image_big->cutFromCenter(371, 373);
				//	$image_big->maxareafill(371, 373, 255, 255, 255); 
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                //    $image_small->resizeToWidth(300);
                 //   $image_small->resizeToHeight(171);
				//	$image_small->cutFromCenter(600, 450);
					$image_small->maxareafill(160, 110, 241, 241, 241); 
                    $image_small->save($name_small); 

					$image_extra = new SimpleImage();
                    $image_extra->load($uploadedfile);
					$image_extra->maxareafill(160, 110, 242, 194, 0);  
                    $image_extra->save($name_extra);  
                } 
                
                if(!$image)
                {      
                    $products_object = $products_model->getProduct($product_id);
                    $newname = $products_object['image'];
                }							
 
                $products_model->updateProduct(
                                                    $product_id,
                                                    $category_id,
													$parent_id,
                                                    $manufacturer_id,
                                                    $sort_order,
                                                    $product_model,
													$vendor_code,
                                                    $price,
													$price_per_unit,
													$price_unit,
													$amount,
                                                    $newname,
													$status_in_out_of_stock,
													$stock_availability_01,
													$stock_availability_02,
													$latest,
													$special,
													$recommended,
                                                    $intro_text,
                                                    $product_description,
													$product_characteristics,
													$product_configurations,
													$product_overlook,
                                                    $url_seo,
                                                    $header_tags_title,
                                                    $header_tags_description,
                                                    $header_tags_keywords,
													$alt
                                                );
												
				$attributes_array = array();
				$attributes_array = $_POST['product_attribute']; //Zend_Debug::dump($attributes_array);exit;
				
				if (!empty($attributes_array[0]['product_id'])) {
				
				foreach ($attributes_array as $product_attribute)  // Zend_Debug::dump($value);exit;
				{   
					$productsattributes_model->deleteAttributeAjax(
																		$product_attribute['product_id'],
																		$product_attribute['attribute_id']
																	   ); 
				
					$productsattributes_model->addProductsAttributes(
																		$product_attribute['product_id'],
																		$product_attribute['attribute_id'],
																		$product_attribute['text'] 
																	);
				} 
				}
                $product_option_array = array();
				$product_option_array = $_POST['product_option']; 
			//	Zend_Debug::dump($product_option_array);exit;
				
				$product_option_model = new Default_Model_DbTable_Productoption();
				$product_option_value_model = new Default_Model_DbTable_Productoptionvalue();
			//	if($option_id != 0) {
					$product_option_model->deleteProductOption(
																$product_id
															);
					$product_option_value_model->deleteProductOptionValue(
																			$product_id
																		);
			
					foreach ($product_option_array as $product_option) 
					{
						$product_option_model->addProductOption(
																	$product_id,
																	$product_option['option_id']
																);
																	
							
						$product_option_id = $product_option_model->getLastInsertId();
						$this->view->product_option_id = $product_option_id;
						
						if (isset($product_option['product_option_value']) && count($product_option['product_option_value']) > 0 ) {
							foreach ($product_option['product_option_value'] as $product_option_value) {
							
								
								$product_option_value_model->addProductOptionValue(
																					$product_option_id,
																					$product_id,
																					$product_option['option_id'],
																					$product_option_value['option_value_id'],
																					$product_option_value['price_prefix'],
																					$product_option_value['price'],
																					$product_option_value['sort_order'] 				
																				);
								
							}
						} else {
							$product_option_model->deleteProductOption(
																			$product_option_id
																		);
						}
					}
					
					
			//	}
                $this->_helper->redirector('index', 'adminproducts', 'admin', array('page' => $page));
				} else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        } else { 
            $product_id = $this->_getParam('product_id', 0);
            if ($product_id > 0)
            {
                $form->populate($products_model->getProduct($product_id));
                $products_object = $products_model->getProduct($product_id);
                $this->view->product_name = $products_object['product_model'];
            }
        }
    }
    
    public function deleteproductAction()
    {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $product_id = $this->getRequest()->getPost('product_id');
            
                $products_model = new Default_Model_DbTable_Products();
                $page_products = $products_model->getProduct($product_id);
                
                $old_photo_file = $products_model->getProductImage($product_id); //Zend_Debug::dump($old_photo_file);exit;
                $products_photo_model = new Default_Model_DbTable_Productsphoto();
                $all_photos = $products_photo_model->getAllPhotosForProduct($product_id); //Zend_Debug::dump($all_photos);exit;
                $productsattributes_model = new Default_Model_DbTable_Productsattributes();
				
                $directory_photos = "media/photos/products";
                
                foreach($all_photos as $photo)
                {
                    unlink($directory_photos .'/big_'.$photo["photo_file"]);
                    unlink($directory_photos .'/small_'.$photo["photo_file"]); 
					unlink($directory_photos .'/extra_'.$photo["photo_file"]);
                }
                 
                $directory = "media/photos/products";
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
				{
					unlink("$directory/big_$old_photo_file");
					unlink("$directory/small_$old_photo_file");
					unlink("$directory/extra_$old_photo_file");
				}  
            
                $products_photo_model->deleteAllPhotosForProduct(
																	$product_id
																);
				
				$productsattributes_model->deleteProductsAttributes(
																		$product_id
																	);
                
                $products_model->deleteProduct(
												$product_id
												);
            }       
            $this->_helper->redirector('index');
        } else {
            $product_id = $this->_getParam('product_id'); // Zend_Debug::dump($nashi_zeny_id);exit;
            
            $products_model = new Default_Model_DbTable_Products();
            
            $this->view->products_object = $products_model->getProduct($product_id);
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        if ($this->getRequest()->isPost())
        {
            $product_id = $this->getRequest()->getParam('product_id');
            $status = '1';
            $products_model->updateProductstatus(
                                                    $product_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        if ($this->getRequest()->isPost())
        {
            $product_id = $this->getRequest()->getParam('product_id');
            $status = '0';
            $products_model->updateProductstatus(
                                                    $product_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        $product_id = $this->getRequest()->getParam('product_id');
        
        $old_photo_file = $products_model->getProductImage($product_id);
        $directory = "media/photos/products";
        
        if ($this->getRequest()->isPost())
        {
            $product_id = $this->getRequest()->getParam('product_id');
            $image = '';
           
            $products_model->deleteProductImage(
                                                    $product_id,  
                                                    $image
                                                );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
				unlink("$directory/extra_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
    public function editattributesAction()
    {
        $productsattributes_model = new Default_Model_DbTable_Productsattributes();
        
        $product_id = $this->getRequest()->getParam('product_id'); 
        $this->view->product_id = $product_id;
      
        if ($this->getRequest()->isPost())
        { 
            $attributes = array();
            $attributes = $_POST['product_attribute']; //Zend_Debug::dump($attributes[0]);exit;
            
            if (!empty($attributes[0]['product_id'])) {
            
            foreach ($attributes as $product_attribute)  // Zend_Debug::dump($value);exit;
            {   
                $productsattributes_model->deleteAttributeAjax(
                                                                        $product_attribute['product_id'],
                                                                        $product_attribute['attribute_id']
                                                                   ); 
            
                $productsattributes_model->addProductsAttributes(
                                                                    $product_attribute['product_id'],
                                                                    $product_attribute['attribute_id'],
                                                                    $product_attribute['text'] 
                                                                );
            } 
            }
                
            $this->_helper->redirector('index');
           
        }
    }
	
	public function deleteattributeajaxAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $productsattributes_model = new Default_Model_DbTable_Productsattributes();
        
        if ($this->getRequest()->isPost())
        {
            $product_id = $_POST['product_id']; //Zend_Debug::dump($product_id);exit;
            $attribute_id = $_POST['attribute_id']; //Zend_Debug::dump($attribute_id);exit;
           
                $productsattributes_model->deleteAttributeAjax(
                                                                    $product_id,
                                                                    $attribute_id               
                                                            );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function searchAction()
    {
		$form = new Default_Form_Search_SearchAdmin();
		$this->view->form = $form;
		
			
		$products_model = new Default_Model_DbTable_Products();
		$categories_model = new Default_Model_DbTable_Categories();
		$query = $this->getRequest()->getParam('query');
	//	Zend_Debug::dump($query);exit;
		$this->view->query = $query;
	
		$pieces = explode(" ", $query);
		$query_01 = $pieces[0];
		@$query_02 = $pieces[1];
		@@$query_03 = $pieces[2];
		@$query_04 = $pieces[3];
		@$query_05 = $pieces[4];
		
		$implode = $products_model->getAllProductsSearchAdmin(
																$query_01,
																$query_02,
																$query_03,
																$query_04,
																$query_05
															);
	//	Zend_Debug::dump($implode);exit;
		
		$paginator = Zend_Paginator::factory($implode);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);
		
    }

}

