<?php

/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Menu extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_menu';
	
	public function getCategory($category_id)
    {
        $category_id = (int)$category_id;
        $row = $this->fetchRow('category_id = ' . $category_id);
        if(!$row) {
            throw new Exception("Нет записи с category_id - $category_id");
        }
        return $row->toArray();
    }
	
	public function addCategory(
                                    $parent_id,
                                    $sort_order,
									$category_name,
                                    $url_seo,
									$title,
									$text,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                )
    {
        $data = array(
            'parent_id' => $parent_id,
            'sort_order' => $sort_order,
			'category_name' => $category_name,
            'url_seo' => $url_seo,
			'title' => $title,
			'text' => $text,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        $this->insert($data);
    }
	
	public  function updateCategory(
                                        $category_id,
                                        $parent_id,
                                        $sort_order,
                                        $category_name,
                                        $url_seo,
										$title,
										$text,
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                        )
    {
        $data = array(
            'parent_id' => $parent_id,
            'sort_order' => $sort_order,
            'category_name' => $category_name,
            'url_seo' => $url_seo,
			'title' => $title,
			'text' => $text,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords  
        );
        
        $this->update($data, 'category_id = ' . (int)$category_id);
    }
   
    public function getAllCategoresDescAdmin()
    {
        $_name = 'zend_menu';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('category_id');
     
        $category = $this->fetchAll($select);
        return $category;
    }
    
    
    
    public function getAllCategoresDescIndex()
    {
        $_name = 'zend_menu';
     //   $_name_01 = 'zend_menu_first_level';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', 0)
                //    ->joinLeft(array('name_01' => $_name_01),'name.menu_first_level_id = name_01.menu_first_level_id', array('name_01.menu_first_level_name_ru'))
                 //   ->group('name.menu_first_level_id')
                    ->order('category_id DESC');
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    
    public function getСertainCategory($category_seo)
    {
        $_name = 'zend_menu';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_seo = ?', $category_seo);
                     
        $category_object = $this->fetchAll($select);    
      //  $subcategory_name = $subcategory_object["subcategory_name"];     
        return $category_object;
    }
    
    public function getAllFirstLevelCategories($parent_id = 0)
    {
        $_name = 'zend_menu';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $parent_id);
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    
    public function getAllSecondLevelCategories($parent_id)
    {
        $_name = 'zend_menu';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $parent_id)
                    ->order('sort_order');
     
        $categores = $this->fetchAll($select);
        return $categores;
	//	return $categores->toArray();
    }
  
	public function getCategoryForFrontPage()
	{
        $_name = 'zend_menu';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', 0)
					->order('sort_order');
     
		$category = $this->fetchAll($select);
        return $category;
    }
    
    public function getCategoryIdByUrl($url_seo)
	{
	    $_name = 'zend_menu';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$category_rec = $this->fetchRow($select);	
        $category_id = $category_rec["category_id"];		
        return $category_id;
    }
	
	public function getCategoryNameByUrl($url_seo)
	{
	    $_name = 'zend_menu';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$category_rec = $this->fetchRow($select);	
        $category_id = $category_rec["category_name"];		
        return $category_id;
    }
	
	public function getPageTextByUrl($url_seo)
	{
	    $_name = 'zend_menu';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$category_rec = $this->fetchRow($select);	
        $text = $category_rec["text"];		
        return $text;
    }
	
	public function getPageTitleByUrl($url_seo)
	{
	    $_name = 'zend_menu';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$category_rec = $this->fetchRow($select);	
        $title = $category_rec["title"];		
        return $title;
    }
    
    public function getCategoryUrlByName($category_name)
	{
	    $_name = 'zend_menu';
        $select = $this->select()
                    ->from($_name)
					->where('category_name = ?', $category_name);

		$category_rec = $this->fetchRow($select);	
        $url_seo = $category_rec["url_seo"];		
        return $url_seo;
    }
    
    public function getCategoryItem($category_id)
    {
        $_name = 'zend_menu';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $category_id);
                   // ->limit(1);

        $object = $this->fetchAll($select);
        return $object;
    } 
	
	public function deleteCategory($category_id)
    {
        $this->delete('category_id = ' . (int)$category_id);
    }
    
    public function checkActivCategory($category_id)
    {
        $_name_01 = 'zend_menu';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('category_id = ?', $category_id)
                    ->limit(1);

        $category_object = $this->fetchRow($select);    
        $category_id = $category_object["category_id"];     
        return $category_id;
    }
	
	public function checkParentCategory($category_id)
    {
        $_name_01 = 'zend_menu';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('parent_id = ?', $category_id)
                    ->limit(1);

        $category_object = $this->fetchRow($select);    
        $category_id = $category_object["category_id"];     
        return $category_id;
    }
   
    public function getParent($category_id)
    {
        $_name_01 = 'zend_menu';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('parent_id = ?', $category_id)
                    ->limit(1);

        $category_object = $this->fetchRow($select);    
        $category_name = $category_object["category_name"];     
        return $category_name;
    }
	
	public  function deleteCategoryImage(
                                            $category_id, 
                                            $main_photo
                                        )
    {
        $data = array(
			'main_photo' => $main_photo,
        );
        
        $this->update($data, 'category_id = ' . (int)$category_id);
    }
	
	public function getCategoryImage($category_id)
	{
	    $_name = 'zend_menu';
        $select = $this->select()
                    ->from($_name)
					->where('category_id = ?', $category_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec['main_photo'];		
        return $main_photo_file_name;
    }
	
    public  function updateCategoryStatus(
                                            $category_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'category_id = ' . (int)$category_id);
    }
    
    public function getCatItemMeta($category_id)
    {
        $_name = 'zend_menu';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('category_id = ?', $category_id);
                   // ->limit(1);

        $object = $this->fetchAll($select);
        return $object;
    } 
	
	public function getCategoriesByParent($category_id)
    {
        $_name = 'zend_menu';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $category_id);

        $categories = $this->fetchAll($select);
        return $categories;
    }
	
	public function getParentCategory($category_id)
	{
	    $_name = 'zend_menu';
        $select = $this->select()
                    ->from($_name)
					->where('category_id = ?', $category_id);

		$category_rec = $this->fetchRow($select);	
        $parent_id = $category_rec["parent_id"];		
        return $parent_id;
    }
	
	public function getPrice()
    {
        $_name = 'oc_category';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name));
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public  function updatePrice( 
                                    $category_id,
                                    $parent_id   
								 )
    {
        $data = array(
            
            'parent_id' => $parent_id
            
        );
        $this->update($data, 'category_id = ' . (int)$category_id);
    }
	
	public function getAllCategorySearchAdmin(
												$query_01,
												$query_02,
												$query_03,
												$query_04,
												$query_05
											)
    {
		$_name = 'zend_category';
	
		$select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('category_name  LIKE ?', '%'.$query_01.'%')
					->where('category_name  LIKE ?', '%'.$query_02.'%')
					->where('category_name  LIKE ?', '%'.$query_03.'%')
					->where('category_name  LIKE ?', '%'.$query_04.'%')
					->where('category_name  LIKE ?', '%'.$query_05.'%');
					           
        $all_objects = $this->fetchAll($select);
		return $all_objects;
    }
	
	public function getAllCategorySearch()
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name));
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
   
}
