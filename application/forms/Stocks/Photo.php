<?php
/**
 *  
 * 
 * 
 */
class Default_Form_Stocks_Photo extends Zend_Form
{
    public function init()
    {	
	    // Init form
        $this
          ->setDescription('Choose photos on your computer to add to this album.')
          ->setAttrib('id', 'form-upload')
		  ->setAttrib('onsubmit', 'download()')
          ->setAttrib('name', 'photo_create')
          ->setAttrib('enctype','multipart/form-data')
          ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));
		  
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		
		$uploadId = new Zend_Form_Element_Hidden('uploadId');
		$uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
		    ->setAttrib('id', 'uploadId')
		    ->setAttrib('value', md5(uniqid(rand())));
		
		$photo_id = new Zend_Form_Element_Hidden('photo_id');
        $photo_id->addFilter('Int');
		
		$photo_file = new Zend_Form_Element_File('photo_file');
		$photo_file->setLabel("Выбрать фото:")
		    ->setDescription("Допустимые форматы: jpg, png, gif")
		    ->setAttrib('size', '50')
			->addValidator('Count', false, array('min' => 1, 'max' => 5))
			->setMultiFile(5)
			->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
			->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/stocks/pages')
			->setValueDisabled(true)
			->addValidator('NotEmpty', true);
			
	/*	$checkbox = $this->createElement(
                'checkBox',
                'checkbox',
                array(
                    'label' => 'Tricky check box',
                    'checkedValue' => 1,
                    'uncheckedValue' => 0
                )
            );	
		$checkbox->setAttrib('onclick','changeValue()'); */
				
        $this->addElements(array($uploadId, $photo_id, $photo_file));
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'type' => 'submit',
		  'class' => 'btn btn-primary pull-left',
          'ignore' => true,
        ));	
		
		
				
        
        
    }
}

