<?php
/**
 * Default controller for this application.
 * 
 */
class Sorting_IndexController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
	    $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('category_list');	
    }
	
	public function productsofcategoryAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('category_list'); 
		$limit = $this->getRequest()->getParam('limit'); //Zend_Debug::dump($limit);exit;
		$brend = $this->getRequest()->getParam('brend'); //Zend_Debug::dump($brend);exit;
	//	foreach ($brend as $key => $value) {
	//		$q[] = '$brend['.$key.'],';
	//	}
	//	Zend_Debug::dump($q);exit;
		$gender = $this->getRequest()->getParam('gender');
	
		$size = $this->getRequest()->getParam('size');
		
		$this->view->brend = $brend;
		$this->view->gender = $gender;
		$this->view->size = $size;
		
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
	
        $products_model = new Default_Model_DbTable_Products();
        $categories_model = new Default_Model_DbTable_Categories();

			$url_seo = $this->getRequest()->getPost('url_seo');  //Zend_Debug::dump($url_seo);exit;
			$url_seo_get = $this->getRequest()->getParam('url_seo');  //Zend_Debug::dump($url_seo);exit;
		if($url_seo) {
			$category_id = $categories_model->getCategoryIdByUrl($url_seo);
		} else if($url_seo_get) { 
			$category_id = $categories_model->getCategoryIdByUrl($url_seo_get);
		}
		
			@$this->view->category_id = $category_id;
			$this->view->url_seo_get = $url_seo_get;
			
			if(@$brend[0]) {
				$brend[0] = $brend[0];
			} else {
				$brend[0] = 'q';
			}
			if(@$brend[1]) {
				$brend[1] = $brend[1];
			} else {
				$brend[1] = 'q';
			}
			if(@$brend[2]) {
				$brend[2] = $brend[2];
			} else {
				$brend[2] = 'q';
			}
			
			if(@$gender[0]) {
				$gender[0] = $gender[0];
			} else {
				$gender[0] = 'q';
			}
			if(@$gender[1]) {
				$gender[1] = $gender[1];
			} else {
				$gender[1] = 'q';
			}
			
			if(@$size[0]) {
				$size[0] = $size[0];
			} else {
				$size[0] = 'q';
			}
			if(@$size[1]) {
				$size[1] = $size[1];
			} else {
				$size[1] = 'q';
			}
			if(@$size[2]) {
				$size[2] = $size[2];
			} else {
				$size[2] = 'q';
			}
	
			$products = $products_model->getProductsSpecificCategoryForFrontFilterSort(
																						$category_id,
																						$brend[0],
																						$brend[1],
																						$brend[2],
																						$gender[0],
																						$gender[1],
																						$size[0],
																						$size[1],
																						$size[2]
																					); 
		
		//	Zend_Debug::dump($products);exit;
			$paginator = Zend_Paginator::factory($products);
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$this->view->paginator = $paginator;
			if(!$limit) { $limit = 12; }
				$count_per_page = Zend_Paginator::setDefaultItemCountPerPage($limit); 
			
			$sum_cart = $sessionNamespace->sum_cart;// Zend_Debug::dump($sum_cart);exit;
			$this->view->sum_cart = $sum_cart;
    }
	
    public function indexAction()
    {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('realestate/index.phtml');
        $realestate_object = new Default_Model_DbTable_Realestate();
        $lang = $this->getRequest()->getParam('lang');// Zend_Debug::dump($lang);exit;
        $this->view->lang = $lang;
        $category_id = $this->getRequest()->getParam('category_id');  
        $transaction_type_id = $this->getRequest()->getParam('transaction_type_id');
        $region_id = $this->getRequest()->getParam('region_id');
        $rooms_from = $this->getRequest()->getParam('rooms_from');
		$rooms_to = $this->getRequest()->getParam('rooms_to');
        $area_from = $this->getRequest()->getParam('area_from');
        $area_to = $this->getRequest()->getParam('area_to');
        $price_from = $this->getRequest()->getParam('price_from');
        $price_to = $this->getRequest()->getParam('price_to');
      //  Zend_Debug::dump($category_id);exit;
        $page = $realestate_object->searchingRealestate($transaction_type_id,
														$category_id,
														$region_id,
														$rooms_from,
														$rooms_to,
														$area_from,
														$area_to,
														$price_from,
														$price_to );
														
		$this->view->category = $category_id;												
  
        $paginator = Zend_Paginator::factory($page);  
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);
        
        $page_text_object = new Default_Model_DbTable_Pagetext();
        $pagetext = $page_text_object->getPagetextObject($category_id); //Zend_Debug::dump($pagetext);exit; 
        $this->view->pagetext = $pagetext;  
	
    }
	
	public function searchingAction()
    {
        $form = new Default_Form_Searching_Searching(); 
        $this->view->form = $form;
        $lang = $this->getRequest()->getParam('lang');
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) 
            { 
				$transaction_type_id = $form->getValue('transaction_type_id');
				$category_id = $form->getValue('category_id');
				$region_id = $form->getValue('region_id');
				$rooms_from = $form->getValue('rooms_from');
				if($rooms_from == NULL){
					$rooms_from = 1;
				}
				$rooms_to = $form->getValue('rooms_to');
				if($rooms_to == NULL){
					$rooms_to = 10000000;
				}
				$area_from = $form->getValue('area_from');
				if($area_from == NULL){
					$area_from = 1;
				}
                $area_to = $form->getValue('area_to'); 
				if($area_to == NULL){
					$area_to = 1000000000000000000;
				}
				$price_from = $form->getValue('price_from');
				if($price_from == NULL){
					$price_from = 1;
				}
				$price_to = $form->getValue('price_to');
				if($price_to == NULL){
					$price_to = 100000000000000000;
				}
				           
            /*    $model_object = new Default_Model_DbTable_Searching();
               
                $model_object->searchingRealestate(	
													$transaction_type,
													$category_id,
													$region_id,
													$rooms_from,
													$rooms_to,
													$area_from,
													$area_to,
													$price_from,
													$price_to
												  );
              */
                $this->_helper->redirector('index', 'index', 'searching', array('transaction_type_id' => $transaction_type_id, 
                                                                                'category_id' => $category_id, 
                                                                                'region_id' => $region_id,
                                                                                'rooms_from' => $rooms_from, 
                                                                                'rooms_to' => $rooms_to, 
                                                                                'area_from' => $area_from, 
                                                                                'area_to' => $area_to, 
                                                                                'price_from' => $price_from,
                                                                                'price_to' => $price_to, 
                                                                                'lang' => $lang
                                                                                )
                                                                                );
            } else {
                $this->view->errMessage = "rrrrrrrrrrrrr";
            }
        }
		
    }
	
	public function searchinglistAction()
    {
        $form = new Default_Form_Searching_Searching(); 
        $this->view->form = $form;
        $lang = $this->getRequest()->getParam('lang');
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) 
            { 
				$transaction_type_id = $form->getValue('transaction_type_id');
				$category_id = $form->getValue('category_id');
				$region_id = $form->getValue('region_id');
				$rooms_from = $form->getValue('rooms_from');
				if($rooms_from == NULL){
					$rooms_from = 1;
				}
				$rooms_to = $form->getValue('rooms_to');
				if($rooms_to == NULL){
					$rooms_to = 10000000;
				}
				$area_from = $form->getValue('area_from');
				if($area_from == NULL){
					$area_from = 1;
				}
                $area_to = $form->getValue('area_to'); 
				if($area_to == NULL){
					$area_to = 1000000000000000000;
				}
				$price_from = $form->getValue('price_from');
				if($price_from == NULL){
					$price_from = 1;
				}
				$price_to = $form->getValue('price_to');
				if($price_to == NULL){
					$price_to = 100000000000000000;
				}
				           
            /*    $model_object = new Default_Model_DbTable_Searching();
               
                $model_object->searchingRealestate(	
													$transaction_type,
													$category_id,
													$region_id,
													$rooms_from,
													$rooms_to,
													$area_from,
													$area_to,
													$price_from,
													$price_to
												  );
              */
                $this->_helper->redirector('index', 'index', 'searching', array('transaction_type_id' => $transaction_type_id, 
                                                                                'category_id' => $category_id, 
                                                                                'region_id' => $region_id,
                                                                                'rooms_from' => $rooms_from, 
                                                                                'rooms_to' => $rooms_to, 
                                                                                'area_from' => $area_from, 
                                                                                'area_to' => $area_to, 
                                                                                'price_from' => $price_from,
                                                                                'price_to' => $price_to, 
                                                                                'lang' => $lang
                                                                                )
                                                                                );
            } else {
                $this->view->errMessage = "rrrrrrrrrrrrr";
            }
        }
		
    }
	
	function sortingAction()
    {
		$form = new Default_Form_Products_ProductSort(); 
        $this->view->form = $form; 
	//	$limit = $this->getRequest()->getParam('limit');
	/*	$url_seo = $this->getRequest()->getPost('url_seo');
        $form = new Default_Form_Products_ProductFilter(); 
        $this->view->form = $form; 
		
		if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
        
            if ($form->isValid($formData)) 
            { 
				$pieces = explode("/", $_SERVER['REQUEST_URI']); 
				$pieces_02 = explode("?", $pieces[2]);				//Zend_Debug::dump($pieces[1]);exit; 
				$url = '';
				$url .= $pieces_02[0].'?';
				$brend = $_POST['brend'];
				$gender = $_POST['gender'];
				$size = $_POST['size'];
				foreach($brend as $key=>$value) {
					$brend_arr[] = '&brend['.$key.']='.$brend[$key];
				}
				$comma_separated_brend_arr = implode("", $brend_arr);
				
				$comma_separated_brend_arr = substr($comma_separated_brend_arr, 1);
				
				foreach($gender as $key=>$value) {
					$gender_arr[] = '&gender['.$key.']='.$gender[$key];
				}
				$comma_separated_gender_arr = implode("", $gender_arr);
				
				foreach($size as $key=>$value) {
					$size_arr[] = '&size['.$key.']='.$size[$key];
				}
				$comma_separated_size_arr = implode("", $size_arr);
			//	echo $comma_separated_brend_arr; exit;
			//	print_r ( $qq ); exit;
			//	Zend_Debug::dump($qq);exit;
				
				$price_from = $form->getValue('price_from');
				if($price_from == NULL){
					$price_from = 1;
				}
				$price_to = $form->getValue('price_to');
				if($price_to == NULL){
					$price_to = 100000000000000000;
				}
			
				$url .= $comma_separated_brend_arr;
				$url .= $comma_separated_gender_arr;
				$url .= $comma_separated_size_arr;
				$url .= '&sort=price';
				
				header('Location: http://mebel-rostov.su/filter/'.$url);
            } else {
                $this->view->errMessage = "rrrrrrrrrrrrr";
            }
        } 
		
       */
    }
}