<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Page05 extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_page_05';
    
    public function getPage($page_id)
    {
        $page_id = (int)$page_id;
        $row = $this->fetchRow('page_id = ' . $page_id);
        if(!$row) {
            throw new Exception("Нет записи с page_id - $page_id");
        }
        return $row->toArray();
    }
	
    public  function updatePage(
                                    $page_id,
                                    $text,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                )
    {
        $data = array(
            'text' => $text,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'page_id = ' . (int)$page_id);
    }
    
    
	public function getPageText()
	{
        $_name = 'zend_page_05';
        $select = $this->select()
                    ->from($_name);
     
		$text = $this->fetchAll($select);
        return $text;
    }
   
}
