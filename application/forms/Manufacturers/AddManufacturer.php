<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Manufacturers_AddManufacturer extends Zend_Form
{
    public function init()
    {   
		$uploadId = new Zend_Form_Element_Hidden('uploadId');
		$uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
		    ->setAttrib('id', 'uploadId')
		    ->setAttrib('value', md5(uniqid(rand())));
        
        $manufacturer_id = new Zend_Form_Element_Hidden('manufacturer_id');
        $manufacturer_id->addFilter('Int');
        
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'));
        
        
           
		$name = new Zend_Form_Element_Text('name');
        $name->setLabel("Название:")
            ->setAttrib('class', 'span14')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true
            );

		$image = new Zend_Form_Element_File('image');
		$image->setLabel("Выбрать фото:")
		    ->setDescription("Допустимые форматы: jpg, png, gif")
		    ->setAttrib('size', '50')
		//	->addValidator('Count', false, array('min' => 1, 'max' => 5))
		//	->setMultiFile(1)
			->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
			->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/manufacturers')
			->setValueDisabled(true);
		//	->setRequired(true)
		//	->addValidator('NotEmpty', false);
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
									$uploadId,
                                    $manufacturer_id,
                                    $name,
									$image,
                                    $sort_order,
                                    $submit
                                ));
       
    }
}