<?php
/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_Members_Avatar extends Zend_Form
{
    public function init()
    {	
	   
	    // Init form
        $this
          ->setDescription('Choose photos on your computer to add to this album.')
          ->setAttrib('id', 'form-upload')
          ->setAttrib('name', 'avatar_create')
          ->setAttrib('enctype','multipart/form-data')
          ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));
		  
		$isEmptyMessage = 'aaaaaaaaaaaaaaaaa'; 
		  
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		
		$user_id = new Zend_Form_Element_Hidden('user_id');
        $user_id->addFilter('Int');
		
		$avatar_file = new Zend_Form_Element_File('avatar_file');
		$avatar_file->setLabel($translate->translate('Choose avatar:'))
		    ->setDescription($translate->translate('Valid formats: jpg, png, gif'))
		    ->setAttrib('size', '50')
			->removeDecorator('label')
			->addValidator('Count', false, 1)
			->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '2MB'))
			->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/user_avatar')
			->setValueDisabled(true)
			->addValidator('NotEmpty', true,
                array('messages' => array('isEmpty' => $isEmptyMessage)));
				
        $this->addElements(array($avatar_file));
		
		
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить")
			->removeDecorator('label')
			->setAttrib('class', 'btn btn-primary pull-left'); 
				
        $cancel = $this->createElement('submit', 'cancel'); 

        

        $cancel->setLabel($translate->translate('Cancel')); 

        
        $this->addElements(array($cancel, $submit)); 

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons', 

        array('disableLoadDefaultDecorators' => true)); 



        $group = $this->getDisplayGroup('buttons'); 

        $group->addDecorators(array( 

            array('FormElements'), 
            array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')), 

        ));
        
    }
}

