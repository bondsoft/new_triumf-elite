﻿<?php
/**
 * 
 *
 * User_Account controller.
 * 
 * 
 * 
 * 
 */
class User_AccountController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('layout');
    }
	
	public function indexAction()
    {
       // nothing to do here, index.phtml will be displayed
	   
	    $auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id;
	   
        $users_object = new Default_Model_DbTable_Users();
		$user = $users_object->getUser($user_id);                //   Zend_Debug::dump($user_id);exit; 
		$this->view->user = $user;  
	
    }
    
    public function addAction()
    {
	    $translate = Zend_Registry::get('Zend_Translate');
	    $lang = Zend_Registry::get('Zend_Lang');
		
        $form = new Default_Form_Auth_Avatar();     
        $this->view->form = $form;
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id;
	   
        $users_object = new Default_Model_DbTable_Users();
		$user = $users_object->getUser($user_id);                 // Zend_Debug::dump($user_id);exit;
		$this->view->user = $user;
		$old_avatar_file = $users_object->getAvatar_file($user_id);   // Zend_Debug::dump($old_photo_file);exit;
        
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();
		   
		   	if ($form->isValid($formData) && $form->submit->isChecked())
			{
			    $avatar_file = $form->getValue('avatar_file');
				
			    $directory = "images/user_avatar";
		    	// 
			    if(is_file("$directory/$old_avatar_file"))
     		    {
     		        unlink("$directory/$old_avatar_file");
			    }
            
				if( empty($avatar_file) )
                {
					$this->view->errMessage = $translate->translate('You have not selected a file!');
                    return;
                }
				
				$data = $_FILES['avatar_file'];
				$uploadedfile = $data['tmp_name'];
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = substr($data["name"], -4); 
				    $newname = date("Ymd")."_".rand(1000,9999).$ext;//
				    $name = "images/user_avatar/".$newname;
					
				   /* ********************************************** */
					include( 'SimpleImage.php' );
				    $image = new SimpleImage();
				    $image->load($uploadedfile);
					$image->resizeToHeigth(90);
				    $image->save($name);   
				} 
				
                $users_object->updateAvatar($user_id, $newname);
				
				// 
                 unlink($_FILES['photo_file']['tmp_name']);
                
                $this->_helper->redirector('index', 'account', 'user', array('user_id' => $user_id, 'lang' => $lang));
            }  else {
                $this->_helper->redirector('index', 'account', 'user', array('user_id' => $user_id, 'lang' => $lang));
            }
        } else {
            // 
            $user_id = $this->_getParam('user_id', 0);
            if ($user_id > 0) {
                
            $users_object = new Default_Model_DbTable_Users();
            // 
            $form->populate($users_object->getUser($user_id));
            }
        }
    }
	
	public function delavatarAction()
    {
	    $lang = Zend_Registry::get('Zend_Lang');
	    // 
        if ($this->getRequest()->isPost()) {
		
            // 
            $del = $this->getRequest()->getPost('del');
			
            // 
            if ($del == 'yes') {
                // 
                $user_id = $this->getRequest()->getParam('user_id');
		
                $users_object = new Default_Model_DbTable_Users();
				// 
				$avatar_file = $users_object->getAvatar_file($user_id);
				
			//	Zend_Debug::dump($avatar_file);exit;
			
				$directory = "images/user_avatar";
				// 
				if(is_file("$directory/$avatar_file"))
				{
     		        unlink("$directory/$avatar_file");
				}	
             
                $users_object->updateAvatar($user_id, '');
            }
            $this->_helper->redirector('index', 'account', 'user', array('user_id' => $user_id, 'lang' => $lang));
        } else {
            
            $user_id = $this->_getParam('user_id');
            
            $users_object = new Default_Model_DbTable_Users();
            
            $this->view->avatar = $users_object->getUser($user_id);
        }
    }
	
	public function updatepassAction()
    {
	    $translate = Zend_Registry::get('Zend_Translate');
		
	    $lang = Zend_Registry::get('Zend_Lang');
		$translate = Zend_Registry::get('Zend_Translate');
		
	    $form_passwup = new Default_Form_Auth_Passwup();     
        $this->view->form_passwup = $form_passwup;
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id;
		
	    // 
        if ($this->getRequest()->isPost())
		{
		    $formData = $this->getRequest()->getPost();
			
			$user_id = $this->getRequest()->getParam('user_id');
			$password = $this->getRequest()->getPost('password');
			$passconf = $this->getRequest()->getPost('passconf');  //  Zend_Debug::dump($password);exit;
          
		 /*   if(!$password)
			{
				$this->view->errMessage = $translate->translate('Enter the Password');
                return;
            } */
			
			if($password !== $passconf)
			{
				$this->view->errMessage = $translate->translate('Passwords did not match');
                return;
            }
			
		    $valid = new Zend_Validate_NotEmpty();
            $value  = $password;
            $result = $valid->isValid($value);
			
			if (!$result)
			{
			    $this->view->errMessage = $translate->translate('Enter the Password');
                return;   
			}
             
			
			$validator = new Zend_Validate_StringLength(array('min' => 6, 'max' => 32));
            $res = $validator->isValid($password);
            if(!$res)
			{
			    $this->view->errMessage = $translate->translate('Passwords must be at least 6 characters in length.');
                return; 
			}
			
		   	if ($form_passwup->isValid($formData))
			{
                $users_object = new Default_Model_DbTable_Users();
				
                $users_object->update_password($user_id, md5($password));
            }
            $this->_helper->redirector('index', 'account', 'user', array('user_id' => $user_id, 'lang' => $lang));
        }
    }
	
	public function deleteAction()
    {
	    $lang = Zend_Registry::get('Zend_Lang'); 
        
        if ($this->getRequest()->isPost()) {
		
            $del = $this->getRequest()->getPost('del');
			
            // 
            if ($del == 'yes') {
                // 
                $user_id = $this->getRequest()->getParam('user_id');
		
                $users_object = new Default_Model_DbTable_Users();
				// 
				$avatar_file = $users_object->getAvatar_file($user_id);
				
			//	Zend_Debug::dump($avatar_file);exit;
			
				$directory = "images/user_avatar";
				// 
				if(is_file("$directory/$avatar_file"))
				{
     		        unlink("$directory/$avatar_file");
				}	
             
                $users_object->deleteUser($user_id);
            }
            $this->_helper->redirector('index', 'account', 'user', array('user_id' => $user_id, 'lang' => $lang));
        } else {
           
            $user_id = $this->_getParam('user_id');
            
            $users_object = new Default_Model_DbTable_Users();
            
            $this->view->avatar = $users_object->getUser($user_id);
        }
    }
	
	

}

