<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Sliderthree extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_slider_three';
	
	public function getSlider($slider_id)
    {
        $slider_id = (int)$slider_id;
        $row = $this->fetchRow('slider_id = ' . $slider_id);
        if(!$row) {
            throw new Exception("no record slider_id - $slider_id");
        }
        return $row->toArray();
    }
	
	public function addSlider(    
								$slider_title,
								$slider_url,
								$slider_title_extra,
								$slider_title_01,
								$slider_text,
                                $slider_worktime,
                                $department,
								$alt,
                                $sort_order,
                                $image,
								$header_tags_title,
								$header_tags_description,
								$header_tags_keywords,
                                $clinic_number						
                            )
    {
        $data = array(
			'slider_url' => $slider_url,
			'slider_title' => $slider_title,
			'slider_title_extra' => $slider_title_extra,
			'slider_title_01' => $slider_title_01,
			'slider_text' => $slider_text,
            'slider_worktime' => $slider_worktime,
            'department' => $department,
			'alt' => $alt,
			'sort_order' => $sort_order,
			'image' => $image,
			'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords,
            'clinic_number' => $clinic_number
        );
        $this->insert($data);
    }
	
	public function deleteSlider($slider_id)
    {
        $this->delete('slider_id = ' . (int)$slider_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateSlider( 
                                    $slider_id,
									$slider_title,
									$slider_url,
									$slider_title_extra,
									$slider_title_01,
									$slider_text,
                                    $slider_worktime,
                                    $department,
									$alt,
									$sort_order,
									$image,
									$header_tags_title,
									$header_tags_description,
									$header_tags_keywords,
                                    $clinic_number							
								 )
    {
        $data = array(
			'slider_url' => $slider_url,
			'slider_title' => $slider_title,
			'slider_title_extra' => $slider_title_extra,
			'slider_title_01' => $slider_title_01,
			'slider_text' => $slider_text,
            'slider_worktime' => $slider_worktime,
            'department' => $department,
			'alt' => $alt,
			'sort_order' => $sort_order,
			'image' => $image,
			'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords,
            'clinic_number' => $clinic_number
        );
        
        $this->update($data, 'slider_id = ' . (int)$slider_id);
    }
   
   
    public function getAllSliderAdmin()
    {  
        $_name = 'zend_slider_three'; 
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('slider_id DESC');
                     
        $all_sliders = $this->fetchAll($select);
        return $all_sliders;
    }
    
    public function getAllSliderFront()
    {  
        $_name = 'zend_slider_three'; 
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
                    ->order('sort_order');
                     
        $all_sliders = $this->fetchAll($select);
        return $all_sliders;
    }
	
	public function getSliderItem( $slider_url )
    {  
        $_name = 'zend_slider_three'; 
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('slider_url = ?', $slider_url);
                     
        $slider = $this->fetchRow($select);
        return $slider;
    }
	
	public function getSliderItemSeo( $slider_url )
    {  
        $_name = 'zend_slider_three'; 
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('slider_url = ?', $slider_url);
                     
        $slider = $this->fetchAll($select);
        return $slider;
    }
	
	public  function updateSliderStatus(
                                            $slider_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'slider_id = ' . (int)$slider_id);
    }
	
	public function getSliderImage($slider_id)
	{
	    $_name = 'zend_slider_three';
        $select = $this->select()
                    ->from($_name)
					->where('slider_id = ?', $slider_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["image"];		
        return $main_photo_file_name;
    }
	
	public function getMainPhotoFile($slider_id)
    {
        $_name = 'zend_slider_three';
        $select = $this->select()
                    ->from($_name)
                    ->where('slider_id = ?', $slider_id);

        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec['image'];    
        return $main_photo_file_name;
    }
	
	public  function deleteSliderImage(
                                            $slider_id,  
                                            $image
                                        )
    {
        $data = array(
            'image' => $image,
        );
        
        $this->update($data, 'slider_id = ' . (int)$slider_id);
    }
	
	public function getSliderIdByUrl($slider_url)
	{
	    $_name = 'zend_slider_three';
        $select = $this->select()
                    ->from($_name)
					->where('slider_url = ?', $slider_url);

		$slider_rec = $this->fetchRow($select);	
        $slider_id = $slider_rec["slider_id"];		
        return $slider_id;
    }
   
}
