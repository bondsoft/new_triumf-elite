
function loadScript(url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) { // only required for IE <9
        script.onreadystatechange = function () {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function () {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}


var map_load = false;
window.onscroll = function() {
	
	if( map_load === false ) {
		
		map_load = true;
		
		var filename = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
		loadScript(filename, function () {
			ymaps.ready(init)
		});
		
	}

}



function init() {
        var myMap;
    
        ymaps.ready(init);
        
        function init () {
            myMap = new ymaps.Map('mymap', {
                center: [47.293876, 39.713605],
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            });
        
            myMap.behaviors.disable('scrollZoom');
            myMap.behaviors.disable('drag');
            
            myGeoObject = new ymaps.GeoObject({
                
            });
            
            myMap.geoObjects
            .add(myGeoObject)
            .add(new ymaps.Placemark([47.293541, 39.715076], {
                balloonContent: '<strong>ТРИУМФ-ЭЛИТ</strong> <br/>на Космонавтав, <br/>пр. Космонавтов, 26'
            }, {
                preset: 'islands#blueMedicalIcon'
            }))
            
            
            .add(new ymaps.Placemark([47.294012, 39.711141], {
                balloonContent: '<strong>ТРИУМФ-ЭЛИТ</strong> <br/>на Королева, <br/>пр. Королёва, 18'
            }, {
                preset: 'islands#blueMedicalIcon'
            }))
            
            ;
        
        }
};