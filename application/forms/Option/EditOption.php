<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Option_EditOption extends Zend_Form
{
    public function init()
    {   
        $uploadId = new Zend_Form_Element_Hidden('uploadId');
        $uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
            ->setAttrib('id', 'uploadId')
            ->setAttrib('value', md5(uniqid(rand())));
        
        $option_id = new Zend_Form_Element_Hidden('option_id');
        $option_id->addFilter('Int');
        
        // prepare menu_first_level_id
      //  $validator_typ = new Zend_Validate_Between(array('min' => 1, 'max' => 100));
      //  $validator_typ->setMessage('Не выбран родительский пункт меню');
        
        $options_model = new Default_Model_DbTable_Option();
        $options = $options_model->getAllOptionsAdmin(); 
      
        if (count($options)!=0)
        {
          //  $options_prepared['не указана']= "не указана";
            foreach ($options as $option)
            {
                $options_prepared[$option->option_id]= $option->name;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $typ = new Zend_Form_Element_Select('typ');
            $typ ->setLabel("option:")
                ->addMultiOptions($options_prepared)
                ->setAttrib('class', array('span3'))
				->removeDecorator('label'); 
              //  ->addValidator($validator_typ);
        } 
        
         
    
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'))
					->removeDecorator('label');
        
        
		$name = new Zend_Form_Element_Text('name');
        $name->setLabel("Название:")
            ->setAttrib('class', 'span6')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
			->removeDecorator('label');
			
		
        $image = new Zend_Form_Element_File('image');
        $image->setLabel("Выбрать фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif, jpeg")
            ->setAttrib('size', '50')
        //  ->addValidator('Count', false, array('min' => 1, 'max' => 5))
        //  ->setMultiFile(1)
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif,jpeg')
            ->setDestination('media/photos/products')
            ->setValueDisabled(true)
			->removeDecorator('label');
        //  ->setRequired(true)
        //  ->addValidator('NotEmpty', false); additional  
        
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $uploadId,
                                    $name,
                                    $typ,
                                    $sort_order,
                                    $submit
                                ));
       
    }
}