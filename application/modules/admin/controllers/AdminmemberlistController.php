<?php

class Admin_AdminmemberlistController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');		
    }

    public function indexAction()
    {
		$users_object = new Default_Model_DbTable_Users();
		$users = $users_object->fetchAll();
		$paginator = Zend_Paginator::factory($users);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
		$count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);
    }
	
	public function addusersAction()
    {
        $form = new Default_Form_Auth_Account();
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
           
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
			{
                $user_object = new Default_Model_DbTable_Users($form->getValues());
				$data = array(
                    'username'       => $form->getValue('username'),
                    'password'       => md5($form->getValue('password')),
					'email'          => $form->getValue('email'),
					'user_role'      => $form->getValue('user_role'),
					'last_access'    => date('Y-m-d H:i:s'),
					'creation_date'  => date('Y-m-d H:i:s')
                );
				$user_object->insert($data);
                  
                $this->_helper->redirector('index', 'adminmemberlist', 'admin');
            } else {
                
                $form->populate($formData);
            }
        }
    }
	
	public function editusersAction()
    {
        $form = new Default_Form_Auth_EditAccount();    
        $this->view->form = $form;
        $user_id = $this->getRequest()->getParam('user_id'); //Zend_Debug::dump($user_id);exit;
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
			{
				$user_role = $form->getValue('user_role');
               
                $user_object = new Default_Model_DbTable_Users();
				
                $user_object->updateUser(	$user_id,
											$user_role);
                
                $this->_helper->redirector('index', 'adminmemberlist', 'admin');
            } else {
                $form->populate($formData);
            }
        } else {
           
            $user_id = $this->_getParam('user_id', 0);
            if ($user_id > 0){
                $user_object = new Default_Model_DbTable_Users();   
                $form->populate($user_object->getUser($user_id));
            }
        }
    }
	
	public function deleteAction()
    {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $user_id = $this->getRequest()->getPost('user_id');
                
                $users_object = new Default_Model_DbTable_Users();
                
                $users_object->deleteUser($user_id);		
            }
            
            $this->_helper->redirector('index');
		   
        } else {
            $user_id = $this->_getParam('user_id');
            $users_object = new Default_Model_DbTable_Users();
            $this->view->users = $users_object->getUser($user_id);
        }
    }
    

}

