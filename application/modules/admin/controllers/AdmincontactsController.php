<?php

class Admin_AdmincontactsController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
	
	public function indexAction()
    {
        $contacts_model = new Default_Model_DbTable_Contacts();
		$page_text = $contacts_model ->getContactsText();
		$this->view->page_text = $page_text;
    }
	
    public function editAction()
    {
        $form = new Default_Form_Contacts_Contacts();     
        $this->view->form = $form;
        $contacts_model = new Default_Model_DbTable_Contacts();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
           //  Zend_Debug::dump($formData);exit;
   
            if ($form->isValid($formData)) {
         
                $contacts_id = (int)$form->getValue('contacts_id');
                $text = $form->getValue('text');
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                
                $contacts_model->updateContacts(
												$contacts_id,
												$text,
												$header_tags_title,
												$header_tags_description,
												$header_tags_keywords
											);

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $contacts_id = $this->_getParam('contacts_id', 0);
            if ($contacts_id > 0) {
                $form->populate($contacts_model->getContacts($contacts_id));
            }
        }
    }
}

