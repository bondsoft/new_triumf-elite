<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Productoption extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_product_option';
	
	public function getOption($option_id)
    {
        $option_id = (int)$option_id;
        $row = $this->fetchRow('option_id = ' . $option_id);
        if(!$row) {
            throw new Exception("no record option_id - $option_id");
        }
        return $row->toArray();
    }
	
	public function addProductOption(
										$product_id,
										$option_id                    
									)
    {
        $data = array(
			'product_id' => $product_id,
			'option_id' => $option_id
        );
        $this->insert($data);
    }
	
	public  function addOptionValueImage( 
											$option_id,
											$image 									
										)
    {
        $data = array(
            'image' => $image
        );
        
        $this->update($data, 'option_id = ' . (int)$option_id);
    }
	
	public  function getLastInsertId()
    {
        $_name = 'zend_product_option';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('product_option_id DESC')
                    ->limit(1);
                     
        $LastInsertId_rec = $this->fetchRow($select);	
        $LastInsertId = $LastInsertId_rec["product_option_id"];		
        return $LastInsertId;
    }
	
	public  function updateProductOption( 
											$product_option_id,
											$product_id,
											$option_id  
									 )
    {
        $data = array(
            'product_id' => $product_id,
			'option_id' => $option_id
        );
        
        $this->update($data, 'product_option_id = ' . (int)$product_option_id);
    }
	
	public function deleteOptionValue($option_id)
    {
        $this->delete('option_id = ' . (int)$option_id);
    }
	
	public function deleteOptionValueRow($option_value_id)
    {
        $this->delete('option_value_id = ' . (int)$option_value_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	
    
    public function getAllOptionsAdmin()
    {
        $_name = 'zend_option';
		$_name_01 = 'zend_option_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id', array('name'))
                    ->order('type');
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }
	
	public function getAllPhotosForOption($option_id)
	{
        $_name = 'zend_option_value';
        $select = $this->select()
                    ->from($_name)
                    ->where('option_id = ?', $option_id);
     
		$photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function getOptionItemAdmin($option_id)
    {  
        $_name = 'zend_option_value';
		$_name_01 = 'zend_option_value_description';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.option_id = ?', $option_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.option_value_id = name_01.option_value_id');
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }

	public  function deleteProductOption(
                                            $product_id
                                        )
    {
		
        $this->delete(array(
            'product_id = ?' => $product_id
        ));
    } 
    

	public  function deleteOptionImage(
                                            $option_value_id,  
                                            $image
                                        )
    {
        $data = array(
            'image' => $image,
        );
        
        $this->update($data, 'option_value_id = ' . (int)$option_value_id);
    }
	
	public function getMainphoto($kiosks_id)
	{
        $_name = 'zend_products';
		$_name_01 = 'category';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('kiosks_id = ?', $kiosks_id)
					->join(array('name_01' => $_name_01),'name.category_id = name_01.category_id');
     
		$pdf = $this->fetchAll($select);
        return $pdf;
    }
	
	public function getOptionImage($option_value_id)
	{
	    $_name = 'zend_option_value';
        $select = $this->select()
                    ->from($_name)
					->where('option_value_id = ?', $option_value_id);

		$photo_file_rec = $this->fetchAll($select);	
        $photo_file_name = $photo_file_rec["image"];		
        return $photo_file_name;
    }
	
	public function getOptionValuesProduct($product_id) {
		$option_value_data = array();

		$_name = 'zend_product_option';
		$_name_01 = 'zend_product_option_value';
		$_name_02 = 'zend_option_value_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
					->joinLeft(array('name_01' => $_name_01),'name.product_option_id = name_01.product_option_id')
					->joinLeft(array('name_02' => $_name_02),'name.option_id = name_02.option_id')
					->group('name_02.option_id')
					->order('name_02.sort_order');

		foreach ($this->fetchAll($select) as $option_value) {
			$option_value_data[] = array(
				'option_value_id' => $option_value['option_value_id'],
				'name'            => $option_value['name']
			);
		}

		return $option_value_data;
	}
 

}
