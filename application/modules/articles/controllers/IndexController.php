<?php
/**
 *
 * Default controller for this application.
 * 
 */
class Articles_IndexController extends Zend_Controller_Action
{  
    public function init()
    {
        /* Initialize action controller here */ 
    }
	
    public function indexAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('news');
		
        $articlesPerPage = 10;
        $articles_model = new Default_Model_DbTable_Articles();
		
		$articles = $articles_model->getAllArticlesFront( $this->getRequest()->getParam('show_our_work') ); //Zend_Debug::dump($articles);exit;
	  
		$paginator = Zend_Paginator::factory($articles);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage($articlesPerPage);
        $articlesArray = $articles->toArray();
        $maxNumberOfArticles = count($articlesArray);
        $maxNumberOfPages = ceil($maxNumberOfArticles / $articlesPerPage);

        if($this->_getParam('page', 1) > $maxNumberOfPages) {
            $this->getResponse()->setHttpResponseCode(404);
            $layout->setLayout('page_not_found_layout');
        }
       
        $articles = new Default_Model_DbTable_Articlesseo();
        $articles_name = $articles->fetchAll();
        
        $page = Zend_Paginator::factory($articles_name);

        $this->view->page = $page;

        $sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
    }
	
	public function articlemenuAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $articles_model = new Default_Model_DbTable_Articles();
        
        $articles = $articles_model->getAllArticlesFront();
		//Zend_Debug::dump($config_telephone);exit;
        
        $this->view->articles = $articles;   
    }
	
	public function recommendationsAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $articles_model = new Default_Model_DbTable_Articles();
        
        $intro_text = $articles_model->getArticleIntroText('rekomendacii-po-ustanovke-karniza'); //Zend_Debug::dump($page);exit;
        $this->view->intro_text = $intro_text; 
    }

	function articleAction()
	{  
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('article_item');
	//	$form = new Default_Form_Reviews_AddReviewFront(); 
	//	$this->view->form = $form;
		
		$articles_model = new Default_Model_DbTable_Articles();
     
        $url_seo = $this->getRequest()->getParam('url_seo'); // Zend_Debug::dump($article_id);exit;
        
        $article_title = $articles_model->getArticleTitle($url_seo); //Zend_Debug::dump($page);exit;
        $this->view->article_title = $article_title;
		
		$page = $articles_model->getArticlePage($url_seo); //Zend_Debug::dump($page);exit;
        $this->view->page = $page;
        $paginator = Zend_Paginator::factory($page);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(1);
        
        $photo_model = new Default_Model_DbTable_Articlesphoto();
        
        $article_id = $articles_model->getArticleIdByUrl($url_seo);
        $this->view->article_id = $article_id;
        $photos = $photo_model->getAllPhotosForObject($article_id); //Zend_Debug::dump($photos);exit;
        
        $this->view->photos = $photos;
	//	$reviews_model = new Default_Model_DbTable_Reviews();
	//	$reviews = $reviews_model->getRatingForIndex($article_id);
	//	Zend_Debug::dump($reviews);exit;
	//	$paginator_reviews = Zend_Paginator::factory($reviews);
     //   $paginator_reviews->setCurrentPageNumber($this->_getParam('page', 1));
    //    $this->view->paginator_reviews = $paginator_reviews;
    //    $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(7); 
		
	/*	foreach ($reviews as $key => $value) {
			$answers[$key] = $reviews_model->getAnswersForIndex($reviews[$key]['review_id']);
		} */
	//	$this->view->answers = @$answers;
		//Zend_Debug::dump($answers[1][0]['text']);exit;
	}	
	
	public function articlesfrontAction()
    {
        $articles_model = new Default_Model_DbTable_Articles();
		
		$articles = $articles_model->getAllArticlesRanrom(); //Zend_Debug::dump($articles);exit;
	  
        $this->view->articles = $articles;
        
    }
	
	function sendreviewAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        
        $form = new Default_Form_Reviews_AddReviewFront(); 
        $form->isValid($this->_getAllParams());
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();//Zend_Debug::dump($userdata);exit;
	    @$user_id = $userdata->user_id; 
        @$realname = $userdata->realname;
		@$surname = $userdata->surname;
		@$user_name = $realname.' '.$surname;
        if ($form->isValid($request->getPost())) {
                 
				$parent_id = @$_POST['rev_id']; //Zend_Debug::dump($rev_id);exit;
				$article_id = $_POST['article_id'];
				$user_id       = $user_id; 
                $author       = $user_name; 
                $text      = $form->getValue('text');
				$status = 1;
           
                $reviews_model = new Default_Model_DbTable_Reviews();
                if($parent_id) {
					$reviews_model->addAnswer(
												$article_id,
												$parent_id,
												$user_id,
												$author,
												$text,
												$status
											);
				
				} else {
					$reviews_model->addReview(
												$article_id,
												$user_id,
												$author,
												$text,
												$status
											);
				}
										
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
     //   echo Zend_Json::encode($json);
     //   exit;
       
        echo Zend_Json::encode($json);
        exit;
    
    }
}
