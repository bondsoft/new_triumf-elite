<?php
/**
 * @uses       Zend_Form
 * @package    
 * @subpackage Form
 */
class Default_Form_News_MainPhoto extends Zend_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init()
    {	
	    $this
          ->setDescription('Choose photos on your computer to add to this album.')
          ->setAttrib('id', 'form-upload')
		  ->setAttrib('onsubmit', 'download()')
          ->setAttrib('name', 'photo_create')
          ->setAttrib('enctype','multipart/form-data')
          ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));
		  
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		
		$uploadId = new Zend_Form_Element_Hidden('uploadId');
		$uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
		    ->setAttrib('id', 'uploadId')
		    ->setAttrib('value', md5(uniqid(rand())));
			
		$news_id = new Zend_Form_Element_Hidden('news_id');
        $news_id->addFilter('Int');
		
        $main_photo = new Zend_Form_Element_File('main_photo');
		$main_photo->setLabel("Выбрать фото:")
		    ->setDescription("Допустимые форматы: jpg, png, gif")
		    ->setAttrib('size', '50')
		//	->addValidator('Count', false, array('min' => 1, 'max' => 5))
		//	->setMultiFile(1)
			->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
			->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/news/main_photos')
			->setValueDisabled(true)
			->setRequired(true)
			->addValidator('NotEmpty', false);	
			
		$this->addElements(array($uploadId,
								 $news_id, 
		                         $main_photo)
					       );	
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'class' => 'btn btn-primary',
          'type' => 'submit',
          'ignore' => true,
        ));	
			       
    }
}

