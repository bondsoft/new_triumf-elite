<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Towns_Towns extends Zend_Form
{
    public function init()
    {	
		$town_id = new Zend_Form_Element_Hidden('town_id');
        $town_id->addFilter('Int');
        
		$town_name = new Zend_Form_Element_Text('town_name');
        $town_name->setLabel("Город")
		    ->setAttrib('size', '120')
            ->setRequired(true)
			->setAttrib('class', 'span8')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true);
       
        $this->addElements(array(
                                    $town_id,
                                    $town_name
                                ));
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'type' => 'submit',
		  'class' => 'btn btn-primary pull-left',
          'ignore' => true,
        ));	 
    }
}

