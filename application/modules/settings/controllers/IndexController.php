<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 * 
 * 
 *  
 */
class Settings_IndexController extends Zend_Controller_Action
{  
    public function init()
    {
        /* Initialize action controller here */
       
    }
    
    public function indexAction()
    {
        
    } 
	
    public function configtelephone01Action()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_telephone_01 = $settings_object->getConfigTelephone_01();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_telephone_01 = $config_telephone_01;   
    }
    
    public function configtelephone02Action()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_telephone_02 = $settings_object->getConfigTelephone_02();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_telephone_02 = $config_telephone_02;   
    }
    
    public function configtelephone03Action()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_telephone_03 = $settings_object->getConfigTelephone_03();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_telephone_03 = $config_telephone_03;   
    }
	
	public function configtelephone04Action()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_telephone_04 = $settings_object->getConfigTelephone_04();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_telephone_04 = $config_telephone_04;   
    }

    public function configemailAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_email = $settings_object->getConfigEmail();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_email = $config_email;   
    }
    
    public function configaddressAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_address = $settings_object->getConfigAddress();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_address = $config_address;   
    }
	
	public function configmapAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_map = $settings_object->getConfigMap();  
	//	Zend_Debug::dump($config_yandex_metrika);exit;
        
        $this->view->config_map = $config_map;   
    }
	
	public function configyandexmetrikaAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_yandex_metrika = $settings_object->getConfigYandexMetrika();    
	//	Zend_Debug::dump($config_yandex_metrika['setting_value']);exit;
        
        $this->view->config_yandex_metrika = $config_yandex_metrika; 
	//	$this->view->config_yandex_metrika = $config_yandex_metrika['setting_value']; 
    }
	
	public function instagramAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_instagram = $settings_object->getConfigInstagram();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_instagram = $config_instagram;   
    }
	
	public function vkAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_vk = $settings_object->getConfigVk();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_vk = $config_vk;   
    }
	
	public function facebookAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_facebook = $settings_object->getConfigFacebook();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_facebook = $config_facebook;   
    }
	
	public function youtubeAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_youtube = $settings_object->getConfigYoutube();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_youtube = $config_youtube;   
    }
	
	public function okAction()
    {
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $settings_object = new Default_Model_DbTable_Settings();
        
        $config_ok = $settings_object->getConfigOk();  //Zend_Debug::dump($config_telephone);exit;
        
        $this->view->config_ok = $config_ok;   
    }
	
}
