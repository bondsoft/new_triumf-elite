<?php

/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminnewsphotoController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
        
//Zend_Debug::dump($this->view->news_main_photo_file);exit;
    }
	
    function indexAction()
    {
        $newsphoto_model = new Default_Model_DbTable_Newsphotoforpage();
        $news_id = $this->getRequest()->getParam('news_id');
		$photos = $newsphoto_model->getPhoto_galary($news_id);
		//Zend_Debug::dump($photos);exit;
		
		$paginator = Zend_Paginator::factory($photos);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator; 
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);	

        $news_id = $this->getRequest()->getParam('news_id');
        $news_model = new Default_Model_DbTable_News();
        $news_main_photo_file = $news_model->getMainphoto_file($news_id);
        $this->view->news_main_photo_file = $news_main_photo_file; 
		$news_object = $news_model->getNews($news_id);
		$this->view->news_object = $news_object;
	}
   
    public function addphotoAction()
    {
	    include( 'SimpleImage.php' );
		
        $form = new Default_Form_News_Photo();
        $this->view->form = $form;
        
        $news_model = new Default_Model_DbTable_News();
        $news_id = $this->getRequest()->getParam('news_id');
        $news_object = $news_model->getNews($news_id);
        $this->view->news_object = $news_object;
		
		$values = $form->getValues();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
			{
				$news_id = $this->getRequest()->getParam('news_id');
                $photo_file = $form->getValue('photo_file'); 
				
				if( empty($photo_file) )
                {
	                echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
					echo "Вы не выбрали файл!";
	                echo "</font></b>" ;
                    return;
                }
			
                $photo_model = new Default_Model_DbTable_Newsphotoforpage();
				
				$data = $_FILES['photo_file'];
				
				foreach ($data['type'] as $key => $value) // Zend_Debug::dump($value);exit;
                {	
                    if(	$value != NULL )
                    {				
                        $uploadedfile = $data['tmp_name'][$key];   // Zend_Debug::dump($uploadedfile);exit;
                        
                        if(file_exists($uploadedfile))
                        {
                        /* ********************************************** */
                            $ext = explode(".",$data["name"][$key]); 
                            $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                         //   Zend_Debug::dump($newname);exit;
                            $name_big = "media/photos/news/pages/big_".$newname;
                            $name_small = "media/photos/news/pages/small_".$newname;
                            
                        /* ********************************************** */
                            
                            $image_big = new SimpleImage();
                            $image_big->load($uploadedfile);
                        //    $image_big->resizeToWidth(600);
                            $image_big->save($name_big);
                            
                            $image_small = new SimpleImage();
                            $image_small->load($uploadedfile);
                            $image_small->cutFromCenter(322, 250);
                            $image_small->save($name_small); 
                            
                        } 
                        
                        $photo_model->addPhoto(
                                                    $news_id, 
                                                    $newname
                                                );
                    }
				}
				// Уничтожаем файл во временном каталоге
                   unlink($_FILES['photo_file']['tmp_name']);
               
                  $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id));
            } else {
                // Если форма заполнена неверно,
                $this->view->errMessage = "ффффффффффффффф" ;
            }
        }
    }
	
	public function editphotoAction()
    {
	    include( 'SimpleImage.php' );
        $form = new Default_Form_News_EditPhotoFile();    
        $this->view->form = $form;
        
        $news_model = new Default_Model_DbTable_News();
        $news_id = $this->getRequest()->getParam('news_id');
        $news_object = $news_model->getNews($news_id);
        $this->view->news_object = $news_object;
		
		$news_id = $this->getRequest()->getParam('news_id');//Zend_Debug::dump($realestate_id);exit;
		$photo_id = $this->getRequest()->getParam('photo_id');// Zend_Debug::dump($photo_id);exit;
		
		$photo_model = new Default_Model_DbTable_Newsphotoforpage();
				
		$this->view->photo = $photo_model->getPhoto($photo_id); //Zend_Debug::dump($this->view->photo);exit;
		$old_photo_file = $photo_model->getPhoto_file($photo_id);//Zend_Debug::dump($old_photo_file);exit;
		  
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();
		   
		   	if ($form->isValid($formData) && $form->submit->isChecked())
			{
			    $photo_file = $form->getValue('photo_file');
				
			    $directory = "media/photos/news/pages";
		    	
            
				if( empty($photo_file) )
                {
					$this->view->errMessage = "Вы не выбрали файл!";
                    return;
                }
                
                // удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
				
				$data = $_FILES['photo_file'];
				$uploadedfile = $data['tmp_name']; //Zend_Debug::dump($uploadedfile);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name_big = "media/photos/news/pages/big_".$newname;
                    $name_small = "media/photos/news/pages/small_".$newname;
					
				   /* ********************************************** */
					
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //    $image_big->resizeToWidth(180);
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->cutFromCenter(322, 250);
                    $image_small->save($name_small); 
				} 
				
                $photo_model->updatePhoto_fileForPage(
                                                        $photo_id,
                                                        $newname
                                                    );
				
				// Уничтожаем файл во временном каталоге
                 unlink($_FILES['photo_file']['tmp_name']);
                
                $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id, 'photo_id' => $photo_id));
            }  else {
                $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id, 'photo_id' => $photo_id));
            }
        } else {
            // Если мы выводим форму, то получаем id записи, которую хотим обновить
            $photo_id = $this->_getParam('photo_id', 0);
            if ($photo_id > 0) {
                
				$this->view->photo = $photo_model->getPhoto($photo_id);
                
                $form->populate($photo_model->getPhoto($photo_id));
            }
        }
    }
	
	public function deleteAction()
    {
        $photo_model = new Default_Model_DbTable_Newsphotoforpage();
        if ($this->getRequest()->isPost()) {

            $del = $this->getRequest()->getPost('del'); // Zend_Debug::dump($del);exit;

                $news_id = $this->getRequest()->getParam('news_id');
				$photo_id = $this->getRequest()->getParam('photo_id');
            
            // Если пользователь поддтвердил своё желание удалить запись
            if ($del == 'yes') {
                $old_photo_file = $photo_model->getPhoto_file($photo_id);//Zend_Debug::dump($old_photo_file);exit;
              
                
				$photo_file = $photo_model->getPhoto_file($photo_id);
				
			//	Zend_Debug::dump($photo_file);exit;
			
				$directory = "media/photos/news/pages";
				// удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
                
              //  Zend_Debug::dump($photo_file);exit;
               
                // Вызываем метод модели для удаления записи
                $photo_model->deletePhotoForPage($photo_id);
            }
            $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id));
        } else {
            // Если запрос не Post, выводим сообщение для поддтвержения
            // Получаем id записи, которую хотим удалить
            $photo_id = $this->_getParam('photo_id');
           
            $this->view->photo = $photo_model->getPhoto($photo_id);
        }
    }
	
	public function addmainphotoAction()
    {
		include( 'SimpleImage.php' );
		$news_model = new Default_Model_DbTable_News();
		$news_id = $this->getRequest()->getParam('news_id');
		$news_object = $news_model->getNews($news_id);
		$this->view->news_object = $news_object;

		$mainphoto_form = new Default_Form_News_MainPhoto(); 
        $this->view->form = $mainphoto_form;
		
	//	Zend_Debug::dump($news_id);exit;
        if ($this->getRequest()->isPost()) {  
            $formData = $this->getRequest()->getPost();  
            if ($mainphoto_form->isValid($formData)) 
			{
				$main_photo = $mainphoto_form->getValue('main_photo');
			//	Zend_Debug::dump($main_photo);exit;
				if( empty($main_photo) )
                {
	                echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
					echo "Вы не выбрали файл!";
	                echo "</font></b>" ;
                    return;
                }
				
				$data = $_FILES['main_photo']; // Zend_Debug::dump($data);exit;
				$uploadedfile = $data['tmp_name']; 
             			//	Zend_Debug::dump($uploadedfile);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
				    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name_big = "media/photos/news/main_photos/big_".$newname;
					$name_small = "media/photos/news/main_photos/small_".$newname;
					
				   /* ********************************************** */
				    $image_big = new SimpleImage();
				    $image_big->load($uploadedfile);
				//	$image_big->resizeToWidth(180);
				    $image_big->save($name_big);
					
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->cutFromCenter(322, 250);
				    $image_small->save($name_small);  
				}  
                
                $news_model->updateMainphoto(
												$news_id, 
												$newname
											);
				
				unlink($_FILES['main_photo']['tmp_name']);
                
                $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id));
            } else {
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editmainphotoAction()
    {
		include( 'SimpleImage.php' );
        $form = new Default_Form_News_EditMainPhotoFile();    
        $this->view->form = $form;
        
        $news_id = $this->getRequest()->getParam('news_id');
        $news_model = new Default_Model_DbTable_News();
        $news_main_photo_file = $news_model->getMainphoto_file($news_id);
        $this->view->news_main_photo_file = $news_main_photo_file; 
		$news_object = $news_model->getNews($news_id);
		$this->view->news_object = $news_object;
               
        $old_photo_file = $news_model->getMainphoto_file($news_id); //Zend_Debug::dump($old_photo_file);exit;
          
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
           
            if ($form->isValid($formData) && $form->submit->isChecked())
            {
                $main_photo = $form->getValue('main_photo');
                
                $directory = "media/photos/news/main_photos";
               
            
                if( empty($main_photo) )
                {
                    $this->view->errMessage = "Вы не выбрали файл!";
                    return;
                }
                // удаляем файл из каталога
                    if(is_file("$directory/small_$old_photo_file"))
                    {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                    }
                
                $data = $_FILES['main_photo'];  //Zend_Debug::dump($data);exit;
                $uploadedfile = $data['tmp_name']; //Zend_Debug::dump($uploadedfile_mainphoto);exit;
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name_big = "media/photos/news/main_photos/big_".$newname;
					$name_small = "media/photos/news/main_photos/small_".$newname;
					
				   /* ********************************************** */
				    $image_big = new SimpleImage();
				    $image_big->load($uploadedfile);
				//	$image_big->resizeToWidth(180);
				    $image_big->save($name_big);
					
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->cutFromCenter(322, 250);
				    $image_small->save($name_small); 
                } 
                
                $news_model->updateMainphoto(
												$news_id, 
												$newname
											);
                
                // Уничтожаем файл во временном каталоге
                 unlink($_FILES['main_photo']['tmp_name']);
                
                $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id));
            }  else {
                $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id));
            }
        } else {
            // Если мы выводим форму, то получаем id записи, которую хотим обновить
            $news_id = $this->_getParam('news_id', 0);
            if ($news_id > 0) { 
                $this->view->news = $news_model->getNews($news_id);
                
                $form->populate($news_model->getNews($news_id));
            }
        }
    }
	
	public function deletemainphotoAction()
    {
		$news_model = new Default_Model_DbTable_News();
		$news_id = $this->getRequest()->getParam('news_id');
		$news_object = $news_model->getNews($news_id);
		$this->view->news_object = $news_object;
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del'); // Zend_Debug::dump($del);exit;
            $news_id = $this->getRequest()->getParam('news_id');
            
            // Если пользователь поддтвердил своё желание удалить запись
            if ($del == 'yes') { 
                // получаем имя удаляемого файла
                $old_photo_file = $news_model->getMainphoto_file($news_id);//Zend_Debug::dump($old_photo_file);exit;
                $main_photo == NULL;
                
            //  Zend_Debug::dump($old_photo_file);exit;
            
                $directory = "media/photos/news/main_photos";
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                } 
                
              //  Zend_Debug::dump($old_photo_file);exit;
               
                $news_model->updateMainphoto(
												$news_id, 
												$newname
											);
            }
            $this->_helper->redirector('index', 'adminnewsphoto', 'admin', array('news_id' => $news_id));
        } else {
            $news_id = $this->_getParam('news_id');
            
            $this->view->news_object = $news_model->getNews($news_id); 
			//Zend_Debug::dump( $this->view->main_photo);exit;
        }
    }
    

   
}