/* =========================================================
Comment Form
============================================================ */

jQuery(document).ready(function($){
    $('.submit_formdata').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/contact/index/validateform',
            data: $(this).parents('form').serializeArray(),
            success: function(data){
                if (data['name']){
                    $.each(data['name'], function(index, value){
                        $('input[name=name]').css('border', 'red 2px solid')
                    });
                } else 
                $('input[name=name]').css('border', 'grey 2px solid')
                
                if (data['email']){
                    $.each(data['email'], function(index, value){
                        $('input[name=email]').css('border', 'red 2px solid')
                    });
                } else 
                $('input[name=email]').css('border', 'grey 2px solid')
                 
                if (data['phone']){
                    $.each(data['phone'], function(index, value){
                        $('input[name=phone]').css('border', 'red 2px solid')
                    });
                } else 
                $('input[name=phone]').css('border', 'grey 2px solid')
               
                
                
                if(data['success'] == true){
                 //   $('#response').html("Ваше сообщение отправлено.");
                 //   setTimeout(function(){$('#response').html("")}, 3000);
    jQuery.fancybox('<h2 class="title">Спасибо!</h2><div class="callback-text text">Ваша заявка отправлена.<br>В ближайшее время с вами свяжется<br>менеджер для уточнения деталей.');
               
                }
                
            }
        })
    });
    
    $('.submit_order_formdata').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/contact/index/ordervalidateform',
            data: $(this).parents('form').serializeArray(),
            success: function(data){
                if (data['order_name']){
                    $.each(data['order_name'], function(index, value){
                        $('input[name=order_name]').css('border', 'red 2px solid')
                    });
                } else 
                $('input[name=order_name]').css('border', 'grey 2px solid')
                 
                if (data['order_phone']){
                    $.each(data['order_phone'], function(index, value){
                        $('input[name=order_phone]').css('border', 'red 2px solid')
                    });
                } else 
                $('input[name=order_phone]').css('border', 'grey 2px solid')
               
                if(data['success'] == true){
                 //   $('#response').html("Ваше сообщение отправлено.");
                 //   setTimeout(function(){$('#response').html("")}, 3000);
    jQuery.fancybox('<h2 class="title">Спасибо!</h2><div class="callback-text text">Ваша заявка отправлена.<br>В ближайшее время с вами свяжется<br>менеджер для уточнения деталей.');
               
                }
                
            }
        })
    });
   
   /* news begin */
   
    $('.status_news_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminnews/statuson',
            data: 'news_id=' + $(this).attr('data-status'),
            dataType: 'html',
        })
        $("#response" + $(this).attr('data-status')).html('<p>Включено</p>');
    });
    
    $('.status_news_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminnews/statusoff',
            data: 'news_id=' + $(this).attr('data-status'),
            dataType: 'html',
        })
        $('#response' + $(this).attr('data-status')).html('<p style="color:red;">Отключено</p>');
    });
	
	$('.remove_news_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminnews/deleteimage',
            data: 'news_id=' + $(this).attr('data-status-news-image'),
            dataType: 'html',
        })
        $('#response_news_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
	
	$('.news_prime_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminnews/primeon',
            data: 'news_id=' + $(this).attr('data-status-news-prime'),
            dataType: 'html',
        })
        $("#response_news_prime" + $(this).attr('data-status-news-prime')).html('<p>Включено</p>');
    });
    
    $('.news_prime_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminnews/primeoff',
            data: 'news_id=' + $(this).attr('data-status-news-prime'),
            dataType: 'html',
        })
        $('#response_news_prime' + $(this).attr('data-status-news-prime')).html('<p style="color:red;">Отключено</p>');
    });
	
	/* news end */
	/* article begin */
   
    $('.status_article_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminarticles/statuson',
            data: 'article_id=' + $(this).attr('data-status-article'),
            dataType: 'html',
        })
        $("#response_article" + $(this).attr('data-status-article')).html('<p>Включено</p>');
    });
    
    $('.status_article_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminarticles/statusoff',
            data: 'article_id=' + $(this).attr('data-status-article'),
            dataType: 'html',
        })
        $('#response_article' + $(this).attr('data-status-article')).html('<p style="color:red;">Отключено</p>');
    });
	
	$('.remove_article_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminarticles/deleteimage',
            data: 'article_id=' + $(this).attr('data-status-article-image'),
            dataType: 'html',
        })
        $('#response_article_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
	
	$('.article_prime_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminarticles/primeon',
            data: 'article_id=' + $(this).attr('data-status-article-prime'),
            dataType: 'html',
        })
        $("#response_article_prime" + $(this).attr('data-status-article-prime')).html('<p>Включено</p>');
    });
    
    $('.article_prime_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminarticles/primeoff',
            data: 'article_id=' + $(this).attr('data-status-article-prime'),
            dataType: 'html',
        })
        $('#response_article_prime' + $(this).attr('data-status-article-prime')).html('<p style="color:red;">Отключено</p>');
    });
	
	/* article end */
    
    $('.product_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminproducts/statuson',
            data: 'product_id=' + $(this).attr('data-status-product'),
            dataType: 'html',
        })
        $("#response_product" + $(this).attr('data-status-product')).html('<p>Включено</p>');
    });
    
    $('.product_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminproducts/statusoff',
            data: 'product_id=' + $(this).attr('data-status-product'),
            dataType: 'html',
        })
        $('#response_product' + $(this).attr('data-status-product')).html('<p style="color:red;">Отключено</p>');
    });
	
    $('.remove_manufacturer_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminmanufacturers/deleteimage',
            data: 'manufacturer_id=' + $(this).attr('data-status-image'),
            dataType: 'html',
        })
        $('#response_manufacturer_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
    
    $('.remove_category_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/admincategories/deleteimage',
            data: 'category_id=' + $(this).attr('data-status-category-image'),
            dataType: 'html',
        })
        $('#response_category_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
    
    $('.remove_product_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminproducts/deleteimage',
            data: 'product_id=' + $(this).attr('data-status-product-image'),
            dataType: 'html',
        })
        $('#response_product_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
	
	$('.remove_option_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
		var option_row;
        $.ajax({
            type: 'POST',
            url: '/admin/adminoptions/deleteimage',
            data: 'option_value_id=' + $(this).attr('data-status-option-image'),
            dataType: 'html',
        })
        $('#response_option_image').html('<td><img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" ></td><td><input type="file" name="option_value[' + option_row + '][image]" /></td>');
    });
	
	$('.option_value_del').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminoptions/deleteoptionajax',
            data: {'option_value_id' : $(this).attr('data-status-option-del'),
                    'option_row' : $(this).attr('data-status-option-row')
            },
            dataType: 'html',
        });
            $('#option-row'+$(this).attr('data-status-option-row')).remove();
    });
	
	$('#option-add').change(function(event){ //alert('qqqqqqqqqq');
        var option_id = $(this).find('option:selected').attr('value');
	//	alert(option_id);
		if (option_id == '0') {
            $('#option_value_id0').html('<option>- выберите значение -</option>');
            $('#option_value_id0').attr('disabled', true);
			$('#option_value_id1').html('<option>- выберите значение -</option>');
            $('#option_value_id1').attr('disabled', true);
			$('#option_value_id2').html('<option>- выберите значение -</option>');
            $('#option_value_id2').attr('disabled', true);
			$('#option_value_id3').html('<option>- выберите значение -</option>');
            $('#option_value_id3').attr('disabled', true);
			$('#option_value_id4').html('<option>- выберите значение -</option>');
            $('#option_value_id4').attr('disabled', true);
			$('#option_value_id5').html('<option>- выберите значение -</option>');
            $('#option_value_id5').attr('disabled', true);
			$('#option_value_id6').html('<option>- выберите значение -</option>');
            $('#option_value_id6').attr('disabled', true);
			$('#option_value_id7').html('<option>- выберите значение -</option>');
            $('#option_value_id7').attr('disabled', true);
			$('#option_value_id8').html('<option>- выберите значение -</option>');
            $('#option_value_id8').attr('disabled', true);
			$('#option_value_id9').html('<option>- выберите значение -</option>');
            $('#option_value_id9').attr('disabled', true);
			$('#option_value_id10').html('<option>- выберите значение -</option>');
            $('#option_value_id10').attr('disabled', true);
			$('#option_value_id11').html('<option>- выберите значение -</option>');
            $('#option_value_id11').attr('disabled', true);
			$('#option_value_id12').html('<option>- выберите значение -</option>');
            $('#option_value_id12').attr('disabled', true);
			$('#option_value_id13').html('<option>- выберите значение -</option>');
            $('#option_value_id13').attr('disabled', true);
			$('#option_value_id14').html('<option>- выберите значение -</option>');
            $('#option_value_id14').attr('disabled', true);
            return(false);
		}
		$('#option_value_id0').attr('disabled', true);
        $('#option_value_id0').html('<option>загрузка...</option>');
		$('#option_value_id1').attr('disabled', true);
        $('#option_value_id1').html('<option>загрузка...</option>');
		$('#option_value_id2').attr('disabled', true);
        $('#option_value_id2').html('<option>загрузка...</option>');
		$('#option_value_id3').attr('disabled', true);
        $('#option_value_id3').html('<option>загрузка...</option>');
		$('#option_value_id4').attr('disabled', true);
        $('#option_value_id4').html('<option>загрузка...</option>');
		$('#option_value_id5').attr('disabled', true);
        $('#option_value_id5').html('<option>загрузка...</option>');
		$('#option_value_id6').attr('disabled', true);
        $('#option_value_id6').html('<option>загрузка...</option>');
		$('#option_value_id7').attr('disabled', true);
        $('#option_value_id7').html('<option>загрузка...</option>');
		$('#option_value_id8').attr('disabled', true);
        $('#option_value_id8').html('<option>загрузка...</option>');
		$('#option_value_id9').attr('disabled', true);
        $('#option_value_id9').html('<option>загрузка...</option>');
		$('#option_value_id10').attr('disabled', true);
        $('#option_value_id10').html('<option>загрузка...</option>');
		$('#option_value_id11').attr('disabled', true);
        $('#option_value_id11').html('<option>загрузка...</option>');
		$('#option_value_id12').attr('disabled', true);
        $('#option_value_id12').html('<option>загрузка...</option>');
		$('#option_value_id13').attr('disabled', true);
        $('#option_value_id13').html('<option>загрузка...</option>');
		$('#option_value_id14').attr('disabled', true);
        $('#option_value_id14').html('<option>загрузка...</option>');


        $.ajax({
            type: 'POST',
            url: '/admin/adminoptions/options',
            data: "option_id=" + option_id,
            dataType: 'json',
        
			success: function(result){
	//	alert(json.option_id);
                    var options = '';
					var i = 0;
                    $(result.option_value_id).each(function() { 

                        options += '<option value="' + result['option_value_id'][i] + '">' + result['name'][i] + '</option>';
						i++;
                    });
 
                    $('#option_value_id0').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id1').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id2').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id3').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id4').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id5').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id6').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id7').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id8').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id9').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id10').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id11').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id12').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id13').html('<option value="0">- выберите значение -</option>'+options);
					$('#option_value_id14').html('<option value="0">- выберите значение -</option>'+options);
                    $('#option_value_id0').attr('disabled', false); 
					$('#option_value_id1').attr('disabled', false);
					$('#option_value_id2').attr('disabled', false);
					$('#option_value_id3').attr('disabled', false);
					$('#option_value_id4').attr('disabled', false);	
					$('#option_value_id5').attr('disabled', false);
					$('#option_value_id6').attr('disabled', false);
					$('#option_value_id7').attr('disabled', false);
					$('#option_value_id8').attr('disabled', false);
					$('#option_value_id9').attr('disabled', false);
					$('#option_value_id10').attr('disabled', false);
					$('#option_value_id11').attr('disabled', false);
					$('#option_value_id12').attr('disabled', false);
					$('#option_value_id13').attr('disabled', false);
					$('#option_value_id14').attr('disabled', false);
			}
            
		})

    });
	
/*	$('#add-option').change(function(event){ //alert('qqqqqqqqqq');
		
        var option_id = document.getElementById("query-admin-options").value;
		//alert(option_id);
		
        $.ajax({
            type: 'POST',
            url: '/admin/adminoptions/options',
            data: "option_id=" + option_id,
            dataType: 'json',
        
			success: function(result){
	
                    var options = '';
					var i = 0;
                    $(result.option_value_id).each(function() { 

                        options += '<option value="' + result['option_value_id'][i] + '">' + result['name'][i] + '</option>';
						i++;
                    });
 
                    
			}
            
		})

    }); */
    
    $('.product_attribute_del').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminproducts/deleteattributeajax',
            data: {'product_id' : $(this).attr('data-status-product-del'),
                    'attribute_id' : $(this).attr('data-status-attribute-del'),
                    'attribute_row' : $(this).attr('data-status-attribute-row')
            },
            dataType: 'html',
        });
            $('#attribute-row'+$(this).attr('data-status-attribute-row')).remove();
    });
	
	$('.product_order_del').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminorders/deleteorderproductajax',
            data: {'order_product_id' : $(this).attr('data-order-product-id-del'),
                    'order_option_id' : $(this).attr('data-order-option-id-del'),
					'order_id' : $(this).attr('data-order-id-del'),
                    'product_row' : $(this).attr('data-product-row')
            },
			dataType: 'json',
			success: function(json){
				$('#cart-total-admin').html(json['sum']);
			}
        });
        $('#product-row'+$(this).attr('data-product-row')).remove();    
    });
	
	$('.product_order_add').click(function(event){ 
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminorders/addorderproductajax',
            data: $(this).parents('form').serializeArray(),
			dataType: 'json',
			success: function(json){
				$('#cart-total-admin').html(json['sum']);
			}
        });
      //  $('#product-row'+$(this).attr('data-status-product-row')).remove();    
    });
	
	$('.category_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/admincategories/statuson',
            data: 'category_id=' + $(this).attr('data-status-category'),
            dataType: 'html',
        })
        $("#response_category" + $(this).attr('data-status-category')).html('<p>Включено</p>');
    });
    
    $('.category_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/admincategories/statusoff',
            data: 'category_id=' + $(this).attr('data-status-category'),
            dataType: 'html',
        })
        $('#response_category' + $(this).attr('data-status-category')).html('<p style="color:red;">Отключено</p>');
    });
	
	/* sliderone begin */
	
	$('.sliderone_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminsliderone/statuson',
            data: 'slider_id=' + $(this).attr('data-status-sliderone'),
            dataType: 'html',
        })
        $("#response_sliderone" + $(this).attr('data-status-sliderone')).html('<p>Включено</p>');
    });
    
    $('.sliderone_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminsliderone/statusoff',
            data: 'slider_id=' + $(this).attr('data-status-sliderone'),
            dataType: 'html',
        })
        $('#response_sliderone' + $(this).attr('data-status-sliderone')).html('<p style="color:red;">Отключено</p>');
    });
	
	$('.remove_sliderone_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/adminsliderone/deleteimage',
            data: 'slider_id=' + $(this).attr('data-status-sliderone-image'),
            dataType: 'html',
        })
        $('#response_sliderone_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
	
	/* sliderone end */
	/* sliderthree begin */
	
	$('.sliderthree_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/sliderthree/statuson',
            data: 'slider_id=' + $(this).attr('data-status-sliderthree'),
            dataType: 'html',
        })
        $("#response_sliderthree" + $(this).attr('data-status-sliderthree')).html('<p>Включено</p>');
    });
    
    $('.sliderthree_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/sliderthree/statusoff',
            data: 'slider_id=' + $(this).attr('data-status-sliderthree'),
            dataType: 'html',
        })
        $('#response_sliderthree' + $(this).attr('data-status-sliderthree')).html('<p style="color:red;">Отключено</p>');
    });
	
	$('.remove_sliderthree_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/sliderthree/deleteimage',
            data: 'slider_id=' + $(this).attr('data-status-sliderthree-image'),
            dataType: 'html',
        })
        $('#response_sliderthree_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
	
	/* sliderthree end */
	/* gallery begin*/
	$('.gallery_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/gallery/statuson',
            data: 'gallery_id=' + $(this).attr('data-status-gallery'),
            dataType: 'html',
        })
        $("#response_gallery" + $(this).attr('data-status-gallery')).html('<p>Включено</p>');
    });
    
    $('.gallery_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/gallery/statusoff',
            data: 'gallery_id=' + $(this).attr('data-status-gallery'),
            dataType: 'html',
        })
        $('#response_gallery' + $(this).attr('data-status-gallery')).html('<p style="color:red;">Отключено</p>');
    });
	
	$('.photo_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/gallery/photostatuson',
            data: 'photo_id=' + $(this).attr('data-status-photo'),
            dataType: 'html',
        })
        $("#response_gallery_photo" + $(this).attr('data-status-photo')).html('<p>Включено</p>');
    });
    
    $('.photo_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/gallery/photostatusoff',
            data: 'photo_id=' + $(this).attr('data-status-photo'),
            dataType: 'html',
        })
        $('#response_gallery_photo' + $(this).attr('data-status-photo')).html('<p style="color:red;">Отключено</p>');
    });
	
	$('.remove_gallery_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/gallery/deleteimage',
            data: 'photo_id=' + $(this).attr('data-status-gallery-image'),
            dataType: 'html',
        })
        $('#response_gallery_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
	/* gallery end*/
	
	/* videolessons begin */
	
	$('.videolessons_on').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/videolessons/statuson',
            data: 'video_id=' + $(this).attr('data-status-videolessons'),
            dataType: 'html',
        })
        $("#response_videolessons" + $(this).attr('data-status-videolessons')).html('<p>Включено</p>');
    });
    
    $('.videolessons_off').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/videolessons/statusoff',
            data: 'video_id=' + $(this).attr('data-status-videolessons'),
            dataType: 'html',
        })
        $('#response_videolessons' + $(this).attr('data-status-videolessons')).html('<p style="color:red;">Отключено</p>');
    });
	
	$('.remove_videolessons_image').click(function(event){
        $('.error').remove();
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/admin/videolessons/deleteimage',
            data: 'video_id=' + $(this).attr('data-status-videolessons-image'),
            dataType: 'html',
        })
        $('#response_videolessons_image').html('<img src="/media/photos/no_photo.jpeg" border = "0" height = "100" WIDTH = "140" >');
    });
	
	/* videolessons end */
	
 //   $('.submit_callback_formdata').click(function(event){
    $('body').on('click', '.submit_callback_formdata', function(event){     
        event.preventDefault();
        $('.error').remove();
        var form = $(this).parents('form');
        $.ajax({
            type: 'POST',
            url: '/contact/index/callbackvalidateform',
            data: $(this).parents('form').serializeArray(),
            success: function(data){
                if (data['callback_name']){
                    $.each(data['callback_name'], function(index, value){
                        $('input[name=callback_name]').css('border', 'red 2px solid')
                    });
                } else 
                $('input[name=callback_name]').css('border', 'grey 2px solid')
                 
                if (data['callback_phone']){
                    $.each(data['callback_phone'], function(index, value){
                        $('input[name=callback_phone]').css('border', 'red 2px solid')
                    });
                } else 
                $('input[name=callback_phone]').css('border', 'grey 2px solid')
                
                if(data['success'] == true){
                    $('#response').html("Ваше сообщение отправлено.");
                    setTimeout(function(){$('#response').html("")}, 3000);
                    $("input[name=callback_phone], input[name=callback_name]").val("");
  //  jQuery.fancybox('<h2 class="title">Спасибо!</h2><div class="callback-text text">Ваша заявка отправлена.<br>В ближайшее время с вами свяжется<br>менеджер для уточнения деталей.');
               
                }
                
            }
        })
    });
    
    
    $('a.fancy').click(function(event){
        jQuery.fancybox(jQuery("#form_call").html());
    });
	
	$('.search_keyword_admin').blur(function(event){
		setTimeout(function(){
			window.location.href = '/admin/adminproducts/search?query=' + document.getElementById("query-admin").value;
		},1000); 
    });
   
    $("#menu ul").hide();
    $("#menu li span a").click(function(event) {  
        event.preventDefault();
        $("#menu ul:visible").slideUp("normal");
        if (($(this).parent().next().is("ul")) && (!$(this).parent().next().is(":visible"))) {
          //  $(this).parent().next().slideDown("normal");
            $(this).parent().next().stop(true, true).slideUp('normal');
        } 
    
    });
    
    $('ul.nav > li').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn();
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut();
        })
    
});




function formContactUsername() {
    if(document.getElementById("contact_name").value =='Ваш имя...' ) document.getElementById("contact_name").value='';
}

function formContactPhone() {
    if(document.getElementById("contact_phone").value =='Ваш телефон...' ) document.getElementById("contact_phone").value='';
}

function formContactComment() {
    if(document.getElementById("contact_message").value =='Ваш вопрос...' ) document.getElementById("contact_message").value='';
}

function orderformContactUsername() {
    if(document.getElementById("order_contact_name").value =='Ваш имя...' ) document.getElementById("order_contact_name").value='';
}

function orderformContactPhone() {
    if(document.getElementById("order_contact_phone").value =='Введите контактный телефон *' ) document.getElementById("order_contact_phone").value='';
}

function callbackformContactUsername() {
    if(document.getElementById("callback_contact_name").value =='Ваш имя...' ) document.getElementById("callback_contact_name").value='';
}

function callbackformContactPhone() {
    if(document.getElementById("callback_contact_phone").value =='Введите контактный телефон *' ) document.getElementById("callback_contact_phone").value='';
}


