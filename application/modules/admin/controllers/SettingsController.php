<?php

/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_SettingsController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    function indexAction()
    {
        $settings_object = new Default_Model_DbTable_Settings();
        $settings = $settings_object->getSettingsAdmin();
        
        $paginator = Zend_Paginator::factory($settings);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);       
    }
  
    public function editsettingAction()
    {
        $form = new Default_Form_Settings_Settings();     
        $this->view->form = $form;
        $settings_model = new Default_Model_DbTable_Settings();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {
                $setting_id = (int)$form->getValue('setting_id');
                $setting_value = $form->getValue('setting_value');
 
                $settings_model->updateSettings(
                                                    $setting_id, 
                                                    $setting_value
                                                );
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $setting_id = $this->_getParam('setting_id', 0);
            if ($setting_id > 0)
            {
                $form->populate($settings_model->getSettings($setting_id));
                $settings_object = $settings_model->getSettings($setting_id);
                $this->view->setting_name = $settings_object['setting_name'];
            }
        }
    }
    
    public function deleteAction()
    {
        if ($this->getRequest()->isPost())
        {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
            {
                $article_id = $this->getRequest()->getPost('article_id');
                
                $article = new Default_Model_DbTable_Articles(); //  Zend_Debug::dump($article);exit;
               
                $article->deleteArticle($article_id);
            }
            $this->_helper->redirector('index', 'articles', 'admin', array('article_id' => $article_id, 'page' => $page));
        } else {
            $article_id = $this->_getParam('article_id');
            
            $article = new Default_Model_DbTable_Articles();
            
            $this->view->article = $article->getArticle($article_id);
        }
    }
    
    function seoAction()
    {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('articles/index.phtml');
        $articles = new Default_Model_DbTable_Articlesseo();
        $articles_name = $articles->fetchAll();
        
        $paginator = Zend_Paginator::factory($articles_name);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);   
       
    }
    
    public function seoeditAction()
    {
        $form = new Default_Form_Articles_Articlesseo();     
        $this->view->form = $form;
        $article = new Default_Model_DbTable_Articlesseo();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {
                $articles_seo_id = (int)$form->getValue('articles_seo_id');
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
 
                $article_page = $article->fetchAll();
                $this->view->article_page = $article_page;
               
                $article->updateArticlesseo(
                                            $articles_seo_id,  
                                            $header_tags_title,
                                            $header_tags_description,
                                            $header_tags_keywords
                                        );
                
                $this->_helper->redirector('seo', 'articles', 'admin');
            } else {
                $form->populate($formData);
            }
        } else {
            
            $articles_seo_id = $this->_getParam('articles_seo_id', 0);
            if ($articles_seo_id > 0)
            {
                $form->populate($article->getArticlesseo($articles_seo_id));
            }
        }
    }
   
}