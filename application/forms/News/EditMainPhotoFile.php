<?php
/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_News_EditMainPhotoFile extends Zend_Form
{
    public function init()
    {	
	   
	    // Init form
        $this
          ->setDescription('Choose photos on your computer to add to this album.')
          ->setAttrib('id', 'form-upload')
          ->setAttrib('name', 'avatar_create')
          ->setAttrib('enctype','multipart/form-data')
          ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));
		  
		$isEmptyMessage = 'aaaaaaaaaaaaaaaaa'; 
		  
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		
		$news_id = new Zend_Form_Element_Hidden('news_id');
        $news_id->addFilter('Int');
		
		$main_photo = new Zend_Form_Element_File('main_photo');
		$main_photo->setLabel("Выбрать фото:")
		    ->setDescription("Допустимые форматы: jpg, png, gif")
		    ->setAttrib('size', '50')
			->addValidator('Count', false, 1)
			->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '2MB'))
			->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/news/main_photos')
			->setValueDisabled(true)
			->addValidator('NotEmpty', true,
                array('messages' => array('isEmpty' => $isEmptyMessage)));
				
        $this->addElements(array($main_photo));
		
		
		$submit = $this->createElement('submit', 'submit')
						->setAttrib('class', 'btn btn-primary');		
				
        $cancel = $this->createElement('submit', 'cancel')
						->setAttrib('class', 'btn btn-default'); 

        $submit->setLabel("Сохранить"); 		

        $cancel->setLabel("Отменить"); 

        
        $this->addElements(array($cancel, $submit)); 

        $this->addDisplayGroup(array('submit', 'cancel'), 'buttons', 

        array('disableLoadDefaultDecorators' => true)); 



        $group = $this->getDisplayGroup('buttons'); 

        $group->addDecorators(array( 

            array('FormElements'), 
            array('HtmlTag', array('tag' => 'div', 'class' => 'buttons')), 

        ));
        
    }
}

