<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Products_SelectNumber extends Zend_Form
{
    public function init()
    {   
		$limit = new Zend_Form_Element_Select('limit');
        $limit ->setLabel("Количество:")
								->removeDecorator('label')
								->setAttrib('class', array('span3'))
								->setAttrib('id', 'show_by')
								->addMultiOptions(array(
									'12' => "12", 
									'24' => "24",
									'48' => "48"
								));


        $this->addElements(array(
									$limit,
                                ));
       
    }
}