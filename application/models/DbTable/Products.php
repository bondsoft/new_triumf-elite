<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Products extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_products';
	
	public function getProduct($product_id)
    {
        $product_id = (int)$product_id;
        $row = $this->fetchRow('product_id = ' . $product_id);
        if(!$row) {
            throw new Exception("no record product_id - $product_id");
        }
        return $row->toArray();
    }
	
	public function getProductForCart(
										$product_id
									)
    {  
        $_name = 'zend_products';
	
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id);
                     
      //  $row = $this->fetchRow($select);
	
		foreach ($this->fetchAll($select) as $product) {
				$product_data[] = array(
					'url_seo' => $product['url_seo'],
					'product_model'         => $product['product_model'],
					'price'                   => $product['price'],
					'image'            => $product['image']					
				);
			}
		return $product_data;
    }
	
	public function addProduct(
                                $category_id,
								$parent_id,
                                $manufacturer_id,
                                $sort_order,
                                $product_model,
								$vendor_code,
                                $price,
                                $price_per_unit,
								$price_unit,
								$amount,
                                $image,
                                $status_in_out_of_stock,
								$stock_availability_01,
								$stock_availability_02,
								$latest,
								$special,
								$recommended,
                                $intro_text,
                                $product_description,
								$product_characteristics,
								$product_configurations,
								$product_overlook,
                                $url_seo,
                                $header_tags_title,
                                $header_tags_description,
                                $header_tags_keywords                                                         
                            )
    {
        $data = array(
			'category_id' => $category_id,
			'parent_id' => $parent_id,
			'manufacturer_id' => $manufacturer_id,
			'sort_order' => $sort_order,
			'product_model' => $product_model,
			'vendor_code' => $vendor_code,
			'price' => $price,
			'price_per_unit' => $price_per_unit,
			'price_unit' => $price_unit,
			'amount' => $amount,
			'image' => $image,
			'status_in_out_of_stock' => $status_in_out_of_stock,
			'stock_availability_01' => $stock_availability_01,
			'stock_availability_02' => $stock_availability_02,
            'latest' => $latest,
			'special' => $special,
			'recommended' => $recommended,
			'intro_text' => $intro_text,
			'product_description' => $product_description,
			'product_characteristics' => $product_characteristics,
			'product_configurations' => $product_configurations,
			'product_overlook' => $product_overlook,
			'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords,
			'creation_date' => date('Y-m-d H:i:s')
        );
        $this->insert($data);
    }
	
	public function deleteProduct($product_id)
    {
        $this->delete('product_id = ' . (int)$product_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateProduct( 
                                    $product_id,
                                    $category_id,
									$parent_id,
                                    $manufacturer_id,
                                    $sort_order,
                                    $product_model,
									$vendor_code,
                                    $price,
                                    $price_per_unit,
									$price_unit,
									$amount,
                                    $image,
                                    $status_in_out_of_stock,
									$stock_availability_01,
									$stock_availability_02,
									$latest,
									$special,
									$recommended,
                                    $intro_text,
                                    $product_description,
									$product_characteristics,
									$product_configurations,
									$product_overlook,
                                    $url_seo,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords     
								 )
    {
        $data = array(
            'category_id' => $category_id,
			'parent_id' => $parent_id,
            'manufacturer_id' => $manufacturer_id,
            'sort_order' => $sort_order,
            'product_model' => $product_model,
			'vendor_code' => $vendor_code,
            'price' => $price,
            'price_per_unit' => $price_per_unit,
			'price_unit' => $price_unit,
			'amount' => $amount,
            'image' => $image,
            'status_in_out_of_stock' => $status_in_out_of_stock,
			'stock_availability_01' => $stock_availability_01,
			'stock_availability_02' => $stock_availability_02,
            'latest' => $latest,
			'special' => $special,
			'recommended' => $recommended,
            'intro_text' => $intro_text,
            'product_description' => $product_description,
			'product_characteristics' => $product_characteristics,
			'product_configurations' => $product_configurations,
			'product_overlook' => $product_overlook,
            'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
   
    public function getAllProductsAdmin()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                 //   ->where('status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getAllProductsFront()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function getLatestProductsFront()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
                    ->where('latest = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getSpecialProductsFront()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
                    ->where('special = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getRecommendedProductsFront()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
                    ->where('recommended = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getProductsSpecificCategoryForFront($category_id)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id)
                    ->where('status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getSingleProductForFront($product_id)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
		$_name_04 = 'zend_manufacturers';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->joinLeft(array('name_04' => $_name_04),'name.manufacturer_id = name_04.manufacturer_id', array('name'))
				//	->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getSingleProductCategory($product_id)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'));
                     
        $products_object = $this->fetchRow($select);    
        $category_name = $products_object["category_name"];     
        return $category_name;
    }
    
    public function getProductsAttributesAdmin($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';  
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getProductsAttributesForFront($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_attribute';
        $_name_02 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_02' => $_name_02),'name_01.attribute_id = name_02.attribute_id', array('attribute_id', 'attribute_name'))
				//	->group('name_02.attribute_name')
                    ->order('name_02.sort_order');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getProductsAttributesText($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_attribute';
        $_name_02 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_02' => $_name_02),'name_01.attribute_id = name_02.attribute_id', array('attribute_id', 'attribute_name'))
                    ->order('name_01.attribute_id');
                     
        $attributes_rec = $this->fetchRow($select);	
        $attributes_text = $attributes_rec["text"];		
        return $attributes_text;
    }
    
    public function getProductPriceForBasket($product_id)
    {  
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id);
                     
        $product_rec = $this->fetchRow($select);	
        $price_rub = $product_rec["price"];		
        return $price_rub;
    }
    
    public function getManufacturersProductsForFront($manufacturer_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('manufacturer_id = ?', $manufacturer_id)
                    ->where('name.status = ?', '1');

        $kiosk = $this->fetchAll($select);    
         
        return $kiosk;
       
    }
	
	public function getManufacturersProductsAjaks($manufacturer_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('manufacturer_id = ?', $manufacturer_id)
                    ->where('name.status = ?', '1');

        $object = $this->fetchAll($select);    
         
        return $object->toArray();
       
    }
 
	
	public  function deleteProductImage(
                                            $product_id,  
                                            $image
                                        )
    {
        $data = array(
            'image' => $image,
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
	
	public function getMainphoto($kiosks_id)
	{
        $_name = 'zend_products';
		$_name_01 = 'category';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('kiosks_id = ?', $kiosks_id)
					->join(array('name_01' => $_name_01),'name.category_id = name_01.category_id');
     
		$pdf = $this->fetchAll($select);
        return $pdf;
    }
	
	public function getProductImage($product_id)
	{
	    $_name = 'zend_products';
        $select = $this->select()
                    ->from($_name)
					->where('product_id = ?', $product_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["image"];		
        return $main_photo_file_name;
    }
 
	
    public function getSimilarObjects($similarmodels)
    {
        $_name = 'zend_products';
        $_name_01 = 'kiosks_types';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
                    ->where('monitor LIKE ?', $similarmodels)
                    ->joinLeft(array('name_01' => $_name_01),'name.kiosks_types_id = name_01.kiosks_types_id')
                    ->order('kiosks_id DESC')
                    ->limit(4);
                     
        $all_software = $this->fetchAll($select);
        return $all_software;
    }

    public  function updateProductstatus(
                                            $product_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
    
    public  function addProductsAttributes( 
                                                $product_id,
                                                $attribute_id,
                                                $text      
                                            )
    {
        $data = array(
            'product_id' => $product_id,
            'attribute_id' => $attribute_id,
            'text' => $text
        );
        
        $this->insert($data);
    }
    
    public  function deleteProductsAttributes(
                                                $product_id
                                            )
    {
        $this->delete('product_id = ' . (int)$product_id);
    }
	
	public  function getLastInsertId()
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('product_id DESC')
                    ->limit(1);
                     
        $LastInsertId_rec = $this->fetchRow($select);	
        $LastInsertId = $LastInsertId_rec["product_id"];		
        return $LastInsertId;
    }
	
	public function checkActivManufacturer($manufacturer_id)
    {
        $_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('manufacturer_id = ?', $manufacturer_id)
                    ->limit(1);

        $products_object = $this->fetchRow($select);    
        $product_model = $products_object["product_model"];     
        return $product_model;
    }
    
    public function getProductModel($product_id)
    {
        $_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('product_id = ?', $product_id);

        $products_object = $this->fetchRow($select);    
        $product_model = $products_object["product_model"];     
        return $product_model;
    }
	
	public function getProductUrlSeo($product_id)
    {
        $_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('product_id = ?', $product_id);

        $products_object = $this->fetchRow($select);    
        $product_url_seo = $products_object["url_seo"];     
        return $product_url_seo;
    }
    
 
    public function getProductsForFront($category_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('category_id = ?', $category_id)
                    ->where('status = ?', '1');

        $object = $this->fetchAll($select);
        return $object;
    } 
	
/*	public function getAllProductsCategoryForFront($category_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $category_id)
                    ->where('status = ?', '1');

        $object = $this->fetchAll($select);
        return $object;
    } */
	
	public function getProductsForFrontParentCategory($category_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('category_id = ?', $category_id)
                    ->where('status = ?', '1');

        $object = $this->fetchAll($select);
        return $object;
    } 
    
    public function getProductIdByUrl($url_seo)
	{
	    $_name = 'zend_products';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$product_rec = $this->fetchRow($select);	
        $product_id = $product_rec["product_id"];		
        return $product_id;
    }
	
/*	public function getProductsSpecificCategoryForFrontFilter(
																$category_id,
																$brend_0,
																$brend_1,
																$brend_2,
																$gender_0,
																$gender_1,
																$size_0,
																$size_1,
																$size_2,
																$price_from,
																$price_to
															)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id)
					->where('name.status = ?', '1')
					->where("name_02.text = '$brend_0' OR name_02.text = '$brend_1' OR name_02.text = '$brend_2'")
					->where("name.gender = '$gender_0' OR name.gender = '$gender_1'")
					->where("name.size = '$size_0' OR name.size = '$size_1' OR name.size = '$size_2'")
					->where('name.price > ?', $price_from)
					->where('name.price < ?', $price_to)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
     //    die($select->__toString());            
        $all_products = $this->fetchAll($select);
        return $all_products;
    } */
	
	public function getProductsSpecificCategoryForFrontFilter(
																$category_id,
																$brend_01,
																$brend_02,
																$brend_03,
																$brend_04,
															//	$option_value,
																$price_from,
																$price_to
															)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
		$_name_04 = 'zend_product_option_value';
		
		if(($brend_01 != 'q') OR ($brend_02 != 'q') OR ($brend_03 != 'q') OR ($brend_04 != 'q')) {
			$where_brend = 'where';
		} else {
			$where_brend = 'where';
			$brend_01 = '1';
			$brend_02 = '2';
			$brend_03 = '3';
			$brend_04 = '4';
		}
		
	/*	if($option_value != 'q') {
			$where_option = 'where';
		} else {
			$where_option = 'where';
			$option_value = '49';
		} */
		
		
		
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id)
					->where('name.status = ?', '1')
					->$where_brend("name.manufacturer_id = '$brend_01' OR name.manufacturer_id = '$brend_02' OR name.manufacturer_id = '$brend_03' OR name.manufacturer_id = '$brend_04'")
				//	->where('name_04.option_value_id = ?', $option_value)
				//	->$where_option("name_04.option_value_id = '$option_value'")
					->where('name.price > ?', $price_from)
					->where('name.price < ?', $price_to)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					
					->group('product_id')
                    ->order('price');
    //     die($select->__toString());            
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function getProductsSpecificCategoryForFrontFilterNew(
															//	$category_id,
																$date_sub,
																$brend_01,
																$brend_02,
																$brend_03,
																$brend_04,
															//	$option_value,
																$price_from,
																$price_to
															)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
		$_name_04 = 'zend_product_option_value';
		
		if(($brend_01 != 'q') OR ($brend_02 != 'q') OR ($brend_03 != 'q') OR ($brend_04 != 'q')) {
			$where_brend = 'where';
		} else {
			$where_brend = 'where';
			$brend_01 = '1';
			$brend_02 = '2';
			$brend_03 = '3';
			$brend_04 = '4';
		}
		
	/*	if($option_value != 'q') {
			$where_option = 'where';
		} else {
			$where_option = 'where';
			$option_value = '49';
		} */
		
		
		
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                 //   ->where('name.category_id = ?', $category_id)
					->where('name.status = ?', '1')
					->where('name.creation_date > ?', $date_sub)
					->$where_brend("name.manufacturer_id = '$brend_01' OR name.manufacturer_id = '$brend_02' OR name.manufacturer_id = '$brend_03' OR name.manufacturer_id = '$brend_04'")
				//	->where('name_04.option_value_id = ?', $option_value)
				//	->$where_option("name_04.option_value_id = '$option_value'")
					->where('name.price > ?', $price_from)
					->where('name.price < ?', $price_to)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					
					->group('product_id')
                    ->order('price');
    //     die($select->__toString());            
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function getProductsOptionAdmin($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option_value'; 
        $_name_03 = 'zend_product_option_value'; 
		$_name_04 = 'zend_option_value_description'; 
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id')
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_id = name_02.option_id', array('option_id', 'image'))
					->joinLeft(array('name_03' => $_name_03),'name_02.option_value_id = name_03.option_value_id', array('price'))
					->joinLeft(array('name_04' => $_name_04),'name_02.option_value_id = name_04.option_value_id', array('name', 'option_value_id'));
				//	->group('name_02.option_id');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function getProductsOptionFront($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_product_option_value'; 
		$_name_03 = 'zend_option_value'; 
		$_name_04 = 'zend_option_value_description'; 
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
				//	->where('name_02.price != ?', 0)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id')
					->joinLeft(array('name_02' => $_name_02),'name_01.product_id = name_02.product_id')
                    ->joinLeft(array('name_03' => $_name_03),'name_01.option_id = name_03.option_id', array('option_id', 'image'))	
					->joinLeft(array('name_04' => $_name_04),'name_02.option_value_id = name_04.option_value_id', array('name', 'option_value_id'))
					->group('name_04.option_value_id');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function getProductsFirstOptionFront($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option_value'; 
        $_name_03 = 'zend_product_option_value'; 
		$_name_04 = 'zend_option_value_description'; 
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
				//	->where('name_03.price != ?', 0)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id')
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_id = name_02.option_id', array('option_id', 'image'))
					->joinLeft(array('name_03' => $_name_03),'name_02.option_value_id = name_03.option_value_id')
					->joinLeft(array('name_04' => $_name_04),'name_02.option_value_id = name_04.option_value_id', array('name', 'option_value_id'))
					->limit(1);
				//	->group('name_02.option_id');
                     
        $row = $this->fetchRow($select);
        return $row;
    }
	
	
	
	public function getProductOptionForCart(
												$product_id,
												$option_value_id
											)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option_value'; 
        $_name_03 = 'zend_product_option_value'; 
		$_name_04 = 'zend_option_value_description'; 
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
					->where('name_03.option_value_id = ?', $option_value_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id')
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_id = name_02.option_id', array('option_id'))
					->joinLeft(array('name_03' => $_name_03),'name_02.option_value_id = name_03.option_value_id', array('product_option_value_id', 'product_option_id', 'option_id', 'option_value_id'))
					->joinLeft(array('name_04' => $_name_04),'name_02.option_value_id = name_04.option_value_id', array('name', 'option_value_id'));
                     
        $row = $this->fetchRow($select);
        return $row;
    } 
	
/*	public function getProductOptionForCart(
												$option_value_id
											)
    {  
		$_name = 'zend_product_option_value';     
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option_value'; 
		$_name_03 = 'zend_products';
		$_name_04 = 'zend_option_value_description'; 
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                 //   ->where('name.product_id = ?', $product_id)
					->where('name.option_value_id = ?', $option_value_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id')
                    ->joinLeft(array('name_02' => $_name_02),'name.option_value_id = name_02.option_value_id')
					->joinLeft(array('name_03' => $_name_03),'name.product_id = name_03.product_id', array('product_model', 'url_seo', 'intro_text', 'image', 'price', 'price_per_unit'))
					->joinLeft(array('name_04' => $_name_04),'name_02.option_value_id = name_04.option_value_id', array('name'));
                     
        $row = $this->fetchRow($select);
        return $row;
    }*/
	
	public function getAllProductsCategoryForFront($category_id)
    {
		$_name = 'zend_category';
        $_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.parent_id = ?', $category_id)
                    ->where('name_01.status = ?', '1')
					->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id');

        $object = $this->fetchAll($select);
        return $object;
    } 
	
	public function getProductsAttributesForFrontLimit($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_attribute';
        $_name_02 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_02' => $_name_02),'name_01.attribute_id = name_02.attribute_id', array('attribute_id', 'attribute_name'))
				//	->group('name_02.attribute_name')
                    ->order('name_02.sort_order')
					->limit(5);
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function getAllObjectsSearch(
											$query_01,
											$query_02,
											$query_03,
											$query_04,
											$query_05
										)
    {
		$_name = 'zend_products';
	
		$select = $this->select()
                //    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
					->where('product_model  LIKE ?', '%'.$query_01.'%')
					->where('product_model  LIKE ?', '%'.$query_02.'%')
					->where('product_model  LIKE ?', '%'.$query_03.'%')
					->where('product_model  LIKE ?', '%'.$query_04.'%')
					->where('product_model  LIKE ?', '%'.$query_05.'%');
					           
        $all_objects = $this->fetchAll($select);
		return $all_objects;
    }
	
	public function getAllProductsSearchAdmin(
												$query_01,
												$query_02,
												$query_03,
												$query_04,
												$query_05
											)
    {
		$_name = 'zend_products';
		$_name_01 = 'zend_category';
	
		$select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
					->where('product_model  LIKE ?', '%'.$query_01.'%')
					->where('product_model  LIKE ?', '%'.$query_02.'%')
					->where('product_model  LIKE ?', '%'.$query_03.'%')
					->where('product_model  LIKE ?', '%'.$query_04.'%')
					->where('product_model  LIKE ?', '%'.$query_05.'%')
					->orwhere('vendor_code  LIKE ?', '%'.$query_01.'%')
					->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'));
					           
        $all_objects = $this->fetchAll($select);
		return $all_objects;
    }
	
	public function getAllProductsSearch()
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function getProductItemMeta($product_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('product_id = ?', $product_id);
                   // ->limit(1);

        $object = $this->fetchAll($select);
        return $object;
    } 
	
	
	
	public function getProductOptions($product_id)
    {
		$product_option_data = array();
		
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option';
		$_name_03 = 'zend_option_description'; 
		$_name_04 = 'zend_product_option_value';
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
                    ->where('name_01.product_id = ?', $product_id)
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_id = name_02.option_id')
					->joinLeft(array('name_03' => $_name_03),'name_02.option_id = name_03.option_id');
					
		foreach ($this->fetchAll($select) as $product_option) {
			$product_option_value_data = array();
			$select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_04' => $_name_04))
                    ->where('name_04.product_option_id = ?', $product_option['product_option_id']);
					
			foreach ($this->fetchAll($select) as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix']					
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],			
				'product_option_value' => $product_option_value_data,
				'option_value'         => $product_option['option_value']			
			);
			
		}
                     
        return $product_option_data;
    }
	
	public function getProductOptionsFront($product_id)
    {
		$product_option_data = array();
		
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option';
		$_name_03 = 'zend_option_description'; 
		$_name_04 = 'zend_product_option_value';
		$_name_05 = 'zend_option_value';
		$_name_06 = 'zend_option_value_description';
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
                    ->where('name_01.product_id = ?', $product_id)
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_id = name_02.option_id')
					->joinLeft(array('name_03' => $_name_03),'name_02.option_id = name_03.option_id');
					
					
					
		foreach ($this->fetchAll($select) as $product_option) {
			$product_option_value_data = array();
			$select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_04' => $_name_04))
                    ->where('name_04.product_id = ?', $product_id)
					->where('name_04.product_option_id = ?', $product_option['product_option_id'])
					->joinLeft(array('name_05' => $_name_05),'name_05.option_value_id = name_04.option_value_id', array('option_value_id', 'option_id', 'image'))
					->joinLeft(array('name_06' => $_name_06),'name_06.option_value_id = name_05.option_value_id')
					->order('name_04.sort_order');
					
					
			foreach ($this->fetchAll($select) as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'name'                    => $product_option_value['name'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'sort_order'              => $product_option_value['sort_order']					
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],			
				'product_option_value' => $product_option_value_data,
				'option_value'         => $product_option['option_value']			
			);
			
		}
                     
        return $product_option_data;
    }
	
	public function getProductOptionCart(
											$product_option_id,
											$product_option_value_id
										)
    {  
        $_name = 'zend_product_option_value';
		$_name_01 = 'zend_option_value_description';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_option_id = ?', $product_option_id)
					->where('name.product_option_value_id = ?', $product_option_value_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.option_value_id = name_01.option_value_id');
					
	//	$object = $this->fetchAll($select);
     //   return $object;		
		
			$product_option_data = array();   
		
			foreach ($this->fetchAll($select) as $product_option) {
				$product_option_data[] = array(
					'name'                    => $product_option['name'],
					'price'                   => $product_option['price'],
					'price_prefix'            => $product_option['price_prefix']					
				);
			}
			
		return $product_option_data;
    }
	
	public function getProductOptionsFrontCart(
													$product_id,
													$product_option_id,
													$product_option_value_id
												)
    {
		$product_option_data = array();
		
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option';
		$_name_03 = 'zend_option_description'; 
		$_name_04 = 'zend_product_option_value';
		$_name_05 = 'zend_option_value';
		$_name_06 = 'zend_option_value_description';
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
                    ->where('name_01.product_id = ?', $product_id)
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_id = name_02.option_id')
					->joinLeft(array('name_03' => $_name_03),'name_02.option_id = name_03.option_id')
					->group('name_01.product_id');
					
				
					
		foreach ($this->fetchAll($select) as $product_option) {
			$product_option_value_data = array();
			$select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_04' => $_name_04))
                    ->where('name_04.product_id = ?', $product_id)
					->where('name_04.product_option_id = ?', $product_option_id)
					->where('name_04.product_option_value_id = ?', $product_option_value_id)
					->joinLeft(array('name_05' => $_name_05),'name_05.option_value_id = name_04.option_value_id')
					->joinLeft(array('name_06' => $_name_06),'name_06.option_value_id = name_05.option_value_id');
					
					
			foreach ($this->fetchAll($select) as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'name'                    => $product_option_value['name'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'sort_order'              => $product_option_value['sort_order']					
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],			
				'product_option_value' => $product_option_value_data,
				'option_value'         => $product_option['option_value']			
			);
			
		}
                     
        return $product_option_data;
    }
	
	public function getProductOption(
										$product_option_id,
										$product_id
									)
    {
		
        $_name_01 = 'zend_product_option';
		$_name_02 = 'zend_option';
		$_name_03 = 'zend_option_description';
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
					->where('name_01.product_option_id = ?', $product_option_id)
					->where('name_01.product_id = ?', $product_id)
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_id = name_02.option_id')
					->joinLeft(array('name_03' => $_name_03),'name_02.option_id = name_03.option_id');
					
				
					
		$row = $this->fetchRow($select);
        return $row->toArray();
    }
	
	public function getProductOptionValue(
											$option_value,
											$product_option_id
										)
    {
		
		$_name_01 = 'zend_product_option_value';
		$_name_02 = 'zend_option_value';
		$_name_03 = 'zend_option_value_description';
	
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
					->where('name_01.product_option_value_id = ?', (int)$option_value)
					->where('name_01.product_option_id = ?', (int)$product_option_id)
                    ->joinLeft(array('name_02' => $_name_02),'name_01.option_value_id = name_02.option_value_id')
					->joinLeft(array('name_03' => $_name_03),'name_02.option_value_id = name_03.option_value_id');
					
				
					
		$row = $this->fetchRow($select);
        return $row->toArray();
    }
	
	 public function getProductForCart1($product_id)
    {  
        $_name = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id);
                     
        $all_products = $this->fetchRow($select);
        return $all_products;
    }

}
