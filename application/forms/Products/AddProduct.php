<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Products_AddProduct extends Zend_Form
{
    public function init()
    {   
        $uploadId = new Zend_Form_Element_Hidden('uploadId');
        $uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
            ->setAttrib('id', 'uploadId')
            ->setAttrib('value', md5(uniqid(rand())));
        
        $product_id = new Zend_Form_Element_Hidden('product_id');
        $product_id->addFilter('Int');
        
        $request = Zend_Controller_Front::getInstance();
        $id = $request->getRequest()->getParam('product_id');
        
        // prepare menu_first_level_id
        $validator_categories = new Zend_Validate_Between(array('min' => 1, 'max' => 100));
        $validator_categories->setMessage('Не выбрана категория');
        
        $categories_model = new Default_Model_DbTable_Categories();
        $category_id = $categories_model->fetchAll(); 
      
        if (count($category_id)!=0)
        {
            $category_prepared['не указана']= "не указана";
            foreach ($category_id as $category)
            {
                $category_prepared[$category->category_id]= $category->category_name;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $category_id = new Zend_Form_Element_Select('category_id');
            $category_id ->setLabel("Категории:")
                ->addMultiOptions($category_prepared)
                ->setAttrib('class', array('span6'))
				->removeDecorator('label')
                ->addValidator($validator_categories);
        } 
        
		$validator_manufacturers = new Zend_Validate_Between(array('min' => 1, 'max' => 100));
        $validator_manufacturers->setMessage('Не выбран производитель');
		
        $manufacturers_model = new Default_Model_DbTable_Manufacturers();
        $manufacturers = $manufacturers_model->fetchAll(); 
      
        if (count($manufacturers)!=0)
        {
            $manufacturer_prepared['не указана']= "не указан";
            foreach ($manufacturers as $manufacturer)
            {
                $manufacturer_prepared[$manufacturer->manufacturer_id]= $manufacturer->name;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $manufacturer_id = new Zend_Form_Element_Select('manufacturer_id');
            $manufacturer_id ->setLabel("Производители:")
                ->addMultiOptions($manufacturer_prepared)
                ->setAttrib('class', array('span4'))
				->removeDecorator('label') 
                ->addValidator($validator_manufacturers);
        }   

	/*	$option_model = new Default_Model_DbTable_Option();
        $options = $option_model->getAllOptionsAdmin(); 
      
        if (count($options)!=0)
        {
            $option_prepared['не указана']= "не указана";
            foreach ($options as $option)
            {
                $option_prepared[$option->option_id]= $option->name;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $option_id = new Zend_Form_Element_Select('option_id');
            $option_id ->setLabel("option:")
                ->addMultiOptions($option_prepared)
                ->setAttrib('class', array('span4'))
				->setAttrib('id', 'option_id')
				->removeDecorator('label'); 
               // ->addValidator($validator_menu_first_level_id);
        } */
    
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'))
					->removeDecorator('label');
        
        $url_seo = new Zend_Form_Element_Text('url_seo');
        $url_seo->setLabel("SEO Url:")
            ->setAttrib('class', 'span14')
			->removeDecorator('label')
            ->setAttrib('id', 'alias')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
            ->addValidator('Db_NoRecordExists', false, array(   'zend_products', 
                                                                'url_seo', 
                                                                'exclude' => array(
                                                                                    'field' => 'product_id',
                                                                                    'value' => $id
                                                                                    )
                                                            )
            );  
           
		$product_model = new Zend_Form_Element_Text('product_model');
        $product_model->setLabel("Название:")
            ->setAttrib('class', 'span14')
            ->setAttrib('id', 'name')
            ->setAttrib('onkeyup', 'translit()')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
			->removeDecorator('label');
			
		$vendor_code = new Zend_Form_Element_Text('vendor_code');
        $vendor_code->setLabel("vendor_code:")
                    ->setAttrib('class', array('span4'))
					->removeDecorator('label');
					
		$amount = new Zend_Form_Element_Text('amount');
        $amount->setLabel("amount:")
                    ->setAttrib('class', array('span4'))
					->removeDecorator('label');
					
		$alt = new Zend_Form_Element_Text('alt');
        $alt->setLabel("alt:")
            ->setAttrib('class', 'span10')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
			
		$status_in_out_of_stock = new Zend_Form_Element_Select('status_in_out_of_stock');
        $status_in_out_of_stock ->setLabel("Статус:")
								->removeDecorator('label')
								->addMultiOptions(array(
									'0' => "Нет в наличии", 
									'1' => "В наличии",
									'2' => "Заканчивается"
									));
									
		$latest = new Zend_Form_Element_Select('latest');
        $latest ->setLabel("Статус:")
								->removeDecorator('label')
								->setAttrib('class', array('span3'))
								->addMultiOptions(array(
									'0' => "Не новинка", 
									'1' => "Новинка"
									));
		$special = new Zend_Form_Element_Select('special');
        $special ->setLabel("Специальное предложение:")
								->removeDecorator('label')
								->setAttrib('class', array('span2'))
								->addMultiOptions(array(
									'0' => "Нет", 
									'1' => "Да"
									));

		$recommended = new Zend_Form_Element_Select('recommended');
        $recommended ->setLabel("Рекомендуемые товары:")
								->removeDecorator('label')
								->setAttrib('class', array('span2'))
								->addMultiOptions(array(
									'0' => "Нет", 
									'1' => "Да"
									));
									
									
	/*	$latest = new Zend_Form_Element_Checkbox('latest');
        $latest->setLabel("")
				->removeDecorator('label') 
				->setAttrib('class', 'checkbox')
				->setAttrib('id', 'checkbox')
				->setCheckedValue(1)
				->setUncheckedValue(0)
				->setDecorators (array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )); */
            
        $price = new Zend_Form_Element_Text('price');
        $price->setLabel("Цена:")
            ->setAttrib('class', array('span3'))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
          //  ->addValidator('Digits')
			->removeDecorator('label');   

		$price_per_unit = new Zend_Form_Element_Text('price_per_unit');
        $price_per_unit->setLabel("Цена:")
            ->setAttrib('class', array('span3'))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
          //  ->addValidator('Digits')
			->removeDecorator('label');  
			
		$price_unit = new Zend_Form_Element_Text('price_unit');
        $price_unit->setLabel("Цена:")
            ->setAttrib('class', array('span3'))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
          //  ->addValidator('Digits')
			->removeDecorator('label'); 
            
        $image = new Zend_Form_Element_File('image');
        $image->setLabel("Выбрать фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif, jpeg")
            ->setAttrib('size', '50')
        //  ->addValidator('Count', false, array('min' => 1, 'max' => 5))
        //  ->setMultiFile(1)
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif,jpeg')
            ->setDestination('media/photos/products')
            ->setValueDisabled(true)
			->removeDecorator('label');
        //  ->setRequired(true)
        //  ->addValidator('NotEmpty', false); additional  
        
        $image_additional = new Zend_Form_Element_File('image_additional');
        $image_additional->setLabel("Выбрать фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif")
            ->setAttrib('size', '50')
			->addValidator('Count', false, array('min' => 1, 'max' => 5))
			->setMultiFile(5)
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/products')
            ->setValueDisabled(true)
			->removeDecorator('label');
        //  ->setRequired(true)
        //  ->addValidator('NotEmpty', false); 
        
        $intro_text = new Zend_Form_Element_Text('intro_text');
        $intro_text->setLabel('')
            ->setAttrib('cols', 120)
            ->setAttrib('rows', 10)
			->setAttrib('class', 'span14')
            ->setAttrib('id', 'text_01')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
            ->removeDecorator('label');
            
        $product_description = new Zend_Form_Element_Textarea('product_description');
        $product_description->setLabel('')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_02')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
			->removeDecorator('label');  

		$product_characteristics = new Zend_Form_Element_Textarea('product_characteristics');
        $product_characteristics->setLabel('')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_03')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
			->removeDecorator('label'); 
			
		$product_configurations = new Zend_Form_Element_Textarea('product_configurations');
        $product_configurations->setLabel('')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_04')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
			->removeDecorator('label'); 
			
		$product_overlook = new Zend_Form_Element_Textarea('product_overlook');
        $product_overlook->setLabel('')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_05')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
			->removeDecorator('label'); 
       
        $header_tags_title = new Zend_Form_Element_Text('header_tags_title');
        $header_tags_title->setLabel("title:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
            
        $header_tags_description = new Zend_Form_Element_Text('header_tags_description');
        $header_tags_description->setLabel("description:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');

        $header_tags_keywords = new Zend_Form_Element_Text('header_tags_keywords');
        $header_tags_keywords->setLabel("keywords:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label'); 	
            
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить")
			->setAttrib('id', 'submitbutton')
			->setAttrib('class', 'btn btn-primary pull-right');    

        $this->addElements(array(
                                    $uploadId,
                                    $category_id,
                                    @$manufacturer_id,
								//	$option_id,
                                    $product_id,
                                    $sort_order,
                                    $product_model,
									$vendor_code,
									$amount,
                                    $url_seo,
                                    $status_in_out_of_stock,
                                    $latest,
									$special,
									$recommended,
                                    $price,
									$price_unit,
									$price_per_unit,
                                    $image,
                                    $intro_text,
                                    $product_description,
									$product_characteristics,
									$product_configurations,
									$product_overlook,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords,
                                    $submit
                                ));
       
    }
}