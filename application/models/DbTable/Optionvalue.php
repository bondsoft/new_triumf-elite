<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Optionvalue extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_option_value';
	
	public function getOptionvalue($option_value_id)
    {
        $option_value_id = (int)$option_value_id;
        $row = $this->fetchRow('option_value_id = ' . $option_value_id);
        if(!$row) {
            throw new Exception("no record option_value_id - $option_value_id");
        }
        return $row->toArray();
    }
	
	public function getOptionvalueAjax($product_option_value_id)
    {  
		$_name = 'zend_product_option_value';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_option_value_id = ?', $product_option_value_id);
                     
        $option = $this->fetchRow($select);
        return $option;
    }
	
	public function addOptionValue(
									$option_id,
									$image                   
								)
    {
        $data = array(
			'option_id' => $option_id,
			'image' => $image
        );
        $this->insert($data);
    }
	
	public  function addOptionValueImage( 
											$option_id,
											$image 									
										)
    {
        $data = array(
            'image' => $image
        );
        
        $this->update($data, 'option_id = ' . (int)$option_id);
    }
	
	public function deleteOptionValue($option_id)
    {
        $this->delete('option_id = ' . (int)$option_id);
    }
	
	public function deleteOptionValueRow($option_value_id)
    {
        $this->delete('option_value_id = ' . (int)$option_value_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateOptionValue( 
										$option_value_id,
										$option_id,
										$image
									 )
    {
        $data = array(
            'option_id' => $option_id,
			'image' => $image
        );
        
        $this->update($data, 'option_id = ' . (int)$option_id);
    }
	
	public  function updateOptionValue_01( 
										$option_value_id,
										$option_id,
										$image
									 )
    {
        $data = array(
            'option_id' => $option_id,
			'image' => $image
        );
        
        $this->update($data, 'option_value_id = ' . (int)$option_value_id);
    }
    
    public function getAllOptionsAdmin()
    {
        $_name = 'zend_option';
		$_name_01 = 'zend_option_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id', array('name'))
                    ->order('type');
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }
	
	public function getAllPhotosForOption($option_id)
	{
        $_name = 'zend_option_value';
        $select = $this->select()
                    ->from($_name)
                    ->where('option_id = ?', $option_id);
     
		$photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function getOptionItemAdmin($option_id)
    {  
        $_name = 'zend_option_value';
		$_name_01 = 'zend_option_value_description';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.option_id = ?', $option_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.option_value_id = name_01.option_value_id');
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }
	
	public function getOptionImage($option_value_id)
	{
	    $_name = 'zend_option_value';
        $select = $this->select()
                    ->from($_name)
					->where('option_value_id = ?', $option_value_id);

		$photo_file_rec = $this->fetchAll($select);	
        $photo_file_name = $photo_file_rec["image"];		
        return $photo_file_name;
    }
	
	public function getOptionValues($option_id) {
		$option_value_data = array();

		$_name = 'zend_option';
		$_name_01 = 'zend_option_value_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.option_id = ?', $option_id)
					->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id');

		foreach ($this->fetchAll($select) as $option_value) {
			$option_value_data[] = array(
				'option_value_id' => $option_value['option_value_id'],
				'name'            => $option_value['name']
			);
		}

		return $option_value_data;
	}
	
	public  function getLastInsertId()
    {
        $_name = 'zend_option_value';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('option_value_id DESC')
                    ->limit(1);
                     
        $LastInsertId_rec = $this->fetchRow($select);	
        $LastInsertId = $LastInsertId_rec["option_value_id"];		
        return $LastInsertId;
    }
 
}
