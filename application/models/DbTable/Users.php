<?php

/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Users extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_users';
	
	public function getUser($user_id)
    {
        $user_id = (int)$user_id;
        $row = $this->fetchRow('user_id = ' . $user_id);
        if(!$row) {
            throw new Exception("no record user_id - $user_id");
        }
        return $row->toArray();
    }
	
	public function getAvatar_file($user_id)
	{
	    $_name = 'zend_users';
        $select = $this->select()
                    ->from($_name)
					->where('user_id = ?', $user_id);
;
		$avatar_file_rec = $this->fetchRow($select);	
        $avatar_file_name = $avatar_file_rec["avatar_file"];		
        return $avatar_file_name;
    }
	
	public function getUser_id($email)
	{
	    $_name = 'zend_users';
        $select = $this->select()
                    ->from($_name)
					->where('email = ?', $email);
;
		$user_id_rec = $this->fetchRow($select);	
        $user_id = $user_id_rec["user_id"];		
        return $user_id;
    }
	
	public function updateAvatar($user_id, $avatar_file)
    {
		$data = array(  
            'avatar_file' => $avatar_file,			
        );
        
        $this->update($data, 'user_id = ' . (int)$user_id);
    }
	
	public function updateLast_access($user_id)
    {
		$data = array(  
            'last_access' => date('Y-m-d H:i:s'),			
        );
        
        $this->update($data, 'user_id = ' . (int)$user_id);
    }
	
	public function updateUser(	$user_id,
								$user_role)
    {
		$data = array(  'user_role' => $user_role
        );
        
        $this->update($data, 'user_id = ' . (int)$user_id);
    }
	
	public function deleteUser($user_id)
    {
	  // $_name = 'users';
	  // $condition = array(
      //   'photo_id = ?' => $photo_id
      // );
      //  $this->delete($_name, $condition); 
        $this->delete('user_id = ' . (int)$user_id);
    }
	
	
    // This function makes a new password from a plaintext password. 
    public function get_new_password($length)
    {
	    $salt = (string) rand(1000000, 9999999);
	    $len = strlen($salt);
		
        $password = '';
        //$salty_password = $salt.$password;
        for ($i=0; $i<$len; $i++)
		{
            $password .= rand();
        }

        $new_password = substr($password, 0, $length);
	
        $password = $new_password;
   
        return $password;
    }
 

    public function get_random_password()
    {
        $pas = md5($this->get_new_password(8));
        return $pas;
    }

    public function send_new_password_to_user($email, $user_id)
    {     
        $password = $this->get_new_password(8);
		$data = array(  
            'password' =>  md5($password),			
        );
		$result = $this->update($data, 'user_id = ' . (int)$user_id);
		
        $from = "From: root@localhost \r\n";
        $mesg = "��� ��� ����� ������ $password \r\n"
              ."�� ������ �������� ������ � ������ ��������. \r\n";
			  
        mail($email, "����� ������", $mesg, $from); 

		if (!$result)
            return false;  // not changed
        else
            return true;  // changed successfully  
    }

    // ��������� ������ ������ ������������ � ���� ������
    public function reset_password($user_id)
    {  
        $new_password = $this->get_random_password();
		$data = array(  
            'password' => $new_password,			
        );
        
        $result = $this->update($data, 'user_id = ' . (int)$user_id);
		
        if (!$result)
            return false;  // not changed
        else
            return true;  // changed successfully  
    }
	
	public function update_password($user_id, $new_password)
    {  
		$data = array(  
            'password' => $new_password,			
        );
        
        $result = $this->update($data, 'user_id = ' . (int)$user_id);
		
        if (!$result)
            return false;  // not changed
        else
            return true;  // changed successfully  
    }
	
	public function getUsersAdmin()
    {  
        $_name = 'zend_users';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.user_role = ?', 'admin')
                    ->order('name.user_id');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
	
	public function updateAccount(
									$user_id,
									$realname,
									$surname,
									$town  
								)
    {
		$data = array( 
			'realname' => $realname,
			'surname' => $surname,
			'town' => $town
        );
        
        $this->update($data, 'user_id = ' . (int)$user_id);
    }
}
