<?php

/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Homepage extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_homepage';
	
	public function getHomepage($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if(!$row) {
            throw new Exception("Нет записи с id - $id");
        }
        return $row->toArray();
    }
	
	public function getPageText()
	{
        $_name = 'zend_homepage';
        $select = $this->select()
                    ->from($_name);
     
		$text = $this->fetchAll($select);
        return $text;
    }
    
    public function getIntroHomeTextFront()
	{
        $_name = 'zend_homepage';
        $select = $this->select()
                    ->from($_name);
     
		$rec = $this->fetchRow($select);	
        $text = $rec["intro_text"];		
        return $text;
    }
    
    public function getHometestFront()
	{
        $_name = 'zend_homepage';
        $select = $this->select()
                    ->from($_name);
     
		$rec = $this->fetchRow($select);	
        $text = $rec["text"];		
        return $text;
    }
	
// variant	
/*	public  function updateHomepage($id, $text)
    {   $homepage = new Default_Model_DbTable_Homepage();
        $data = array(
            'text' => $text,
        );
        $where = $homepage->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateHomepage(
                                        $id,
                                        $intro_text,
                                        $text,
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                    )
    {
        $data = array(
            'intro_text' => $intro_text,
            'text' => $text,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'id = ' . (int)$id);
    }
	

}
