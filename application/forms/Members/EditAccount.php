<?php
/**
 * 
 *
 *
 */
class Default_Form_Members_EditAccount extends Zend_Form
{

  public function init()
  {     
        $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
		$request = Zend_Controller_Front::getInstance();
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id; 
        $id = $user_id;
		
        $this->addElement('Text', 'realname', array(
			'label' => 'Имя:',
			'size'  => '30',
		//	'class'  => 'text',		  
			'required' => true,
			'filters'    => array('StringTrim'),
			'validators' => array(
				array('NotEmpty', true)
			),
			'attribs' => array('class' => 'valid'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        )); 
        $element = $this->getElement('realname');
    //    $element->removeDecorator('Label'); 

        $this->addElement('Text', 'surname', array(
			'label' => 'Фамилия:',
			'size'  => '30', 
		//	'class'  => 'text',
			'required' => true,
			'filters'    => array('StringTrim'),
			'validators' => array(
				array('NotEmpty', true)
			),
			'attribs' => array('class' => 'valid'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        ));
        $element = $this->getElement('surname');
    //    $element->removeDecorator('Label'); 
        
        $this->addElement('Text', 'town', array(
			'label' => 'Город:',
			'size'  => '30', 
		//	'class'  => 'text',
			'required' => true,
			'filters'    => array('StringTrim'),
			'validators' => array(
				array('NotEmpty', true)
			),
			'attribs' => array('class' => 'valid'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        ));
        $element = $this->getElement('town');
    //    $element->removeDecorator('Label'); 
    	
	
		// Element: email
    /*    $this->addElement('Text', 'email', array(
			'label' => 'Email Address:',
			'size'  => '30', 
		//	'class'  => 'text',
			'required' => true,
			'allowEmpty' => false,
			'validators' => array(
				array('NotEmpty', true),
				array('EmailAddress', true),
				array('Db_NoRecordExists', false, array(	
															'zend_users',
															'email',
															'exclude' => array(
                                                                                    'field' => 'user_id',
                                                                                    'value' => $id
                                                                                    )
														)
					)
			),
			'attribs' => array('class' => 'valid'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
		));
        $element = $this->getElement('email');
        $element->removeDecorator('Label'); */
	
         $this->addElement('Button', 'submit', array(
            'label' => 'Сохранить',
            'type' => 'submit',
            'ignore' => true,
      //      'attribs' => array('class' => 'submit_account'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
          
        ));
        $element = $this->getElement('submit');
        $element->removeDecorator('Label'); 
		
    }
  
}
