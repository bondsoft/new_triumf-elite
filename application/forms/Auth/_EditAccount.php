<?php
/**
 * 
 *
 *
 */
class Default_Form_Auth_EditAccount extends Zend_Form
{

  public function init()
  {     
        $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		// указываем имя формы
        $this->setName('signup');
		
		$this->addElement('Select', 'user_role', array(
          'label' => $translate->translate('status:'),
          'class' => 'form-control-col-md-3',
		  'MultiOptions' => array(     
                    'guest' => "Не активен", 
					'admin' => "Активен"					
                    ),  
        ));  
		
		
		
		// Init submit
        $this->addElement('Button', 'submit', array(
          'label' => 'Сохранить',
          'class' => 'btn btn-primary pull-left',
          'type' => 'submit',
          'ignore' => true,
        ));
				
        // And finally add some CSRF protection
        $this->addElement('hash', 'csrf', array(
            'ignore' => true,
        ));
        
    }
  
}
