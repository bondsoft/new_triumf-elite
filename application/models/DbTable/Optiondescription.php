<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Optiondescription extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_option_description';
	
	public function getOption($option_id)
    {
        $option_id = (int)$option_id;
        $row = $this->fetchRow('option_id = ' . $option_id);
        if(!$row) {
            throw new Exception("no record option_id - $option_id");
        }
        return $row->toArray();
    }
	
	public function addOptionDescription(
											$option_id,
											$name         
										)
    {
        $data = array(
			'option_id' => $option_id,
			'name' => $name
        );
        $this->insert($data);
    }
	
	public function deleteOptionDescription($option_id)
    {
        $this->delete('option_id = ' . (int)$option_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateOptionDescription( 
												$option_id,
												$name       
											)
    {
        $data = array(
            'name' => $name
        );
        
        $this->update($data, 'option_id = ' . (int)$option_id);
    }
    
    public function getAllOptionsAdmin()
    {
        $_name = 'zend_option';
		$_name_01 = 'zend_option_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id', array('name'))
                    ->order('type');
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }
    
    public function getAllProductsFront()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getSpecialProductsFront()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
                    ->where('special = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getRecommendedProductsFront()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
                    ->where('recommended = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getProductsSpecificCategoryForFront($category_id)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id)
                    ->where('status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
					->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getSingleProductForFront($product_id)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
				//	->group('product_id')
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getSingleProductCategory($product_id)
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'));
                     
        $products_object = $this->fetchRow($select);    
        $category_name = $products_object["category_name"];     
        return $category_name;
    }
    
    public function getProductsAttributesAdmin($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';  
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getProductsAttributesForFront($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_attribute';
        $_name_02 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_02' => $_name_02),'name_01.attribute_id = name_02.attribute_id', array('attribute_id', 'attribute_name'))
				//	->group('name_02.attribute_name')
                    ->order('name_02.sort_order');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getProductsAttributesText($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_product_attribute';
        $_name_02 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_02' => $_name_02),'name_01.attribute_id = name_02.attribute_id', array('attribute_id', 'attribute_name'))
                    ->order('name_01.attribute_id');
                     
        $attributes_rec = $this->fetchRow($select);	
        $attributes_text = $attributes_rec["text"];		
        return $attributes_text;
    }
    
    public function getProductPriceForBasket($product_id)
    {  
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id);
                     
        $product_rec = $this->fetchRow($select);	
        $price_rub = $product_rec["price_rub"];		
        return $price_rub;
    }
    
    public function getManufacturersProductsForFront($manufacturer_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('manufacturer_id = ?', $manufacturer_id)
                    ->where('name.status = ?', '1');

        $kiosk = $this->fetchAll($select);    
         
        return $kiosk;
       
    }
    
	public function getPhotos($kiosks_id)
	{
        $_name = 'zend_products';
		$_name_01 = 'kiosks_photos';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('name.kiosks_id = ?', $kiosks_id)
					->join(array('name_01' => $_name_01),'name.kiosks_id = name_01.kiosks_id');
     
		$object = $this->fetchAll($select);
        return $object;
    }
	
	public  function deleteProductImage(
                                            $product_id,  
                                            $image
                                        )
    {
        $data = array(
            'image' => $image,
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
	
	public function getMainphoto($kiosks_id)
	{
        $_name = 'zend_products';
		$_name_01 = 'category';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('kiosks_id = ?', $kiosks_id)
					->join(array('name_01' => $_name_01),'name.category_id = name_01.category_id');
     
		$pdf = $this->fetchAll($select);
        return $pdf;
    }
	
	public function getProductImage($product_id)
	{
	    $_name = 'zend_products';
        $select = $this->select()
                    ->from($_name)
					->where('product_id = ?', $product_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["image"];		
        return $main_photo_file_name;
    }
 
	
    public function getSimilarObjects($similarmodels)
    {
        $_name = 'zend_products';
        $_name_01 = 'kiosks_types';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
                    ->where('monitor LIKE ?', $similarmodels)
                    ->joinLeft(array('name_01' => $_name_01),'name.kiosks_types_id = name_01.kiosks_types_id')
                    ->order('kiosks_id DESC')
                    ->limit(4);
                     
        $all_software = $this->fetchAll($select);
        return $all_software;
    }

    public  function updateProductstatus(
                                            $product_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
    
    public  function addProductsAttributes( 
                                                $product_id,
                                                $attribute_id,
                                                $text      
                                            )
    {
        $data = array(
            'product_id' => $product_id,
            'attribute_id' => $attribute_id,
            'text' => $text
        );
        
        $this->insert($data);
    }
    
    public  function deleteProductsAttributes(
                                                $product_id
                                            )
    {
        $this->delete('product_id = ' . (int)$product_id);
    }
	
	public  function getLastInsertId()
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('product_id DESC')
                    ->limit(1);
                     
        $LastInsertId_rec = $this->fetchRow($select);	
        $LastInsertId = $LastInsertId_rec["product_id"];		
        return $LastInsertId;
    }
	
	public function checkActivManufacturer($manufacturer_id)
    {
        $_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('manufacturer_id = ?', $manufacturer_id)
                    ->limit(1);

        $products_object = $this->fetchRow($select);    
        $product_model = $products_object["product_model"];     
        return $product_model;
    }
    
    public function getProductModel($product_id)
    {
        $_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('product_id = ?', $product_id);

        $products_object = $this->fetchRow($select);    
        $product_model = $products_object["product_model"];     
        return $product_model;
    }
    
 
    public function getProductsForFront($category_id)
    {
        $_name = 'zend_products';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('category_id = ?', $category_id)
                    ->where('status = ?', '1');

        $object = $this->fetchAll($select);
        return $object;
    } 
    
    public function getProductIdByUrl($url_seo)
	{
	    $_name = 'zend_products';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$product_rec = $this->fetchRow($select);	
        $product_id = $product_rec["product_id"];		
        return $product_id;
    }
	

}
