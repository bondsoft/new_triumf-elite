<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Optionvaluedescription extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_option_value_description';
	
	public function getOption($option_id)
    {
        $option_id = (int)$option_id;
        $row = $this->fetchRow('option_id = ' . $option_id);
        if(!$row) {
            throw new Exception("no record option_id - $option_id");
        }
        return $row->toArray();
    }
	
	public function addOptionValueDescription(
												$option_value_id,
												$option_id,
												$name
											)
    {
        $data = array(
			'option_value_id' => $option_value_id,
			'option_id' => $option_id,
			'name' => $name
        );
        $this->insert($data);
    }
	
	public function deleteOptionValueDescription($option_id)
    {
        $this->delete('option_id = ' . (int)$option_id);
    }
	
	public function deleteOptionValueDescriptionRow($option_value_id)
    {
        $this->delete('option_value_id = ' . (int)$option_value_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateOptionValueDescription( 
													$option_value_id,
													$option_id,
													$name
													
												)
    {
        $data = array(
            'option_value_id' => $option_value_id,
			'option_id' => $option_id,
			'name' => $name
        );
        
        $this->update($data, 'option_id = ' . (int)$option_id);
    }
	
	public  function updateOptionValueDescription_01( 
													$option_value_id,
													$option_id,
													$name
													
												)
    {
        $data = array(
            'option_value_id' => $option_value_id,
			'option_id' => $option_id,
			'name' => $name
        );
        
        $this->update($data, 'option_value_id = ' . (int)$option_value_id);
    }
    
	public  function addProductsAttributes( 
                                                $product_id,
                                                $attribute_id,
                                                $text      
                                            )
    {
        $data = array(
            'product_id' => $product_id,
            'attribute_id' => $attribute_id,
            'text' => $text
        );
        
        $this->insert($data);
    }
	
	public  function deleteOptionAjax(
                                            $option_id,
											$attribute_id
                                        )
    {
		
        $this->delete(array(
            'product_id = ?' => $product_id,
            'attribute_id = ?' => $attribute_id
        ));
    } 
   
}
