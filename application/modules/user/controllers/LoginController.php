<?php

/**
 * 
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class User_LoginController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('login');
    }
	
	function indexAction()
    {
       // $this->_redirect('/');
		//$this->_helper->redirector('login');
    }
	
    public function loginAction()
    {
	    $lang = Zend_Registry::get('Zend_Lang');
		$translate = Zend_Registry::get('Zend_Translate');

        $form_login = new Default_Form_Auth_Login();
        $this->view->form_login = $form_login;

        // 
        if ($this->getRequest()->isPost()) {
            // 
            $formData = $this->getRequest()->getPost();
            
            // 
            if ($form_login->isValid($formData)) {
                // 
                $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
                
                // указываем таблицу, где необходимо искать данные о пользователях
                // кололку, где искать имена пользователей,
                // а так же колонку, где хранятся пароли
                $authAdapter->setTableName('zend_users')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password');
                
                // 
                $username = $this->getRequest()->getPost('username');
                $password = $this->getRequest()->getPost('password');
                
                // подставляем полученные данные из формы
                $authAdapter->setIdentity($username)
                    ->setCredential(md5($password));
              
                // получаем экземпляр Zend_Auth
                $auth = Zend_Auth::getInstance();
                
                // делаем попытку авторизировать пользователя
                $result = $auth->authenticate($authAdapter);
                
                // если авторизация прошла успешно
                if ($result->isValid()) {
                    // используем адаптор для извлечения оставшихся данных о пользователе
                    $identity = $authAdapter->getResultRowObject();
                    
                    // получаем доступ к хранилищу данных Zend
                    $authStorage = $auth->getStorage();
                    
                    // помещаем туда информацию о пользователе,
                    // чтобы иметь к ним доступ при конфигурировании ACL
                    $authStorage->write($identity);
					
					$auth = Zend_Auth::getInstance();
	                $userdata = $auth->getIdentity();            
	                $user_id = $userdata->user_id;
					
					$users_object = new Default_Model_DbTable_Users();
					$users_object->updateLast_access($user_id);
                    
                
                   // $this->_helper->redirector('index', 'index', array('lang' => $lang));
				   
					$this->user_role = $userdata->user_role;
					if ($this->user_role == 'admin'){
						$this->_redirector = $this->_helper->getHelper('Redirector');
						$this->_redirector->gotoUrl('/admin');
					} elseif ($this->user_role == 'guest'){
						$this->view->errMessage = "Вам отказано в доступе. Обратитесь к администратору.";
					}
                } else {
                    $this->view->errMessage = $translate->translate('You have entered an incorrect username or password.');
                }
            }
        } 
 
    }
	
	public function fogotpasAction()
    {
        $form_fogotpass = new Default_Form_Auth_Fogotpass();
        $this->view->form_fogotpass = $form_fogotpass;

        // 
        if ($this->getRequest()->isPost())
		{
            // 
            $formData = $this->getRequest()->getPost();
            
            // 
            if ($form_fogotpass->isValid($formData))
			{
			    // 
                $email = $this->getRequest()->getPost('email');  // Zend_Debug::dump($email);exit;
				
			    $users_object = new Default_Model_DbTable_Users();
				
				$user_id = $users_object->getUser_id($email);  //  Zend_Debug::dump($email);exit;
                                             				
			//	$users_object->get_new_password(8);       Zend_Debug::dump($users_object->get_new_password(8));exit;
				
                $users_object->reset_password($user_id);  
			
		        if (!$users_object->send_new_password_to_user($email))
				{
				    $this->view->errMessage = $translate->translate('There was an error sending the message. Please try again later.');
				} else {
				    $this->view->errMessage = $translate->translate('You have been sent an email with new password.');
                }
                
            }
        }
 
    }

    public function logoutAction()
    {
       // уничтожаем информацию об авторизации пользователя
        Zend_Auth::getInstance()->clearIdentity();
        
        $lang = Zend_Registry::get('Zend_Lang');
		$this->_redirector = $this->_helper->getHelper('Redirector');
		$this->_redirector->gotoUrl('/user/login/login');
		
      //  $this->_helper->redirector('index', 'index', array('lang' => $lang));
    }

   
}

