<?php
/**
 *
 * @uses       Zend_Form
 * @package    
 * @subpackage Form
 */
class Default_Form_Request_Requesten extends Zend_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init()
    {	
        $this->setName('requesten');
		$request_id = new Zend_Form_Element_Hidden('request_id');
        $request_id->addFilter('Int');
		
		$lang = new Zend_Form_Element_Hidden('lang');
        $lang->setAttrib('value', 'en');
			       
        $header_tags_title = new Zend_Form_Element_Text('header_tags_title');
        $header_tags_title->setLabel("title:")
            ->setAttrib('class', 'form-control')
            ->setAttrib('size', '120')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');
            
        $header_tags_description = new Zend_Form_Element_Text('header_tags_description');
        $header_tags_description->setLabel("description:")
            ->setAttrib('class', 'form-control')
            ->setAttrib('size', '120')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');

        $header_tags_keywords = new Zend_Form_Element_Text('header_tags_keywords');
        $header_tags_keywords->setLabel("keywords:")
            ->setAttrib('class', 'form-control')
            ->setAttrib('size', '120')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');         
		
		$submit = new Zend_Form_Element_Submit("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-right');
        $this->addElements(array(  
                                    $request_id,
                                    $lang,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords,
                                    $submit
                                ));
			       
    }
}

