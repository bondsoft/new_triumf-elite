<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Gallery_EditPhoto extends Zend_Form
{
    public function init()
    {   
        $uploadId = new Zend_Form_Element_Hidden('uploadId');
        $uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
            ->setAttrib('id', 'uploadId')
            ->setAttrib('value', md5(uniqid(rand())));
        
        $photo_id = new Zend_Form_Element_Hidden('photo_id');
        $photo_id->addFilter('Int');
    
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'))
					->removeDecorator('label');
      
        $image = new Zend_Form_Element_File('image');
        $image->setLabel("Выберите фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif, jpeg")
            ->setAttrib('size', '50')
        //  ->addValidator('Count', false, array('min' => 1, 'max' => 5))
        //  ->setMultiFile(1)
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif,jpeg')
            ->setDestination('media/photos/gallery')
            ->setValueDisabled(true)
			->removeDecorator('label');
		//	->setRequired(true)
		//	->addValidator('NotEmpty', false); 
        
		$alt = new Zend_Form_Element_Text('alt');
        $alt->setLabel("alt:")
            ->setAttrib('class', 'span10')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
			
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $uploadId,
									$photo_id,
                                    $sort_order,
                                    $alt,
									$image,
                                    $submit
                                ));  
    }
}

