<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Articlesphoto extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_articles_photo';
	
	public function getPhoto($photo_id)
    {
        $photo_id = (int)$photo_id;
        $row = $this->fetchRow('photo_id = ' . $photo_id);
        if(!$row) {
            throw new Exception("no photo_id - $photo_id");
        }
        return $row->toArray();
    }
	
	public function updatePhotoFile(
                                                $photo_id,
                                                $photo_file
                                            )
    {
		$data = array(  
            'photo_file' => $photo_file,			
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }
	
	public  function updatePhoto($photo_id, $photo_text)
    {
        $data = array(
            'photo_text' => $photo_text,		
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }

	public function getPhotoFile($photo_id)
	{
	    $_name = 'zend_articles_photo';
        $select = $this->select()
                    ->from($_name)
					->where('photo_id = ?', $photo_id);

		$photo_file_rec = $this->fetchRow($select);	
        $photo_file_name = $photo_file_rec["photo_file"];		
        return $photo_file_name;
    }
	
	public function addPhoto(
                                $article_id,
                                $photo_file)
                            {
        $data = array(
            'article_id' => $article_id,
            'photo_file' => $photo_file
        );
        $this->insert($data);
    } 
	
	public function getAllPhotosForObject($article_id)
	{
        $_name = 'zend_articles_photo';
        $select = $this->select()
                    ->from($_name)
                    ->where('article_id = ?', $article_id);
     
		$photos = $this->fetchAll($select);
        return $photos;
    }
    
    public function getAllPhoto()
    {
        $_name = 'zend_articles_photo';
        $select = $this->select()
                    ->from($_name);
     
        $photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function deletePhotoForObject($photo_id)
    {
	 //  $_name = 'galary_photos';
	 //  $condition = array(
     //    'photo_id = ?' => $photo_id
     //  );
     //   $this->delete($_name, $condition); 
        $this->delete('photo_id = ' . (int)$photo_id);
    }
    
    public function deleteAllPhotosForObject($article_id)
    {
        $this->delete('article_id = ' . (int)$article_id);
    }
}
