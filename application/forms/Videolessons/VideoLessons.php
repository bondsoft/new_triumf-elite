<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Videolessons_VideoLessons extends Zend_Form
{
    public function init()
    {   
        $uploadId = new Zend_Form_Element_Hidden('uploadId');
        $uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
            ->setAttrib('id', 'uploadId')
            ->setAttrib('value', md5(uniqid(rand())));
        
        $video_id = new Zend_Form_Element_Hidden('video_id');
        $video_id->addFilter('Int');
		
		$video_title = new Zend_Form_Element_Text('video_title');
        $video_title->setLabel("alt:")
            ->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
			
		$html_code = new Zend_Form_Element_Text('html_code');
        $html_code->setLabel("alt:")
            ->setAttrib('class', 'span12')
        //    ->addFilter('StripTags')
        //    ->addFilter('StringTrim')
			->removeDecorator('label');
        
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'))
					->removeDecorator('label');
          
		$alt = new Zend_Form_Element_Text('alt');
        $alt->setLabel("alt:")
            ->setAttrib('class', 'span10')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
		   
        $image = new Zend_Form_Element_File('image');
        $image->setLabel("Выбрать фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif, jpeg")
            ->setAttrib('size', '50')
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif,jpeg')
            ->setDestination('media/photos/slider')
            ->setValueDisabled(true)
			->removeDecorator('label');
           
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $uploadId,
                                    $video_id,
									$video_title,
									$html_code,
                                    $alt,
                                    $image,
									$sort_order,
                                    $submit
                                ));
       
    }
}