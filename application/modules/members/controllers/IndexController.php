<?php
/**
 * User_Signup controller
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class Members_IndexController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
	//	$helper = $this->_helper->getHelper('Layout');
    //    $layout = $helper->getLayoutInstance();
	 //   $layout->setLayout('layout');
    }
	
    function indexAction()
    {
	    $lang = Zend_Registry::get('Zend_Lang');
        $request = $this->getRequest();
        $form_account = new Default_Form_Members_Account();
		$this->view->form_account = $form_account;
		$form_account->isValid($this->_getAllParams());
        if ($form_account->isValid($request->getPost())) {
                $user_object = new Default_Model_DbTable_Users($form_account->getValues());
				$data = array(
					'realname'           => $form_account->getValue('realname'),
					'surname'        => $form_account->getValue('surname'),
					'town'        => $form_account->getValue('town'),
                    'password'       => md5($form_account->getValue('password')),
					'email'          => $form_account->getValue('email'),
					'user_role'      => 'member',
					'last_access'    => date('Y-m-d H:i:s'),
					'creation_date'  => date('Y-m-d H:i:s'),
					'avatar_file'    => '',
                );
				$user_object->insert($data);
				
				$authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
                
                // указываем таблицу, где необходимо искать данные о пользователях
                // кололку, где искать имена пользователей,
                // а так же колонку, где хранятся пароли
                $authAdapter->setTableName('zend_users')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');
                
                $email = $form_account->getValue('email');
                $password = $form_account->getValue('password'); //Zend_Debug::dump($password);exit;
				
				// подставляем полученные данные из формы
                $authAdapter->setIdentity($email)
                    ->setCredential(md5($password));
              
                // получаем экземпляр Zend_Auth
                $auth = Zend_Auth::getInstance();
                
                // делаем попытку авторизировать пользователя
                $result = $auth->authenticate($authAdapter);
				
				// если авторизация прошла успешно
                if ($result->isValid()) {
                    // используем адаптор для извлечения оставшихся данных о пользователе
                    $identity = $authAdapter->getResultRowObject();
                    
                    // получаем доступ к хранилищу данных Zend
                    $authStorage = $auth->getStorage();
                    
                    // помещаем туда информацию о пользователе,
                    // чтобы иметь к ним доступ при конфигурировании ACL
                    $authStorage->write($identity);
					
					$auth = Zend_Auth::getInstance();
	                $userdata = $auth->getIdentity();            
	                $user_id = $userdata->user_id;
					
					$users_object = new Default_Model_DbTable_Users();
					$users_object->updateLast_access($user_id);
				
				    header('Content-Type: application/json');
					echo Zend_Json::encode(array('success' => true));
					exit;
					
					// $this->_redirector = $this->_helper->getHelper('Redirector');
					// $this->_redirector->gotoUrl('/');
					
                }
				
				//$email = $_POST['email']; //Zend_Debug::dump($email);exit;
				
		//		header('Content-Type: application/json');
        //    echo Zend_Json::encode(array('success' => true));
        //    exit;
				
                
		}  
		$json = $form_account->getMessages();
        header('Content-Type: application/json');
     //   echo Zend_Json::encode($json);
     //   exit;
        $captcha = $form_account->getElement('captcha')->getCaptcha(); //Zend_Debug::dump($captcha);exit;
        $json['captcha_id'] = $captcha->generate();
        $json['captcha_image'] = $captcha->getImgUrl() .
                   $captcha->getId() .
                   $captcha->getSuffix();
        echo Zend_Json::encode($json);
        exit;
	}
    
    
	
}