<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminattributesController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    function indexAction()
    {
        $attributes_model = new Default_Model_DbTable_Attributes();

        $attributes = $attributes_model->getAllAttributesAdmin(); //Zend_Debug::dump($categories_01);exit;
     
        $paginator = Zend_Paginator::factory($attributes);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);
       
    }
   
    public function addattributeAction()
    {
        $form = new Default_Form_Attributes_Attributes();
        $this->view->form = $form;
      //  Zend_Debug::dump($form);exit;
        $attributes_model = new Default_Model_DbTable_Attributes();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $attribute_group_id = $form->getValue('attribute_group_id');
                $attribute_name = $form->getValue('attribute_name');    //Zend_Debug::dump($menu_first_level_name_ru);exit;
                $sort_order = $form->getValue('sort_order');
                
                
                $attributes_model->addAttribute(
                                                    $attribute_group_id,
                                                    $attribute_name,
                                                    $sort_order                                                           
                                                );
                
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }
   
   
    public function editattributeAction()
    {
        $attribute_id = $this->getRequest()->getParam('attribute_id');
        $form = new Default_Form_Attributes_Attributes();
        $this->view->form = $form;
      //  Zend_Debug::dump($category_id);exit;
        $attributes_model = new Default_Model_DbTable_Attributes();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {  
                $attribute_id = $form->getValue('attribute_id');
                $attribute_group_id = $form->getValue('attribute_group_id');
                $sort_order = $form->getValue('sort_order');
                $attribute_name = $form->getValue('attribute_name'); 
                
 
                $attributes_model->updateAttribute(
                                                        $attribute_id, 
                                                        $attribute_group_id,
                                                        $attribute_name,
                                                        $sort_order 
                                                    );
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $attribute_id = $this->_getParam('attribute_id', 0);
            if ($attribute_id > 0)
            {
                $form->populate($attributes_model->getAttribute($attribute_id));
                $attributes_object = $attributes_model->getAttribute($attribute_id);
                $this->view->attribute_name = $attributes_object['attribute_name'];
            }
        }
    }
    
    public function deleteattributeAction()
    {
        $attributes_model = new Default_Model_DbTable_Attributes();
        $attribute_id = $this->getRequest()->getParam('attribute_id');
        $page = $this->getRequest()->getPost('page');
        if ($this->getRequest()->isPost())
        {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
            {
                $attributes_model = new Default_Model_DbTable_Attributes();
                $productsattributes_model = new Default_Model_DbTable_Productsattributes();
                $first_attribute_id = $productsattributes_model->checkFirstAttribute($attribute_id);
             // Zend_Debug::dump($first_attribute_id);exit;
                if($first_attribute_id != NULL){
                    $this->view->errMessage = "Этот атрибут активен. Удалить нельзя!";
                    return;
                } else {
                    $attributes_model->deleteAttribute($attribute_id);   
                }     
            }
            $this->_helper->redirector('index', 'adminattributes', 'admin', array('page' => $page));
        } else {
            $attribute_id = $this->_getParam('attribute_id');
            $attributes = $attributes_model->getAttribute($attribute_id);
            
            $this->view->attributes = $attributes;
        }
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $categories_model = new Default_Model_DbTable_Categories();
        $category_id = $this->getRequest()->getParam('category_id');
        
        $old_photo_file = $categories_model->getCategoryImage($category_id);
        $directory = "media/photos/categories";
        
        if ($this->getRequest()->isPost())
        {
            $category_id = $this->getRequest()->getParam('category_id');
            $main_photo = '';
           
            $categories_model->deleteCategoryImage(
                                                    $category_id,  
                                                    $main_photo
                                                );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
}