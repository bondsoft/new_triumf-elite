<?php
/**
 * 
 *
 */
class Admin_AlbumController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
    }
   
    public function indexAction()
    {
       // nothing to do here, index.phtml will be displayed
	    Zend_View_Helper_PaginationControl::setDefaultViewPartial('albums/index.phtml');
        $album_photos = new Default_Model_DbTable_Album();
		
		$page = $album_photos->fetchAll();
	  
		$paginator = Zend_Paginator::factory($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(4);
    }
   
    public function addAction()
    {
        // ������ �����
        $form = new Default_Form_Album_Album();
        
        // ������� ����� � view
        $this->view->form = $form;
		
		$values = $form->getValues();
        
        // ���� � ��� ��� Post ������
        if ($this->getRequest()->isPost()) {
            // ��������� ���
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
			{ 
                $photo_file = $form->getValue('photo_file'); 
           
                if( empty($photo_file) )
                {
	                echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
					echo iconv("windows-1251", "UTF-8", "�� �� ������� ����!");
	                echo "</font></b>" ;
                    return;
                }	
				
	            // ������ ������ ������
                $photo = new Default_Model_DbTable_Album();
				
				$data = $_FILES['photo_file'];
				$uploadedfile = $data['tmp_name'];
			
			//	array(5) {
            //      ["name"] => string(15) "����024.jpg"
            //      ["type"] => string(24) "application/octet-stream"
            //      ["tmp_name"] => string(25) "C:\WINDOWS\Temp\php94.tmp"
            //      ["error"] => int(0)
            //      ["size"] => string(5) "97964"
            //    }
				
			//	Zend_Debug::dump($image);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				   // $percent = 0.5;
				   // list($width, $height) = getimagesize($uploadedfile);
				
				   // $newwidth = $width * $percent;
                   // $newheight = $height * $percent;
				
                   // $new_image = imagecreatetruecolor($newwidth, $newheight);
                   // $old_image = imagecreatefromjpeg($uploadedfile);
					
                   // imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				    $newname = date("Ymd")."_".rand(1000,9999).$ext;//���������� ����� ��� ����� �� ��������� ���������� ��������
				    $str = substr($data["name"], -4);    
				    $strsum = $newname.$str;
				    $name = "photos/".$strsum;
					$name_l = "photos/l_".$strsum;
					
				  //  move_uploaded_file( $new_image, $name );
				  //  imagejpeg($new_image,$name,100);
					
				/* ********************************************** */
					include( 'SimpleImage.php' );
				    $image = new SimpleImage();
				    $image->load($uploadedfile);
					$image->resizeToWidth(640);
				    $image->save($name);
					$image->resizeToHeigth(102);
				    $image->save($name_l);   
				} 
             	 
                $photo->addPhoto($strsum);
                
                // ���������� ������������ helper ��� ��������� �� action = index
                $this->_helper->redirector('index', 'index', 'admin');
            } else {
                
				$this->view->errMessage = iconv("windows-1251", "UTF-8", "���������������") ;
            }
        }
    }
   
    public function editAction()
    {
        // ������ �����
        $form = new Default_Form_Album_Photo();     
        $this->view->form = $form;
        
        // ���� � ��� ��� Post ������
        if ($this->getRequest()->isPost()) {
            // ��������� ���
            $formData = $this->getRequest()->getPost();
           //  Zend_Debug::dump($formData);exit;
            // ���� ����� ��������� �����
            if ($form->isValid($formData)) {
                // ��������� id
                $photo_id = (int)$form->getValue('photo_id');
                $photo_file = $form->getValue('photo_file');
				$photo_text = $form->getValue('photo_text');
               
                // ������ ������ ������
                $photo = new Default_Model_DbTable_Album();
				$photo_page = $photo->fetchAll();
		        $this->view->photo_page = $photo_page;
               
                // �������� ����� ������ updateHomepage ��� ���������� ������
                $photo->updatePhoto($photo_id, $photo_text);
                
                // ���������� ������������ helper ��� ��������� �� action = index
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            // ���� �� ������� �����, �� �������� id ������, ������� ����� ��������
            $photo_id = $this->_getParam('photo_id', 0);
            if ($photo_id > 0) {
                // ������ ������ ������
                $photo = new Default_Model_DbTable_Album();
				
				$this->view->photo = $photo->getAlbum($photo_id);
                
                // ��������� ����� ����������� ��� ������ ������ populate
                $form->populate($photo->getAlbum($photo_id));
            }
        }
    }
	
	public function deleteAction()
    {
        // ���� � ��� ��� Post ������
        if ($this->getRequest()->isPost()) {
		
            // ��������� ��������
            $del = $this->getRequest()->getPost('del');
            
            // ���� ������������ ����������� ��� ������� ������� ������
            if ($del == 'yes') {
                // ��������� id ������, ������� ����� �������
                $photo_id = $this->getRequest()->getPost('photo_id');
				
				// ������ ������ ������
                $photo = new Default_Model_DbTable_Album();
				// �������� ��� ���������� �����
				$photo_file = $photo->getPhoto_file($photo_id);
				
				$directory = "photos";
				// ������� ���� �� ��������
				if(is_file("$directory/$photo_file"))
     		    {
     		        unlink("$directory/$photo_file");
					unlink("$directory/l_$photo_file");
				}
                
              //  Zend_Debug::dump($photo_file);exit;
               
                // �������� ����� ������ ��� �������� ������
                $photo->deletePhoto($photo_id);
            }
            $this->_helper->redirector('index', 'index', 'admin');
        } else {
            // ���� ������ �� Post, ������� ��������� ��� �������������
            // �������� id ������, ������� ����� �������
            $photo_id = $this->_getParam('photo_id');
            
            // ������ ������ ������
            $photo = new Default_Model_DbTable_Album();
            
            // ������ ������ � ������� � view
            $this->view->photo = $photo->getAlbum($photo_id);
        }
    }
	
	

  
}