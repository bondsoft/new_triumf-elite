<?php
/**
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
 
class Catalog_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        /* Initialize action controller here */
	    $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('catalog');	
    }
   
    public function indexAction()
    {
		$categories_model = new Default_Model_DbTable_Categories();

        $categories_object = $categories_model->getCategoryForFrontPage(); //Zend_Debug::dump($categories_01);exit;
     
        $this->view->categories = $categories_object;
    } 
    
    public function productsofcategoryAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('products_of_category'); 
		$limit = $this->getRequest()->getParam('limit'); //Zend_Debug::dump($limit);exit;
		$brend_01 = $this->getRequest()->getParam('brend_01');
		$brend_02 = $this->getRequest()->getParam('brend_02');
		$brend_03 = $this->getRequest()->getParam('brend_03');
		$gender_01 = $this->getRequest()->getParam('gender_01');
		$gender_02 = $this->getRequest()->getParam('gender_02');
		$size_01 = $this->getRequest()->getParam('size_01');
		$size_02 = $this->getRequest()->getParam('size_02');
		$size_03 = $this->getRequest()->getParam('size_03');
		$price_from = $this->getRequest()->getParam('price_from');
		$price_to = $this->getRequest()->getParam('price_to');
	
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
		
		$price_from_cookie = @$_COOKIE['price_from'];
		$price_to_cookie = @$_COOKIE['price_to'];
	//	Zend_Debug::dump($price_from_cookie);exit;
		if(!$price_from && $price_from_cookie != 900 && $price_from_cookie != NULL){
			$price_from = $price_from_cookie;
		} else if(!$price_from){
			$price_from = 900;
		}
		
		if(!$price_to && $price_to_cookie != 6000 && $price_from_cookie != NULL){
			$price_to = $price_to_cookie;
		} else 	if(!$price_to){
			$price_to = 6000;
		}
	//	Zend_Debug::dump($price_from);exit;
		
		$sort = $this->getRequest()->getParam('sort'); //Zend_Debug::dump($sort);exit;
		$order = $this->getRequest()->getParam('order'); 
		if($sort == 'cheap' || $sort == 'expensive') {
			$sort = 'price';
		}
		if(!$sort) {
			$sort = 'product_id';
		}
		if(!$order) {
			$order = 'DESC';
		}
		if($sort == 'novelty') {
			$sort = 'latest';
		}
		
		$this->view->brend_01 = $brend_01;
		$this->view->brend_02 = $brend_02;
		$this->view->brend_03 = $brend_03;
		$this->view->gender_01 = $gender_01;
		$this->view->gender_02 = $gender_02;
		$this->view->size_01 = $size_01;
		$this->view->size_02 = $size_02;
		$this->view->size_03 = $size_03;
		$this->view->price_from = $price_from;
		$this->view->price_to = $price_to;
		
		
	
        $products_model = new Default_Model_DbTable_Products();
        $categories_model = new Default_Model_DbTable_Categories();
		
		

			
			$url_seo = $this->getRequest()->getParam('url_seo'); // Zend_Debug::dump($url_seo);exit;
		
			$category_id = $categories_model->getCategoryIdByUrl($url_seo);
			$category_name = $categories_model->getCategoryNameByUrl($url_seo);
		
		//Zend_Debug::dump($category_id);exit;
			@$this->view->category_id = $category_id;
			$this->view->url_seo = $url_seo;
			$this->view->category_name = $category_name;
			
			$parent_id = $categories_model->getParentCategory($category_id);
		//	$parent_name = $categories_model->getCategory($parent_id);
		//	$this->view->parent_name = $parent_name['category_name'];
		//	$this->view->parent_url_seo = $parent_name['url_seo'];
			
			$category_object = $categories_model->getCategory($category_id);
			$this->view->category_text = $category_object['category_text'];
			
			$cat_object = $categories_model->getCatItemMeta($category_id);
			$this->view->page = $cat_object;
			
			
			if(@$brend_01) {
				$brend_01 = $brend_01;
			} else {
				$brend_01 = 'q';
			}
			if(@$brend_02) {
				$brend_02 = $brend_02;
			} else {
				$brend_02 = 'q';
			}
			if(@$brend_03) {
				$brend_03 = $brend_03;
			} else {
				$brend_03 = 'q';
			}
			
			if(@$gender_01) {
				$gender_01 = $gender_01;
			} else {
				$gender_01 = 'q';
			}
			if(@$gender_02) {
				$gender_02 = $gender_02;
			} else {
				$gender_02 = 'q';
			}
			
			if(@$size_01) {
				$size_01 = $size_01;
			} else {
				$size_01 = 'q';
			}
			if(@$size_02) {
				$size_02 = $size_02;
			} else {
				$size_02 = 'q';
			}
			if(@$size_03) {
				$size_03 = $size_03;
			} else {
				$size_03 = 'q';
			}
	$products = $products_model->getProductsForFront(
														$category_id,
														$sort,
														$order 
													);
//	Zend_Debug::dump($products);exit;
	
	$product_attributes = array();
	foreach ($products as $key => $value) { 
		$product_attributes[$value->product_id] = $products_model->getProductsAttributesForFront($value->product_id);
		 
	}
	$this->view->product_attributes = $product_attributes;
	
	$product_option = array();
	foreach ($products as $product) {
		$product_option[$product->product_id] = $products_model->getProductOptionsFront($product['product_id']);
		 
	}
//	Zend_Debug::dump($product_option);exit;
	$this->view->product_option = $product_option;

	
		//	Zend_Debug::dump($products);exit;
			$paginator = Zend_Paginator::factory($products);
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$this->view->paginator = $paginator;
			if($limit) { $limit = 1000; }
			if(!$limit) { $limit = 1000; }
				$count_per_page = Zend_Paginator::setDefaultItemCountPerPage($limit); 
			
			$sum_cart = $sessionNamespace->sum_cart;// Zend_Debug::dump($sum_cart);exit;
			$this->view->sum_cart = $sum_cart;
			
			if (isset($_COOKIE['phone_number'])) {
				$this->view->phone_number = $_COOKIE['phone_number'];
			}
			if (isset($_COOKIE['address'])) {
				$this->view->address = $_COOKIE['address'];
			}
			if (isset($_COOKIE['work_schedule'])) {
				$this->view->work_schedule = $_COOKIE['work_schedule'];
			}
    }
	
	public function productAction()
    {
       $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('product'); 
		
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
        $categories_model = new Default_Model_DbTable_Categories();
        $products_model = new Default_Model_DbTable_Products();
        $url_seo = $this->getRequest()->getParam('url_seo');// Zend_Debug::dump($url_seo);exit;
        
        $product_id = $products_model->getProductIdByUrl($url_seo); 
	//	Zend_Debug::dump($product_id);exit;
        
        
        $product = $products_model->getSingleProductForFront($product_id); // Zend_Debug::dump($product);exit;
        
        $product_model = $products_model->getProductModel($product_id); // Zend_Debug::dump($product);exit;
        $this->view->product_model = $product_model;
        
        $category_name = $products_model->getSingleProductCategory($product_id); // Zend_Debug::dump($product);exit;
        $this->view->category_name = $category_name;
        
        $url_seo_category = $categories_model->getCategoryUrlByName($category_name);
        $this->view->url_seo_category = $url_seo_category;
		$category_id = $categories_model->getCategoryIdByUrl($url_seo_category);
		$parent_id = $categories_model->getParentCategory($category_id);
	//	$parent_name = $categories_model->getCategory($parent_id);
	//	$this->view->parent_name = $parent_name['category_name'];
	//	$this->view->parent_url_seo = $parent_name['url_seo'];
        
        $product_image = $products_model->getProductImage($product_id); // Zend_Debug::dump($product);exit;
        $this->view->product_image = $product_image;
        
        $attributes_model = new Default_Model_DbTable_Attributes();
        $attributes = $attributes_model->getAllAttributesAdmin(); //Zend_Debug::dump($attributes);exit;
        $this->view->attributes = $attributes; 
        
        $product_attributes = $products_model->getProductsAttributesForFront($product_id); // Zend_Debug::dump($product_attributes);exit;
        $this->view->product_attributes = $product_attributes;
        
        $attributes_text = $products_model->getProductsAttributesText($product_id); // Zend_Debug::dump($attributes_text);exit;
        $this->view->attributes_text = $attributes_text;
		
		$product_options = $products_model->getProductOptionsFront($product_id); 
	//	Zend_Debug::dump($product_options);exit;
        $this->view->product_options = $product_options; 
		
		$this->view->page = $product;
		
        $paginator = Zend_Paginator::factory($product);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(1); 
        
        $products_photo_model = new Default_Model_DbTable_Productsphoto();
		$photos = $products_photo_model->getAllPhotosForProduct($product_id);
		$this->view->photos = $photos;
		
		$sum_cart = $sessionNamespace->sum_cart;// Zend_Debug::dump($sum_cart);exit;
		$this->view->sum_cart = $sum_cart;
		
		if (isset($_COOKIE['phone_number'])) {
			$this->view->phone_number = $_COOKIE['phone_number'];
		}
		if (isset($_COOKIE['address'])) {
			$this->view->address = $_COOKIE['address'];
		}
		if (isset($_COOKIE['work_schedule'])) {
			$this->view->work_schedule = $_COOKIE['work_schedule'];
		}
    }
	
	public function addproducttobasketAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        $sessionNamespace = new Zend_Session_Namespace('Foo');
        $json = array();
		
        if ($this->getRequest()->isPost())
        {
			$product_id = $this->getRequest()->getPost('product_id');
			$quantity = $this->getRequest()->getPost('quantity');
			$need_measure = $this->getRequest()->getPost('need_measure');
			$individual_fastening = $this->getRequest()->getPost('individual_fastening');
			$sessionNamespace = new Zend_Session_Namespace('Foo');
		//	Zend_Debug::dump($product_id);//exit;
		
			if ($this->getRequest()->getPost('option')) {
				$option = array_filter($this->getRequest()->getPost('option'));
			} else {
				$option = array();	
			}
			
			$key = (int)$product_id . ':';

			if ($option) {
				$key .= base64_encode(serialize($option)) . ':';
			}  else {
				$key .= ':';
			}
			if (!isset($sessionNamespace->array['cart'][$key])) {
				$sessionNamespace->array['cart'][$key] = (int)$quantity;
			} else {
				$sessionNamespace->array['cart'][$key] += (int)$quantity;
			}	
		//	unset($sessionNamespace->products);
			$total_items = $sessionNamespace->total_items = 0;
			$total_price = $sessionNamespace->total_price = '0.00';
			$sum_cart = $sessionNamespace->sum_cart;
		
			$quantityCart = $sessionNamespace->quantity_cart;
			$quantityCart = $sessionNamespace->quantity_cart =  $quantityCart + $quantity;
		
			header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true, 'quantity' => $quantityCart));
            exit; 
        }
       
		header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function in_object($val, $obj){

		if($val == ""){
			trigger_error("in_object expects parameter 1 must not empty", E_USER_WARNING);
			return false;
		}
		if(!is_object($obj)){
			$obj = (object)$obj;
		}

		foreach($obj as $key => $value){
			if(!is_object($value) && !is_array($value)){
				if($value == $val){
					return true;
				}
			}else{
				return $this->in_object($val, $value);
			}
		}
		return false;
	}
	
	public function showoptionimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        $json = array();

        if ($this->getRequest()->isPost())
        {
            $option_value_id = $this->getRequest()->getPost('option_value_id');
			
			
			$option_value_model = new Default_Model_DbTable_Optionvalue();
			$option_value_row = $option_value_model->getOptionvalueAjax($option_value_id);
			// Zend_Debug::dump($option_value_row);exit;
			$image = $option_value_row['image'];
			$price = $option_value_row['price'];
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true, 'image' => $image, 'price' => $price, 'option_value_id' => $option_value_id));
            exit;                         
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function selectoptionAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        $json = array();

        if ($this->getRequest()->isPost())
        {
            $product_option_value_id = $this->getRequest()->getPost('product_option_value_id');
			$product_id = $this->getRequest()->getPost('product_id');
		//	 Zend_Debug::dump($product_id);exit;
			
			$option_value_model = new Default_Model_DbTable_Optionvalue();
			$option_value_row = $option_value_model->getOptionvalueAjax($product_option_value_id);
			// Zend_Debug::dump($option_value_row);exit;
			$price_product = $products_model->getProductPriceForBasket( $product_id );
			$price_option = number_format($option_value_row['price'], 2, ',', ' ');
			$price_prefix = $option_value_row['price_prefix'];
			
			$price = $price_product + $price_prefix + $price_option.' руб.';
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true, 'price_prefix' => $price_prefix, 'price' => $price, 'product_option_value_id' => $product_option_value_id));
            exit;                         
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
/*	public function addproducttobasketAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        $json = array();

        if ($this->getRequest()->isPost())
        {
            $product_id = $this->getRequest()->getPost('product_id');// Zend_Debug::dump($product_id);exit;
            $price_rub = $this->getRequest()->getPost('price_rub');
		//	Zend_Debug::dump($price_rub);exit;
			$quantity = 1;
			
				
			$price_rub = number_format($products_model->getProductPriceForBasket($product_id), 0, '', '');
		//	Zend_Debug::dump($price_rub);exit;
			Zend_Session::start();
			$sessionNamespace = new Zend_Session_Namespace('Foo');
			
		//	$price = $sessionNamespace->product_price_rub; Zend_Debug::dump($price);exit;
			
			@$sessionNamespace->product_price_rub[$price_rub]++; 
			$price = $sessionNamespace->product_price_rub; // Zend_Debug::dump($price);exit;
			
			@$sessionNamespace->product_id[$product_id]++; 
			$product_id = $sessionNamespace->product_id;
		//	Zend_Debug::dump($product_id);exit;
			foreach($price as $key => $value)
			{ 
				$product_price_rub[] = ($key * $value);	
			}
			$quantity = count($product_price_rub);
			$sum = array_sum($product_price_rub);
		//	Zend_Debug::dump($sum);exit;
		
		//	$product_price_rub = $sessionNamespace->product_price_rub = $product_price_rub;
			
			$price_cart = $sessionNamespace->price_cart = $sum; 
			$quantity_cart = $sessionNamespace->quantity_cart = $quantity; 
			
			//Zend_Debug::dump($quantity_cart);exit;
			$json = $sum;
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true, 'sum' => $json, 'quantity' => $quantity));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    } */
    
    public function productsshowbyAction()
    {
		$request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
		
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		
        if ($this->getRequest()->isPost())
        {
			$number = $this->getRequest()->getPost('number'); //Zend_Debug::dump($number);exit;
			$category_id = $this->getRequest()->getPost('category_id'); //Zend_Debug::dump($category_id);exit;
			$url_seo = $this->getRequest()->getPost('url_seo'); //Zend_Debug::dump($url_seo);exit;
			$url_seo_session = $sessionNamespace->url_seo_session = $url_seo;
			$number = $sessionNamespace->number = $number; //Zend_Debug::dump($number);exit;
			$category_id_session = $sessionNamespace->category_id_session = $category_id;
		
			$this->_helper->redirector('productsofcategory', 'index', 'catalog');
		              
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    
    }
    
    function sendreviewAction() 
    { 
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        
        $form = new Default_Form_Reviews_AddReviewFront(); 
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
                 
				$product_id = $_POST['product_id'];
                $author       = $form->getValue('author'); 
                $text      = $form->getValue('text');
                $rating      = $form->getValue('rating');
           
                $review_model = new Default_Model_DbTable_Reviews();
                
                $review_model->addReview(
											$product_id,
											$author,
											$text,
											$rating
										);
             
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
     //   echo Zend_Json::encode($json);
     //   exit;
        $captcha = $form->getElement('captcha')->getCaptcha(); //Zend_Debug::dump($captcha);exit;
        $json['captcha_id'] = $captcha->generate();
        $json['captcha_image'] = $captcha->getImgUrl() .
                   $captcha->getId() .
                   $captcha->getSuffix();
        echo Zend_Json::encode($json);
        exit;
    
    }
	
	function productfilterAction()
    {
	//	$limit = $this->getRequest()->getParam('limit');
		$url_seo = $this->getRequest()->getPost('url_seo');
        $form = new Default_Form_Products_ProductFilter(); 
        $this->view->form = $form; 
		
		if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
        
            if ($form->isValid($formData)) 
            { 
				$pieces = explode("/", $_SERVER['REQUEST_URI']); 
				$pieces_02 = explode("?", $pieces[2]);				//Zend_Debug::dump($_SERVER['SERVER_NAME']);exit; 
				$url = '';
				$url .= $pieces_02[0].'?';
				$brend = $_POST['brend'];
				$gender = $_POST['gender'];
				$size = $_POST['size'];
				$price_from = $_POST['price_from'];// Zend_Debug::dump($price_from);exit; 
				$price_to = $_POST['price_to'];
				foreach($brend as $key=>$value) {
					$brend_arr[] = '&brend['.$key.']='.$brend[$key];
				}
				$comma_separated_brend_arr = implode("", $brend_arr);
				
				$comma_separated_brend_arr = substr($comma_separated_brend_arr, 1);
				
				foreach($gender as $key=>$value) {
					$gender_arr[] = '&gender['.$key.']='.$gender[$key];
				}
				$comma_separated_gender_arr = implode("", $gender_arr);
				
				foreach($size as $key=>$value) {
					$size_arr[] = '&size['.$key.']='.$size[$key];
				}
				$comma_separated_size_arr = implode("", $size_arr);
			//	echo $comma_separated_brend_arr; exit;
			//	print_r ( $qq ); exit;
			//	Zend_Debug::dump($qq);exit;
				
				$price_from_get = $this->getRequest()->getParam('price_from');
				$price_to_get = $this->getRequest()->getParam('price_to');
				if($price_from_get && $price_from == NULL) {
					$price_from = $this->getRequest()->getParam('price_from');
				} else
				if($price_from == NULL){
					$price_from = 900;
				}
				if($price_to_get && $price_to == NULL) {
					$price_to = $this->getRequest()->getParam('price_to');
				} else
				if($price_to == NULL){
					$price_to = 6000;
				}
			
				$url .= $comma_separated_brend_arr;
				$url .= $comma_separated_gender_arr;
				$url .= $comma_separated_size_arr;
				$url .= '&price_from='.$price_from;
				$url .= '&price_to='.$price_to;
				
			//	header('Location: http://'.$_SERVER['SERVER_NAME'].'/filter/'.$url);
            } else {
                $this->view->errMessage = "rrrrrrrrrrrrr";
            }
        } 
		
       
    }
    
    public function brandAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('category_list'); 
		$limit = $this->getRequest()->getParam('limit'); 
	
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
        $products_model = new Default_Model_DbTable_Products();
        $manufacturers_model = new Default_Model_DbTable_Manufacturers();
       
		
		$url_seo = $this->getRequest()->getParam('url_seo');  //Zend_Debug::dump($url_seo);exit;
		
		$manufacturer_id = $manufacturers_model->getManufacturerIdByUrl($url_seo);
		
		$manufacturer_name = $manufacturers_model->getManufacturerNameByUrl($url_seo);
		$this->view->manufacturer_name = $manufacturer_name;
		
			
		$products = $products_model->getManufacturersProductsForFront($manufacturer_id);
																		
	
		//	Zend_Debug::dump($products);exit;
			$paginator = Zend_Paginator::factory($products);
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$this->view->paginator = $paginator;
			if($limit) { $limit = 1000; }
			if(!$limit) { $limit = 15; }
				$count_per_page = Zend_Paginator::setDefaultItemCountPerPage($limit); 
			
			$sum_cart = $sessionNamespace->sum_cart;// Zend_Debug::dump($sum_cart);exit;
			$this->view->sum_cart = $sum_cart;
    }
	
	public function categoryAction()
    {
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('category_list'); 
		$limit = $this->getRequest()->getParam('limit'); //Zend_Debug::dump($limit);exit;
		
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
        $products_model = new Default_Model_DbTable_Products();
        $categories_model = new Default_Model_DbTable_Categories();
		
			$url_seo = $this->getRequest()->getPost('url_seo'); 
			//Zend_Debug::dump($url_seo);exit;
			$url_seo_get = $this->getRequest()->getParam('url_seo');  
			//Zend_Debug::dump($url_seo);exit;
		if($url_seo) {
			$category_id = $categories_model->getCategoryIdByUrl($url_seo);
			$category_name = $categories_model->getCategoryNameByUrl($url_seo);
		} else if($url_seo_get) { 
			$category_id = $categories_model->getCategoryIdByUrl($url_seo_get);
			$category_name = $categories_model->getCategoryNameByUrl($url_seo_get);
		//	Zend_Debug::dump($category_id);exit;
		}
		
		$sort = $this->getRequest()->getParam('sort'); //Zend_Debug::dump($sort);exit;
		$order = $this->getRequest()->getParam('order'); 
		if($sort == 'cheap' || $sort == 'expensive') {
			$sort = 'price';
		}
		if(!$sort) {
			$sort = 'product_id';
		}
		if(!$order) {
			$order = 'DESC';
		}
		if($sort == 'novelty') {
			$sort = 'latest';
		}
		
			@$this->view->category_id = $category_id;
			$this->view->url_seo_get = $url_seo_get;
			$this->view->category_name = $category_name;
			
			$category_object = $categories_model->getCategory($category_id);
			$this->view->category_text = $category_object['category_text'];
			
			$cat_object = $categories_model->getCatItemMeta($category_id);
			$this->view->page = $cat_object;
			
			$categories = $categories_model->getCategoriesByParent($category_id);
			$this->view->categories = $categories;
			$category_in = array();
			foreach ($categories as $category) {
				$check_parent[] = $categories_model->checkParentCategory($category->category_id);
			}
	//Zend_Debug::dump($check_parent);exit;
			$this->view->check_parent = $check_parent;
			$products = $products_model->getAllProductsCategoryForFront(
																			$category_id,
																			$sort,
																			$order
																		);	
			$paginator = Zend_Paginator::factory($products);
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));
			$this->view->paginator = $paginator;
			if($limit) { $limit = 1000; }
			if(!$limit) { $limit = 15; }
				$count_per_page = Zend_Paginator::setDefaultItemCountPerPage($limit); 
			
			$sum_cart = $sessionNamespace->sum_cart;// Zend_Debug::dump($sum_cart);exit;
			$this->view->sum_cart = $sum_cart;;
	
	}
    
	public function productstAction()
    {
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('product_st'); 
		
    } 
    public function categorystAction()
    {
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('category_st'); 
		
    } 
}
