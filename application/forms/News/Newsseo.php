<?php

/**
 * 
 *  
 * 
 */
class Default_Form_News_Newsseo extends Zend_Form
{
    public function init()
    {	
	    
        // Set the method for the display form to POST
        $this->setMethod('post');
		
	    // указываем имя формы
        $this->setName('news');
		$news_seo_id = new Zend_Form_Element_Hidden('news_seo_id');
        $news_seo_id->addFilter('Int');
          
        $header_tags_title = new Zend_Form_Element_Text('header_tags_title');
        $header_tags_title->setLabel("title:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');
            
        $header_tags_description = new Zend_Form_Element_Text('header_tags_description');
        $header_tags_description->setLabel("description:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');

        $header_tags_keywords = new Zend_Form_Element_Text('header_tags_keywords');
        $header_tags_keywords->setLabel("keywords:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');    
				
	//  вариант кнопки	
	//	$submit = new Zend_Form_Element_Submit("Зберегти");
    //  $submit->setAttrib('poetry_id', 'submitbutton');
	
        $this->addElements(array(
                                    $news_seo_id,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                ));
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'class' => 'btn btn-primary pull-left',
          'type' => 'submit',
          'ignore' => true,
        ));	 
    }
}

