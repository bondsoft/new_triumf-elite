<?php
/**
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminnewsController extends Zend_Controller_Action
{
	public function init()
	{
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
	}
	
	function indexAction()
	{
        $news_model = new Default_Model_DbTable_News();
		$news = $news_model->getAllNewsAdmin();
		
		$paginator = Zend_Paginator::factory($news);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);	   
	}
	
	public function additemAction()
    {
        $form = new Default_Form_News_AddNews();     
        $this->view->form = $form;
        $news_model = new Default_Model_DbTable_News();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                if(!$creation_date = $form->getValue('creation_date')) {
                    $creation_date = date('Y-m-d H:i:s');
                } 
                $news_title = $form->getValue('news_title');
                $intro_text = $form->getValue('intro_text');
                $news_text = $form->getValue('news_text');
                $url_seo = $form->getValue('url_seo');
				$header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
				 
                $news_model->addNews(
                                        $creation_date,
										$news_title,
										$intro_text,
										$news_text,
										$url_seo,
										$header_tags_title,
										$header_tags_description,
										$header_tags_keywords
									);
                 
                $this->_helper->redirector('index');
            } else { 
                $form->populate($formData);
            }
        }
    }
   
    public function edititemAction()
    {
        $form = new Default_Form_News_EditNews();     
        $this->view->form = $form;
        $news_model = new Default_Model_DbTable_News();
        
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
			{
                $news_id = (int)$form->getValue('news_id');
                if(!$creation_date = $form->getValue('creation_date')) {
                    $creation_date = date('Y-m-d H:i:s');
                } 
                $news_title = $form->getValue('news_title');
                $intro_text = $form->getValue('intro_text');
                $news_text = $form->getValue('news_text');
                $url_seo = $form->getValue('url_seo');
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
 
                $news_model->updateNews(
                                            $news_id, 
                                            $creation_date,
                                            $news_title,
                                            $intro_text,
                                            $news_text,
                                            $url_seo,
                                            $header_tags_title,
                                            $header_tags_description,
                                            $header_tags_keywords
                                        );
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $news_id = $this->_getParam('news_id', 0);
            if ($news_id > 0)
			{
                $form->populate($news_model->getNews($news_id));
            }
        }
    }
	
	public function deleteitemAction()
    {
		$news_model = new Default_Model_DbTable_News(); //  Zend_Debug::dump($article);exit;
		$newstext_model = new Default_Model_DbTable_Newstext();
        if ($this->getRequest()->isPost())
		{
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
			{
                $news_id = $this->getRequest()->getPost('news_id');
                
                $newstext_model->deleteAllPagesForNews($news_id);
                $news_model->deleteNews($news_id);
            }
            $this->_helper->redirector('index', 'adminnews', 'admin');
        } else {
            $news_id = $this->_getParam('news_id');
            
            $this->view->news = $news_model->getNews($news_id);
        }
    }
    
    function seoAction()
    {
        $newsseo_model = new Default_Model_DbTable_Newsseo();
        $newsseo = $newsseo_model->fetchAll();
        
        $paginator = Zend_Paginator::factory($newsseo);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);   
       
    }
    
    public function seoeditAction()
    {
        $form = new Default_Form_News_Newsseo();     
        $this->view->form = $form;
        $newsseo_model = new Default_Model_DbTable_Newsseo();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {
                $news_seo_id = (int)$form->getValue('news_seo_id'); 
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
 
                $newsseo_model->updateNewsseo(
                                                $news_seo_id,  
                                                $header_tags_title,
                                                $header_tags_description,
                                                $header_tags_keywords
                                            );
                
                $this->_helper->redirector('seo', 'adminnews', 'admin');
            } else {
                $form->populate($formData);
            }
        } else {   
            $news_seo_id = $this->_getParam('news_seo_id', 0);
            if ($news_seo_id > 0)
            {
                $form->populate($newsseo_model->getNewsseo($news_seo_id));
            }
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $news_model = new Default_Model_DbTable_News();
        
        if ($this->getRequest()->isPost())
        {
            $news_id = $this->getRequest()->getParam('news_id');
            $status = '1';
            $news_model->updateNewsstatus(
                                            $news_id,  
                                            $status
                                        );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $news_model = new Default_Model_DbTable_News();
        
        if ($this->getRequest()->isPost())
        {
            $news_id = $this->getRequest()->getParam('news_id');
            $status = '0';
            $news_model->updateNewsstatus(
                                            $news_id,  
                                            $status
                                        );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
}