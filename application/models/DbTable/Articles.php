<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Articles extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_articles';
	
	public function getArticle($article_id)
    {
        $article_id = (int)$article_id;
        $row = $this->fetchRow('article_id = ' . $article_id);
        if(!$row) {
            throw new Exception("Нет записи с article_id - $article_id");
        }
        return $row->toArray();
    }
	
	public function addArticle(
									$article_title,
									$url_seo,
									$intro_text,
									$full_text,
									$creation_date,
									$main_photo,
									$alt,
                                    $show_photo_inside,
									$header_tags_title,
									$header_tags_description,
									$header_tags_keywords,
									$show_our_work
								)
    {
        $data = array(
            'article_title' => $article_title,
            'url_seo' => $url_seo,
            'intro_text' => $intro_text,
            'full_text' => $full_text,
            'creation_date' => $creation_date,
			'main_photo' => $main_photo,
			'alt' => $alt,
            'show_photo_inside' => $show_photo_inside,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords,
			'show_our_work' => $show_our_work
        );
        $this->insert($data);
    }
	
	public  function updateArticle(
										$article_id,
										$article_title,
										$url_seo,
										$intro_text,
										$full_text,
										$creation_date,
										$main_photo,
										$alt,
                                        $show_photo_inside,
										$header_tags_title,
										$header_tags_description,
										$header_tags_keywords,
										$show_our_work
									)
    {
        $data = array( 
            'article_title' => $article_title,
            'url_seo' => $url_seo,
            'intro_text' => $intro_text,
            'full_text' => $full_text,
            'creation_date' => $creation_date,
			'main_photo' => $main_photo,
			'alt' => $alt,
            'show_photo_inside' => $show_photo_inside,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords,
			'show_our_work' => $show_our_work
        );
        
        $this->update($data, 'article_id = ' . (int)$article_id);
    }
	
	public function getAllArticlesAdmin()
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->order('article_id DESC');
     
		$articles = $this->fetchAll($select);
        return $articles;
    }
	
	public function getAllArticlesFront($show_our_work=0)
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->where('status = ?', '1')
					->where('show_our_work = ?', $show_our_work)
					->order('creation_date DESC');
     
		$articles = $this->fetchAll($select);
        return $articles;
    }
	
	public function getAllArticlesRanrom()
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->where('status = ?', '1')
					->order('RAND()')
					->limit(10);
     
		$articles = $this->fetchAll($select);
        return $articles;
    }
    
	public function getLastArticles()
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
					->where('prime = ?', '1')
                    ->order('creation_date DESC')
					->limit(2);
     
		$lastarticles = $this->fetchAll($select);
        return $lastarticles;
    }
   
	public function getMainPhotoFile($article_id)
	{
	    $_name = 'zend_articles';
        $select = $this->select()
                    ->from($_name)
					->where('article_id = ?', $article_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec['main_photo'];		
        return $main_photo_file_name;
    }
	
	public function getArticleTitle($url_seo)
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
		$article_title_rec = $this->fetchRow($select);	
        $article_title = $article_title_rec['article_title'];		
        return $article_title;
    }
    
	public function getArticleIntroText($url_seo)
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
		$article_rec = $this->fetchRow($select);	
        $full_text = $article_rec['intro_text'];		
        return $full_text;
    }
	
    public function getArticleIdByUrl($url_seo)
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
		$article_title_rec = $this->fetchRow($select);	
        $article_id = $article_title_rec['article_id'];		
        return $article_id;
    }
	
	public function deleteArticle($article_id)
    {
        $this->delete('article_id = ' . (int)$article_id);
    }
	
	public  function updateMainphoto(
										$article_id, 
										$main_photo
	                                )
    {
        $data = array(
			'main_photo' => $main_photo,
        );
        
        $this->update($data, 'article_id = ' . (int)$article_id);
    }
    
    public function getArticlePage($url_seo)
    {
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
        $news = $this->fetchAll($select);
        return $news;
    }
    
    public  function updateArticleStatus(
											$article_id,
											$status
										)
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'article_id = ' . (int)$article_id);
    }
    
    public  function updatePrimeArticle(
											$article_id,
											$prime
										)
    {
        $data = array( 
            'prime' => $prime  
        );
        
        $this->update($data, 'article_id = ' . (int)$article_id);
    }
	
	public function getArticleImage($article_id)
	{
	    $_name = 'zend_articles';
        $select = $this->select()
                    ->from($_name)
					->where('article_id = ?', $article_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["main_photo"];		
        return $main_photo_file_name;
    }
	
	public  function deleteArticleImage(
											$article_id,  
											$main_photo
										)
    {
        $data = array(
            'main_photo' => $main_photo,
        );
        
        $this->update($data, 'article_id = ' . (int)$article_id);
    }
}
