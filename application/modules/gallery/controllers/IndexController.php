<?php
/**
 * Contact controller
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class Gallery_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('page');
    }
	
    public function indexAction()
    {	
		$gallery_model = new Default_Model_DbTable_Gallery();
	
        $gallery = $gallery_model->getGalleryFront();
		$this->view->gallery = $gallery;
		
		$gallery_photo = array();
		foreach ($gallery as $key => $value) {
			$gallery_photo[$value->gallery_id] = $gallery_model->getPhotoGallery($value->gallery_id);
			 
		}
	//	Zend_Debug::dump($gallery_photo);exit;
		$this->view->gallery_photo = $gallery_photo;
		
		$galleryseo_model = new Default_Model_DbTable_Galleryseo();
        $galleryseo = $galleryseo_model->fetchAll();
        
        $page = Zend_Paginator::factory($galleryseo);
        
        $this->view->page = $page;
    }
}