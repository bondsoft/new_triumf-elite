<?php
/**
 * Contact controller
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class Page05_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('page');
    }
	
    public function indexAction()
    {	
		$page_model = new Default_Model_DbTable_Page05();
		$page = $page_model ->fetchAll();
		$this->view->page = $page;
		
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
    }
}