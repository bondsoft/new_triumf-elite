<?php

/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_PhotoController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
        
        $realestate_id = $this->getRequest()->getParam('realestate_id');
        $realestate_object = new Default_Model_DbTable_Realestate();
        $realestate_page = $realestate_object->getMainphoto_file($realestate_id);
        $this->view->realestate = $realestate_page; //Zend_Debug::dump($this->view->realestate);exit;
    }
	
    function indexAction()
    {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('photo/index.phtml');
        $photo_object = new Default_Model_DbTable_Photos();
        $realestate_id = $this->getRequest()->getParam('realestate_id');
		$this->view->realestate_id = $realestate_id;
		$photos = $photo_object->getPhoto_galary($realestate_id);
		//  Zend_Debug::dump($photos);exit;
		
		$paginator = Zend_Paginator::factory($photos);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);	

		//$realestate_object = new Default_Model_DbTable_Realestate();
		//$realestate_page = $realestate_object->getMainphoto_file($realestate_id);
		//$this->view->realestate = $realestate_page; //Zend_Debug::dump($this->view->realestate);exit;
    }
   
    public function addAction()
    {
	    include( 'SimpleImage.php' );
		
        $form = new Default_Form_Realestate_Photo();
        $this->view->form = $form;
		
		$values = $form->getValues();
	
        // Если к нам идёт Post запрос
        if ($this->getRequest()->isPost()) {
            // Принимаем его
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
			{
				$realestate_id = $this->getRequest()->getParam('realestate_id');
                $photo_file = $form->getValue('photo_file'); 
				
				if( empty($photo_file) )
                {
	                echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
					echo "Вы не выбрали файл!";
	                echo "</font></b>" ;
                    return;
                }
			
                $photo_object = new Default_Model_DbTable_Photos();
				
				$data = $_FILES['photo_file'];
				
				foreach ($data['type'] as $key => $value) // Zend_Debug::dump($value);exit;
                {	
                if(	$value != NULL )
                {				
				$uploadedfile = $data['tmp_name'][$key];   // Zend_Debug::dump($uploadedfile);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"][$key]); 
				    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name = "photos/".$newname;
					$name_l = "photos/l_".$newname;
					
				/* ********************************************** */
					
				    $image = new SimpleImage();
				    $image->load($uploadedfile);
					$image->resizeToWidth(720);
				    $image->save($name);
				//	$image->resizeToWidth(190);
                  //  $image->cutFromCenter(110, 73);
                    $image->cutFromCenter(300, 200);
				    $image->save($name_l);
					   
				} 
				
				$photo_object->addPhoto($realestate_id, $newname);
				}
				}
				// Уничтожаем файл во временном каталоге
                   unlink($_FILES['photo_file']['tmp_name']);
                
			   // Перенаправление на /admin/poetrytext/index/poetry_id/number
                  $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id));
            } else {
                // Если форма заполнена неверно,
                $this->view->errMessage = "ффффффффффффффф" ;
            }
        }
    }
	
	public function editphotoAction()
    {
	    
        $form = new Default_Form_Realestate_EditPhotoFile();    
        $this->view->form = $form;
		
		$realestate_id = $this->getRequest()->getParam('realestate_id');//Zend_Debug::dump($realestate_id);exit;
		$photo_id = $this->getRequest()->getParam('photo_id');// Zend_Debug::dump($photo_id);exit;
		
		$photo_object = new Default_Model_DbTable_Photos();
				
		$this->view->photo = $photo_object->getPhoto($photo_id); //Zend_Debug::dump($this->view->photo);exit;
		$old_photo_file = $photo_object->getPhoto_file($photo_id);//Zend_Debug::dump($old_photo_file);exit;
		  
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();
		   
		   	if ($form->isValid($formData) && $form->submit->isChecked())
			{
			    $photo_file = $form->getValue('photo_file');
				
			    $directory = "photos";
		    	
            
				if( empty($photo_file) )
                {
					$this->view->errMessage = "Вы не выбрали файл!";
                    return;
                }
                
                // удаляем файл из каталога
                if(is_file("$directory/$old_photo_file"))
                {
                    unlink("$directory/$old_photo_file");
                    unlink("$directory/l_$old_photo_file");
                }
				
				$data = $_FILES['photo_file'];
				$uploadedfile = $data['tmp_name']; //Zend_Debug::dump($uploadedfile);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
				    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name = "photos/".$newname;
					$name_l = "photos/l_".$newname;
					
				   /* ********************************************** */
					include( 'SimpleImage.php' );
				    $image = new SimpleImage();
				    $image->load($uploadedfile);
					$image->resizeToWidth(720);
				    $image->save($name);
					//$image->resizeToWidth(190);
				//	$image->cutFromCenter(110, 73);
					$image->cutFromCenter(300, 200);
				    $image->save($name_l);  
				} 
				
                $photo_object->updatePhoto_file($photo_id, $newname);
				
				// Уничтожаем файл во временном каталоге
                 unlink($_FILES['photo_file']['tmp_name']);
                
                $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id, 'photo_id' => $photo_id));
            }  else {
                $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id, 'photo_id' => $photo_id));
            }
        } else {
            // Если мы выводим форму, то получаем id записи, которую хотим обновить
            $photo_id = $this->_getParam('photo_id', 0);
            if ($photo_id > 0) {
                // Создаём объект модели
                $photo_object = new Default_Model_DbTable_Photos();
				
				$this->view->photo = $photo_object->getPhoto($photo_id);
                
                // Заполняем форму информацией при помощи метода populate
                $form->populate($photo_object->getPhoto($photo_id));
            }
        }
    }
	
	public function deleteAction()
    {
        // Если к нам идёт Post запрос
        if ($this->getRequest()->isPost()) {
		
            // Принимаем значение
            $del = $this->getRequest()->getPost('del'); // Zend_Debug::dump($del);exit;
			
			//получаем galary_id из браузера
                $realestate_id = $this->getRequest()->getParam('realestate_id');
				$photo_id = $this->getRequest()->getParam('photo_id');
            
            // Если пользователь поддтвердил своё желание удалить запись
            if ($del == 'yes') {
                
				// Создаём объект модели
                $photo_object = new Default_Model_DbTable_Photos();
				// получаем имя удаляемого файла
				$photo_file = $photo_object->getPhoto_file($photo_id);
				
			//	Zend_Debug::dump($photo_file);exit;
			
				$directory = "photos";
				// удаляем файл из каталога
				if(is_file("$directory/$photo_file"))
				{
     		        unlink("$directory/$photo_file");
					unlink("$directory/l_$photo_file");
				}	
                
              //  Zend_Debug::dump($photo_file);exit;
               
                // Вызываем метод модели для удаления записи
                $photo_object->deletePhoto($photo_id);
            }
            $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id));
        } else {
            // Если запрос не Post, выводим сообщение для поддтвержения
            // Получаем id записи, которую хотим удалить
            $photo_id = $this->_getParam('photo_id');
            
            // Создаём объект модели
            $photo_object = new Default_Model_DbTable_Photos();
            
            // Достаём запись и передаём в view
            $this->view->photo = $photo_object->getPhoto($photo_id);
        }
    }
	
	public function addmainphotoAction()
    {
		$mainphoto_form = new Default_Form_Realestate_MainPhoto(); 
        $this->view->form = $mainphoto_form;
		$realestate_id = $this->getRequest()->getParam('realestate_id');
	//	Zend_Debug::dump($realestate_id);exit;
        if ($this->getRequest()->isPost()) {  
            $formData = $this->getRequest()->getPost();  
            if ($mainphoto_form->isValid($formData)) 
			{
				$main_photo = $mainphoto_form->getValue('main_photo');
			//	Zend_Debug::dump($main_photo);exit;
				if( empty($main_photo) )
                {
	                echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
					echo "Вы не выбрали файл!";
	                echo "</font></b>" ;
                    return;
                }
				               
                $model_object = new Default_Model_DbTable_Realestate(); 
				
				$data = $_FILES['main_photo']; // Zend_Debug::dump($data);exit;
				$uploadedfile = $data['tmp_name']; 
             //				Zend_Debug::dump($uploadedfile_mainphoto);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
				    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name = "images/main_photos/".$newname;
					$name_l = "images/main_photos/l_".$newname;
					
				   /* ********************************************** */
					include( 'SimpleImage.php' );
				    $image = new SimpleImage();
				    $image->load($uploadedfile);
					$image->resizeToWidth(615);
				    $image->save($name);
					$image->cutFromCenter(280, 187);
				    $image->save($name_l);  
				}  
                
                $model_object->updateMainphoto($realestate_id, 
				                               $newname
						                      );
				
				unlink($_FILES['uploadedfile_mainphoto']['tmp_name']);
                
                $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id));
            } else {
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editmainphotoAction()
    {
        $form = new Default_Form_Realestate_EditMainPhotoFile();    
        $this->view->form = $form;
        
        $realestate_id = $this->getRequest()->getParam('realestate_id');
        $realestate_object = new Default_Model_DbTable_Realestate();
        $realestate_mainphoto_file = $realestate_object->getMainphoto_file($realestate_id);
        $this->view->realestate = $realestate_mainphoto_file; //Zend_Debug::dump($this->view->realestate);exit;
        
       
               
        $old_photo_file = $realestate_object->getMainphoto_file($realestate_id); //Zend_Debug::dump($old_photo_file);exit;
          
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
           
            if ($form->isValid($formData) && $form->submit->isChecked())
            {
                $main_photo = $form->getValue('main_photo');
                
                $directory = "images/main_photos";
               
            
                if( empty($main_photo) )
                {
                    $this->view->errMessage = "Вы не выбрали файл!";
                    return;
                }
                // удаляем файл из каталога
                    if(is_file("$directory/$old_photo_file"))
                    {
                    unlink("$directory/$old_photo_file");
                    unlink("$directory/l_$old_photo_file");
                    }
                
                $data = $_FILES['main_photo'];  //Zend_Debug::dump($data);exit;
                $uploadedfile = $data['tmp_name']; //Zend_Debug::dump($uploadedfile);exit;
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name = "images/main_photos/".$newname;
                    $name_l = "images/main_photos/l_".$newname;
                    
                   /* ********************************************** */
                    include( 'SimpleImage.php' );
                    $image = new SimpleImage();
                    $image->load($uploadedfile);
                    $image->resizeToWidth(615);
                    $image->save($name);
                    $image->cutFromCenter(280, 187);
                    $image->save($name_l);  
                } 
                
                $realestate_object->updateMainphoto($realestate_id, 
                                               $newname
                                              );
                
                // Уничтожаем файл во временном каталоге
                 unlink($_FILES['main_photo']['tmp_name']);
                
                $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id, 'photo_id' => $photo_id));
            }  else {
                $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id, 'photo_id' => $photo_id));
            }
        } else {
            // Если мы выводим форму, то получаем id записи, которую хотим обновить
            $realestate_id = $this->_getParam('realestate_id', 0);
            if ($realestate_id > 0) {
                // Создаём объект модели
                $realestate_object = new Default_Model_DbTable_Realestate();
                
                $this->view->realestate_object = $realestate_object->getRealestate($realestate_id);
                
                // Заполняем форму информацией при помощи метода populate
                $form->populate($realestate_object->getRealestate($realestate_id));
            }
        }
    }
	
	public function deletemainphotoAction()
    {
        $realestate_id = $this->getRequest()->getParam('realestate_id');
        $realestate_object = new Default_Model_DbTable_Realestate();
        $realestate_page = $realestate_object->getMainphoto_file($realestate_id);
        $this->view->realestate = $realestate_page; //Zend_Debug::dump($this->view->realestate);exit;
        if ($this->getRequest()->isPost()) {
        
            // Принимаем значение
            $del = $this->getRequest()->getPost('del'); // Zend_Debug::dump($del);exit;
            
            //получаем galary_id из браузера
                $realestate_id = $this->getRequest()->getParam('realestate_id');
            
            // Если пользователь поддтвердил своё желание удалить запись
            if ($del == 'yes') {
                
                
                // получаем имя удаляемого файла
                $old_photo_file = $realestate_object->getMainphoto_file($realestate_id); //Zend_Debug::dump($old_photo_file);exit;
                $main_photo == NULL;
                
            //  Zend_Debug::dump($old_photo_file);exit;
            
                $directory = "images/main_photos";
                // удаляем файл из каталога
                if(is_file("$directory/$old_photo_file"))
                {
                    unlink("$directory/$old_photo_file");
                    unlink("$directory/l_$old_photo_file");
                }   
                
              //  Zend_Debug::dump($old_photo_file);exit;
               
                // Вызываем метод модели для удаления записи
                $realestate_object->updateMainphoto($realestate_id, 
                                                    $main_photo
                                                    );
            }
            $this->_helper->redirector('index', 'photo', 'admin', array('realestate_id' => $realestate_id));
        } else {
            // Если запрос не Post, выводим сообщение для поддтвержения
            // Получаем id записи, которую хотим удалить
            $realestate_id = $this->_getParam('realestate_id');
            
            // Создаём объект модели
            $realestate_object = new Default_Model_DbTable_Realestate();
            
            // Достаём запись и передаём в view
            $this->view->main_photo = $realestate_object->getRealestate($realestate_id); //Zend_Debug::dump( $this->view->main_photo);exit;
        }
    }
    

   
}