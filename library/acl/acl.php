<?php

/**
 * 
 * 
 * 
 * 
 */
require_once 'Zend/Acl.php';
$acl = new Zend_Acl();


	
require_once 'Zend/Acl/Resource.php';	
//$acl->add(new Zend_Acl_Resource('blog_index'));	
$acl->addResource('blog_index');
$acl->addResource('add', 'blog_index');

require_once 'Zend/Acl/Role.php';
$acl->addRole(new Zend_Acl_Role('guest'))
    ->addRole(new Zend_Acl_Role('member'))
    ->addRole(new Zend_Acl_Role('admin'));
	
$acl->allow('admin');

$acl->allow('member');

$acl->deny('guest');

Zend_Registry::set('acl', $acl);