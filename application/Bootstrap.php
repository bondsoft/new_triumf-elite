<?php
/**
 * Application bootstrap
 * 
 * @uses    Zend_Application_Bootstrap_Bootstrap
 * @package info.aspp@yandex.ru
 */
require_once 'acl/acl.php';

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Bootstrap autoloader for application resources
     * 
     * @return Zend_Application_Module_Autoloader
     */
    protected function _initAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Default',
            'basePath'  => dirname(__FILE__),
        ));
        return $autoloader;
    }
    
    public function _initRoutes()  
    {  
    
        $this->bootstrap('FrontController');  
        $this->_frontController = $this->getResource('FrontController');  
        $router = $this->_frontController->getRouter(); 
     
        
      
        $langRoute = new Zend_Controller_Router_Route( ':lang/',  
            array(  
                'lang' => 'ru'  
            )  
        ); 
    /*    $langRoute = new Zend_Controller_Router_Route(':lang/',
            array(
                'lang'  => 'ru'
            ),
            array(
                'lang'  => '^(en|ru)$'
            )
        ); */
        
        $homeRouter = new Zend_Controller_Router_Route( '/page/:page',
            array(
                'controller'=>'index',  
                'action'=>'index',
                'page' =>''
            )
        );
		
		/* event begin */
        
        $eventRouter = new Zend_Controller_Router_Route_Static( 'events',
                array(
                    'module'=>'events',
                    'controller' => 'index',
                    'action' => 'index'
                )
        ); 
        
        $event_itemRouter = new Zend_Controller_Router_Route( 'event-single/:url_seo',
                array(
                    'module'=>'events',
                    'controller' => 'index',
                    'action' => 'event',
                    'url_seo' =>''
                )
        ); 
        
        $event_item_pageRouter = new Zend_Controller_Router_Route( 'events/page/:page',
                array(
                    'module'=>'events',
                    'controller' => 'index',
                    'action' => 'index',
                    'url_seo' =>'',
                    'page' =>''
                )
        ); 
		
		/* event end */
		/* article begin */
		
		$articleRouter = new Zend_Controller_Router_Route_Static( 'news',
                array(
                    'module'=>'articles',
                    'controller' => 'index',
                    'action' => 'index',
					'show_our_work' => 0
                )
        ); 
        
        $article_itemRouter = new Zend_Controller_Router_Route( 'news-single/:url_seo',
                array(
                    'module'=>'articles',
                    'controller' => 'index',
                    'action' => 'article',
                    'url_seo' =>''
                )
        );
		
		$our_workRouter = new Zend_Controller_Router_Route_Static( 'our_work',
                array(
                    'module'=>'articles',
                    'controller' => 'index',
                    'action' => 'index',
                    'show_our_work' => 1
                )
        );
        
        $article_item_pageRouter = new Zend_Controller_Router_Route( 'news/page/:page',
                array(
                    'module'=>'articles',
                    'controller' => 'index',
                    'action' => 'index',
                    'url_seo' =>'',
                    'page' =>'',
					'show_our_work' => 0
                )
        ); 
		
		$our_work_item_Router = new Zend_Controller_Router_Route_Static( 'our_work/page/:page',
                array(
                    'module'=>'articles',
                    'controller' => 'index',
                    'action' => 'index',
                    'url_seo' =>'',
                    'page' =>'',
					'show_our_work' => 1
                )
        );
		
		/* article end */

        /* stock begin */
        
        $stockRouter = new Zend_Controller_Router_Route_Static( 'stocks',
                array(
                    'module'=>'stocks',
                    'controller' => 'index',
                    'action' => 'index'
                )
        ); 
        
        $stock_itemRouter = new Zend_Controller_Router_Route( 'stock-single/:url_seo',
                array(
                    'module'=>'stocks',
                    'controller' => 'index',
                    'action' => 'stock',
                    'url_seo' =>''
                )
        ); 
        
        $stock_item_pageRouter = new Zend_Controller_Router_Route( 'stocks/page/:page',
                array(
                    'module'=>'stocks',
                    'controller' => 'index',
                    'action' => 'index',
                    'url_seo' =>'',
                    'page' =>''
                )
        ); 
        /* stock end */
		
	/*	$newsRouter = new Zend_Controller_Router_Route_Static( 'news',
            array( 
                'module'=>'news',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
    
        $news_itemRouter = new Zend_Controller_Router_Route( 'news-single/:url_seo',
                array(
                    'module'=>'news',
                    'controller' => 'index',
                    'action' => 'news',
                    'url_seo' =>''
                )
        ); 
        
        $news_pageRouter = new Zend_Controller_Router_Route( 'news/page/:page',  
                array(
                    'module'=>'news',
                    'controller'=>'index',
                    'action'=>'index',
                    'page' =>''
                )  
        );
        
        $news_item_pageRouter = new Zend_Controller_Router_Route( 'news-single/:url_seo/page/:page',
                array(
                    'module'=>'news',
                    'controller' => 'index',
                    'action' => 'news',
                    'url_seo' =>'',
                    'page' =>''
                )
        ); */
		
		/* partners-articles begin */
		
		$partnersarticlesRouter = new Zend_Controller_Router_Route_Static( 'partners-articles',
                array(
                    'module'=>'partnersarticles',
                    'controller' => 'index',
                    'action' => 'index'
                )
        ); 
        
        $partnersarticle_itemRouter = new Zend_Controller_Router_Route( 'partners-article-single/:url_seo',
                array(
                    'module'=>'partnersarticles',
                    'controller' => 'index',
                    'action' => 'article',
                    'url_seo' =>''
                )
        ); 
        
        $partnersarticles_item_pageRouter = new Zend_Controller_Router_Route( 'partners-articles/page/:page',
                array(
                    'module'=>'partnersarticles',
                    'controller' => 'index',
                    'action' => 'index',
                    'url_seo' =>'',
                    'page' =>''
                )
        ); 
		/* partners-articles end */
		/* partners-science begin */
		
		$partnersscienceRouter = new Zend_Controller_Router_Route_Static( 'partners-science',
                array(
                    'module'=>'partnersscience',
                    'controller' => 'index',
                    'action' => 'index'
                )
        ); 
        
        $partnersscience_itemRouter = new Zend_Controller_Router_Route( 'partners-science-single/:url_seo',
                array(
                    'module'=>'partnersscience',
                    'controller' => 'index',
                    'action' => 'article',
                    'url_seo' =>''
                )
        ); 
        
        $partnersscience_item_pageRouter = new Zend_Controller_Router_Route( 'partners-science/page/:page',
                array(
                    'module'=>'partnersscience',
                    'controller' => 'index',
                    'action' => 'index',
                    'url_seo' =>'',
                    'page' =>''
                )
        ); 
		/* partners-science end */
		/* partners-calendar begin */
		
		$partnerscalendarRouter = new Zend_Controller_Router_Route_Static( 'partners-calendar',
                array(
                    'module'=>'partnerscalendar',
                    'controller' => 'index',
                    'action' => 'index'
                )
        ); 
        
		/* partners-calendar end */
		/* forum begin */
		
		$forumRouter = new Zend_Controller_Router_Route_Static( 'forum',
                array(
                    'module'=>'forum',
                    'controller' => 'index',
                    'action' => 'index'
                )
        ); 
        
        $forum_itemRouter = new Zend_Controller_Router_Route( 'forum-single/:url_seo',
                array(
                    'module'=>'forum',
                    'controller' => 'index',
                    'action' => 'article',
                    'url_seo' =>''
                )
        ); 
        
        $forum_item_pageRouter = new Zend_Controller_Router_Route( 'forum/page/:page',
                array(
                    'module'=>'forum',
                    'controller' => 'index',
                    'action' => 'index',
                    'url_seo' =>'',
                    'page' =>''
                )
        ); 
		/* forum end */
		$pageStomatologiya = new Zend_Controller_Router_Route( '/stomatologiya',
            array( 
                'module'=>'page06',
                'controller'=>'index',
                'action'=>'index',
                'page_id' => 2,
            )
        );
        $pageKosmetologiya = new Zend_Controller_Router_Route( '/kosmetologiya',
            array( 
                'module'=>'page06',
                'controller'=>'index',
                'action'=>'index',
                'page_id' => 3,
            )
        );

        $pagePrices = new Zend_Controller_Router_Route( '/prices',
            array( 
                'module'=>'page06',
                'controller'=>'index',
                'action'=>'index',
                'page_id' => 4,
            )
        );
        
        $page06Router = new Zend_Controller_Router_Route( '/about/videos',
            array( 
                'module'=>'page06',
                'controller'=>'index',
                'action'=>'index',
                'page_id' => 1,
            )
        );
		$aboutRouter = new Zend_Controller_Router_Route_Static( '/about',
            array( 
                'module'=>'about',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
		
		$contactsRouter = new Zend_Controller_Router_Route_Static( 'contacts',  
                array(
                    'module'=>'contacts',
                    'controller'=>'index',
                    'action'=>'index'
                )  
        );
       
        $page01Router = new Zend_Controller_Router_Route_Static( '/reception-hours',
            array( 
                'module'=>'page01',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
		
		$page02Router = new Zend_Controller_Router_Route_Static( '/legal-information',
            array( 
                'module'=>'page02',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
        
        $page03Router = new Zend_Controller_Router_Route_Static( '/clinic-tour-1',
            array( 
                'module'=>'page03',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
		
		$page04Router = new Zend_Controller_Router_Route_Static( '/clinic-tour-2',
            array( 
                'module'=>'page04',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
        
        $page05Router = new Zend_Controller_Router_Route_Static( '/politica-zashity-persinalnyh-dannyh',
            array( 
                'module'=>'page05',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
        
        
        
       
         
		
		/* video begin */
		
		$videoRouter = new Zend_Controller_Router_Route_Static( 'video',
            array( 
                'module'=>'video',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
    
        $video_itemRouter = new Zend_Controller_Router_Route( 'video-single/:url_seo',
                array(
                    'module'=>'video',
                    'controller' => 'index',
                    'action' => 'video',
                    'url_seo' =>''
                )
        ); 
        
        $video_pageRouter = new Zend_Controller_Router_Route( 'video/page/:page',  
                array(
                    'module'=>'video',
                    'controller'=>'index',
                    'action'=>'index',
                    'page' =>''
                )  
        );
		
		/* video end */
		/* partners-video begin */
		
		$partnersvideoRouter = new Zend_Controller_Router_Route_Static( 'partners-video',
            array( 
                'module'=>'partnersvideo',  
                'controller'=>'index',  
                'action'=>'index'
            )
        );
    
        $partnersvideo_itemRouter = new Zend_Controller_Router_Route( 'partners-video-single/:url_seo',
                array(
                    'module'=>'partnersvideo',
                    'controller' => 'index',
                    'action' => 'video',
                    'url_seo' =>''
                )
        ); 
        
        $partnersvideo_pageRouter = new Zend_Controller_Router_Route( 'partners-video/page/:page',  
                array(
                    'module'=>'partnersvideo',
                    'controller'=>'index',
                    'action'=>'index',
                    'page' =>''
                )  
        );
		
		/* partners-video end */
		/* slider begin */
		
        $slider_itemRouter = new Zend_Controller_Router_Route( '/our-team/:slider_url',
                array(
                    'module'=>'sliderthree',
                    'controller' => 'index',
                    'action' => 'slider',
                    'slider_url' =>''
                )
        ); 
        
		/* slider end */
		/* catalog begin */
		$booksRouter = new Zend_Controller_Router_Route_Static( 'books',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'books'
            )
        );
		$bookitemRouter = new Zend_Controller_Router_Route_Static( 'book-item',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'bookitem'
            )
        );
		
		$product_stRouter = new Zend_Controller_Router_Route_Static( 'productst',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'productst'
            )
        );
		
		$category_stRouter = new Zend_Controller_Router_Route_Static( 'services',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'categoryst'
            )
        );
		
		
		
		$category_itemRouter = new Zend_Controller_Router_Route( '/category/:url_seo',
                array(
                    'module'=>'category',
                    'controller' => 'index',
                    'action' => 'category',
                    'url_seo' =>''
                )
        );
        
        $catalog_pageRouter = new Zend_Controller_Router_Route( '/category/:url_seo/page/:page',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'productsofcategory',
                'url_seo' =>'',
                'page' =>''
            )
        );
		
		
		
		
		$consultationsRouter = new Zend_Controller_Router_Route_Static( 'consultations',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'consultations'
            )
        );
		
		$consultationitemRouter = new Zend_Controller_Router_Route_Static( 'consultation-item',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'consultationitem'
            )
        );
		
		$productRouter = new Zend_Controller_Router_Route( 'products/:url_seo',
            array( 
                'module'=>'catalog',  
                'controller'=>'index',  
                'action'=>'product',
				'url_seo' =>''
            )
        );
		
		$basketRouter = new Zend_Controller_Router_Route_Static( 'basket',  
                array(
                    'module'=>'basket',
                    'controller'=>'index',
                    'action'=>'index'
                )  
        );
        
        $checkoutRouter = new Zend_Controller_Router_Route_Static( 'checkout',  
                array(
                    'module'=>'basket',
                    'controller'=>'index',
                    'action'=>'checkout'
                )  
        );
		
		$checkoutcardRouter = new Zend_Controller_Router_Route_Static( 'checkoutcard',  
                array(
                    'module'=>'basket',
                    'controller'=>'index',
                    'action'=>'checkoutcard'
                )  
        );
		
		$checkoutsuccessRouter = new Zend_Controller_Router_Route_Static( 'checkoutsuccess',  
                array(
                    'module'=>'basket',
                    'controller'=>'index',
                    'action'=>'checkoutsuccess'
                )  
        );
        
        $orderRouter = new Zend_Controller_Router_Route( 'order/orders_id/:orders_id',
            array( 
                'module'=>'basket',  
                'controller'=>'index',  
                'action'=>'add',
                'orders_id' =>''
            )
        );
		
	/*	$checkoutRouter = new Zend_Controller_Router_Route_Static( 'payment_ru.html',  
                array(
                    'module'=>'checkout',
                    'controller'=>'index',
                    'action'=>'index'
                )  
        );*/
		/* catalog end */
        $adminRouter = new Zend_Controller_Router_Route_Static( 'admin',  
                array(
                    'module'=>'admin',
                    'controller'=>'index',
                    'action'=>'index'
                )  
        ); 
		
		$adminordersRouter = new Zend_Controller_Router_Route( 'admin/adminorders/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminorders',
                    'action'=>'index',
                    'page' =>''
                )  
        );
		
		$adminnewstextRouter = new Zend_Controller_Router_Route( 'admin/adminnewstext/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminnewstext',
                    'action'=>'index',
                    'page' =>''
                )  
        );
		
		$adminarticlesRouter = new Zend_Controller_Router_Route( 'admin/adminarticles/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminarticles',
                    'action'=>'index',
                    'page' =>''
                )  
        );

        $adminstocksRouter = new Zend_Controller_Router_Route( 'admin/adminstocks/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminstocks',
                    'action'=>'index',
                    'page' =>''
                )  
        );
		
		$admincategoresRouter = new Zend_Controller_Router_Route( 'admin/categores/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'categores',
                    'action'=>'index',
                    'page' =>''
                )  
        );

		$adminmessagesRouter = new Zend_Controller_Router_Route( 'admin/adminmessages/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminmessages',
                    'action'=>'index',
                    'page' =>''
                )  
        ); 
        
        $adminproductsRouter = new Zend_Controller_Router_Route( 'admin/adminproducts/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminproducts',
                    'action'=>'index',
                    'page' =>''
                )  
        ); 
		
		$admincategoryphotoRouter = new Zend_Controller_Router_Route( 'admin/categoryphoto/index/category_id/:category_id/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'categoryphoto',
                    'action'=>'index',
					'slider_id'=>'',
                    'page' =>''
                )  
        ); 
        
        $adminsliderthreephotoRouter = new Zend_Controller_Router_Route( 'admin/adminsliderthreephoto/index/slider_id/:slider_id/page/:page',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminsliderthreephoto',
                    'action'=>'index',
                    'slider_id'=>'',
                    'page' =>''
                )  
        ); 
		
		$adminaboutRouter = new Zend_Controller_Router_Route_Static( 'admin/about',  
                array(
                    'module'=>'admin',
                    'controller'=>'about',
                    'action'=>'index'
                )  
        );  

        $adminpage06Router = new Zend_Controller_Router_Route_Static( 'admin/services',  
                array(
                    'module'=>'admin',
                    'controller'=>'page06',
                    'action'=>'index'
                )  
        );  
		
		$adminservicesRouter = new Zend_Controller_Router_Route_Static( 'admin/adminservices',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminservices',
                    'action'=>'index'
                )  
        );

		$admindocumentsRouter = new Zend_Controller_Router_Route_Static( 'admin/admindocuments',  
                array(
                    'module'=>'admin',
                    'controller'=>'admindocuments',
                    'action'=>'index'
                )  
        );
        
        $adminrequestRouter = new Zend_Controller_Router_Route_Static( 'admin/adminrequest',  
                array(
                    'module'=>'admin',
                    'controller'=>'adminrequest',
                    'action'=>'index'
                )  
        );

		$adminusersRouter = new Zend_Controller_Router_Route_Static( 'admin/users',  
                array(
                    'module'=>'admin',
                    'controller'=>'users',
                    'action'=>'index'
                )  
        );
		
		$adminsettingsRouter = new Zend_Controller_Router_Route_Static( 'admin/settings',  
                array(
                    'module'=>'admin',
                    'controller'=>'settings',
                    'action'=>'index'
                )  
        );
		
		$adminlangRouter = new Zend_Controller_Router_Route_Static( 'admin/lang',  
                array(
                    'module'=>'admin',
                    'controller'=>'lang',
                    'action'=>'index'
                )  
        );
		
		$adminusersRouter = new Zend_Controller_Router_Route_Static( 'admin/users',  
                array(
                    'module'=>'admin',
                    'controller'=>'users',
                    'action'=>'index'
                )  
        );
        
        $admintownsRouter = new Zend_Controller_Router_Route_Static( 'admin/admintowns',  
                array(
                    'module'=>'admin',
                    'controller'=>'admintowns',
                    'action'=>'index'
                )  
        );
        
        $adminloginRouter = new Zend_Controller_Router_Route_Static( 'login',  
                array(
                    'module'=>'user',
                    'controller'=>'login',
                    'action'=>'login'
                )  
        ); 
        
        $adminlogoutRouter = new Zend_Controller_Router_Route_Static( 'adminlogout',  
                array(
                    'module'=>'user',
                    'controller'=>'login',
                    'action'=>'logout'
                )  
        );  
		
		$logoutRouter = new Zend_Controller_Router_Route_Static( 'logout',  
                array(
                    'module'=>'members',
                    'controller'=>'login',
                    'action'=>'logout'
                )  
        );
		
		$memberslogoutRouter = new Zend_Controller_Router_Route_Static( 'memberslogout',  
                array(
                    'module'=>'members',
                    'controller'=>'login',
                    'action'=>'logout'
                )  
        ); 
		
		/* account begin */
		$logoutRouter = new Zend_Controller_Router_Route_Static( 'logout',  
                array(
                    'module'=>'members',
                    'controller'=>'login',
                    'action'=>'logout'
                )  
        );
		
		$accountRouter = new Zend_Controller_Router_Route_Static( 'account',  
                array(
                    'module'=>'members',
                    'controller'=>'account',
                    'action'=>'index'
                )  
        );
		
		$addavatarRouter = new Zend_Controller_Router_Route( '/add-avatar/user_id/:user_id',  
                array(
                    'module'=>'members',
                    'controller'=>'account',
                    'action'=>'addavatar',
					'user_id' =>''
                )  
        );
		
		$deleteavatarRouter = new Zend_Controller_Router_Route( '/delete-avatar/user_id/:user_id',  
                array(
                    'module'=>'members',
                    'controller'=>'account',
                    'action'=>'deleteavatar',
					'user_id' =>''
                )  
        );
		
		$updatepassRouter = new Zend_Controller_Router_Route( '/updatepass/user_id/:user_id',  
                array(
                    'module'=>'members',
                    'controller'=>'account',
                    'action'=>'updatepass',
					'user_id' =>''
                )  
        );
		
		$deleteaccountRouter = new Zend_Controller_Router_Route( '/delete-account/user_id/:user_id',  
                array(
                    'module'=>'members',
                    'controller'=>'account',
                    'action'=>'delete',
					'user_id' =>''
                )  
        );
		
        
		
		/* account end */
        

        $router->addRoute('langRoute', $langRoute); 
		
		$router->addRoute('homeRouter', $homeRouter);
		$router->addRoute('eventRouter', $eventRouter);
		$router->addRoute('event_itemRouter', $event_itemRouter);
		$router->addRoute('event_item_pageRouter', $event_item_pageRouter);
		$router->addRoute('articleRouter', $articleRouter);
		$router->addRoute('our_workRouter', $our_workRouter);
		$router->addRoute('our_work_item_Router', $our_work_item_Router);
		$router->addRoute('article_itemRouter', $article_itemRouter);
		$router->addRoute('article_item_pageRouter', $article_item_pageRouter);

        $router->addRoute('stockRouter', $stockRouter);
        $router->addRoute('stock_itemRouter', $stock_itemRouter);
        $router->addRoute('stock_item_pageRouter', $stock_item_pageRouter);

		$router->addRoute('partnersarticlesRouter', $partnersarticlesRouter);
		$router->addRoute('partnersarticle_itemRouter', $partnersarticle_itemRouter);
		$router->addRoute('partnersarticles_item_pageRouter', $partnersarticles_item_pageRouter);
		$router->addRoute('partnersscienceRouter', $partnersscienceRouter);
		$router->addRoute('partnersscience_itemRouter', $partnersscience_itemRouter);
		$router->addRoute('partnersscience_item_pageRouter', $partnersscience_item_pageRouter);
		$router->addRoute('partnerscalendarRouter', $partnerscalendarRouter);
	//	$router->addRoute('newsRouter', $newsRouter);
    //    $router->addRoute('news_pageRouter', $news_pageRouter);
    //    $router->addRoute('news_itemRouter', $news_itemRouter);
    //    $router->addRoute('news_item_pageRouter', $news_item_pageRouter);
		$router->addRoute('aboutRouter', $aboutRouter);
		$router->addRoute('page01Router', $page01Router);
		$router->addRoute('page02Router', $page02Router);
		$router->addRoute('page03Router', $page03Router);
		$router->addRoute('page04Router', $page04Router);
		$router->addRoute('page05Router', $page05Router);
		$router->addRoute('page06Router', $page06Router);
        $router->addRoute('pagePrices', $pagePrices);
        $router->addRoute('pageStomatologiya', $pageStomatologiya);
        $router->addRoute('pageKosmetologiya', $pageKosmetologiya);
		$router->addRoute('contactsRouter', $contactsRouter);
		
		$router->addRoute('product_stRouter', $product_stRouter);
		$router->addRoute('category_stRouter', $category_stRouter);
		
		
		$router->addRoute('category_itemRouter', $category_itemRouter);
		
		
		
		$router->addRoute('videoRouter', $videoRouter);
        $router->addRoute('video_pageRouter', $video_pageRouter);
        $router->addRoute('video_itemRouter', $video_itemRouter);
		
		$router->addRoute('partnersvideoRouter', $partnersvideoRouter);
        $router->addRoute('partnersvideo_pageRouter', $partnersvideo_pageRouter);
        $router->addRoute('partnersvideo_itemRouter', $partnersvideo_itemRouter);
		
		$router->addRoute('slider_itemRouter', $slider_itemRouter);
		
		
        $router->addRoute('catalog_pageRouter', $catalog_pageRouter);
        $router->addRoute('productRouter', $productRouter);
        $router->addRoute('basketRouter', $basketRouter);
        $router->addRoute('checkoutRouter', $checkoutRouter);
        $router->addRoute('orderRouter', $orderRouter);
		
	/*	$router->addRoute('booksRouter', $booksRouter);
		$router->addRoute('bookitemRouter', $bookitemRouter);
		$router->addRoute('consultationsRouter', $consultationsRouter);
		$router->addRoute('consultationitemRouter', $consultationitemRouter);
		$router->addRoute('productRouter', $productRouter);
		$router->addRoute('basketRouter', $basketRouter);
        $router->addRoute('checkoutRouter', $checkoutRouter);
		$router->addRoute('checkoutcardRouter', $checkoutcardRouter);
        $router->addRoute('orderRouter', $orderRouter);
		$router->addRoute('checkoutsuccessRouter', $checkoutsuccessRouter); */
        
        $router->addRoute('adminRouter', $adminRouter);
		$router->addRoute('adminordersRouter', $adminordersRouter); 
		$router->addRoute('adminarticlesRouter', $adminarticlesRouter);

        $router->addRoute('adminstocksRouter', $adminstocksRouter);

		$router->addRoute('adminnewstextRouter', $adminnewstextRouter);
		$router->addRoute('adminaboutRouter', $adminaboutRouter); 
        $router->addRoute('adminpage06Router', $adminpage06Router); 
		$router->addRoute('adminservicesRouter', $adminservicesRouter);
		$router->addRoute('admindocumentsRouter', $admindocumentsRouter);
		$router->addRoute('adminrequestRouter', $adminrequestRouter);
		$router->addRoute('admincategoresRouter', $admincategoresRouter); 
		$router->addRoute('adminusersRouter', $adminusersRouter); 
		$router->addRoute('adminsettingsRouter', $adminsettingsRouter);
		$router->addRoute('adminlangRouter', $adminlangRouter); 
		$router->addRoute('adminmessagesRouter', $adminmessagesRouter);
		$router->addRoute('adminproductsRouter', $adminproductsRouter);
		$router->addRoute('admincategoryphotoRouter', $admincategoryphotoRouter);
		$router->addRoute('adminsliderthreephotoRouter', $adminsliderthreephotoRouter);
		$router->addRoute('admintownsRouter', $admintownsRouter);
       
		$router->addRoute('adminloginRouter', $adminloginRouter);
		$router->addRoute('adminlogoutRouter', $adminlogoutRouter);
		$router->addRoute('memberslogoutRouter', $memberslogoutRouter);
		
		$router->addRoute('logoutRouter', $logoutRouter);
		$router->addRoute('accountRouter', $accountRouter);
		$router->addRoute('addavatarRouter', $addavatarRouter);
		$router->addRoute('deleteavatarRouter', $deleteavatarRouter);
		$router->addRoute('updatepassRouter', $updatepassRouter);
		$router->addRoute('deleteaccountRouter', $deleteaccountRouter);
          
    }   
    /**
     * Bootstrap the view doctype
     * 
     * @return void
     */
    protected function _initPlaceholders()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_TRANSITIONAL');
		Zend_Layout::startMvc();
		
		$view = new Zend_View();
		$view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
		 
		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
		$viewRenderer->setView($view);
		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
		
	//	$view->headMeta()->appendHttpEquiv('Contakt - Type', 'text/phtml:charset->utf-8');
		
	//	Zend_View_Helper_PaginationControl::setDefaultViewPartial('controls.phtml');
			
    }
	
	protected function _initLocale()
	{
	    $route = $_SERVER['REQUEST_URI'];
			
            if ($var = strstr($route, 'ru'))
			{ 
			    $lang = 'ru';
			}
			elseif ($var = strstr($route, 'en'))
			{ 
			    $lang = 'en';
				
			}
		    else {
			    $lang = 'ru';
		    }	
			
		/*	$adapter = new Zend_Translate(
                array(
                    'adapter' => 'array',
                    'content' => '../application/languages/'.$lang.'/'.$lang.'.php',
                    'locale'  => $lang
                )
            ); */
			
            
        $config = new Zend_Config(
        /*array( 
            'database' => array(
                'adapter' => 'PDO_MYSQL',
                'params'  => array(
                    'host'     => 'p330789.mysql.ihc.ru',
                    'dbname'   => 'p330789_trielit',
                    'username' => 'p330789_trielit',
                    'password' => '56st4Jb9pm',
                    'charset' => 'UTF8',
                )
            )
        )*/
		array( 
            'database' => array(
                'adapter' => 'PDO_MYSQL',
                'params'  => array(
                    'host'     => 'localhost',
                    'dbname'   => 'triumfelite_db',
                    'username' => 'triumfelite_db',
                    'password' => 'b8rfgg56z2Rs',
                    'charset' => 'UTF8',
                )
            )
        )
        );
 
        $dbAdapter = Zend_Db::factory($config->database);//Zend_Debug::dump($dbAdapter);exit;
      
        $settings = new Default_Model_DbTable_Settings( array('db' => $dbAdapter)); //Zend_Debug::dump($translations);exit;
        $email = $settings->getConfigEmail(); //Zend_Debug::dump($email); //exit;
        
            $translations = new Default_Model_DbTable_Translations( array('db' => $dbAdapter)); //Zend_Debug::dump($translations);exit;
            $data = $translations->getTranslations($lang); //Zend_Debug::dump($data);exit;
            
            $adapter = new Zend_Translate(
                array(
                    'adapter' => 'db',
					'content' => $data,
                    'locale'  => $lang
                )
            ); 
			
			$translate = new Zend_View_Helper_Translate($adapter);
	
        Zend_Registry::set('Zend_Lang', $lang);
		Zend_Registry::set('Zend_Adapter', $adapter);
		Zend_Registry::set('Zend_Translate', $translate);
        Zend_Registry::set('Email', $email);
	}
	
		
}
