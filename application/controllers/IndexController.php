<?php
class IndexController extends Zend_Controller_Action
{
    public function init()
    {
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('index');

    }
	
    public function indexAction()
    {
    	
    	
		//$pieces = explode("/", $_SERVER['REQUEST_URI']);
		// BS  - замена, чтобы не учитывались GET-параметры (utm-метки)
    	$bsUrlParts = explode("?", $_SERVER['REQUEST_URI']);
    	$pieces = explode("/", $bsUrlParts[0]);
    	
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
		if($pieces[1] == '') {
			$layout->setLayout('index');
		} else {
            $this->getResponse()->setHttpResponseCode(404);
			$layout->setLayout('page_not_found_layout');
		}
	//	$this->_helper->redirector('index', 'login', 'index');
        $sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
		$sum_cart = $sessionNamespace->sum_cart;// Zend_Debug::dump($sum_cart);exit;
		$this->view->sum_cart = $sum_cart;
		
		$homepage_model = new Default_Model_DbTable_Homepage();
		$page_text = $homepage_model->getPageText();
	    $this->view->page = $page_text;
		
		$intro_text = $homepage_model->getIntroHomeTextFront();
	    $this->view->intro_text = $intro_text;
    }
	
	public function headAction()
    {   
        
    }

    public function mainheadmenuAction()
    {   
        
    } 
	
	public function headerAction()
    {   
        
    } 
	
	public function footerAction()
    {   
        
    }
    
    public function lastnewsAction()
    {   
        $news_model = new Default_Model_DbTable_News();
        $lastnews = $news_model->getLastNews();
        $this->view->lastnews = $lastnews;
    } 
    
    public function introhometextAction()
    {   
		$homepage_model = new Default_Model_DbTable_Homepage();

        $introhometext = $homepage_model->getIntrohometextFront(); 
        $this->view->introhometext = $introhometext;    
    }
    
    public function hometextAction()
    {   
		$homepage_model = new Default_Model_DbTable_Homepage();

        $hometext = $homepage_model->getHometestFront(); 
        $this->view->hometext = $hometext;    
    }
    
    public function manufacturersAction()
    {   
		$manufacturers_model = new Default_Model_DbTable_Manufacturers();

        $manufacturers = $manufacturers_model->getAllManufacturersFront(); 
        $this->view->manufacturers = $manufacturers;    
    }
	
	public function latestAction()
    {   
		$products_model = new Default_Model_DbTable_Products();

        $products = $products_model->getLatestProductsFront(); 
        $this->view->products = $products;    
    }
    
    public function specialAction()
    {   
		$products_model = new Default_Model_DbTable_Products();

        $products = $products_model->getSpecialProductsFront(); 
        $this->view->products = $products;    
    }
    
    public function recommendedAction()
    {   
		$products_model = new Default_Model_DbTable_Products();

        $products = $products_model->getRecommendedProductsFront(); 
        $this->view->products = $products;    
    }
    
    public function documentsAction()
    {   
        $lang = Zend_Registry::get('Zend_Lang');

        $documents_model = new Default_Model_DbTable_Documents();
        $page_text = $documents_model->getDocuments_text($lang);
        $this->view->page_text = $page_text;    
    }
    
    function messagesAction()
    {   
        
    }
   
    public function categoriesAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();

        $categories_01 = $categories_model->getAllFirstLevelCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
			$children_data = array();
			$children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
                            'url_seo'  => $child['url_seo']
                        );
				$children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  //Zend_Debug::dump($children_lv3); exit;
				foreach ($children_lv3 as $child_lv3) {
					$children_lv3_data[] = array(
								'category_name'  => $child_lv3['category_name'],
								'url_seo'  => $child_lv3['url_seo']
							);
					
				}
            }
			if(@$children_lv3)
			{
				
				
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'children_lv3' => $children_lv3_data,
						'url_seo'     => $category['url_seo']
					);
			} else {
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'url_seo'     => $category['url_seo']
					);
			}
			
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
    }
	
    public function servmenuAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();

        $categories_01 = $categories_model->getAllFirstLevelCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
            $children_data = array();
            $children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $childKey => $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
                            'url_seo'  => $child['url_seo']
                        );
                $children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  //Zend_Debug::dump($children_lv3); exit;
                foreach ($children_lv3 as $child_lv3Key => $child_lv3) {
                    $children_lv3_data[] = array(
                                'category_name'  => $child_lv3['category_name'],
                                'url_seo'  => $child_lv3['url_seo']
                            );
                    $children_data[$childKey]['children_lv3'][] = array(
                                'category_name'  => $child_lv3['category_name'],
                                'url_seo'  => $child_lv3['url_seo']
                            );
                }
            }
            if(@$children_lv3)
            {
                
                
                $categories[][] = array(
                        'category_name'     => $category['category_name'],
                        'children' => $children_data,
                        'children_lv3' => $children_lv3_data,
                        'url_seo'     => $category['url_seo']
                    );
            } else {
                $categories[][] = array(
                        'category_name'     => $category['category_name'],
                        'children' => $children_data,
                        'url_seo'     => $category['url_seo']
                    );
            }
            
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
    }

    public function kosmenuAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();

        $categories_01 = $categories_model->getAllFirstLevelCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
            $children_data = array();
            $children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $childKey => $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
                            'url_seo'  => $child['url_seo']
                        );
                $children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  //Zend_Debug::dump($children_lv3); exit;
                foreach ($children_lv3 as $child_lv3Key => $child_lv3) {
                    $children_lv3_data[] = array(
                                'category_name'  => $child_lv3['category_name'],
                                'url_seo'  => $child_lv3['url_seo']
                            );
                    $children_data[$childKey]['children_lv3'][] = array(
                                'category_name'  => $child_lv3['category_name'],
                                'url_seo'  => $child_lv3['url_seo']
                            );
                }
            }
            if(@$children_lv3)
            {
                
                
                $categories[][] = array(
                        'category_name'     => $category['category_name'],
                        'children' => $children_data,
                        'children_lv3' => $children_lv3_data,
                        'url_seo'     => $category['url_seo']
                    );
            } else {
                $categories[][] = array(
                        'category_name'     => $category['category_name'],
                        'children' => $children_data,
                        'url_seo'     => $category['url_seo']
                    );
            }
            
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
    }
    
	public function categoriesbottomAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();

        $categories_01 = $categories_model->getAllFirstLevelCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
			$children_data = array();
			$children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
							'main_photo'  => $child['main_photo'],
                            'url_seo'  => $child['url_seo']
                        );
				$children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  //Zend_Debug::dump($children_lv3); exit;
				foreach ($children_lv3 as $child_lv3) {
					$children_lv3_data[] = array(
								'category_name'  => $child_lv3['category_name'],
								'url_seo'  => $child_lv3['url_seo']
							);
					
				}
            }
			if(@$children_lv3)
			{
				
				
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'children_lv3' => $children_lv3_data,
						'url_seo'     => $category['url_seo']
					);
			} else {
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'main_photo'  => $category['main_photo'],
						'children' => $children_data,
						'url_seo'     => $category['url_seo']
					);
			}
			
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
    }
	
	public function townsAction()
    {   
        $form = new Default_Form_Towns_TownsSelect(); 
        $this->view->form = $form;  
    }
	
	public function townselectAction()
    {   
		$request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $towns_model = new Default_Model_DbTable_Towns();
		
        if ($this->getRequest()->isPost())
        {
            $town_id = $this->getRequest()->getParam('city_id');
		//	Zend_Debug::dump($town_id);exit; 
            $town_object = $towns_model->getTown( $town_id );
			
            $phone_number = $town_object['phone_number'];
			$work_schedule = $town_object['work_schedule'];
           
        //   Zend_Debug::dump($phone_number);exit;                        
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true, 'phone_number' => $phone_number, 'work_schedule' => $work_schedule));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
     
    public function getpriceAction()
    {   
        $form = new Default_Form_Contact_Getprice(); 
        $this->view->form = $form;  
    }
	
	function contactAction()
    {   
        $form = new Default_Form_Contact_Contact(); 
        $this->view->form = $form; 
       
    }
	
	function showthenumberAction()
    {   
		$limit = $this->getRequest()->getParam('limit');
        $form = new Default_Form_Products_SelectNumber(); 
        $this->view->form = $form; 
		$form->populate( array('limit' => $limit) );
       
    }
    
    function productfilterAction()
    {
	//	$limit = $this->getRequest()->getParam('limit');
        $form = new Default_Form_Products_ProductFilter(); 
        $this->view->form = $form; 
	//	$form->populate( array('limit' => $limit) );
       
    }
    
    public function categoriessliderAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();
        
        $categories_bags = $categories_model->getBagsCategoriesForSlider();

        $categories_01 = $categories_model->getBagsCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
			$children_data = array();
			$children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
                            'url_seo'  => $child['url_seo']
                        );
				$children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  //Zend_Debug::dump($children_lv3); exit;
				foreach ($children_lv3 as $child_lv3) {
					$children_lv3_data[] = array(
								'category_name'  => $child_lv3['category_name'],
								'url_seo'  => $child_lv3['url_seo']
							);
                    
					
				}
            }
			if($children_lv3)
			{
				
				
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'children_lv3' => $children_lv3_data,
						'url_seo'     => $category['url_seo']
					);
			} else {
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'url_seo'     => $category['url_seo']
					);
			}
			
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
        $this->view->categories_bags = $categories_bags;
    }
    
    public function creationcategoriessliderAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();

        $categories_01 = $categories_model->getCreationCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
			$children_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
							'main_photo'  => $child['main_photo'],
                            'url_seo'  => $child['url_seo']
                        );
            }
            $categories[][] = array(
                    'category_name'     => $category['category_name'],
					'main_photo'  => $category['main_photo'],
                    'children' => $children_data,
                    'url_seo'     => $category['url_seo']
                );
        }
      //  Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
    }
    
    public function educationcategoriessliderAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();

        $categories_01 = $categories_model->getEducationCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
			$children_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
							'main_photo'  => $child['main_photo'],
                            'url_seo'  => $child['url_seo']
                        );
            }
            $categories[][] = array(
                    'category_name'     => $category['category_name'],
					'main_photo'  => $category['main_photo'],
                    'children' => $children_data,
                    'url_seo'     => $category['url_seo']
                );
        }
      //  Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
    }
	
	function accountformAction()
    {
        $form_account = new Default_Form_Members_Account();
		$this->view->form_account = $form_account;
    } 
	
	function loginformAction()
    {
        $form_login = new Default_Form_Members_Login();
		$this->view->form_login = $form_login;
    } 
	
	function fogotpassformAction()
    {
        $form_fogotpass = new Default_Form_Members_Fogotpass();
		$this->view->form_fogotpass = $form_fogotpass;
    } 
}