<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Productsattributes extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_product_attribute';
	
	public function getProduct($product_id)
    {
        $product_id = (int)$product_id;
        $row = $this->fetchRow('product_id = ' . $product_id);
        if(!$row) {
            throw new Exception("no record product_id - $product_id");
        }
        return $row->toArray();
    }
	
	public function addProduct(
                                $category_id,
                                $manufacturer_id,
                                $sort_order,
                                $product_model,
                                $price,
                                $main_photo,
                                $product_description,
                                $url_seo,
                                $header_tags_title,
                                $header_tags_description,
                                $header_tags_keywords                                                         
                            )
    {
        $data = array(
			'category_id' => $category_id,
			'manufacturer_id' => $manufacturer_id,
			'sort_order' => $sort_order,
			'product_model' => $product_model,
			'price' => $price,
			'main_photo' => $main_photo,
			'product_description' => $product_description,
			'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords,
			'creation_date' => date('Y-m-d H:i:s')
        );
        $this->insert($data);
    }
	
	public function deleteKiosks($kiosks_id)
    {
        $this->delete('kiosks_id = ' . (int)$kiosks_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateProduct( 
                                    $product_id,
                                    $category_id,
                                    $manufacturer_id,
                                    $sort_order,
                                    $product_model,
                                    $price,
                                    $main_photo,
                                    $product_description,
                                    $url_seo,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords     
								 )
    {
        $data = array(
            'category_id' => $category_id,
            'manufacturer_id' => $manufacturer_id,
            'sort_order' => $sort_order,
            'product_model' => $product_model,
            'price' => $price,
            'main_photo' => $main_photo,
            'product_description' => $product_description,
            'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
    
    public function getAllKiosks()
    {
        $_name = 'zend_products';
        $_name_01 = 'kiosks_types';
        $_name_02 = 'software';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.kiosks_types_id = name_01.kiosks_types_id')
                    ->joinLeft(array('name_02' => $_name_02),'name.software_id = name_02.software_id', array('model', 'software_main_photo', 'software_seo'))
                    ->order('kiosks_id DESC');
                   // ->limit(4);
                    
        $all_software = $this->fetchAll($select);
        return $all_software;
    }
    
    public function getAllProductsAdmin()
    {
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';
    //    $_name_02 = 'software';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                 //   ->where('status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('text'))
                    ->joinLeft(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getProductsAttributesAdmin($product_id)
    {  
        $_name = 'zend_products';
        $_name_01 = 'zend_category';
        $_name_02 = 'zend_product_attribute';
        $_name_03 = 'zend_attributes';  
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.product_id = ?', $product_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('category_name'))
                    ->joinLeft(array('name_02' => $_name_02),'name.product_id = name_02.product_id', array('attribute_id', 'text'))
                    ->joinRight(array('name_03' => $_name_03),'name_02.attribute_id = name_03.attribute_id', array('attribute_id', 'attribute_name'))
                    ->order('product_id DESC');
                     
        $all_products = $this->fetchAll($select);
        return $all_products;
    }
    
    public function getKioskSeo($kiosk_seo)
    {
        $_name = 'zend_products';
        $_name_01 = 'kiosks_types';
        $_name_02 = 'software';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.status = ?', '1')
                    ->joinLeft(array('name_01' => $_name_01),'name.kiosks_types_id = name_01.kiosks_types_id')
                    ->joinLeft(array('name_02' => $_name_02),'name.software_id = name_02.software_id', array('model', 'software_main_photo', 'software_seo'))
                    ->where('kiosk_seo = ?', $kiosk_seo);

        $kiosk = $this->fetchAll($select);    
         
        return $kiosk;
       
    }
    
    public function getKiosksWeight($cookie_name)
    {
        $_name = 'zend_products';
        $select = $this->select()
                    ->from(array('name' => $_name), array('weight', 'price'))
                    ->where('kiosks_model = ?', $cookie_name);

        $kiosk = $this->fetchAll($select);    
         
        return $kiosk;
       
    }
    
	public function getPhotos($kiosks_id)
	{
        $_name = 'zend_products';
		$_name_01 = 'kiosks_photos';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('name.kiosks_id = ?', $kiosks_id)
					->join(array('name_01' => $_name_01),'name.kiosks_id = name_01.kiosks_id');
     
		$object = $this->fetchAll($select);
        return $object;
    }
	
	public  function deleteProductImage(
                                            $product_id,  
                                            $main_photo
                                        )
    {
        $data = array(
            'main_photo' => $main_photo,
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
	
	public function getMainphoto($kiosks_id)
	{
        $_name = 'zend_products';
		$_name_01 = 'category';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->where('kiosks_id = ?', $kiosks_id)
					->join(array('name_01' => $_name_01),'name.category_id = name_01.category_id');
     
		$pdf = $this->fetchAll($select);
        return $pdf;
    }
	
	public function getProductImage($product_id)
	{
	    $_name = 'zend_products';
        $select = $this->select()
                    ->from($_name)
					->where('product_id = ?', $product_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["main_photo"];		
        return $main_photo_file_name;
    }
 
	
    public function getSimilarObjects($similarmodels)
    {
        $_name = 'zend_products';
        $_name_01 = 'kiosks_types';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
                    ->where('monitor LIKE ?', $similarmodels)
                    ->joinLeft(array('name_01' => $_name_01),'name.kiosks_types_id = name_01.kiosks_types_id')
                    ->order('kiosks_id DESC')
                    ->limit(4);
                     
        $all_software = $this->fetchAll($select);
        return $all_software;
    }

    public  function updateProductstatus(
                                            $product_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'product_id = ' . (int)$product_id);
    }
    
    public  function addProductsAttributes( 
                                                $product_id,
                                                $attribute_id,
                                                $text      
                                            )
    {
        $data = array(
            'product_id' => $product_id,
            'attribute_id' => $attribute_id,
            'text' => $text
        );
        
        $this->insert($data);
    }
    
    public  function deleteProductsAttributes(
                                                $product_id
                                            )
    {
        $this->delete('product_id = ' . (int)$product_id);
    }
	
	public  function deleteAttributeAjax(
                                            $product_id,
											$attribute_id
                                        )
    {
		
        $this->delete(array(
            'product_id = ?' => $product_id,
            'attribute_id = ?' => $attribute_id
        ));
    } 
	
	public function checkFirstAttribute($attribute_id)
    {
        $_name_01 = 'zend_product_attribute';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('attribute_id = ?', $attribute_id)
                    ->limit(1);

        $product_attribute_object = $this->fetchRow($select);    
        $product_attribute_model = $product_attribute_object["product_id"];     
        return $product_attribute_model;
    }
/*	
DELETE FROM `sasha_zend_shop`.`zend_product_attribute` WHERE `zend_product_attribute`.`product_id` = 42 AND `zend_product_attribute`.`attribute_id` = 12 */
}
