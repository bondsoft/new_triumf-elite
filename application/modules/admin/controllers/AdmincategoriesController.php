<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdmincategoriesController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    function indexAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();

        $categories_01 = $categories_model->getAllFirstLevelCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
            $children_data = array();
			$children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
						//	'category_group'     => $category['category_group'],
                            'category_name'  => $child['category_name'],
                            'main_photo'  => $child['main_photo'],
                            'url_seo'  => $child['url_seo'],
                            'category_id'  => $child['category_id'],
                            'sort_order'  => $child['sort_order']
                        );
						
				$children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  
				foreach ($children_lv3 as $child_lv3) {
					$children_lv3_data[] = array(
							//	'category_group'     => $category['category_group'],
								'category_name'  => $child_lv3['category_name'],
								'main_photo'  => $child_lv3['main_photo'],
								'url_seo'  => $child_lv3['url_seo'],
								'category_id'  => $child_lv3['category_id'],
								'sort_order'  => $child_lv3['sort_order']
							);
					
				}
            }
			if(@$children_lv3)
			{
				$categories[][] = array(
						//'category_group'     => $category['category_group'],
						'category_name'     => $category['category_name'],
						'main_photo'  => $category['main_photo'],
						'children' => $children_data,
						'children_lv3' => $children_lv3_data,
						'url_seo'     => $category['url_seo'],
						'category_id'  => $category['category_id'],
						'sort_order'  => $category['sort_order']
					);
			} else {
				$categories[][] = array(
					//	'category_group'     => $category['category_group'],
						'category_name'     => $category['category_name'],
						'main_photo'  => $category['main_photo'],
						'children' => $children_data,
						'url_seo'     => $category['url_seo'],
						'category_id'  => $category['category_id'],
						'sort_order'  => $category['sort_order']
					);
			}
			
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
       
    }
   
    public function addcategoryAction()
    {
        include( 'SimpleImage.php' );
        $form = new Default_Form_Categories_AddCategory();
        $this->view->form = $form;
      //  Zend_Debug::dump($form);exit;
        $categories_model = new Default_Model_DbTable_Categories();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $parent_id = $form->getValue('parent_id');
                $sort_order = $form->getValue('sort_order');
                $category_name = $form->getValue('category_name');    //Zend_Debug::dump($menu_first_level_name_ru);exit;
				$title = $form->getValue('title');
				$title_text = $form->getValue('title_text');
                $category_text = $form->getValue('category_text');
				$category_text_extra = $form->getValue('category_text_extra');
                $url_seo = $form->getValue('url_seo'); //Zend_Debug::dump($menu_first_level_seo);exit;
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                $main_photo = $form->getValue('main_photo');
                $data = $_FILES['main_photo']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $data['tmp_name']; 
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/categories/big_".$newname;
                    $name_small = "media/photos/categories/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->resizeToWidth(350);
                    $image_small->save($name_small);  
                }  
                
                if(!$parent_id) { $parent_id = 0; }
                $categories_model->addCategory(
                                                $parent_id,
                                                $sort_order,
												$category_name,
												$newname,
												$title,
												$title_text,
												$category_text,
												$category_text_extra,
												$url_seo,
												$header_tags_title,
												$header_tags_description,
												$header_tags_keywords                                                         
                                            );
                                            
                unlink($_FILES['image']['tmp_name']);                            
                    
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }
   
   
    public function editcategoryAction()
    {
        include( 'SimpleImage.php' );
        $form = new Default_Form_Categories_EditCategory();
        $this->view->form = $form;
      //  Zend_Debug::dump($category_id);exit;
        $category_id = $this->getRequest()->getParam('category_id');
        $categories_model = new Default_Model_DbTable_Categories();
        $category_image = $categories_model->getCategoryImage($category_id);
        $this->view->category_image = $category_image; 
        $this->view->category_id = $category_id; 
        
        
        $old_photo_file = $categories_model->getCategoryImage($category_id);
        $directory = "media/photos/categories";
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {  
                $category_id = $form->getValue('category_id');
                $parent_id = $form->getValue('parent_id');
                $sort_order = $form->getValue('sort_order');
                $category_name = $form->getValue('category_name');    //Zend_Debug::dump($menu_first_level_name_ru);exit;
				$title = $form->getValue('title');
				$title_text = $form->getValue('title_text');
                $category_text = $form->getValue('category_text');
				$category_text_extra = $form->getValue('category_text_extra');
                $url_seo = $form->getValue('url_seo'); //Zend_Debug::dump($menu_first_level_seo);exit;
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                
                $main_photo = $form->getValue('main_photo');
                $data = $_FILES['main_photo']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $data['tmp_name']; 
                        //  Zend_Debug::dump($uploadedfile);exit;
                if($main_photo)
                {       
                    if(is_file("$directory/small_$old_photo_file"))
                    {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                    }
                }
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/categories/big_".$newname;
                    $name_small = "media/photos/categories/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->resizeToWidth(350);
                    $image_small->save($name_small);  
                } 
                
                if(!$main_photo)
                {      
                    $categories_object = $categories_model->getCategory($category_id);
                    $newname = $categories_object['main_photo'];
                
                }
 
                $categories_model->updateCategory(
                                                    $category_id, 
                                                    $parent_id,
                                                    $sort_order,
                                                    $category_name,
                                                    $newname,
													$title,
													$title_text,
                                                    $category_text,
													$category_text_extra,
                                                    $url_seo,
                                                    $header_tags_title,
                                                    $header_tags_description,
                                                    $header_tags_keywords
                                                );
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $category_id = $this->_getParam('category_id', 0);
            if ($category_id > 0)
            {
                $form->populate($categories_model->getCategory($category_id));
                $categories_object = $categories_model->getCategory($category_id);
                $this->view->category_name = $categories_object['category_name'];
            }
        }
    }
    
    public function deletecategoryAction()
    {
        $categories_model = new Default_Model_DbTable_Categories();
        $category_id = $this->getRequest()->getParam('category_id');
        $page = $this->getRequest()->getPost('page');
        if ($this->getRequest()->isPost())
        {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
            {
                $category_id = $this->getRequest()->getPost('category_id');
                $category_object = $categories_model->getCategory($category_id);
            
                $this->view->category_object = $category_object;
				
				$category_check = $categories_model->checkActivCategory($category_id);
              //  Zend_Debug::dump($category_check);exit;
				if($category_check != NULL){
                    $this->view->errMessage = "В этой категории есть товары. Удалить нельзя!";
                    return;
                }
                $parent = $categories_model->getParent($category_id);
             // Zend_Debug::dump($subcategory_second_level_name);exit;
                if($parent != NULL){
                    $this->view->errMessage = "Эта категория активна. Удалить нельзя!";
                    return;
                } else {
                    $categories_model->deleteCategory($category_id);
                    
                    $old_photo_file = $categories_model->getMainPhotoFile($category_id);//Zend_Debug::dump($old_photo_file);exit;
                    $directory = "media/photos/categories";
                    // удаляем файл из каталога
                    if(is_file("$directory/small_$old_photo_file"))
                    {
                        unlink("$directory/big_$old_photo_file");
                        unlink("$directory/small_$old_photo_file");
                    } 
                } 
               
            }
            $this->_helper->redirector('index', 'admincategories', 'admin', array('page' => $page));
        } else {
            $category_id = $this->_getParam('category_id');
            $category = $categories_model->getCategory($category_id);
            
            $this->view->category = $category;
        }
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $categories_model = new Default_Model_DbTable_Categories();
        $category_id = $this->getRequest()->getParam('category_id');
        
        $old_photo_file = $categories_model->getCategoryImage($category_id);
        $directory = "media/photos/categories";
        
        if ($this->getRequest()->isPost())
        {
            $category_id = $this->getRequest()->getParam('category_id');
            $main_photo = '';
           
            $categories_model->deleteCategoryImage(
                                                    $category_id,  
                                                    $main_photo
                                                );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
}