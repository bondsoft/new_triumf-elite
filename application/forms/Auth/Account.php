<?php

/**
 * 
 *
 *
 */
class Default_Form_Auth_Account extends Zend_Form
{

  public function init()
  {     
        $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		// указываем имя формы
        $this->setName('signup');
		
		$this->addElement('Select', 'user_role', array(
            'label' => $translate->translate('status:'),
            'class' => 'span3',
            'MultiOptions' => array(     
                    'guest' => "Не активен", 
					'admin' => "Активен"					
                    ),  
        ));  
		
		// Element: username
        $this->addElement('Text', 'username', array(
          'label' => $translate->translate('Username:'),
		  'size'  => '30', 
		  'attribs' => array('class' => 'span5'),
          'required' => true,
          'filters'    => array('StringTrim'),
		  'validators' => array(
            array('NotEmpty', true),
			array('Db_NoRecordExists', false, array('zend_users', 'username'))
          ),
        ));        
    	
		// Element: email
        $this->addElement('Text', 'email', array(
          'label' => $translate->translate('Email Address:'),
		  'size'  => '30',
		  'attribs' => array('class' => 'span5'),
          'required' => true,
          'allowEmpty' => false,
          'validators' => array(
            array('NotEmpty', true),
            array('EmailAddress', true),
			array('Db_NoRecordExists', false, array('zend_users', 'email'))
          ),
        ));
		 	
        // Init password
      $this->addElement('Password', 'password', array(
        'label' => $translate->translate('Password:'),
		'size'  => '30',
		'attribs' => array('class' => 'span5'),
     //   'description' => $translate->translate('Passwords must be at least 6 characters in length.'),
        'required' => true,
        'allowEmpty' => false,
        'validators' => array(
          array('NotEmpty', true),
          array('StringLength', false, array(6, 32)),
        )
      ));
    
      // Init confirm password
      $this->addElement('Password', 'passconf', array(
        'label' => $translate->translate('Password Again:'),
		'size'  => '30',
		'attribs' => array('class' => 'span5'),
      //  'description' => $translate->translate('Enter your password again for confirmation.'),
        'required' => true,
        'validators' => array(
          array('NotEmpty', true),
		  array('identical', false, array('token' => 'password'))
        )
      ));
	  
	  
      	
		// Init submit
        $this->addElement('Button', 'submit', array(
          'label' => 'Сохранить',
          'class' => 'btn btn-primary pull-left',
          'type' => 'submit',
          'ignore' => true,
        ));
				
        // And finally add some CSRF protection
        $this->addElement('hash', 'csrf', array(
            'ignore' => true,
        ));
        
    }
  
}
