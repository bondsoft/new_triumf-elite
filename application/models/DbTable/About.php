<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_About extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_about';
    
    public function getPage($page_id)
    {
        $page_id = (int)$page_id;
        $row = $this->fetchRow('page_id = ' . $page_id);
        if(!$row) {
            throw new Exception("Нет записи с page_id - $page_id");
        }
        return $row->toArray();
    }
	
    public  function updatePage(
                                    $page_id,
                                    $text,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                )
    {
        $data = array(
            'text' => $text,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'page_id = ' . (int)$page_id);
    }
    
    
	public function getAboutPage()
	{
        $_name = 'zend_about';
        $select = $this->select()
                    ->from($_name);
     
		$text = $this->fetchAll($select);
        return $text;
    }
   
}
