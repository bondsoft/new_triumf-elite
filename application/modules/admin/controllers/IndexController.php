<?php

class Admin_IndexController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
	
	public function indexAction()
    {
        $homepage_model = new Default_Model_DbTable_Homepage();
		$page_text = $homepage_model ->getPageText();
		$this->view->page_text = $page_text;
    }
	
    public function editAction()
    {
        $form = new Default_Form_Home_Home();     
        $this->view->form = $form;
        $homepage_model = new Default_Model_DbTable_Homepage();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
           //  Zend_Debug::dump($formData);exit;
   
            if ($form->isValid($formData)) {
         
                $id = (int)$form->getValue('id');
                $intro_text = $form->getValue('intro_text');
				$text = $form->getValue('text');
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                        
				$page_text = $homepage_model->getPageText();
		        $this->view->page_text = $page_text;

                $homepage_model->updateHomepage(
                                                    $id,
													$intro_text,
                                                    $text,
                                                    $header_tags_title,
                                                    $header_tags_description,
                                                    $header_tags_keywords
                                                );

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $id = $this->_getParam('id', 0);
            if ($id > 0) {
                $form->populate($homepage_model->getHomepage($id));
            }
        }
    }
}

