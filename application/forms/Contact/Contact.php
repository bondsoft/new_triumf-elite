<?php

/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_Contact_Contact extends Zend_Form
{
    public function init()
    {	
	    
        // Element: username
        $this->addElement('Text', 'name', array(
            'label' => '',
            'value' => '',
            'size'  => '30', 
            'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('NotEmpty', true),
            ),
         //   'attribs' => array('class' => 'form-control form-textfields', 'placeholder' => 'Представтесь...', 'required id' => 'callbackName', 'tabindex' => '1'),
            'attribs' => array('class' => '', 'id' => 'name', 'placeholder' => ''),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        ));   
	
		// Element: email
        $this->addElement('Text', 'email', array(
            'label' => '',
            'value' => '', 
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array('NotEmpty', true),
                array('EmailAddress', true)
            ),
        //    'attribs' => array('class' => 'form-control form-textfields form-valid', 'placeholder' => 'Е-мейл для ответа...', 'id' => 'callbackEmail', 'tabindex' => '2'),
            'attribs' => array('class' => 'email', 'id' => 'email'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        )); 
        
        $this->addElement('Text', 'phone', array(
            'label' => '',
          //  'value' => 'Ваш телефон...', 
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array('notEmpty', true, array(
                    'messages' => array(
                        'isEmpty'   =>  'Phone can\'t be empty'
                    )
                )),
                array('StringLength', false, array(18, 18, 'messages' => array(
                            'stringLengthInvalid'           => "Phone Length Invalid entry",
                            'stringLengthTooShort'          => "Phone Invalid Length , ex. 1234567890"
                    ))),
            ),
        //    'attribs' => array('class' => 'form-control form-textfields form-valid', 'placeholder' => 'Ваш телефон...', 'id' => 'callbackEmail', 'tabindex' => '2'),
            'attribs' => array('class' => 'phone phone-mask', 'id' => 'phone', 'placeholder' => ''),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        )); 
       
        // Element: textarea
        $this->addElement('textarea', 'message', array(
            'value' => '',
          //  'required' => true,
          //  'allowEmpty' => false,
		    'rows' => '6',
		    'cols' => '80',
		  /*  'validators' => array(
		        array('NotEmpty', true),
                array('StringLength', true, 'options' => array(10, 5000)),	
                ), */
            'attribs' => array('class' => 'comment', 'placeholder' => ''), 
   
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                ),    
		    'filters'     => array('StringTrim')
        ));
				
		$element = $this->getElement('message');
        $element->removeDecorator('Label'); 
       
        $user_file = new Zend_Form_Element_File('user_file');
        $user_file
          //  ->setDescription("Допустимые форматы: jpeg, jpg, png, gif")
          //  ->setAttrib('size', '50')
            ->addValidator('Extension', false, 'jpeg,jpg,png,gif,txt,doc')
            ->setDestination('media/upload');  
            
     //   $this->addElements(array($user_file));    
        
		// Init submit
        $this->addElement('Button', 'submit', array(
            'label' => 'отправить',
            'type' => 'submit',
            'ignore' => true,
            'attribs' => array('class' => 'btn submit_formdata'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
                            
           // 'class'  => 'ajax-validate'
        ));
		$element = $this->getElement('submit');
        $element->removeDecorator('Label');		
        
    }
}

