<?php

class SimpleImage
{
    var $image;
    var $image_type;

    function load($filename)
    {
	    $image_info = getimagesize($filename);
		$this->image_type = $image_info[2];
		    if( $this->image_type == IMAGETYPE_JPEG )
			{ 
			    $this->image = imagecreatefromjpeg($filename);  
            } 
			elseif( $this->image_type == IMAGETYPE_GIF )
            {			
                $this->image = imagecreatefromgif($filename);	
            }
            elseif( $this->image_type == IMAGETYPE_PNG )
            {			
                $this->image = imagecreatefrompng($filename);	
            }			
    }  

    function save($filename, $image_type = IMAGETYPE_JPEG, $compression = 75, $permissions = null)
    {
		if( $image_type == IMAGETYPE_JPEG )
		{ 
			imagejpeg($this->image, $filename, $compression);  
        } 
		elseif( $image_type == IMAGETYPE_GIF )
        {			
            imagegif($this->image, $filename);	
        }
        elseif( $image_type == IMAGETYPE_PNG )
        {			
            imagepng($this->image, $filename);
        }
        if( $permissions != null )
        {
		    chmod($filename, $permissions);
        }		
    } 

    function output($image_type = IMAGETYPE_JPEG)
    {
		if( $image_type == IMAGETYPE_JPEG )
		{ 
			imagejpeg($this->image);  
        } 
		elseif( $image_type == IMAGETYPE_GIF )
        {			
            imagegif($this->image);	
        }
        elseif( $image_type == IMAGETYPE_PNG )
        {			
            imagepng($this->image);
        }		
    }  	

    function getWidth()
    {
        return imagesx( $this->image );
    }

    function getHeigth()
    {
        return imagesy( $this->image );
    }	

    function resizeToHeigth($heigth)
    {
        $ratio = $heigth / $this->getHeigth();
		$width = $this->getWidth() * $ratio;
		$this->resize($width, $heigth);
    }

    function resizeToWidth($width)
    {
        $ratio = $width / $this->getWidth();
		$heigth = $this->getHeigth() * $ratio;
		$this->resize($width, $heigth);
    }
	
	function scale($scale)
    {
        $width = $this->getWidth() * $scale/100;
		$heigth = $this->getHeigth() * $scale/100;
		$this->resize($width, $heigth);
    }

    function resize($width, $heigth)
    {
        $new_image = imagecreatetruecolor($width, $heigth);
		imagecopyresampled( $new_image, $this->image, 0, 0, 0, 0, $width, $heigth, $this->getWidth(), $this->getHeigth() );
		$this->image = $new_image;
    }		

}

