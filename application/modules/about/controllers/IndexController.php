<?php
/**
 * Contact controller
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class About_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('page');
    }
	
    public function indexAction()
    {	
		$about_model = new Default_Model_DbTable_About();
		$page_text = $about_model->getAboutPage(); //Zend_Debug::dump($page_text);exit;
	    $this->view->page = $page_text;	
	    
	    $sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
    } 
}