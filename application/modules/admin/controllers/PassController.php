<?php

/**
 * 
 *
 * User_Account controller.
 * 
 * 
 * 
 * 
 */
class Admin_PassController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
	
	public function indexAction()
    {
	    $auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id;
	   
        $users_object = new Default_Model_DbTable_Users();
		$user = $users_object->getUser($user_id);                //   Zend_Debug::dump($user_id);exit; 
		$this->view->user = $user;  
    }
    
	public function updatepassAction()
    {
	    $form_passwup = new Default_Form_Auth_Passwup();     
        $this->view->form_passwup = $form_passwup;
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id; //Zend_Debug::dump($user_id);exit;
		
	    // Если к нам идёт Post запрос
        if ($this->getRequest()->isPost())
		{
		    $formData = $this->getRequest()->getPost();
			
			//$user_id = $this->getRequest()->getParam('user_id');
			$password = $this->getRequest()->getPost('password');
			$passconf = $this->getRequest()->getPost('passconf');  //  Zend_Debug::dump($password);exit;
          
		 /*   if(!$password)
			{
				$this->view->errMessage = "Пароли не совпадают!" ;
                return;
            } */
			
			if($password !== $passconf)
			{
				$this->view->errMessage = "Пароли не совпадают!" ;
                return;
            }
			
		    $valid = new Zend_Validate_NotEmpty();
            $value  = $password;
            $result = $valid->isValid($value);
			
			if (!$result)
			{
			    $this->view->errMessage = "Введите пароль!";
                return;   
			}
             
			
			$validator = new Zend_Validate_StringLength(array('min' => 6, 'max' => 32));
            $res = $validator->isValid($password);
            if(!$res)
			{
			    $this->view->errMessage = "Длина пароля должна быть от 6 до 32 символов" ;
                return; 
			}
			
		   	if ($form_passwup->isValid($formData))
			{
                $users_object = new Default_Model_DbTable_Users();
				
                $users_object->update_password($user_id, md5($password));
            }
            $this->_helper->redirector('index', 'index', 'admin', array('user_id' => $user_id));
        }
    }
	
}

