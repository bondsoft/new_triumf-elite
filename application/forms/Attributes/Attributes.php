<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Attributes_Attributes extends Zend_Form
{
    public function init()
    {  
        $attribute_id = new Zend_Form_Element_Hidden('attribute_id');
        $attribute_id->addFilter('Int');
        
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'));
        
        // prepare attribute_group_id
        $validator_attribute_group_id = new Zend_Validate_Between(array('min' => 1, 'max' => 100));
        $validator_attribute_group_id->setMessage('Не выбрана группа атрибутов');
        
        $attributesgroup_model = new Default_Model_DbTable_Attributesgroup();
        $attribute_group_id = $attributesgroup_model->fetchAll(); 
      
        if (count($attribute_group_id)!=0)
        {
            $attribute_group_prepared['не указана']= "не указана";
            foreach ($attribute_group_id as $attribute_group)
            {
                $attribute_group_prepared[$attribute_group->attribute_group_id]= $attribute_group->attribute_group_name;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $attribute_group_id = new Zend_Form_Element_Select('attribute_group_id');
            $attribute_group_id ->setLabel("Группы атрибутов:")
                ->addMultiOptions($attribute_group_prepared)
                ->setAttrib('class', array('span4')) 
                ->addValidator($validator_attribute_group_id);
        } 
           
		$attribute_name = new Zend_Form_Element_Text('attribute_name');
        $attribute_name->setLabel("Название:")
            ->setAttrib('class', 'span8')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true
            );

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $attribute_id,
                                    $attribute_group_id,
                                    $attribute_name,
                                    $sort_order,
                                    $submit
                                ));
       
    }
}