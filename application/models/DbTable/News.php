<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_News extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_news';
	
	public function getNews($news_id)
    {
        $news_id = (int)$news_id;
        $row = $this->fetchRow('news_id = ' . $news_id);
        if(!$row) {
            throw new Exception("Нет записи с news_id - $news_id");
        }
        return $row->toArray();
    }
	
	public function addNews(
                                $creation_date,
                                $news_title,
                                $intro_text,
                                $news_text,
                                $url_seo,
                                $header_tags_title,
                                $header_tags_description,
                                $header_tags_keywords
                            )
    {
        $data = array(
            'creation_date' => $creation_date,
            'news_title' => $news_title,
            'intro_text' => $intro_text,
            'news_text' => $news_text,
            'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        $this->insert($data);
    }
	
	public  function updateNews(
                                    $news_id,
                                    $creation_date,
                                    $news_title,
                                    $intro_text,
                                    $news_text,
                                    $url_seo,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                )
    {
        $data = array( 
            'creation_date' => $creation_date,
            'news_title' => $news_title,
            'intro_text' => $intro_text,
            'news_text' => $news_text,
            'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords		
        );
        
        $this->update($data, 'news_id = ' . (int)$news_id);
    }
	
	public function getAllNewsAdmin()
	{
        $_name = 'zend_news';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->order('news_id DESC');
     
		$news = $this->fetchAll($select);
        return $news;
    }
	
	public function getAllNewsFront()
	{
        $_name = 'zend_news';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->where('status = ?', '1')
					->order('creation_date DESC');
     
		$news = $this->fetchAll($select);
        return $news;
    }
	
	
	public function getLastArticles()
	{
        $_name = 'zend_articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('public = ?', '1')
					->order('article_id DESC')
					->limit(3);
     
		$articlebook = $this->fetchAll($select);
        return $articlebook;
    }
	
	public function getMainphoto_file($news_id)
	{
	    $_name = 'zend_news';
        $select = $this->select()
                    ->from($_name)
					->where('news_id = ?', $news_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec['main_photo'];		
        return $main_photo_file_name;
    }
	
	public function getNewsTitle($url_seo)
	{
        $_name = 'zend_news';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
		$news_title_rec = $this->fetchRow($select);	
        $news_title = $news_title_rec['news_title'];		
        return $news_title;
    }
	
	public function deleteNews($news_id)
    {
        $this->delete('news_id = ' . (int)$news_id);
    }
	
	public  function updateMainphoto(
										$news_id, 
										$main_photo
	                                )
    {
        $data = array(
			'main_photo' => $main_photo,
        );
        
        $this->update($data, 'news_id = ' . (int)$news_id);
    }
    
    public function getNewspage($url_seo)
    {
        $_name = 'zend_news';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
     
        $news = $this->fetchAll($select);
        return $news;
    }
    
    public  function updateNewsstatus(
                                    $news_id,
                                    $status
                                )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'news_id = ' . (int)$news_id);
    }
}
