<?php
/**
 *
 * Default controller for this application.
 */
class Basket_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        /* Initialize action controller here */
	    $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('cart');	
    }
   
    public function indexAction()
    {
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		
		$categories_model = new Default_Model_DbTable_Categories();
		$products_model = new Default_Model_DbTable_Products();

        $categories_object = $categories_model->getCategoryForFrontPage(); //Zend_Debug::dump($categories_01);exit;
     
        $this->view->categories = $categories_object;
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		
		@$products = $sessionNamespace->array['cart'];
		$recount_quantity_cart = $sessionNamespace->recount_quantity_cart;
	//	Zend_Debug::dump($recount_quantity_cart);exit;
//	unset($sessionNamespace->array['cart']);
		if(@$products) {
			foreach ($products as $key => $quantity) {
				$product = explode(':', $key);
				$product_id = $product[0];
				
				// Options
				if (!empty($product[1])) {
					$options = unserialize(base64_decode($product[1]));
				} else {
					$options = array();
				} 
				
				$product_query = $products_model->getProductForCart1(
																		$product_id
																	);
				$price = $product_query['price'];
				//	Zend_Debug::dump($product_query);exit;
			//	Zend_Debug::dump($price);exit;
				$option_data = array();
				
				foreach ($options as $product_option_id => $option_value) {
					$option_query = $products_model->getProductOption(
																		$product_option_id,
																		$product_id	
																	);
				//	Zend_Debug::dump($option_query);exit;
					$option_value_query = $products_model->getProductOptionValue(
																					$option_value,
																					$product_option_id	
																				);
				//	Zend_Debug::dump($option_value_query);exit;
					if ($option_value_query['price_prefix'] == '+') {
						@$option_price += $option_value_query['price'];
					} elseif ($option_value_query['price_prefix'] == '-') {
						@$option_price -= $option_value_query['price'];
					}												
					$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $option_value,
										'option_id'               => $option_query['option_id'],
										'option_value_id'         => $option_value_query['option_value_id'],
										'name'                    => $option_query['name'],
										'option_value'            => $option_value_query['name'],
										'type'                    => $option_query['type'],
										'quantity'                => $option_value_query['quantity'],
										'subtract'                => $option_value_query['subtract'],
										'price'                   => $option_value_query['price'],
										'price_prefix'            => $option_value_query['price_prefix']
									);
				}	
				//	Zend_Debug::dump($option_data);exit;
					$products_cart[$key] = array(
						'key'                       => $key,
						'product_id'                => $product_query['product_id'],
						'product_model'             => $product_query['product_model'],
						'url_seo'            	 	=> $product_query['url_seo'],
						'remove'              		=> $key,
						'intro_text'            	=> $product_query['intro_text'],
						'image'                     => $product_query['image'],
						'option'                    => $option_data,
						'quantity'                  => $quantity,
						'price'                     => ($price + @$option_price),
						'total'                     => ($price + @$option_price) * $quantity
					);
			
				
				foreach ($products_cart as $product) {
					$product_total = 0;
					
					$product_total += $product['quantity'];
				/*	foreach ($products_cart as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					} */
					
				/*	$option_data = array();
					
					foreach ($product['option'] as $option) {
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $option['option_value']
						);
					}
					
					$cart['products'][] = array(
					//	'key'                => $product['key'],
						'product_model'      => $product['product_model'],
						'url_seo'            => $product['url_seo'],
						'intro_text'         => $product['intro_text'],
						'image'              => $product['image'],
						'option'             => $option_data,
						'quantity'           => $product['quantity'],
						'price'              => $price
					);*/
				}
			}
		//	$this->view->option_data = $option_data;
			
			$this->view->products_cart = @$products_cart;
		//	Zend_Debug::dump($this->countProducts());exit;
			$sessionNamespace->quantity_cart = $this->countProducts();
			$quantityCart = $sessionNamespace->quantity_cart;
			$this->view->quantityCart = $quantityCart;
			$sum_cart = $sessionNamespace->sum_cart = @$sum_cart;
			$this->view->sum_cart = $sum_cart;
		} else {
			$quantityCart = $sessionNamespace->quantity_cart = '' ;
			$this->view->quantityCart = $quantityCart;
			
			$sum_cart = $sessionNamespace->sum_cart = '';
			$this->view->sum_cart = $sum_cart;
			
			$quantityCart = $sessionNamespace->quantity_cart = '';
			$this->view->quantityCart = $quantityCart;
		}
		
		$homepage_model = new Default_Model_DbTable_Homepage();
		$page_text = $homepage_model->getPageText();
	    $this->view->page = $page_text;
    } 
	
	public function countProducts() {
		$product_total = 0;

		$sessionNamespace = new Zend_Session_Namespace('Foo');
		
		$products = $sessionNamespace->array['cart'];

		foreach ($products as $key => $quantity) {
			$product_total += $quantity;
		}		

		return $product_total;
	}
    
    public function allproductsAction()
    {
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('products'); 
		
		$categories_model = new Default_Model_DbTable_Categories();
		$products_model = new Default_Model_DbTable_Products();
		$url_seo = $this->getRequest()->getParam('url_seo');// Zend_Debug::dump($url_seo);exit;
		
		$category_id = $categories_model->getCategoryIdByUrl($url_seo); //Zend_Debug::dump($category_id);exit;

        $products_object = $products_model->getProductsForFront($category_id);// Zend_Debug::dump($categories_object);exit;
     
        $this->view->products = $products_object;
    }
	
	public function productAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('product_single'); 
        
        $products_model = new Default_Model_DbTable_Products();
        $url_seo = $this->getRequest()->getParam('url_seo');// Zend_Debug::dump($url_seo);exit;
        
        $product_id = $products_model->getProductIdByUrl($url_seo); //Zend_Debug::dump($category_id);exit;
        
        
        $product = $products_model->getSingleProductForFront($product_id); // Zend_Debug::dump($product);exit;
        
        $product_model = $products_model->getProductModel($product_id); // Zend_Debug::dump($product);exit;
        $this->view->product_model = $product_model;
        
        $product_image = $products_model->getProductImage($product_id); // Zend_Debug::dump($product);exit;
        $this->view->product_image = $product_image;
        
        $attributes_model = new Default_Model_DbTable_Attributes();
        $attributes = $attributes_model->getAllAttributesAdmin(); //Zend_Debug::dump($attributes);exit;
        $this->view->attributes = $attributes; 
        
        $product_attributes = $products_model->getProductsAttributesForFront($product_id); // Zend_Debug::dump($product_attributes);exit;
        $this->view->product_attributes = $product_attributes;
        
        $attributes_text = $products_model->getProductsAttributesText($product_id); // Zend_Debug::dump($attributes_text);exit;
        $this->view->attributes_text = $attributes_text;
		
        $paginator = Zend_Paginator::factory($product);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(1); 
        
        $products_photo_model = new Default_Model_DbTable_Productsphoto();
		$photos = $products_photo_model->getAllPhotosForProduct($product_id);
		$this->view->photos = $photos;
       
       
    }
    
    public function singleproductheaderAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('product_single'); 
        
        $products_model = new Default_Model_DbTable_Products();
        $url_seo = $this->getRequest()->getParam('url_seo');// Zend_Debug::dump($url_seo);exit;
        
        $product_id = $products_model->getProductIdByUrl($url_seo); //Zend_Debug::dump($category_id);exit;
        
        
        $product = $products_model->getSingleProductForFront($product_id); // Zend_Debug::dump($product);exit;
        
        $attributes_model = new Default_Model_DbTable_Attributes();
        $attributes = $attributes_model->getAllAttributesAdmin(); //Zend_Debug::dump($attributes);exit;
        $this->view->attributes = $attributes; 
        
        $product_attributes = $products_model->getProductsAttributesForFront($product_id); // Zend_Debug::dump($product_attributes);exit;
        $this->view->product_attributes = $product_attributes;
		
        $paginator = Zend_Paginator::factory($product);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(1); 
        
        $products_photo_model = new Default_Model_DbTable_Productsphoto();
		$photos = $products_photo_model->getAllPhotosForProduct($product_id);
		$this->view->photos = $photos;
       
       
    }
    
    public function productsofcategoryAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('catalog'); 
        
        $products_model = new Default_Model_DbTable_Products();
        $categories_model = new Default_Model_DbTable_Categories();
        $url_seo = $this->getRequest()->getParam('url_seo'); // Zend_Debug::dump($url_seo);exit;
        $category_id = $categories_model->getCategoryIdByUrl($url_seo);
        
        $products = $products_model->getProductsSpecificCategoryForFront($category_id); 
		
        $paginator = Zend_Paginator::factory($products);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(9); 
       
       
    }
    
    public function similarmodelsAction()
    { 
        
        
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
       
        $kiosks_object = new Default_Model_DbTable_Kiosks();
      
        $kiosks_id = $this->getRequest()->getPost('kiosks_id'); //Zend_Debug::dump($kiosks_id);exit;
      
        Zend_Session::start();
     
        $sessionNamespace = new Zend_Session_Namespace('Foo');
        $sessionNamespace->compare[$kiosks_id]++; 
      
        Zend_Debug::dump($sessionNamespace->compare);exit;
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => true));
        exit;
   
    } 
  
   public function removeproductfrombasketAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
		$key = $this->getRequest()->getParam('key');
	//	Zend_Debug::dump($key);exit;
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		if (isset($sessionNamespace->array['cart'][$key])) {
			unset($sessionNamespace->array['cart'][$key]);
		}
		$this->_helper->redirector('index', 'index', 'basket'); 
    }
    
    public function recountsumbasketAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
       
		foreach ($this->getRequest()->getPost('quantity') as $key => $value) {
			$this->update($key, $value);
		}
            $this->_helper->redirector('index', 'index', 'basket');   
          
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function update($key, $qty) {
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$sessionNamespace->array['cart'][$key] = (int)$qty;
	}

	public function checkoutAction()
    {
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('oformlenie_zakaza'); 
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = @$userdata->user_id;
		
        $form = new Default_Form_Checkout_Checkout();
		$formauthorized = new Default_Form_Checkout_CheckoutAuthorized();
        
	//	if ($user_id) {
		//	$this->view->form = $formauthorized;
		//} else {
			$this->view->form = $form;
		//}
		
    //    $users_object = new Default_Model_DbTable_Users();
	//	$user = $users_object->getUser($user_id); 
	//	Zend_Debug::dump($user['email']);exit;
		
		$categories_model = new Default_Model_DbTable_Categories();
		$products_model = new Default_Model_DbTable_Products();

        $categories_object = $categories_model->getCategoryForFrontPage(); //Zend_Debug::dump($categories_01);exit;
     
        $this->view->categories = $categories_object;
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		
		@$products = $sessionNamespace->array['cart'];
	//	Zend_Debug::dump($products);exit;
		if(@$products) {
			foreach ($products as $key => $quantity) {
				$product = explode(':', $key);
				$product_id = $product[0];
				
				// Options
				if (!empty($product[1])) {
					$options = unserialize(base64_decode($product[1]));
				} else {
					$options = array();
				} 
				
				$product_query = $products_model->getProductForCart1(
																		$product_id
																	);
				$price = $product_query['price'];
				//	Zend_Debug::dump($product_query);exit;
			//	Zend_Debug::dump($price);exit;
				$option_data = array();
				
				foreach ($options as $product_option_id => $option_value) {
					$option_query = $products_model->getProductOption(
																		$product_option_id,
																		$product_id	
																	);
				//	Zend_Debug::dump($option_query);exit;
					$option_value_query = $products_model->getProductOptionValue(
																					$option_value,
																					$product_option_id	
																				);
				//	Zend_Debug::dump($option_value_query);exit;
					if ($option_value_query['price_prefix'] == '+') {
						@$option_price += $option_value_query['price'];
					} elseif ($option_value_query['price_prefix'] == '-') {
						@$option_price -= $option_value_query['price'];
					}												
					$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $option_value,
										'option_id'               => $option_query['option_id'],
										'option_value_id'         => $option_value_query['option_value_id'],
										'name'                    => $option_query['name'],
										'option_value'            => $option_value_query['name'],
										'type'                    => $option_query['type'],
										'quantity'                => $option_value_query['quantity'],
										'subtract'                => $option_value_query['subtract'],
										'price'                   => $option_value_query['price'],
										'price_prefix'            => $option_value_query['price_prefix']
									);
				}	
				//	Zend_Debug::dump($option_data);exit;
					$products_cart[$key] = array(
						'key'                       => $key,
						'product_id'                => $product_query['product_id'],
						'product_model'             => $product_query['product_model'],
						'url_seo'            	 	=> $product_query['url_seo'],
						'remove'              		=> $key,
						'intro_text'            	=> $product_query['intro_text'],
						'image'                     => $product_query['image'],
						'option'                    => $option_data,
						'quantity'                  => $quantity,
						'price'                     => ($price + @$option_price),
						'total'                     => ($price + @$option_price) * $quantity
					);
			
			}
			$this->view->products_cart = @$products_cart;
		//	Zend_Debug::dump($products_cart);exit;
			$sessionNamespace->quantity_cart = $this->countProducts();
			$quantityCart = $sessionNamespace->quantity_cart;
		//	Zend_Debug::dump($quantityCart);exit;
			$this->view->quantityCart = $quantityCart;
			$sum_cart = $sessionNamespace->sum_cart = @$sum_cart;
			$this->view->sum_cart = $sum_cart;
		}
		$recount_quantity_cart = $sessionNamespace->recount_quantity_cart;

			
			$this->view->product_quantity_cart = @$product_quantity_cart;
		//	Zend_Debug::dump($product_quantity_cart);exit;
			@$product_quantity_cart = $sessionNamespace->product_quantity_cart = $product_quantity_cart;
			
			$this->view->product_quantity_cart = $product_quantity_cart;
			
			@$products_cart = $sessionNamespace->array['products_cart'] = @$products_cart;
		
			if(@$products) {
				foreach ($products as $key => $value)
				{ 
					$sum[] = @$product_quantity_cart[$key] * number_format($value['price'], 0, '', '');
					@$sum_cart = array_sum($sum);
				}
			}
			
		
				@$sum_cart = $sessionNamespace->sum_cart = @$sum_cart;
				$this->view->sum_cart = @$sum_cart;
			
		//	Zend_Debug::dump($product_quantity);exit;
		
		
		$homepage_model = new Default_Model_DbTable_Homepage();
		$page_text = $homepage_model->getPageText();
	    $this->view->page = $page_text;
    
    }
    
    public function addAction()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('order_mess'); 
        
        $orders_id = $this->getRequest()->getParam('orders_id'); //Zend_Debug::dump($orders_id);exit;
      
        $this->view->orders_id = $orders_id; 
    }
	
	public function sendorderAction()
    {
        $sessionNamespace = new Zend_Session_Namespace('Foo');
		
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = @$userdata->user_id;
		
		
			$form = new Default_Form_Checkout_Checkout();
		
		$form->isValid($this->_getAllParams());
        $users_object = new Default_Model_DbTable_Users();
		if ($user_id) {
			$user = $users_object->getUser($user_id); 
		}
	//	Zend_Debug::dump($user['email']);exit;
	
		
		if ($form->isValid($request->getPost()))
		{
            //    $code_pvz = $_POST['code_pvz'];  //Zend_Debug::dump($code_pvz);exit;
			//	$name = $_POST['name'];
			//	$address = $_POST['address'];
			//	$phone = $_POST['phone'];
			//	$workschedule = $_POST['workschedule'];
			//	$js_pricedelivery = $_POST['js_pricedelivery'];
			//	$period = $_POST['period'];
			//	$prepaid = $_POST['prepaid'];
				$token = md5(uniqid(rand(),1));
                
                $user_phone      = $form->getValue('user_phone');
				
				
					$user_name       = $form->getValue('user_name'); 
				
				
				
					$user_email      = $form->getValue('user_email');
				
            //    $delivery      = $_POST['delivery'];
			//	$city      = $_POST['city'];
				$city       = $form->getValue('city'); 
			//	@$samovivoz_city = $_POST['samovivoz_city'];
				$street_delivery = $form->getValue('street_delivery');
				$house_delivery = $form->getValue('house_delivery');
				$room_delivery = $form->getValue('room_delivery');
            //    $payment      = $form->getValue('payment');
          
                $order_model = new Default_Model_DbTable_Order();
				$order_product_model = new Default_Model_DbTable_Orderproduct();
				$order_option_model = new Default_Model_DbTable_Orderoption();
				$sessionNamespace = new Zend_Session_Namespace('Foo');
				@$products = $sessionNamespace->array['products_cart'];
			
			//	Zend_Debug::dump($products);exit;
				
				if(@$products) {
				foreach ($products as $product)
				{ 
					$sum[] = @$product['quantity'] * number_format($product['price'], 0, '', '');
					@$sum_cart = array_sum($sum);
				}
			}
			
			//	Zend_Debug::dump($sum_cart);exit;
                
                $order_model->addOrder(
										$token,
                                        $user_name,
                                        $user_phone,
                                        $user_email,
										$sum_cart,
									//	$delivery,
									//	$address,
									//	$samovivoz_city,
										$city,
										$street_delivery,
										$house_delivery,
										$room_delivery
									//	$payment
                                        
                                    );
				$order_id = $order_model->getLastInsertId();
				$this->view->order_id = $order_id;
			//	Zend_Debug::dump($order_id);exit;
				$product_quantity_cart = $sessionNamespace->product_quantity_cart;
				
				
				if(@$products) {
					foreach($products as $product)
					{ 
						
						$total = $product['quantity'] * $product['price'];
						
						$order_product_model->addProductOrder(
																$order_id,
																$product['product_id'],
																$product['product_model'],
																$product['image'],
																$product['quantity'],
																$product['price'],
																$total
															);
						$order_product_id = $order_product_model->getLastInsertId();
					//	Zend_Debug::dump($product['option']);exit;
						if(isset($product['option'])) {
							$order_option_model->addOrderOption(
																	$order_id,
																	$order_product_id,
																	$product['option'][0]['product_option_id'],
																	$product['option'][0]['product_option_value_id'],
																	$product['option'][0]['name'],
																	$product['option'][0]['option_value']
																);
						}
					}
				}
				
            $addr = Zend_Registry::get('Email');
            require_once('idna_convert.class.php');
			$idn = new idna_convert(array('idn_version'=>2008));
			$punycode=isset($_SERVER['SERVER_NAME']) ? stripslashes($_SERVER['SERVER_NAME']) : '';
			$punycode=(stripos($punycode, 'xn--')!==false) ? $idn->decode($punycode) : $idn->encode($punycode);
            $subject = 'Ваш заказ в магазине '.$punycode;
            
            $comment = 'Номер вашего заказа: ' .$order_id.'<br><br>'; 
			
			$comment .= '<br>Имя: '.$user_name.'<br>';
			$comment .= '<br>Телефон: '.$user_phone.'<br>';
			$comment .= '<br>E-mail: '.$user_email.'<br>';
			
			if(@$code_pvz) {
				$comment .= '<br>Код выбранного ПВЗ: '.$code_pvz.'<br>';
				$comment .= '<br>Наименование города выбранного ПВЗ: '.$name.'<br>';
				$comment .= '<br>Адрес выбранного ПВЗ: '.$address.'<br>';
				$comment .= '<br>Телефон выбранного ПВЗ: '.$phone.'<br>';
				$comment .= '<br>Время работы выбранного ПВЗ: '.$workschedule.'<br>';
				$comment .= '<br>Стоимость доставки до выбранного ПВЗ: '.$js_pricedelivery.'<br>';
				$comment .= '<br>Срок доставки до выбранного ПВЗ: '.$period.'<br>';
				$comment .= '<br>Отделение работает по предоплате (Yes|No): '.$prepaid.'<br>';
			}
            
       //     $comment .= '<br>Способ доставки: '.$delivery.'<br>';
            if($city) {
				$comment .= '<br>Город доставки: '.$city.'<br>';
				$comment .= '<br>Улица: '.$street_delivery.'<br>';
				$comment .= '<br>Дом: '.$house_delivery.'<br>';
				$comment .= '<br>Квартира: '.$room_delivery.'<br>';
            }
            if(@$samovivoz_city) {
				$comment .= '<br>Город доставки: '.$samovivoz_city.'<br>';
            }
        //    $comment .= '<br>Способ оплаты: '.$payment.'<br>';
            if(@$products) {
				foreach($products as $product)
				{
					if($product['option'][0]['option_value']) { 
						$option['value'] = $product['option'][0]['option_value']; 
					} else {
						$option['value'] = '-';
					}
					$sum = $product['quantity'] * $product['price'];
					$comment .= '<br>'.$product['product_model'].' - '.$product['quantity'].' шт. &nbsp;&nbsp;&nbsp;  '.$product['option'][0]['name'].': '.$option['value'].'&nbsp;&nbsp;&nbsp; Цена: '.number_format($product['price'], 0, ',', ' '). ' р.  &nbsp;&nbsp;&nbsp;  Сумма: '.number_format($sum, 0, ',', ' ').' р.<br>';
					
					if(isset($product['individual_fastening'])) { 
						$comment .= '<br>Индивидуальное крепление: '.$product['quantity'] * $product['individual_fastening'].' р.<br>';
					}
					
					if(isset($product['need_measure'])) { 
						$comment .= '<br>'.$product['need_measure'].'<br>';
					}
				}
			}

			
            $comment .= '<br>Общая стоимость, без учета доставки: '.$sum_cart.' р.<br>';
            
            $mail = new Zend_Mail('UTF-8');
			$mail->setBodyHtml($comment);
			$mail->setFrom($addr, $punycode);
			$mail->addTo($user_email, $user_name);
			$mail->setSubject($subject);
			$mail->send();
			
			$mail = new Zend_Mail('UTF-8');
			$mail->setBodyHtml($comment);
			$mail->setFrom($addr, $punycode);
			$mail->addTo($addr, $punycode);
			$mail->setSubject($subject);
			$mail->send();
			
			unset($sessionNamespace->array['products']);
			
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        
        $json = $form->getMessages();
        header('Content-Type: application/json');
        echo Zend_Json::encode($json);
        exit;
    }
}
