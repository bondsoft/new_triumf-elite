<?php

/**
 * 
 *
 * 
 */
class Default_Model_DbTable_Contact extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_contacts';
	
	public function getContact($contacts_id)
    {
        $contacts_id = (int)$contacts_id;
        $row = $this->fetchRow('contacts_id = ' . $contacts_id);
        if(!$row) {
            throw new Exception("��� ������ � contacts_id - $contacts_id");
        }
        return $row->toArray();
    }
	
// variant	
/*	public  function updateHomepage($id, $text)
    {   $homepage = new Default_Model_DbTable_Homepage();
        $data = array(
            'text' => $text,
        );
        $where = $homepage->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateContact(
                                        $contacts_id,
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                    )
    {
        $data = array(
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'contacts_id = ' . (int)$contacts_id);
    }
    
    public function getContactpage_text($lang)
    {
        $_name = 'zend_contacts';
        $select = $this->select()
                    ->from($_name)
                    ->where('lang = ?', $lang);
     
        $text = $this->fetchAll($select);
        return $text;
    }
	

}
