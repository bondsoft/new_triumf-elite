<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Products_ProductSort extends Zend_Form
{
    public function init()
    {   
		$sorting = new Zend_Form_Element_Select('sorting');
        $sorting ->setLabel("Сортировка:")
								->removeDecorator('label')
								->setAttrib('class', array('span3'))
							//	->setAttrib('id', 'product_sort')
								->setAttrib('onchange', "location = this.value;")
								->addMultiOptions(array(
									'popularity' => "популярности", 
									'cheap' => "от дешевых к дорогим",
									'expensive' => "от дорогих к дешевым",
									'action' => "акциям",
									'novelty' => "новинками"
								));


        $this->addElements(array(
									$sorting,
                                ));
       
    }
}