<?php

/**
 * 
 * 
 * 
 * 
 */
 
class Messages_IndexController extends Zend_Controller_Action
{
   
   
    public function init()
    {
        /* Initialize action controller here */
	    $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('index');	
    } 
   
    public function addAction()
    {
        /* Initialize action controller here */
    }
	
    public function indexAction()
    {
        $lang = Zend_Registry::get('Zend_Lang');

        $homepage = new Default_Model_DbTable_Homepage();
        $page_text = $homepage->getHomepage_text($lang);
        $this->view->page_text = $page_text;
    }
    
    function validateformAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
      //  $layout = $helper->getLayoutInstance();
      //  $layout->setLayout('contacts');   
        $form = new Default_Form_Contact_Contact();
        $form->isValid($this->_getAllParams());
        if ($form->isValid($request->getPost())) {
               
            $model = new Default_Model_DbTable_Messages($form->getValues());
            $data = array(
                'subgect'       => $form->getValue('subgect'),
                'username'       => $form->getValue('username'),
                'email'          => $form->getValue('email'),
                'comment'        => $form->getValue('comment'),
                'creation_date' => date('Y-m-d H:i:s'),
            );
            $model->insert($data);
            
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;
        }
        $json = $form->getMessages();
        header('Content-Type: application/json');
     //   echo Zend_Json::encode($json);
     //   exit;
        $captcha = $form->getElement('captcha')->getCaptcha(); //Zend_Debug::dump($captcha);exit;
        $json['captcha_id'] = $captcha->generate();
        $json['captcha_image'] = $captcha->getImgUrl() .
                   $captcha->getId() .
                   $captcha->getSuffix();
        echo Zend_Json::encode($json);
        exit;
    
    }
   
}