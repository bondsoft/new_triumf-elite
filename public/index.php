<?php
// Set the initial include_path. You may need to change this to ensure that
// Zend Framework is in the include_path; additionally, for performance
// reasons, it's best to move this to your web server configuration or php.ini
// for production.

ob_start();


error_reporting(E_ALL|E_STRICT);
date_default_timezone_set('Europe/Moscow');

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(dirname(__FILE__) . '/../library'),
    get_include_path(),
)));

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap();
$application->run();



$content = ob_get_contents();
ob_end_clean();

// Оптимизируем

// Отложим загрузку изображений + конвертация JPG в WEBP
//$content = optimize_speed_content($content);

// Перенесем некоторые CSS в конец страницы
$content = str_replace('<link rel="stylesheet" href="/js/site/slick-theme.css">', '', $content);
$content = str_replace('<link rel="stylesheet" href="/js/site/slick.css">', '', $content);
$content = str_replace('<link rel="stylesheet" href="/js/site/fancybox/jquery.fancybox.min.css">', '', $content);
$content = str_replace('<link rel="stylesheet" href="/css/jquery.mmenu.all.css">', '', $content);

$content = str_replace('</body>', '<link rel="stylesheet" href="/js/site/slick-theme.css"></body>', $content);
$content = str_replace('</body>', '<link rel="stylesheet" href="/js/site/slick.css"></body>', $content);
$content = str_replace('</body>', '<link rel="stylesheet" href="/js/site/fancybox/jquery.fancybox.min.css"></body>', $content);
$content = str_replace('</body>', '<link rel="stylesheet" href="/css/jquery.mmenu.all.css"></body>', $content);



// Удалим лишние JS
$content = str_replace('<script src="/js/jquery.mask.js" type="text/javascript"></script>', '', $content);
$content = str_replace('<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>', '', $content);

// Вывод
echo $content;


function optimize_speed_content($content)
{
    $add_end_content = ''; // будем вставлять в конец страницы

    // Отложить загрузку изображений
    if (preg_match_all('!<img(.*?)src="(.*?)"(.*?)>!s', $content, $images)) {
        foreach ($images[0] as $k => $img) {
            $src = $images[2][$k];

            if (strpos($src, ".jpg") !== false
                || strpos($src, ".jpeg") !== false
            ) {
                $src .= ".webp";
            }

			//$content = str_replace($img, '<img'.$images[1][$k].'src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMjHxIGmVAAAADUlEQVQYV2NgYGBgAAAABQABijPjAAAAAABJRU5ErkJggg==" data-loadimg="'.$src.'"'.$images[3][$k].'>', $content);
			$content = str_replace($img, '<img'.$images[1][$k].'src="https://' . $_SERVER['HTTP_HOST'] . $src.'" '.$images[3][$k].'>', $content);
        }

        $add_end_content .= '<script>

		var OSC_images = document.querySelectorAll("img");

		for(var i = 0; i < OSC_images.length; i++) {

			var OSC_image = OSC_images[i];

			if( OSC_image.hasAttribute("data-loadimg") ) {

				OSC_image.src = \'https://' . $_SERVER['HTTP_HOST'] . '\' + OSC_image.dataset.loadimg;

			}

		}

		</script>';
    }

    $content = str_replace("</body>", $add_end_content."</body>", $content);

    return $content;
}
