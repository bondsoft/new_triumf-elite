<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Attributes extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_attributes';
	
	public function getAttribute($attribute_id)
    {
        $attribute_id = (int)$attribute_id;
        $row = $this->fetchRow('attribute_id = ' . $attribute_id);
        if(!$row) {
            throw new Exception("Нет записи с attribute_id - $attribute_id");
        }
        return $row->toArray();
    }
	
	public function addAttribute(
                                    $attribute_group_id,
                                    $attribute_name,
                                    $sort_order                       
                                )
    {
        $data = array(
            'attribute_group_id' => $attribute_group_id,
            'attribute_name' => $attribute_name,
            'sort_order' => $sort_order
        );
        $this->insert($data);
    }
	
	public  function updateAttribute(
                                            $attribute_id,
											$attribute_group_id,
											$attribute_name,
											$sort_order
                                        )
    {
        $data = array(
            'attribute_group_id' => $attribute_group_id,
            'attribute_name' => $attribute_name,
            'sort_order' => $sort_order
        );
        
        $this->update($data, 'attribute_id = ' . (int)$attribute_id);
    }
    
    public function deleteAttribute($attribute_id)
    {
        $this->delete('attribute_id = ' . (int)$attribute_id);
    }
    
    public  function deleteManufacturerImage(
                                                $manufacturer_id,
                                                $image
                                            )
    {
        $data = array(
            'image' => $image
        );
        
        $this->update($data, 'manufacturer_id = ' . (int)$manufacturer_id);
    }
    
    public function getAllAttributesAdmin()
    {
        $_name = 'zend_attributes';
        $_name_01 = 'zend_attribute_group';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->joinLeft(array('name_01' => $_name_01),'name.attribute_group_id = name_01.attribute_group_id', array('name_01.attribute_group_name'))
                    ->order('name.sort_order');
     
        $attributes = $this->fetchAll($select);
        return $attributes;
    }
    
    
    
    public function getAllCategoresDescIndex()
    {
        $_name = 'zend_attribute_group';
     //   $_name_01 = 'zend_menu_first_level';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', 0)
                //    ->joinLeft(array('name_01' => $_name_01),'name.menu_first_level_id = name_01.menu_first_level_id', array('name_01.menu_first_level_name_ru'))
                 //   ->group('name.menu_first_level_id')
                    ->order('category_id DESC');
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    
    public function getСertainCategory($category_seo)
    {
        $_name = 'zend_category';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_seo = ?', $category_seo);
                     
        $category_object = $this->fetchAll($select);    
      //  $subcategory_name = $subcategory_object["subcategory_name"];     
        return $category_object;
    }
    
    public function getAllFirstLevelCategories($parent_id = 0)
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $parent_id);
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    
    public function getAllSecondLevelCategories($parent_id)
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $parent_id)
                    ->order('sort_order');
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    /*
    public function getMenuSecondLevel($menu_first_level_id)
    {
        $_name_01 = 'zend_menu_second_level';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('menu_first_level_id = ?', $menu_first_level_id);
                   // ->limit(1);

        $object = $this->fetchRow($select);
        return $object;
    } */
	
	public function getAllSubcategoryDesc()
	{
        $_name = 'subcategory_of_products';
        $_name_01 = 'category_of_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->joinLeft(array('name_01' => $_name_01),'name.category_id = name_01.category_id', array('name_01.category_name'))
                    ->order('subcategory_id DESC');
     
		$subcategory = $this->fetchAll($select);
        return $subcategory;
    }
	
	
	public function getCategoryForIndexPage()
	{
        $_name = 'category_of_products';
        $select = $this->select()
                    ->from(array('name' => $_name))
					->limit(4);
     
		$category = $this->fetchAll($select);
        return $category;
    }
	
	public function getArticlesNames()
	{
        $_name = 'articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('public = ?', '1')
					->order('article_id DESC')
					->limit(15);
     
		$articlebook = $this->fetchAll($select);
        return $articlebook;
    }
    
    public function getArticlepage($url_seo)
    {
        $_name = 'articles';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('url_seo = ?', $url_seo);
     
        $articlebook = $this->fetchAll($select);
        return $articlebook;
    }
	
    public function getTop_image($url_seo)
    {
        $_name = 'articles';
        $select = $this->select()
                    ->from($_name)
                    ->where('url_seo = ?', $url_seo);

        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec["top_image"];     
        return $main_photo_file_name;
    }
    
    public function getFirst_Attribute($attribute_group_id)
    {
        $_name_01 = 'zend_attributes';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('attribute_group_id = ?', $attribute_group_id)
                    ->limit(1);

        $object = $this->fetchRow($select);
        return $object;
    }
    
    public function getSubcategoryDescForIndex($category_id)
    {
        $_name_01 = 'subcategory_of_products';
        $select = $this->select()
                    ->from(array('name' => $_name_01))
                    ->where('category_id = ?', $category_id)
                    ->order('category_id DESC');

        $object = $this->fetchAll($select);
        return $object;
    }
    
    public function getCategorySeo($category_seo)
    {
        $_name = 'category_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_seo = ?', $category_seo);
                     
        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec["category_id"];     
        return $main_photo_file_name;
    }
    
    public function getSubcategoryId($subcategory_seo)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_seo = ?', $subcategory_seo);
                     
        $subcategory_id = $this->fetchRow($select);    
        $subcategory_id = $subcategory_id["subcategory_id"];     
        return $subcategory_id;
    }
    
    public function getCategoryName($category_id)
    {
        $_name = 'category_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id);
                     
        $category_object = $this->fetchRow($select);    
        $category_name = $category_object["category_name"];     
        return $category_name;
    }
    
    public function getCategoryUrl($category_id)
    {
        $_name = 'category_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id);
                     
        $category_object = $this->fetchRow($select);    
        $category_seo = $category_object["category_seo"];     
        return $category_seo;
    }
    
    public function getCategoryText($category_seo)
    {
        $_name = 'category_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_seo = ?', $category_seo);
                     
        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec["category_text"];     
        return $main_photo_file_name;
    }
    
    public function getSubCategoryText($subcategory_seo)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_seo = ?', $subcategory_seo);
                     
        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec["subcategory_text"];     
        return $main_photo_file_name;
    }
    
    public function getSubcategoryIdForIndex($category_id)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id)
                    ->group('name.category_id');
                     
        $subcategory_id = $this->fetchRow($select);    
        $subcategory_id = $subcategory_id["subcategory_id"];     
        return $subcategory_id;
    }
    
    public function getSubCategoryNameBredCrumbId($subcategory_id)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_id = ?', $subcategory_id);
                     
        $subcategory_object = $this->fetchRow($select);    
        $subcategory_name = $subcategory_object["subcategory_name"];     
        return $subcategory_name;
    }
    
    public function getSubCategorySeoBredCrumbId($subcategory_id)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_id = ?', $subcategory_id);
                     
        $subcategory_object = $this->fetchRow($select);    
        $subcategory_seo = $subcategory_object["subcategory_seo"];     
        return $subcategory_seo;
    }
    
    public function getSubCategoryIdBredCrumbId($category_id)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.category_id = ?', $category_id);
                     
        $subcategory_object = $this->fetchRow($select);    
        $subcategory_id = $subcategory_object["subcategory_id"];     
        return $subcategory_id;
    }
    
    public function getSubCategoryIdBredCrumb($subcategory_seo)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_seo = ?', $subcategory_seo);
                     
        $category_object = $this->fetchRow($select);    
        $category_id = $category_object["category_id"];     
        return $category_id;
    }
    
    public function getCategoryIdBredCrumb($subcategory_id)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_id = ?', $subcategory_id);
                     
        $category_object = $this->fetchRow($select);    
        $category_id = $category_object["category_id"];     
        return $category_id;
    }
    
    public function getSubcategoryFirstLevelName($subcategory_id)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_id = ?', $subcategory_id);
                     
        $category_object = $this->fetchRow($select);    
        $subcategory_name = $category_object["subcategory_name"];     
        return $subcategory_name;
    }
    
    public function getSubcategoryFirstLevelSeo($subcategory_id)
    {
        $_name = 'subcategory_of_products';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.subcategory_id = ?', $subcategory_id);
                     
        $category_object = $this->fetchRow($select);    
        $subcategory_seo = $category_object["subcategory_seo"];     
        return $subcategory_seo;
    }
    
    public function getParent($category_id)
    {
        $_name_01 = 'zend_category';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('parent_id = ?', $category_id)
                    ->limit(1);

        $category_object = $this->fetchRow($select);    
        $category_name = $category_object["category_name"];     
        return $category_name;
    }
	
	public  function updateMainphoto(
										$manufacturer_id, 
										$image
	                                )
    {
        $data = array(
			'image' => $image,
        );
        
        $this->update($data, 'manufacturer_id = ' . (int)$manufacturer_id);
    }
	
	public function getManufacturerImage($manufacturer_id)
	{
	    $_name = 'zend_attribute_group';
        $select = $this->select()
                    ->from($_name)
					->where('manufacturer_id = ?', $manufacturer_id);

		$manufacturer_object = $this->fetchRow($select);	
        $image = $manufacturer_object['image'];		
        return $image;
    }
   
}
