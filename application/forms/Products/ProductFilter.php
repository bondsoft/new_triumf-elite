<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Products_ProductFilter extends Zend_Form
{
    public function init()
    {   
		$gender = new Zend_Form_Element_MultiCheckbox('gender');
        $gender ->setLabel("Пол:")
								->removeDecorator('label')
								->setAttrib('class', array('input_row input'))
							//	->setAttrib('id', 'show_by')
								->addMultiOptions(array(
									'male' => "Мальчикам", 
									'female' => "Девочкам"
								));
								
		$optionvaluedescription_model = new Default_Model_DbTable_Optionvaluedescription();
        $option_value_id = $optionvaluedescription_model->fetchAll(); 
      
        if (count($option_value_id)!=0)
        {
            $option_prepared['выбрать цвет']= "выбрать цвет";
            foreach ($option_value_id as $option_value)
            {
                $option_prepared[$option_value->option_value_id]= $option_value->name;
            }
      //  Zend_Debug::dump($option_prepared);exit;
        
            $option_value = new Zend_Form_Element_Select('option_value');
            $option_value ->setLabel("Категории:")
                ->addMultiOptions($option_prepared)
                ->setAttrib('class', array('span6'))
				->removeDecorator('label');
        }
								
	/*	$submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("подобрать");
        $submit->setAttrib('id', 'submit');
        $submit->setAttrib('data-role', 'none');
        $submit->setAttrib('class', 'btn btn-primary '); 
        $submit->setDecorators(array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                ));

		
        $this->addElements(array(
									$submit
                                )); */
                                
		$this->addElement('Button', 'submit', array(
            'label' => 'ОТПРАВИТЬ',
            'type' => 'submit',
            'ignore' => true,
            'attribs' => array('class' => 'submit-contact submit_filter', 'id' => 'submit', 'data-role' => 'none'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
                            
           // 'class'  => 'ajax-validate'
        ));
		$element = $this->getElement('submit');
        $element->removeDecorator('Label');	
		
		$this->addElements(array(
                                    $option_value
                                ));
       
    }
}