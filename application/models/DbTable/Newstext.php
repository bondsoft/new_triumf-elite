<?php
/**
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Newstext extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_newstext';
	
	public function getNewstext($newstext_id)
    {
        $newstext_id = (int)$newstext_id;
        $row = $this->fetchRow('newstext_id = ' . $newstext_id);
        if(!$row) {
            throw new Exception("Нет записи с newstext_id - $newstext_id");
        }
        return $row->toArray();
    }
	
	public function addNewstext(
									$news_id,
									$news_text
								)
    {
        $data = array(
            'news_id' => $news_id,
            'news_text' => $news_text,
        );
        $this->insert($data);
    }
	
	public  function updateNewstext(
										$newstext_id, 
										$news_id,
										$news_text
									)
    {
        $data = array(  
		    'news_id' => $news_id,
            'news_text' => $news_text,
        );
        
        $this->update($data, 'newstext_id = ' . (int)$newstext_id);
    }
	
	public function deleteNewstext($newstext_id)
    {
        $this->delete('newstext_id = ' . (int)$newstext_id);
    }
	
	public function getNewspage($url_seo)
	{
        $_name = 'zend_news';
		$_name_01 = 'zend_newstext';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
                    ->where('name.url_seo = ?', $url_seo)
					->join(array('name' => $_name),'name_01.news_id = name.news_id');
     
		$news = $this->fetchAll($select);
        return $news;
    }
    
    public function getNewspageAdmin($news_id)
    {
        $_name = 'zend_news';
        $_name_01 = 'zend_newstext';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
                    ->where('name.news_id = ?', $news_id)
                    ->join(array('name' => $_name),'name_01.news_id = name.news_id');
     
        $news = $this->fetchAll($select);
        return $news;
    }

	public function deleteAllPagesForNews($news_id)
    {
        $this->delete('news_id = ' . (int)$news_id);
    }
	
	public function getCountArtcomments($category_id, $article_id)
	{
        $_name = 'zend_articles_comments';
		$_name_01 = 'zend_articlestext';
        $select = $this->select()
		            ->setIntegrityCheck(false)
                    ->from(array('name_01' => $_name_01))
					->where('name.artcomments_public = ?', (int)1)
					->where('name.category_id = ?', $category_id)
					->where('name.article_id = ?', $article_id)
					->order('name.artcomments_id DESC')
					->join(array('name' => $_name),'name_01.article_id = name.article_id');
     
		$countartcomments = $this->fetchAll($select);
        return $countartcomments;
    }

}
