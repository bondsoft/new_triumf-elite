<?php

/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Towns extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_towns';
	
	public function getTown($town_id)
    {
        $town_id = (int)$town_id;
        $row = $this->fetchRow('town_id = ' . $town_id);
        if(!$row) {
            throw new Exception("no record town_id - $town_id");
        }
        return $row->toArray();
    }
	public function addTown(
								$town_name
							)
    {
        $data = array(
            'town_name' => $town_name
        );
        $this->insert($data);
    }
	
	public function updateTown(	
								$town_id,
								$town_name
							)
    {
		$data = array(  'town_name' => $town_name
        );
        
        $this->update($data, 'town_id = ' . (int)$town_id);
    }
	
	public function deleteTown($town_id)
    {
	  // $_name = 'users';
	  // $condition = array(
      //   'photo_id = ?' => $photo_id
      // );
      //  $this->delete($_name, $condition); 
        $this->delete('town_id = ' . (int)$town_id);
    }
	
}
