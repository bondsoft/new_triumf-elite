<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_CategoresController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    function indexAction()
    {
      /*  $categores_model = new Default_Model_DbTable_Categores();
        $categores_object = $categores_model->getAllCategoresDescAdmin();
        
        $paginator = Zend_Paginator::factory($categores_object);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(50);   */
        
        $lang = Zend_Registry::get('Zend_Lang');
        
        $categories_model = new Default_Model_DbTable_Categores();

        $categories_01 = $categories_model->getAllSecondLevelCategores(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
            $children_data = array();
            $children = $categories_model->getAllSecondLevelCategores($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name_ru'  => $child['category_name_ru'],
                            'category_name_en'  => $child['category_name_en'],
                            'category_seo'  => $child['category_seo'],
                            'category_id'  => $child['category_id'],
                            'sort_order'  => $child['sort_order']
                        );
            }
            $categories[][] = array(
                    'category_name_ru'     => $category['category_name_ru'],
                    'category_name_en'     => $category['category_name_en'],
                    'children' => $children_data,
                    'category_seo'     => $category['category_seo'],
                    'category_id'  => $category['category_id'],
                    'sort_order'  => $category['sort_order']
                );
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories;
       
    }
   
    public function addcategoryAction()
    {
        $form = new Default_Form_Categores_Category();
        $this->view->form = $form;
      //  Zend_Debug::dump($form);exit;
        $categores_model = new Default_Model_DbTable_Categores();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $parent_id = $form->getValue('parent_id');
                $sort_order = $form->getValue('sort_order');
                $category_name_ru = $form->getValue('category_name_ru');    //Zend_Debug::dump($menu_first_level_name_ru);exit;
                $category_text_ru = $form->getValue('category_text_ru');
                $category_seo = $form->getValue('category_seo'); //Zend_Debug::dump($menu_first_level_seo);exit;
                $header_tags_title_ru = $form->getValue('header_tags_title_ru');
                $header_tags_description_ru = $form->getValue('header_tags_description_ru');
                $header_tags_keywords_ru = $form->getValue('header_tags_keywords_ru');
                $category_name_en = $form->getValue('category_name_en');    
                $category_text_en = $form->getValue('category_text_en');
                $header_tags_title_en = $form->getValue('header_tags_title_en');
                $header_tags_description_en = $form->getValue('header_tags_description_en');
                $header_tags_keywords_en = $form->getValue('header_tags_keywords_en');
                
                              
                $categores_model->addCategory(
                                                $parent_id,
                                                $sort_order,
												$category_name_ru,
												$category_text_ru,
												$category_seo,
												$header_tags_title_ru,
												$header_tags_description_ru,
												$header_tags_keywords_ru,
												$category_name_en,
												$category_text_en,
												$header_tags_title_en,
												$header_tags_description_en,
												$header_tags_keywords_en                                                           
                                            );
                    
                $this->_helper->redirector('index');
            } else {           
                $form->populate($formData);
            }
        }
    }
   
   
    public function editcategoryAction()
    {
        $form = new Default_Form_Categores_Category();
        $this->view->form = $form;
      //  Zend_Debug::dump($category_id);exit;
        $categores_model = new Default_Model_DbTable_Categores();
        
        
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {  
                $category_id = $form->getValue('category_id');
                $parent_id = $form->getValue('parent_id');
                $sort_order = $form->getValue('sort_order');
                $category_name_ru = $form->getValue('category_name_ru');    //Zend_Debug::dump($menu_first_level_name_ru);exit;
                $category_text_ru = $form->getValue('category_text_ru');
                $category_seo = $form->getValue('category_seo'); //Zend_Debug::dump($menu_first_level_seo);exit;
                $header_tags_title_ru = $form->getValue('header_tags_title_ru');
                $header_tags_description_ru = $form->getValue('header_tags_description_ru');
                $header_tags_keywords_ru = $form->getValue('header_tags_keywords_ru');
                $category_name_en = $form->getValue('category_name_en');    
                $category_text_en = $form->getValue('category_text_en');
                $header_tags_title_en = $form->getValue('header_tags_title_en');
                $header_tags_description_en = $form->getValue('header_tags_description_en');
                $header_tags_keywords_en = $form->getValue('header_tags_keywords_en');
 
                $categores_model->updateCategory(
                                                    $category_id, 
                                                    $parent_id,
                                                    $sort_order,
                                                    $category_name_ru,
                                                    $category_text_ru,
                                                    $category_seo,
                                                    $header_tags_title_ru,
                                                    $header_tags_description_ru,
                                                    $header_tags_keywords_ru,
                                                    $category_name_en,
                                                    $category_text_en,
                                                    $header_tags_title_en,
                                                    $header_tags_description_en,
                                                    $header_tags_keywords_en 
                                                );
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $category_id = $this->_getParam('category_id', 0);
            if ($category_id > 0)
            {
                $form->populate($categores_model->getCategory($category_id));
                $categores_object = $categores_model->getCategory($category_id);
                $this->view->category_name_ru = $categores_object['category_name_ru'];
            }
        }
    }
    
    public function deletecategoryAction()
    {
        $categores_model = new Default_Model_DbTable_Categores();
        $page = $this->getRequest()->getPost('page');
        if ($this->getRequest()->isPost())
        {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
            {
                $category_id = $this->getRequest()->getPost('category_id');
                $category_object = $categores_model->getCategory($category_id);
            
                $this->view->category_object = $category_object;
                
                $parent = $categores_model->getParent($category_id);
             // Zend_Debug::dump($subcategory_second_level_name);exit;
                if($parent != NULL){
                    $this->view->errMessage = "Эта категория активна. Удалить нельзя!";
                    return;
                } else {
                    $categores_model->deleteCategory($category_id);
                } 
               
            }
            $this->_helper->redirector('index', 'categores', 'admin', array('page' => $page));
        } else {
            $category_id = $this->_getParam('category_id');
            $category = $categores_model->getCategory($category_id);
            
            $this->view->category = $category;
        }
    }
   
}