<?php
/**
 * 
 *  
 * 
 */
class Default_Form_News_EditNews extends Zend_Form
{
    public function init()
    {	
		$news_id = new Zend_Form_Element_Hidden('news_id');
        $news_id->addFilter('Int');
        
        $request = Zend_Controller_Front::getInstance();
        $id = $request->getRequest()->getParam('news_id');
       
        $creation_date = new Zend_Form_Element_Text('creation_date');
        $creation_date->setLabel("Дата")
            ->setAttrib('size', '30')
            ->setAttrib('id', 'calendar-inputField')
            ->setAttrib('class', 'span4')
            ->addValidator('Date', true, array('yyyy.MM.dd'));
            
		$news_title = new Zend_Form_Element_Text('news_title');
        $news_title->setLabel("Название статьи")
		    ->setAttrib('size', '120')
            ->setRequired(true)
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true);
            
        $intro_text = new Zend_Form_Element_Textarea('intro_text');
        $intro_text->setLabel('Вступление к новости')
            ->setAttrib('cols', 120)
            ->setAttrib('rows', 15)
			->setAttrib('class', 'span12')
            ->setAttrib('id', 'about_text')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534));  
            
        $news_text = new Zend_Form_Element_Textarea('news_text');
        $news_text->setLabel('Текст новости')
            ->setAttrib('cols', 110)
            ->setAttrib('rows', 20)
            ->setAttrib('id', 'article_text')
            ->setAttrib('class', 'span12')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534));     
        
        $url_seo = new Zend_Form_Element_Text('url_seo');
        $url_seo->setLabel("SEO Url")
            ->setAttrib('size', '120')
            ->setRequired(true)
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
            ->addValidator('Db_NoRecordExists', false, array(   'zend_news', 
                                                                'url_seo', 
                                                                'exclude' => array(
                                                                                    'field' => 'news_id',
                                                                                    'value' => $id
                                                                                    )
                                                            )
            );  
            
        $header_tags_title = new Zend_Form_Element_Text('header_tags_title');
        $header_tags_title->setLabel("title")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');
            
        $header_tags_description = new Zend_Form_Element_Text('header_tags_description');
        $header_tags_description->setLabel("description")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');

        $header_tags_keywords = new Zend_Form_Element_Text('header_tags_keywords');
        $header_tags_keywords->setLabel("keywords")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');    
				
	//  вариант кнопки	
	//	$submit = new Zend_Form_Element_Submit("Зберегти");
    //  $submit->setAttrib('poetry_id', 'submitbutton');
	
        $this->addElements(array(
                                    $news_id,
                                    $creation_date,
                                    $news_title,
                                    $intro_text,
                                    $news_text,
                                    $url_seo,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                ));
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'type' => 'submit',
		  'class' => 'btn btn-primary pull-left',
          'ignore' => true,
        ));	 
    }
}

