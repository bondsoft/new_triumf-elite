<?php

/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Messages extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_messages';
	
	public function getMessage($message_id)
    {
        $message_id = (int)$message_id;
        $row = $this->fetchRow('message_id = ' . $message_id);
        if(!$row) {
            throw new Exception("Нет записи с message_id - $message_id");
        }
        return $row->toArray();
    }
	
	public function deleteMessage($message_id)
    {
        $this->delete('message_id = ' . (int)$message_id);
    }
	
	public function getDescMessage()
	{
        $_name = 'zend_messages';
		$_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->joinLeft(array('name_01' => $_name_01),'name.product_id = name_01.product_id', array('product_model', 'image'))
					->order('message_id DESC');
     
		$messages = $this->fetchAll($select);
        return $messages;
    }
	
	
	public function addMessage(
                                $name,
								$email,
                                $phone,
                                $message
                               )
    {
        $data = array(
            'name' => $name,
			'email' => $email,
            'phone' => $phone,
            'message' => $message,
            'creation_date' => date('Y-m-d H:i:s')
        );
        $this->insert($data);
    } 
}
