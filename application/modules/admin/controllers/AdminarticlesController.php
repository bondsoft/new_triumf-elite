<?php
/**
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminarticlesController extends Zend_Controller_Action
{
	public function init()
	{
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
	}
	
	function indexAction()
	{
        $articles_model = new Default_Model_DbTable_Articles();
		$articles = $articles_model->getAllArticlesAdmin();
		
		$paginator = Zend_Paginator::factory($articles);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);	   
	}
	
	public function additemAction()
    {
		include( 'SimpleImage.php' );
        $form = new Default_Form_Articles_AddArticle();     
        $this->view->form = $form;
        $articles_model = new Default_Model_DbTable_Articles();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
				$article_title = $form->getValue('article_title');
				$url_seo = $form->getValue('url_seo');
                $intro_text = $form->getValue('intro_text');
                $full_text = $form->getValue('full_text');
                if(!$creation_date = $form->getValue('creation_date')) {
                    $creation_date = date('Y-m-d H:i:s');
                }
				$header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                $alt = $form->getValue('alt');
                $show_photo_inside = $form->getValue('show_photo_inside');
				$show_our_work = $form->getValue('show_our_work');
				$data = $_FILES['main_photo']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['main_photo']['tmp_name']; 
				
				if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/articles/main_photos/big_".$newname;
                    $name_small = "media/photos/articles/main_photos/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
					$image_big->load($uploadedfile);
				//	$image_big->maxareafill(1020, 450, 241, 241, 247); 
							
					$image_big->save($name_big);
									
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->resizeToWidth(450);
					
					$image_small->save($name_small);
                } 
				 
                $articles_model->addArticle(
												$article_title,
												$url_seo,
												$intro_text,
												$full_text,
												$creation_date,
												$newname,
												$alt,
                                                $show_photo_inside,
												$header_tags_title,
												$header_tags_description,
												$header_tags_keywords,
												$show_our_work
											);
                unlink($_FILES['main_photo']['tmp_name']); 
                $this->_helper->redirector('index');
            } else { 
                $form->populate($formData);
            }
        }
    }
   
    public function edititemAction()
    {
		include( 'SimpleImage.php' );
        $form = new Default_Form_Articles_EditArticle();     
        $this->view->form = $form;
        $articles_model = new Default_Model_DbTable_Articles();
		
		$article_id = $this->getRequest()->getParam('article_id');
        $article_image = $articles_model->getArticleImage($article_id);
        $this->view->article_image = $article_image; 
        $this->view->article_id = $article_id; 
        
		$old_photo_file = $articles_model->getArticleImage($article_id);
        $directory = "media/photos/articles/main_photos";
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
			{
                $article_id = (int)$form->getValue('article_id');
                $article_title = $form->getValue('article_title');
				$url_seo = $form->getValue('url_seo');
                $intro_text = $form->getValue('intro_text');
                $full_text = $form->getValue('full_text');
                if(!$creation_date = $form->getValue('creation_date')) {
                    $creation_date = date('Y-m-d H:i:s');
                }
				$header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
				$alt = $form->getValue('alt');
                $show_photo_inside = $form->getValue('show_photo_inside');
				$show_our_work = $form->getValue('show_our_work');
				$main_photo = $form->getValue('main_photo');
				$data = $_FILES['main_photo'];  // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['main_photo']['tmp_name']; 
				
				if($main_photo)
                {       
                    if(is_file("$directory/small_$old_photo_file"))
                    {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                    }
                }
				
				if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
					//Zend_Debug::dump($newname);exit;
                    $name_big = "media/photos/articles/main_photos/big_".$newname;
                    $name_small = "media/photos/articles/main_photos/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
					$image_big->load($uploadedfile);
				//	$image_big->maxareafill(1020, 450, 241, 241, 247); 
							
					$image_big->save($name_big);
									
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->resizeToWidth(450);
					
					$image_small->save($name_small);
                } 
                
                if(!$main_photo)
                {      
                    $articles_object = $articles_model->getArticle($article_id);
                    $newname = $articles_object['main_photo'];
                }
 
                $articles_model->updateArticle(
												$article_id, 
												$article_title,
												$url_seo,
												$intro_text,
												$full_text,
												$creation_date,
												$newname,
												$alt,
                                                $show_photo_inside,
												$header_tags_title,
												$header_tags_description,
												$header_tags_keywords,
												$show_our_work
											);
                
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $article_id = $this->_getParam('article_id', 0);
            if ($article_id > 0)
			{
                $form->populate($articles_model->getArticle($article_id));
            }
        }
    }
	
	public function deleteitemAction()
    {
		$articles_model = new Default_Model_DbTable_Articles(); //  Zend_Debug::dump($article);exit;
	//	$newstext_model = new Default_Model_DbTable_Newstext();
		$photo_model = new Default_Model_DbTable_Articlesphotoforpage();
        if ($this->getRequest()->isPost())
		{
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
			{
                $article_id = $this->getRequest()->getPost('article_id');
                
                $all_photos = $photo_model->getAllPhotosForArticle($article_id); //Zend_Debug::dump($all_photos);exit;
                
                $directory_photos = "media/photos/articles/pages";
                
                foreach($all_photos as $photo)
                {
                    unlink($directory_photos .'/big_'.$photo["photo_file"]);
                    unlink($directory_photos .'/small_'.$photo["photo_file"]); 
                }
                 
                $directory = "media/photos/articles/main_photos";
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
				{
					unlink("$directory/big_$old_photo_file");
					unlink("$directory/small_$old_photo_file");
				}  
                
                 // получаем имя удаляемого файла
                $old_photo_file = $articles_model->getMainphotoFile($article_id);//Zend_Debug::dump($old_photo_file);exit;
                $main_photo == NULL;
                
            //  Zend_Debug::dump($old_photo_file);exit;
            
                $directory = "media/photos/articles/main_photos";
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
                
                
                $photo_model->deleteAllPhotosForArticle(
															$article_id
														);
            //    $newstext_model->deleteAllPagesForNews($news_id);
                $articles_model->deleteArticle($article_id);
            }
            $this->_helper->redirector('index', 'adminarticles', 'admin');
        } else {
            $article_id = $this->_getParam('article_id');
            
            $this->view->articles = $articles_model->getArticle($article_id);
        }
    }
    
    function seoAction()
    {
        $seo_model = new Default_Model_DbTable_Articlesseo();
        $seo = $seo_model->fetchAll();
        
        $paginator = Zend_Paginator::factory($seo);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);   
       
    }
    
    public function seoeditAction()
    {
        $form = new Default_Form_Articles_Seo();     
        $this->view->form = $form;
        $seo_model = new Default_Model_DbTable_Articlesseo();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {
                $id = (int)$form->getValue('id'); 
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
 
                $seo_model->updateSeo(
                                        $id,  
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                    );
                
                $this->_helper->redirector('seo', 'adminarticles', 'admin');
            } else {
                $form->populate($formData);
            }
        } else {   
            $id = $this->_getParam('id', 0);
            if ($id > 0)
            {
                $form->populate($seo_model->getSeo($id));
            }
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $articles_model = new Default_Model_DbTable_Articles();
        
        if ($this->getRequest()->isPost())
        {
            $article_id = $this->getRequest()->getParam('article_id');
            $status = '1';
            $articles_model->updateArticleStatus(
													$article_id,  
													$status
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $articles_model = new Default_Model_DbTable_Articles();
        
        if ($this->getRequest()->isPost())
        {
            $article_id = $this->getRequest()->getParam('article_id');
            $status = '0';
            $articles_model->updateArticleStatus(
													$article_id,  
													$status
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $articles_model = new Default_Model_DbTable_Articles();
        $article_id = $this->getRequest()->getParam('article_id');
        
        $old_photo_file = $articles_model->getArticleImage($article_id);
        $directory = "media/photos/articles/main_photos";
        
        if ($this->getRequest()->isPost())
        {
            $article_id = $this->getRequest()->getParam('article_id');
            $main_photo = '';
           
            $articles_model->deleteArticleImage(
                                                    $article_id,  
                                                    $main_photo
                                                );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function primeonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $articles_model = new Default_Model_DbTable_Articles();
        
        if ($this->getRequest()->isPost())
        {
            $article_id = $this->getRequest()->getParam('article_id');
            $prime = '1';
            $articles_model->updatePrimeArticle(
													$article_id,  
													$prime
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function primeoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $articles_model = new Default_Model_DbTable_Articles();
        
        if ($this->getRequest()->isPost())
        {
            $article_id = $this->getRequest()->getParam('article_id');
            $prime = '0';
            $articles_model->updatePrimeArticle(
													$article_id,  
													$prime
												);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
}