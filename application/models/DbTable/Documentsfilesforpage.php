<?php

/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Documentsfilesforpage extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_documents_file_for_page';
	
	public function getFile($file_id)
    {
        $file_id = (int)$file_id;
        $row = $this->fetchRow('file_id = ' . $file_id);
        if(!$row) {
            throw new Exception("no file_id - $file_id");
        }
        return $row->toArray();
    }
	
	public function updateFile(
                                    $file_id,
                                    $document_file
								)
    {
		$data = array( 
            'document_file' => $document_file,			
        );
        
        $this->update($data, 'file_id = ' . (int)$file_id);
    }
	
	public  function updatePhoto($photo_id, $photo_text)
    {
        $data = array(
            'photo_text' => $photo_text,		
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }

	public function getPhoto_file($photo_id)
	{
	    $_name = 'zend_about_photo_for_page';
        $select = $this->select()
                    ->from($_name)
					->where('photo_id = ?', $photo_id);

		$photo_file_rec = $this->fetchRow($select);	
        $photo_file_name = $photo_file_rec["photo_file"];		
        return $photo_file_name;
    }
	
	public function addFile(
								$documents_id, 
								$document_file
							)
    {
        $data = array(
		    'documents_id' => $documents_id,
            'document_file' => $document_file
        );
        $this->insert($data);
    } 
	
	public function getPhoto_galary($about_id)
	{
        $_name = 'zend_about_photo_for_page';
        $select = $this->select()
                    ->from($_name)
                    ->where('about_id = ?', $about_id);
     
		$photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function deleteFile($file_id)
    {
	 //  $_name = 'galary_photos';
	 //  $condition = array(
     //    'photo_id = ?' => $photo_id
     //  );
     //   $this->delete($_name, $condition); 
        $this->delete('file_id = ' . (int)$file_id);
    }
    
    public function getPdf($documents_id)
    {
        $_name = 'zend_documents_file_for_page';
        $select = $this->select()
                    ->from($_name)
                    ->where('documents_id = ?', $documents_id);
     
        $pdf_file_rec = $this->fetchRow($select);    
        $pdf_file_name = $pdf_file_rec["pdf_file"];     
        return $pdf_file_name;
    }
}
