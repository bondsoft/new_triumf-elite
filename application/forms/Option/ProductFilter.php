<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Products_ProductFilter extends Zend_Form
{
    public function init()
    {   
		$gender = new Zend_Form_Element_MultiCheckbox('gender');
        $gender ->setLabel("Пол:")
								->removeDecorator('label')
								->setAttrib('class', array('input_row input'))
							//	->setAttrib('id', 'show_by')
								->addMultiOptions(array(
									'male' => "Мальчикам", 
									'female' => "Девочкам"
								));
								
	/*	$submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("подобрать");
        $submit->setAttrib('id', 'submit');
        $submit->setAttrib('data-role', 'none');
        $submit->setAttrib('class', 'btn btn-primary '); 
        $submit->setDecorators(array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                ));

		
        $this->addElements(array(
									$submit
                                )); */
                                
		$this->addElement('Button', 'submit', array(
            'label' => 'ОТПРАВИТЬ',
            'type' => 'submit',
            'ignore' => true,
            'attribs' => array('class' => 'submit-contact submit_filter', 'id' => 'submit', 'data-role' => 'none'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
                            
           // 'class'  => 'ajax-validate'
        ));
		$element = $this->getElement('submit');
        $element->removeDecorator('Label');	
       
    }
}