<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Gallery_Add extends Zend_Form
{
    public function init()
    {   
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'))
					->removeDecorator('label');
					
		$title = new Zend_Form_Element_Text('title');
        $title->setLabel("alt:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
		  
        $text = new Zend_Form_Element_Textarea('text');
        $text->setLabel('')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_02')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
			->removeDecorator('label'); 
        
      
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $sort_order,
                                    $title,
									$text,
                                    $submit
                                ));
       
    }
}