<?php
/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_Checkout_CheckoutAuthorized extends Zend_Form
{
    public function init()
    {	
        
        $this->addElement('Text', 'user_phone', array(
            'label' => '',
          //  'value' => 'Ваш телефон...', 
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array('notEmpty', true, array(
                    'messages' => array(
                        'isEmpty'   =>  'Phone can\'t be empty'
                    )
                )),
                array('StringLength', false, array(10, 16, 'messages' => array(
                            'stringLengthInvalid'           => "Phone Length Invalid entry",
                            'stringLengthTooShort'          => "Phone Invalid Length , ex. 1234567890"
                    ))),
            ),
        //    'attribs' => array('class' => 'form-control form-textfields form-valid', 'placeholder' => 'Ваш телефон...', 'id' => 'callbackEmail', 'tabindex' => '2'),
            'attribs' => array('class' => 'input-text required phone', 'id' => 'user_phone', 'placeholder' => ''),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        )); 
		
						
		$payment = new Zend_Form_Element_Radio('payment');
        $payment->setLabel('payment:')
			->removeDecorator('label')
            ->addMultiOptions(array(
                    'Наличными' => 'Наличными при получении заказа',
                    'Безналичный расчет' => 'Возможность оплаты заказа безналичными способами' 
                        ))
            ->setSeparator(''); 
            
		$this->addElement('Text', 'street_delivery', array(
            'label' => '',
            'value' => '',
            'size'  => '30', 
        //    'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
        //    'validators' => array(
        //        array('NotEmpty', true),
        //    ),
         //   'attribs' => array('class' => 'form-control form-textfields', 'placeholder' => 'Представтесь...', 'required id' => 'callbackName', 'tabindex' => '1'),
            'attribs' => array('class' => 'valid', 'id' => 'street-delivery'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        )); 
        
        $this->addElement('Text', 'house_delivery', array(
            'label' => '',
            'value' => '',
            'size'  => '30', 
        //    'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
        //    'validators' => array(
        //        array('NotEmpty', true),
        //    ),
         //   'attribs' => array('class' => 'form-control form-textfields', 'placeholder' => 'Представтесь...', 'required id' => 'callbackName', 'tabindex' => '1'),
            'attribs' => array('class' => 'valid', 'id' => 'house-delivery'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        )); 
        
        $this->addElement('Text', 'room_delivery', array(
            'label' => '',
            'value' => '',
            'size'  => '30', 
        //    'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
        //    'validators' => array(
        //        array('NotEmpty', true),
        //    ),
         //   'attribs' => array('class' => 'form-control form-textfields', 'placeholder' => 'Представтесь...', 'required id' => 'callbackName', 'tabindex' => '1'),
            'attribs' => array('class' => 'valid', 'id' => 'room-delivery'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        ));  
        
    /*    $town_model = new Default_Model_DbTable_Towns();
        $city = $town_model->getTownsSortFront(); 
      
        if (count($city)!=0)
        {
            $town_prepared['не указана']= "Выбрать город";
            foreach ($city as $town)
            {
                $town_prepared[$town->town_name]= $town->town_name;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $city = new Zend_Form_Element_Select('city');
            $city ->setLabel("Города:")
                ->addMultiOptions($town_prepared)
                ->setAttrib('class', array('span8'))
				->removeDecorator('label'); 
               // ->addValidator($validator_menu_first_level_id);
        } */
        
        $this->addElement('Text', 'city', array(
            'label' => '',
            'value' => '',
            'size'  => '30', 
         //   'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
         //   'validators' => array(
          //      array('NotEmpty', true),
         //   ),
         //   'attribs' => array('class' => 'form-control form-textfields', 'placeholder' => 'Представтесь...', 'required id' => 'callbackName', 'tabindex' => '1'),
            'attribs' => array('class' => 'valid', 'id' => 'city'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        ));   
       
		// Init submit
        $this->addElement('Button', 'submit', array(
            'label' => 'отправить заказ',
            'type' => 'submit',
            'ignore' => true,
            'attribs' => array('class' => 'send_order submit_order'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
                            
           // 'class'  => 'ajax-validate'
        ));
		$element = $this->getElement('submit');
        $element->removeDecorator('Label');	

		$this->addElements(array(
									//$city,
									$payment
                                ));
        
    }
}

