<?php

/**
 * User_Signup controller
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */

class User_IndexController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('layout');
    }
	
    function indexAction()
    {
	    $lang = Zend_Registry::get('Zend_Lang');
        $request = $this->getRequest();
        $form_account = new Default_Form_Auth_Account();
		$this->view->form_account = $form_account;
		// Check to see if this action has been POST'ed to.
        if ($this->getRequest()->isPost())
		{
            
            // Now check to see if the form submitted exists, and
            // if the values passed in are valid for this form.
			
            if ($form_account->isValid($request->getPost()))
			{
             /*   if($form_account->getValue('password') !== $form_account->getValue('passconf'))
				{
					$this->view->errMessage = iconv("windows-1251", "UTF-8", "������ �� ���������!") ;
                    return;
                }*/
				
				
                // Since we now know the form validated, we can now
                // start integrating that data sumitted via the form
                // into our model:
                $user_object = new Default_Model_DbTable_Users($form_account->getValues());
				$data = array(
                    'username'       => $form_account->getValue('username'),
                    'password'       => md5($form_account->getValue('password')),
					'email'          => $form_account->getValue('email'),
					'user_role'      => 'member',
					'last_access'    => date('Y-m-d H:i:s'),
					'creation_date'  => date('Y-m-d H:i:s'),
					'avatar_file'    => '',
					'toun'           => '',
                );
				$user_object->insert($data);
				
                // ��������������� �� /user/login/index/
                  $this->_helper->redirector('login', 'login', 'user', array('lang' => $lang));
            }   
	    }
    } 
}