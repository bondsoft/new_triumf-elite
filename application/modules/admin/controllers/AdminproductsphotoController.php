<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminproductsphotoController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
        
//Zend_Debug::dump($this->view->news_main_photo_file);exit;
    }
	
    function indexAction()
    {
        $products_photo_model = new Default_Model_DbTable_Productsphoto();
        $product_id = $this->getRequest()->getParam('product_id');
		$photos = $products_photo_model->getAllPhotosForProduct($product_id);
		//Zend_Debug::dump($photos);exit;
		
		$paginator = Zend_Paginator::factory($photos);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator; 
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);	

        $product_id = $this->getRequest()->getParam('product_id');
        $products_model = new Default_Model_DbTable_Products();
        
		$products_object = $products_model->getProduct($product_id);
		$this->view->products_object = $products_object;
	}
   
    public function addphotoAction()
    {
	    include( 'SimpleImage.php' );
		
        $form = new Default_Form_Products_Photo();
        $this->view->form = $form;
        
        $products_model = new Default_Model_DbTable_Products();
        $product_id = $this->getRequest()->getParam('product_id');  //Zend_Debug::dump($sports_school_id);exit;
        $products_object = $products_model->getProduct($product_id);
        $this->view->products_object = $products_object;
		
		$values = $form->getValues();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
			{
				$product_id = $this->getRequest()->getParam('product_id');
                $photo_file = $form->getValue('photo_file'); 
				
				if( empty($photo_file) )
                {
	                echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
					echo "Вы не выбрали файл!";
	                echo "</font></b>" ;
                    return;
                }
			
                $products_photo_model = new Default_Model_DbTable_Productsphoto();
				
				$data = $_FILES['photo_file'];
				
				foreach ($data['type'] as $key => $value) // Zend_Debug::dump($value);exit;
                {	
                    if(	$value != NULL )
                    {				
                        $uploadedfile = $data['tmp_name'][$key];   // Zend_Debug::dump($uploadedfile);exit;
                        
                        if(file_exists($uploadedfile))
                        {
                        /* ********************************************** */
                            $ext = explode(".",$data["name"][$key]); 
                            $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                         //   Zend_Debug::dump($newname);exit;
                            $name_big = "media/photos/products/big_".$newname;
                            $name_small = "media/photos/products/small_".$newname;
                            
                        /* ********************************************** */
                            
                            $image_big = new SimpleImage();
                            $image_big->load($uploadedfile);
                            $image_big->resizeToWidth(900);
                            $image_big->save($name_big);
                            
                            $image_small = new SimpleImage();
                            $image_small->load($uploadedfile);
                            $image_small->resizeToWidth(200);
						//	$image_small->cutFromCenter(120, 119);
                            $image_small->save($name_small); 
                            
                        } 
                        
                        $products_photo_model->addPhoto(
															$product_id, 
															$newname
														);
                    }
				}
				// Уничтожаем файл во временном каталоге
                   unlink($_FILES['photo_file']['tmp_name']);
               
                  $this->_helper->redirector('index', 'adminproductsphoto', 'admin', array('product_id' => $product_id));
            } else {
                // Если форма заполнена неверно,
                $this->view->errMessage = "ффффффффффффффф" ;
            }
        }
    }
	
	public function editphotoAction()
    {
	    include( 'SimpleImage.php' );
        $form = new Default_Form_Products_EditPhotoFile();    
        $this->view->form = $form;
        
        $products_model = new Default_Model_DbTable_Products();
        $product_id = $this->getRequest()->getParam('product_id');
        $products_object = $products_model->getProduct($product_id);
        $this->view->products_object = $products_object;
		
		$sports_school_id = $this->getRequest()->getParam('sports_school_id');//Zend_Debug::dump($realestate_id);exit;
		$photo_id = $this->getRequest()->getParam('photo_id');// Zend_Debug::dump($photo_id);exit;
		
		$products_photo_model = new Default_Model_DbTable_Productsphoto();
				
		$this->view->photo = $products_photo_model->getPhoto($photo_id); //Zend_Debug::dump($this->view->photo);exit;
		$old_photo_file = $products_photo_model->getPhotoFile($photo_id);//Zend_Debug::dump($old_photo_file);exit;
		  
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();
		   
		   	if ($form->isValid($formData) && $form->submit->isChecked())
			{
			    $photo_file = $form->getValue('photo_file');
				
			    $directory = "media/photos/products";
		    	
            
				if( empty($photo_file) )
                {
					$this->view->errMessage = "Вы не выбрали файл!";
                    return;
                }
                
                // удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
				
				$data = $_FILES['photo_file'];
				$uploadedfile = $data['tmp_name']; //Zend_Debug::dump($uploadedfile);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name_big = "media/photos/products/big_".$newname;
                    $name_small = "media/photos/products/small_".$newname;
					
				   /* ********************************************** */
					
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                    $image_big->resizeToWidth(900);
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->resizeToWidth(200);
                 //   $image_small->cutFromCenter(120, 119);
                    $image_small->save($name_small); 
				} 
				
                $products_photo_model->updatePhotoFile(
															$photo_id,
															$newname
														);
				
				// Уничтожаем файл во временном каталоге
                 unlink($_FILES['photo_file']['tmp_name']);
                
                $this->_helper->redirector('index', 'adminproductsphoto', 'admin', array('product_id' => $product_id));
            }  else {
                $this->_helper->redirector('index', 'adminproductsphoto', 'admin', array('product_id' => $product_id));
            }
        } else {
            // Если мы выводим форму, то получаем id записи, которую хотим обновить
            $photo_id = $this->_getParam('photo_id', 0);
            if ($photo_id > 0) {
                
				$this->view->photo = $products_photo_model->getPhoto($photo_id);
                
                $form->populate($products_photo_model->getPhoto($photo_id));
            }
        }
    }
	
	public function deleteAction()
    {
        $photo_model = new Default_Model_DbTable_Productsphoto();
        if ($this->getRequest()->isPost()) {

            $del = $this->getRequest()->getPost('del'); // Zend_Debug::dump($del);exit;

                $product_id = $this->getRequest()->getParam('product_id');
				$photo_id = $this->getRequest()->getParam('photo_id');
            
            // Если пользователь поддтвердил своё желание удалить запись
            if ($del == 'yes') {
                $old_photo_file = $photo_model->getPhotoFile($photo_id);//Zend_Debug::dump($old_photo_file);exit;
              
                
				$photo_file = $photo_model->getPhotoFile($photo_id);
				
			//	Zend_Debug::dump($photo_file);exit;
			
				$directory = "media/photos/products";
				// удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
                
              //  Zend_Debug::dump($photo_file);exit;
               
                // Вызываем метод модели для удаления записи
                $photo_model->deletePhotoForProduct($photo_id);
            }
            $this->_helper->redirector('index', 'adminproductsphoto', 'admin', array('product_id' => $product_id));
        } else {
            // Если запрос не Post, выводим сообщение для поддтвержения
            // Получаем id записи, которую хотим удалить
            $photo_id = $this->_getParam('photo_id');
           
            $this->view->photo = $photo_model->getPhoto($photo_id);
            $products_model = new Default_Model_DbTable_Products();
			$product_id = $this->getRequest()->getParam('product_id');
			$products_object = $products_model->getProduct($product_id);
			$this->view->products_object = $products_object;
        }
    }
}