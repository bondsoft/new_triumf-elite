<?php

/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdmindocumentsphotoController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');  
    }
    
    function indexAction()
    {
        $photo_object = new Default_Model_DbTable_Documentsphotoforpage();
        $photos = $photo_object->getAllPhoto();
        //  Zend_Debug::dump($photos);exit;
        
        $paginator = Zend_Paginator::factory($photos);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);
    }
 
    public function addphotoforpageAction()
    {
        include( 'SimpleImage.php' );
        $form = new Default_Form_Photos_PhotosForPage();
        $this->view->form = $form;
		$photo_model = new Default_Model_DbTable_Documentsphotoforpage();
        $values = $form->getValues();
        
        if ($this->getRequest()->isPost()) {
            // Принимаем его
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
            {
                $photo_file = $form->getValue('photo_file'); 
                
                if( empty($photo_file) )
                {
                    echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
                    echo "Вы не выбрали файл!";
                    echo "</font></b>" ;
                    return;
                }
             
                $data = $_FILES['photo_file']; //Zend_Debug::dump($data);exit;
                
                foreach ($data['type'] as $key => $value) // Zend_Debug::dump($value);exit;
                {   
                    if( $value != NULL )
                    {               
                    $uploadedfile = $data['tmp_name'][$key];   // Zend_Debug::dump($uploadedfile);exit;
                    
                        if(file_exists($uploadedfile))
                        {
                        /* ********************************************** */
                            $ext = explode(".",$data["name"][$key]); 
                            $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                            $name_big = "media/photos/pages/big_".$newname;
                            $name_small = "media/photos/pages/small_".$newname;
                              
                        /* ********************************************** */
                            
                            
                            $image_large = new SimpleImage();
                            $image_small = new SimpleImage();
                            $image_large->load($uploadedfile);
                            $image_small->load($uploadedfile);
                            
                         //   $image_large->resizeToWidth(900);
                            $image_large->save($name_big);
                            
                         //   $image_small->cutFromCenter(300, 300);
                            $image_small->resizeToWidth(300);
                            $image_small->save($name_small);  
                            
                        } 
                    
                    $photo_model->addPhoto(
                                                $newname
                                            );
                    }
                }
                // Уничтожаем файл во временном каталоге
                   unlink($_FILES['photo_file']['tmp_name']);
               
                  $this->_helper->redirector('index', 'admindocumentsphoto', 'admin', array('documents_id' => $documents_id));
            } else {
                // Если форма заполнена неверно,
                $this->view->errMessage = "ффффффффффффффф" ;
            }
        }
    }
    
    public function editphotoforpageAction()
    {
        include( 'SimpleImage.php' );
        $form = new Default_Form_Photos_EditPhotoForPage();    
        $this->view->form = $form;
        
        $photo_id = $this->getRequest()->getParam('photo_id');// Zend_Debug::dump($photo_id);exit;
        $documents_id = $this->getRequest()->getParam('documents_id');
        $photo_model = new Default_Model_DbTable_Documentsphotoforpage();
                
        $this->view->photo = $photo_model->getPhoto($photo_id); //Zend_Debug::dump($this->view->photo);exit;
        $old_photo_file = $photo_model->getPhoto_file($photo_id);//Zend_Debug::dump($old_photo_file);exit;
          
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
           
            if ($form->isValid($formData) && $form->submit->isChecked())
            {
                $photo_file = $form->getValue('photo_file');
                
                $directory = "media/photos/pages";
                
            
                if( empty($photo_file) )
                {
                    $this->view->errMessage = "Вы не выбрали файл!";
                    return;
                }
                
                // удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
                
                $data = $_FILES['photo_file'];
                $uploadedfile = $data['tmp_name']; //Zend_Debug::dump($uploadedfile);exit;
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/pages/big_".$newname; //Zend_Debug::dump($name_big);exit;
                    $name_small = "media/photos/pages/small_".$newname;
                    
                   /* ********************************************** */
                    
                    $image_large = new SimpleImage();
                    $image_small = new SimpleImage();
                    $image_large->load($uploadedfile);
                    $image_small->load($uploadedfile);
                            
                //  $image_large->resizeToWidth(900);
                    $image_large->save($name_big);
                            
               //   $image_small->cutFromCenter(300, 300);
                    $image_small->resizeToWidth(300);
                    $image_small->save($name_small);  
                } 
                
                $photo_model->updatePhoto_fileForPage(
                                                        $photo_id,
                                                        $newname
                                                    );
                
                // Уничтожаем файл во временном каталоге
                 unlink($_FILES['photo_file']['tmp_name']);
                
                $this->_helper->redirector('index', 'admindocumentsphoto', 'admin', array('documents_id' => $documents_id));
            }  else {
                $this->_helper->redirector('index', 'admindocumentsphoto', 'admin', array('documents_id' => $documents_id));
            }
        } else {
            // Если мы выводим форму, то получаем id записи, которую хотим обновить
            $photo_id = $this->_getParam('photo_id', 0);
            if ($photo_id > 0) {
                $this->view->photo = $photo_model->getPhoto($photo_id);
                $form->populate($photo_model->getPhoto($photo_id));
            }
        }
    }
    
    public function deletephotoforpageAction()
    {
        $documents_id = $this->getRequest()->getParam('documents_id');
        $photo_model = new Default_Model_DbTable_Documentsphotoforpage();
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del'); // Zend_Debug::dump($del);exit;
            
            $photo_id = $this->getRequest()->getParam('photo_id');
            $old_photo_file = $photo_model->getPhoto_file($photo_id);//Zend_Debug::dump($old_photo_file);exit;
            
            if ($del == 'yes') {
                $photo_file = $photo_model->getPhoto_file($photo_id);
                
            //  Zend_Debug::dump($photo_file);exit;
            
                $directory = "media/photos/pages"; 
                // удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                } 
                
              //  Zend_Debug::dump($photo_file);exit;
                $photo_model->deletePhotoForPage($photo_id);
            }
            $this->_helper->redirector('index', 'admindocumentsphoto', 'admin', array('documents_id' => $documents_id));
        } else {
            $photo_id = $this->_getParam('photo_id');
            $this->view->photo = $photo_model->getPhoto($photo_id);
        }
    }

}