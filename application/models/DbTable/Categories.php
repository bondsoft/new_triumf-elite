<?php

/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Categories extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_category';
	
	public function getCategory($category_id)
    {
        $category_id = (int)$category_id;
        $row = $this->fetchRow('category_id = ' . $category_id);
        if(!$row) {
            throw new Exception("Нет записи с category_id - $category_id");
        }
        return $row->toArray();
    }
	
	public function addCategory(
                                    $parent_id,
                                    $sort_order,
									$category_name,
									$main_photo,
									$title,
									$title_text,
                                    $category_text,
									$category_text_extra,
                                    $url_seo,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                )
    {
        $data = array(
            'parent_id' => $parent_id,
            'sort_order' => $sort_order,
			'category_name' => $category_name,
			'main_photo' => $main_photo,
			'title' => $title,
			'title_text' => $title_text,
            'category_text' => $category_text,
			'category_text_extra' => $category_text_extra,
            'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        $this->insert($data);
    }
	
	public  function updateCategory(
                                        $category_id,
                                        $parent_id,
                                        $sort_order,
                                        $category_name,
                                        $main_photo,
										$title,
										$title_text,
                                        $category_text,
										$category_text_extra,
                                        $url_seo,
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                        )
    {
        $data = array(
            'parent_id' => $parent_id,
            'sort_order' => $sort_order,
            'category_name' => $category_name,
            'main_photo' => $main_photo,
			'title' => $title,
			'title_text' => $title_text,
            'category_text' => $category_text,
			'category_text_extra' => $category_text_extra,
            'url_seo' => $url_seo,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords  
        );
        
        $this->update($data, 'category_id = ' . (int)$category_id);
    }
    
    public function getBagsCategoriesForSlider()
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('category_group = ?', 'bags')
                    ->order('category_id')
                    ->limit(10, 2);
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    
    public function getAllCategoresDescAdmin()
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('category_id');
     
        $category = $this->fetchAll($select);
        return $category;
    }
    
    
    
    public function getAllCategoresDescIndex()
    {
        $_name = 'zend_category';
     //   $_name_01 = 'zend_menu_first_level';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', 0)
                //    ->joinLeft(array('name_01' => $_name_01),'name.menu_first_level_id = name_01.menu_first_level_id', array('name_01.menu_first_level_name_ru'))
                 //   ->group('name.menu_first_level_id')
                    ->order('category_id DESC');
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    
    public function getСertainCategory($url_seo)
    {
        $_name = 'zend_category';
       
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('name.url_seo = ?', $url_seo);
                     
        $category_object = $this->fetchAll($select);    
      //  $subcategory_name = $subcategory_object["subcategory_name"];     
        return $category_object;
    }
    
    public function getAllFirstLevelCategories($parent_id = 0)
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $parent_id)
					->order('sort_order');
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
    
    public function getAllSecondLevelCategories($parent_id)
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $parent_id)
                    ->order('sort_order');
     
        $categores = $this->fetchAll($select);
        return $categores;
    }
  
	public function getCategoryForFrontPage()
	{
        $_name = 'zend_category';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', 0)
					->order('sort_order');
     
		$category = $this->fetchAll($select);
        return $category;
    }
    
    public function getCategoryIdByUrl($url_seo)
	{
	    $_name = 'zend_category';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$category_rec = $this->fetchRow($select);	
        $category_id = $category_rec["category_id"];		
        return $category_id;
    }
	
	public function getCategoryNameByUrl($url_seo)
	{
	    $_name = 'zend_category';
        $select = $this->select()
                    ->from($_name)
					->where('url_seo = ?', $url_seo);

		$category_rec = $this->fetchRow($select);	
        $category_id = $category_rec["category_name"];		
        return $category_id;
    }
    
    public function getCategoryUrlByName($category_name)
	{
	    $_name = 'zend_category';
        $select = $this->select()
                    ->from($_name)
					->where('category_name = ?', $category_name);

		$category_rec = $this->fetchRow($select);	
        $url_seo = $category_rec["url_seo"];		
        return $url_seo;
    }
    
    public function getCategoryItem($category_id)
    {
        $_name = 'zend_category';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('parent_id = ?', $category_id);
                   // ->limit(1);

        $object = $this->fetchAll($select);
        return $object;
    }

    public function getCategoryItemForNav($category_id)
    {   
        $bsRes = array();
        $_name = 'zend_category';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('category_id = ?', $category_id)
                    ->limit(1);
        $object = $this->fetchRow($select);
        $bsRes['category_id'] = $object['category_id'];
        $namePieces = explode("-", $object['category_name']);
        $bsRes['category_name'] = mb_strtoupper(mb_substr(mb_strtolower($namePieces[0]), 0, 1, 'UTF-8'), 'UTF-8') . mb_substr(mb_strtolower($namePieces[0]), 1, null, 'UTF-8');
        $bsRes['url_seo'] = $object['url_seo'];
        $bsRes['parent_id'] = $object['parent_id'];
        return $bsRes;
    }
	
	public function deleteCategory($category_id)
    {
        $this->delete('category_id = ' . (int)$category_id);
    }
    
    public function checkActivCategory($category_id)
    {
        $_name_01 = 'zend_products';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('category_id = ?', $category_id)
                    ->limit(1);

        $category_object = $this->fetchRow($select);    
        $category_id = $category_object["category_id"];     
        return $category_id;
    }
   
    public function getParent($category_id)
    {
        $_name_01 = 'zend_category';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('parent_id = ?', $category_id)
                    ->limit(1);

        $category_object = $this->fetchRow($select);
        $category_name = $category_object["category_name"];
        return $category_name;
    }
	
	public  function deleteCategoryImage(
                                            $category_id, 
                                            $main_photo
                                        )
    {
        $data = array(
			'main_photo' => $main_photo,
        );
        
        $this->update($data, 'category_id = ' . (int)$category_id);
    }
	
	public function getCategoryImage($category_id)
	{
	    $_name = 'zend_category';
        $select = $this->select()
                    ->from($_name)
					->where('category_id = ?', $category_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec['main_photo'];		
        return $main_photo_file_name;
    }
	
	public function getMainPhotoFile($category_id)
    {
        $_name = 'zend_category';
        $select = $this->select()
                    ->from($_name)
                    ->where('category_id = ?', $category_id);

        $main_photo_file_rec = $this->fetchRow($select);    
        $main_photo_file_name = $main_photo_file_rec['main_photo'];    
        return $main_photo_file_name;
    }
    
    public  function updateCategoryStatus(
                                            $category_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'category_id = ' . (int)$category_id);
    }
    
    public function getCatItemMeta($category_id)
    {
        $_name = 'zend_category';
        $select = $this->select()
                  //  ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('category_id = ?', $category_id);
                   // ->limit(1);

        $object = $this->fetchAll($select);
        return $object;
    } 
	
	public function getCategoriesByParent($category_id)
    {
        $_name_01 = 'zend_category';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name_01))
                    ->where('parent_id = ?', $category_id);

        $categories = $this->fetchAll($select);
        return $categories;
    }
	
	public function getParentCategory($category_id)
	{
	    $_name = 'zend_category';
        $select = $this->select()
                    ->from($_name)
					->where('category_id = ?', $category_id);

		$category_rec = $this->fetchRow($select);	
        $parent_id = $category_rec["parent_id"];		
        return $parent_id;
    }
   
}
