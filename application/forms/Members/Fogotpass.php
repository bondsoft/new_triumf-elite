<?php
/**
 * This is the guestbook form.  It is in its own directory in the application 
 * structure because it represents a "composite asset" in your application.  By 
 * "composite", it is meant that the form encompasses several aspects of the 
 * application: it handles part of the display logic (view), it also handles 
 * validation and filtering (controller and model).  
 *
 * @uses       Zend_Form
 * @package    QuickStart
 * @subpackage Form
 */
class Default_Form_Members_Fogotpass extends Zend_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init()
    {
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		
        $this->setName('fogotpass');
		
		// Element: email
        $this->addElement('Text', 'email', array(
          'label' => $translate->translate('Email:'),
		  'size'  => '30', 
		  'description' => $translate->translate('You have been sent an email with new password.'),
          'required' => true,
          'allowEmpty' => false,
          'validators' => array(
            array('NotEmpty', true),
            array('EmailAddress', true),
          ),
		  'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'reset-email', 'placeholder' => 'E-mail'),
        ));
	  	
		// Init submit
        $this->addElement('Button', 'submit', array(
          'label' => $translate->translate('Send Email'),
          'type' => 'submit',
          'ignore' => true,
        ));
		
		
        
        
    }
}

