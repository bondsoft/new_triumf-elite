<?php

class Members_OrdersController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('members');
    }
	
	public function indexAction()
    {
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id; // Zend_Debug::dump($user_id);exit;
	   
        $users_object = new Default_Model_DbTable_Users();
		$user = $users_object->getUser($user_id);     
		// Zend_Debug::dump($user_id);exit;
	//	$this->view->user = $user;
		$email = $user['email'];
	//	Zend_Debug::dump($email);exit;
        $order_model = new Default_Model_DbTable_Order();
		$order_object = $order_model ->getUserOrders( $email );  
	//	 Zend_Debug::dump($order_object);exit;
		$this->view->orders = $order_object; 
		
		$paginator = Zend_Paginator::factory($order_object);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(50); 
    }
    
    public function orderdetailsAction()
    {
		$token = $this->getRequest()->getParam('token');
		
        $order_model = new Default_Model_DbTable_Order();
		$order_id = $order_model ->getOrderIdByToken($token); 
		$order_object = $order_model ->getOrderDetails($order_id); 
	//	 Zend_Debug::dump($order_object);exit;
		$this->view->orders = $order_object; 
		
	//	$order_object_01 = $order_model ->getOrder($order_id);
		$creation_date = $order_object[0]['creation_date'];
		$this->view->creation_date = $creation_date;
		
		$order_object_01 = $order_model ->getOrder($order_id);
		$this->view->order_object_01 = $order_object_01;
		
		$form = new Default_Form_Order_AddProduct();     
        $this->view->form = $form;
    }
	
    public function editAction()
    {
        $form = new Default_Form_Order_Order();     
        $this->view->form = $form;
        $order_model = new Default_Model_DbTable_Order();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
           //  Zend_Debug::dump($formData);exit;
   
            if ($form->isValid($formData)) {
         
                $howtobuy_id = (int)$form->getValue('howtobuy_id');
                $text = $form->getValue('text');
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                
                $howtobuy_model->updateHowtobuy(
													$howtobuy_id,
													$text,
													$header_tags_title,
													$header_tags_description,
													$header_tags_keywords
												);

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $howtobuy_id = $this->_getParam('howtobuy_id', 0);
            if ($howtobuy_id > 0) {
                $form->populate($howtobuy_model->getHowtobuy($howtobuy_id));
            }
        }
    }
    
    public function deleteorderAction()
    {
		$order_model = new Default_Model_DbTable_Order();
		$order_product_model = new Default_Model_DbTable_Orderproduct();
		$order_option_model = new Default_Model_DbTable_Orderoption();
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');

            if ($del == 'yes') {
                $order_id = $this->getRequest()->getPost('order_id');
				
                $order_model->deleteOrder($order_id);
				
				$order_product_model->deleteOrderProduct($order_id);
				
				$order_option_model->deleteOptionProduct($order_id);
            }       
            $this->_helper->redirector('index');
        } else {
            $order_id = $this->_getParam('order_id'); // Zend_Debug::dump($nashi_zeny_id);exit;
            
            $this->view->order_object = $order_model->getOrder($order_id);
        }
    }
	
	public function deleteorderproductajaxAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        
        $order_model = new Default_Model_DbTable_Order();
        $order_product_model = new Default_Model_DbTable_Orderproduct();
		$order_option_model = new Default_Model_DbTable_Orderoption();
        
        if ($this->getRequest()->isPost())
        {
			$order_id = $_POST['order_id'];
			$order_product_id = $_POST['order_product_id']; //Zend_Debug::dump($order_id);exit;
            $order_option_id = $_POST['order_option_id']; //Zend_Debug::dump($product_id);exit;
            
           
                $order_product_model->deleteProductOrderAjax(
                                                                $order_product_id                   
                                                            ); 
				$order_option_model->deleteProductOptionAjax(
                                                                $order_option_id                   
                                                            ); 
                                                            
				$products = $order_model->getOrderDetails($order_id); //Zend_Debug::dump($products);exit;
				
				foreach ($products as $product)
				{ 
					$sum[] = $product['quantity'] * number_format($product['price'], 0, '', '');
					$sum_cart = array_sum($sum);
				}
				$this->view->sum_cart = $sum_cart;
				
				$order_model->updateOrderSum(
												$order_id,
												$sum_cart
											);
				
		//	Zend_Debug::dump($sum_cart);exit;
				
				$json = $sum_cart;
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true, 'sum' => $json));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function addorderproductajaxAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        
        $order_model = new Default_Model_DbTable_Order();
        $order_product_model = new Default_Model_DbTable_Orderproduct();
        
        if ($this->getRequest()->isPost())
        {
			$order_id = $_POST['order_id']; //Zend_Debug::dump($order_id);exit;
            $product_id = $_POST['product_id']; //Zend_Debug::dump($product_id);exit;
            $quantity = $_POST['quantity']; //Zend_Debug::dump($quantity);exit;
           
			$check_order_product = $order_product_model->checkOrderProduct(
																			$order_id,     
																			$product_id			
																		); 
			$quantity_old = $order_product_model->getOrderProductQuantity(
																			$order_id,     
																			$product_id			
																		);
		//	Zend_Debug::dump($quantity_old);exit;
			$quantity_sum = $quantity_old + $quantity;
			if($check_order_product) {
				$order_product_model->updateProductOrderAjax(
																$order_id,     
																$product_id,
																$quantity_sum
															); 
			} else {
                $order_product_model->addProductOrderAjax(
                                                            $order_id,     
															$product_id,
															$quantity							
                                                        ); 
            }                                                
				$products = $order_model->getOrderDetails($order_id); //Zend_Debug::dump($products);exit;
				
				foreach ($products as $product)
				{ 
					$sum[] = $product['quantity'] * number_format($product['price_rub'], 0, '', '');
					$sum_cart = array_sum($sum);
				}
				$this->view->sum_cart = $sum_cart;
				
				$order_model->updateOrderSum(
												$order_id,
												$sum_cart
											);
				
		//	Zend_Debug::dump($sum_cart);exit;
				
				$json = $sum_cart;
				
                       
        //    header('Content-Type: application/json');
        //    echo Zend_Json::encode(array('success' => true, 'sum' => $json));
		//	exit;
			$this->_helper->redirector('orderdetails', 'adminorders', 'admin', array('order_id' => $order_id));
                                    
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
}

