<?php

/**
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Translations extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_translations';
    
    protected $_primary = 'translation_id';
	
	public function getTranslations($lang)
	{
        $_name = 'zend_translations';
        $select = $this->select()
                        ->from(array('name' => $_name))
                        ->where('lang = ?', $lang );  
 
        return $this->fetchAll($select);
    }
    
    public function getTranslation($translation_id)
    {
        $translation_id = (int)$translation_id;
        $row = $this->fetchRow('translation_id = ' . $translation_id);
        if(!$row) {
            throw new Exception("Нет записи с translation_id - $translation_id");
        }
        return $row->toArray();
    }
	
	public function addTranslation(	$lang, 
                                    $translations_key,
                                    $translations_value )
    {
        $data = array(
            'lang' => $lang,
			'translations_key' => $translations_key,
			'translations_value' => $translations_value
        );
        $this->insert($data);
    }
    
    public function updateTranslation(  $translation_id,
                                        $translations_value )
    {
        $data = array(
            'translations_value' => $translations_value
        );
        $this->update($data, 'translation_id = ' . (int)$translation_id);
    }
	
}
