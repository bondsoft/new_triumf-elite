<?php

/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Newsseo extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_news_seo';
	
	public function getNewsseo($news_seo_id)
    {
        $news_seo_id = (int)$news_seo_id;
        $row = $this->fetchRow('news_seo_id = ' . $news_seo_id);
        if(!$row) {
            throw new Exception("��� ������ � news_seo_id - $news_seo_id");
        }
        return $row->toArray();
    }
	
	public  function updateNewsseo(
                                        $news_seo_id,
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                    )
    {
        $data = array( 
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords			
        );
        
        $this->update($data, 'news_seo_id = ' . (int)$news_seo_id);
    }

}
