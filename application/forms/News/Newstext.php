<?php
/**
 * @uses       Zend_Form
 * @package    QuickStart
 * @subpackage Form
 */
class Default_Form_News_Newstext extends Zend_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init()
    {	
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
	    // 
        $this->setTranslator($adapter);
		
       
		$newstext_id = new Zend_Form_Element_Hidden('newstext_id');
        $newstext_id->addFilter('Int');
		$news_id = new Zend_Form_Element_Hidden('news_id');
        $news_id->addFilter('Int');
			
		$news_text = new Zend_Form_Element_Textarea('news_text');
        $news_text->setLabel('')
		    ->setAttrib('cols', 110)
            ->setAttrib('rows', 20)
            ->setAttrib('id', 'article_text')
			->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534)); 	
		
		$submit = new Zend_Form_Element_Submit("Сохранить");
        $submit->setAttrib('articletext_id', 'submitbutton')
				->setAttrib('class', 'btn btn-primary pull-left');
        $this->addElements(array(
                                    $newstext_id,
                                    $news_id,
                                    $news_text, 
                                    $submit
                                ));
		
		
			       
    }
}

