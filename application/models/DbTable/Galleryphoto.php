<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Galleryphoto extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_gallery_photo';
	
	public function getPhoto($photo_id)
    {
        $photo_id = (int)$photo_id;
        $row = $this->fetchRow('photo_id = ' . $photo_id);
        if(!$row) {
            throw new Exception("no record photo_id - $photo_id");
        }
        return $row->toArray();
    }
	
	public function getGalleryAdmin( $gallery_id )
    {
        $_name = 'zend_gallery_photo';
        $select = $this->select()
                    ->from($_name)
					->where('gallery_id = ?', $gallery_id)
					->order('sort_order');
     
        $photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function getGalleryFront()
    {
        $_name = 'zend_gallery_photo';
        $select = $this->select()
                    ->from($_name)
					->where('status = ?', '1')
					->order('sort_order');
     
        $photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function getGalleryImage($photo_id)
	{
	    $_name = 'zend_gallery_photo';
        $select = $this->select()
                    ->from($_name)
					->where('photo_id = ?', $photo_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["image"];		
        return $main_photo_file_name;
    }
	
	public function addPhoto(
								$gallery_id,
								$image,
								$alt,
								$sort_order                                                  
                            )
    {
        $data = array(
			'gallery_id' => $gallery_id,
			'image' => $image,
			'alt' => $alt,
			'sort_order' => $sort_order	
        );
        $this->insert($data);
    }
	
	public  function updateGallery( 
										$photo_id,
                                        $image,
										$alt,
										$sort_order   
								 )
    {
        $data = array(
            'image' => $image,
			'alt' => $alt,
			'sort_order' => $sort_order	
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }
	
	public  function updatePhotoStatus(
                                            $photo_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }
	
	public  function deleteGalleryImage(
                                            $photo_id,  
                                            $image
                                        )
    {
        $data = array(
            'image' => $image,
        );
        
        $this->update($data, 'photo_id = ' . (int)$photo_id);
    }
	
	public function getAllPhotosForGallery($gallery_id)
	{
		$_name = 'zend_gallery_photo';
        $select = $this->select()
                    ->from($_name)
                    ->where('gallery_id = ?', $gallery_id);
     
		$photos = $this->fetchAll($select);
        return $photos;
    }
	
}
