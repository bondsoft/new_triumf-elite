<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Option extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_option';
	
	public function getOption($option_id)
    {
        $option_id = (int)$option_id;
        $row = $this->fetchRow('option_id = ' . $option_id);
        if(!$row) {
            throw new Exception("no record option_id - $option_id");
        }
        return $row->toArray();
    }
	
	public function addOption(
                                $type,
                                $sort_order                                                         
                            )
    {
        $data = array(
			'type' => $type,
			'sort_order' => $sort_order
        );
        $this->insert($data);
    }
	
	public function deleteOption($option_id)
    {
        $this->delete('option_id = ' . (int)$option_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateOption( 
                                    $option_id,
                                    $type,
									$sort_order        
								 )
    {
        $data = array(
            'type' => $type,
			'sort_order' => $sort_order
        );
        
        $this->update($data, 'option_id = ' . (int)$option_id);
    }
    
    public function getAllOptionsAdmin()
    {
        $_name = 'zend_option';
		$_name_01 = 'zend_option_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id')
                    ->order('type');
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }
	
	public function getOptions(
								$data = array()
							)
    {
        $_name = 'zend_option';
		$_name_01 = 'zend_option_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
					->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id')
                    ->where('name  LIKE ?', $data['filter_name'].'%')
					->limit($data['limit']);
                     
        $options = $this->fetchAll($select);
        return $options;
    }
	
	
    
	public function getOptionValueDescriptionAdmin($option_id)
    {  
        $_name = 'zend_option';
		$_name_01 = 'zend_option_value_description';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.option_id = ?', $option_id)
					->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id');
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }
	

	
	public function getOptionItemAdmin($option_id)
    {  
        $_name = 'zend_option';
		$_name_01 = 'zend_option_description';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.option_id = ?', $option_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id', array('name'));
                     
        $row = $this->fetchRow($select);
        return $row->toArray();
    }
	
	public function getOptionItemAdminAjax($option_id)
    {  
        $_name = 'zend_option';
		$_name_01 = 'zend_option_description';
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('name' => $_name))
                    ->where('name.option_id = ?', $option_id)
                    ->joinLeft(array('name_01' => $_name_01),'name.option_id = name_01.option_id', array('name'));
                     
        $all_options = $this->fetchAll($select);
        return $all_options;
    }
	
	public  function addProductsAttributes( 
                                                $product_id,
                                                $attribute_id,
                                                $text      
                                            )
    {
        $data = array(
            'product_id' => $product_id,
            'attribute_id' => $attribute_id,
            'text' => $text
        );
        
        $this->insert($data);
    }
	
	public  function deleteOptionAjax(
                                            $option_id,
											$attribute_id
                                        )
    {
		
        $this->delete(array(
            'product_id = ?' => $product_id,
            'attribute_id = ?' => $attribute_id
        ));
    } 
    
    
	
	public  function getLastInsertId()
    {
        $_name = 'zend_option';
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('option_id DESC')
                    ->limit(1);
                     
        $LastInsertId_rec = $this->fetchRow($select);	
        $LastInsertId = $LastInsertId_rec["option_id"];		
        return $LastInsertId;
    }
	

}
