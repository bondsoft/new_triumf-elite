<?php
/**
 * Contact controller
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class Sliderthree_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('our_team');
    }
	
    public function indexAction()
    {	
		$helper = $this->_helper->getHelper('Layout')->disableLayout();
		
		$slider_model = new Default_Model_DbTable_Sliderthree();
        $sliders = $slider_model->getAllSliderFront(); //Zend_Debug::dump($products);exit;
        
		$this->view->sliders = $sliders;
    }
	
	public function sliderAction()
    {	
		$slider_model = new Default_Model_DbTable_Sliderthree();
		$slider_url = $this->getRequest()->getParam('slider_url');
    $slider = $slider_model->getSliderItem( $slider_url ); //Zend_Debug::dump($products);exit;
        
		$this->view->slider = $slider;
		
		$page = $slider_model->getSliderItemSeo( $slider_url ); //Zend_Debug::dump($products);exit;
        
		$this->view->page = $page;
		
		$slider_id = $slider_model->getSliderIdByUrl($slider_url);
		
		$photo_model = new Default_Model_DbTable_Sliderthreephoto();
		$slider_photo = $photo_model->getAllPhotosForObject($slider_id);
		$this->view->slider_photo = $slider_photo;
    }
  public function ourteamAction()
    {	
    	$helper = $this->_helper->getHelper('Layout')->disableLayout();

			$slider_model = new Default_Model_DbTable_Sliderthree();
      $sliders = $slider_model->getAllSliderFront(); //Zend_Debug::dump($products);exit;

			$this->view->sliders = $sliders;
    }
}