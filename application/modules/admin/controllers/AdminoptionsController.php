<?php

class Admin_AdminoptionsController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    public function indexAction()
    {
        $options_model = new Default_Model_DbTable_Option();
        $options = $options_model->getAllOptionsAdmin(); //Zend_Debug::dump($products);exit;
		
        $paginator = Zend_Paginator::factory($options);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);   
    
    }
    
    public function addAction()
    {
        include( 'SimpleImage.php' );
		
        $form = new Default_Form_Option_AddOption(); 
        $this->view->form = $form;
        $option_model = new Default_Model_DbTable_Option();
		$option_description_model = new Default_Model_DbTable_Optiondescription();
		$option_value_model = new Default_Model_DbTable_Optionvalue();
		$option_value_description_model = new Default_Model_DbTable_Optionvaluedescription();
        
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
		
				$name = $_POST['name'];
				$type = $_POST['type'];    //Zend_Debug::dump($product_model);exit;
                $sort_order = $_POST['sort_order'];
             
                
                $option_model->addOption(
                                                $type,
                                                $sort_order
                                                                   
                                            );
											
				$option_id = $option_model->getLastInsertId();
				$this->view->option_id = $option_id;
			//	Zend_Debug::dump($option_id);exit;
											
				$option_description_model->addOptionDescription(
																	$option_id,
																	$name				   
																);
			
			
				$option_array = array();
				$option_array = $_POST['option_value'];
				$image_array = array();
				$image_array = $_FILES['option_value']; 
			
				foreach ($option_array as $key => $value) 
				{   
				//	Zend_Debug::dump($image_array['name'][$key]['image']);exit;
				
					$uploadedfile = $image_array['tmp_name'][$key]['image'];
					
					
					if(file_exists($uploadedfile))
                        {
                        /* ********************************************** */
                            $ext = explode(".",$image_array['name'][$key]['image']); 
                            $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                            $name_big = "media/photos/option/big_".$newname;
                            $name_small = "media/photos/option/small_".$newname;
                              
                        /* ********************************************** */
                            
                            
                            $image_large = new SimpleImage();
                            $image_small = new SimpleImage();
                            $image_large->load($uploadedfile);
                            $image_small->load($uploadedfile);
                            
                         //   $image_large->resizeToWidth(900);
                            $image_large->save($name_big);
                            
                         //   $image_small->cutFromCenter(300, 300);
                            $image_small->resizeToWidth(300);
                            $image_small->save($name_small);  
                            
                        } 
					
					$option_value_model->addOptionValue(
															$option_id,
															$newname,
															$value['sort_order'] 
														);
					
														
					$option_value_id = $option_value_model->getLastInsertId();
					$this->view->option_value_id = $option_value_id;
																	
					$option_value_description_model->addOptionValueDescription(
																				$option_value_id,
																				$option_id,
																				$value['name']
																			);
				} 
			                             
             //   unlink($_FILES['image']['tmp_name']);                            
                    
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editAction()
    {
        include( 'SimpleImage.php' );
		
		$form = new Default_Form_Option_EditOption(); 
        $this->view->form = $form;
        $option_model = new Default_Model_DbTable_Option();
		$option_description_model = new Default_Model_DbTable_Optiondescription();
		$option_value_model = new Default_Model_DbTable_Optionvalue();
		$option_value_description_model = new Default_Model_DbTable_Optionvaluedescription();
        
        $option_id = $this->getRequest()->getParam('option_id');
        $option_values = $option_value_model->getOptionItemAdmin($option_id); //Zend_Debug::dump($option_values);exit;
        $this->view->option_values = $option_values;
        
        $option = $option_model->getOption($option_id); 
        $this->view->option = $option; 
		
        $directory = "media/photos/option";
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $name = $_POST['name'];
				$type = $_POST['type'];    //Zend_Debug::dump($product_model);exit;
                $sort_order = $_POST['sort_order'];				
 
                $option_model->updateOption(
												$option_id,
												$type,
												$sort_order  
											);
												
			/*	$option_description_model->updateOptionDescription(
																		$option_id,
																		$name				   
																	);
				$option_value_model->deleteOptionValue(
															$option_id
														);
														
				$option_value_description_model->deleteOptionValueDescription(
																				$option_id
																			);*/
																			
			//	$option_value = $option_value_model->getOptionValue( $option_id );
																	
				$option_array = array();
				$option_array = $_POST['option_value'];
			//	Zend_Debug::dump($option_array);exit;
				$image_array = array();
				$image_array = $_FILES['option_value']; 
				
				
				foreach ($option_array as $key => $value) 
				{   
				//	Zend_Debug::dump($image_array['name'][$key]['image']);exit;
				
					$uploadedfile = $image_array['tmp_name'][$key]['image'];
					
					//  Zend_Debug::dump($uploadedfile);exit;
					$option_value_id = $option_value_model->getLastInsertId();
					$this->view->option_value_id = $option_value_id;
					if($option_value_id) {	
						$old_photo_file = $option_value_model->getOptionImage($option_value_id);
					}
				//	Zend_Debug::dump($old_photo_file);exit;

				
					if($uploadedfile)
					{    
						$old_photo_file = $value['img'];
						if(is_file("$directory/small_$old_photo_file"))
						{
							unlink("$directory/big_$old_photo_file");
							unlink("$directory/small_$old_photo_file");
						}
					} 
                
					if(!$uploadedfile)
					{
						$newname = $value['img'];
					}			
					if(file_exists($uploadedfile))
                    {
                        /* ********************************************** */
                        $ext = explode(".",$image_array['name'][$key]['image']); 
                        $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                        $name_big = "media/photos/option/big_".$newname;
                        $name_small = "media/photos/option/small_".$newname;
                              
                        /* ********************************************** */
                              
                        $image_large = new SimpleImage();
                        $image_small = new SimpleImage();
                        $image_large->load($uploadedfile);
                        $image_small->load($uploadedfile);
                            
                         //   $image_large->resizeToWidth(900);
                        $image_large->save($name_big);
                            
                         //   $image_small->cutFromCenter(300, 300);
                        $image_small->resizeToWidth(300);
                        $image_small->save($name_small);  
                            
                    } 
					
					if($value['option_value_id']) {	
						$option_value_model->updateOptionValue_01(
																	$value['option_value_id'],
																	$option_id,
																	$image,
																	$sort_order  
																);
					} else {
						$option_value_model->addOptionValue(
																$option_id,
																$newname,
																$value['sort_order'] 
															);
					}
					
														
					$option_value_id = $option_value_model->getLastInsertId();
					$this->view->option_value_id = $option_value_id;
					
					if($value['option_value_id']) {	
						$option_value_description_model->updateOptionValueDescription_01(
																				$value['option_value_id'],
																				$option_id,
																				$value['name']
																			);
					} else {
						$option_value_description_model->addOptionValueDescription(
																					$option_value_id,
																					$option_id,
																					$value['name']
																				);
					}
				}
												
				
                $this->_helper->redirector('index');
				} else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        } else { 
            $option_id = $this->_getParam('option_id', 0);
            if ($option_id > 0)
            {
                $form->populate($option_model->getOptionItemAdmin($option_id));
                $option_object = $option_model->getOptionItemAdmin($option_id);
                $this->view->option_name = $option_object['name'];
            }
        }
    }
    
    public function deleteAction()
    {
		$options_model = new Default_Model_DbTable_Option();
		$options_description_model = new Default_Model_DbTable_Optiondescription();
		$option_value_model = new Default_Model_DbTable_Optionvalue();
		$option_value_description_model = new Default_Model_DbTable_Optionvaluedescription();
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $option_id = $this->getRequest()->getPost('option_id');
				
				$all_photos = $option_value_model->getAllPhotosForOption($option_id); 
				$directory = "media/photos/option";
			//	Zend_Debug::dump($all_photos);exit;
				foreach($all_photos as $photo)
                {
                    unlink($directory .'/big_'.$photo["image"]);
                    unlink($directory .'/small_'.$photo["image"]); 
                }
				
				$options_model->deleteOption(
												$option_id
											);
                
                $options_description_model->deleteOptionDescription(
																		$option_id
																	);
																	
				$option_value_model->deleteOptionValue(
															$option_id
														);
														
				$option_value_description_model->deleteOptionValueDescription(
																				$option_id
																			);
            }       
            $this->_helper->redirector('index');
        } else {
            $option_id = $this->_getParam('option_id'); // Zend_Debug::dump($nashi_zeny_id);exit;
            
            $options_model = new Default_Model_DbTable_Option();
            $option = $options_model->getOptionItemAdmin($option_id);
            $this->view->option = $option;
			//Zend_Debug::dump($option);exit;
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        if ($this->getRequest()->isPost())
        {
            $product_id = $this->getRequest()->getParam('product_id');
            $status = '1';
            $products_model->updateProductstatus(
                                                    $product_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $products_model = new Default_Model_DbTable_Products();
        
        if ($this->getRequest()->isPost())
        {
            $product_id = $this->getRequest()->getParam('product_id');
            $status = '0';
            $products_model->updateProductstatus(
                                                    $product_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $option_value_model = new Default_Model_DbTable_Optionvalue();
        $option_value_id = $this->getRequest()->getParam('option_value_id');
        
        $old_photo_file = $option_value_model->getOptionImage($option_value_id);
        $directory = "media/photos/option";
        
        if ($this->getRequest()->isPost())
        {
            $option_value_id = $this->getRequest()->getParam('option_value_id');
            $image = '';
           
            $option_value_model->deleteOptionImage(
														$option_value_id,  
														$image
													);
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
    public function editattributesAction()
    {
        $productsattributes_model = new Default_Model_DbTable_Productsattributes();
        
        $product_id = $this->getRequest()->getParam('product_id'); 
        $this->view->product_id = $product_id;
      
        if ($this->getRequest()->isPost())
        { 
            $attributes = array();
            $attributes = $_POST['product_attribute']; //Zend_Debug::dump($attributes[0]);exit;
            
            if (!empty($attributes[0]['product_id'])) {
            
            foreach ($attributes as $product_attribute)  // Zend_Debug::dump($value);exit;
            {   
                $productsattributes_model->deleteAttributeAjax(
                                                                        $product_attribute['product_id'],
                                                                        $product_attribute['attribute_id']
                                                                   ); 
            
                $productsattributes_model->addProductsAttributes(
                                                                    $product_attribute['product_id'],
                                                                    $product_attribute['attribute_id'],
                                                                    $product_attribute['text'] 
                                                                );
            } 
            }
                
            $this->_helper->redirector('index');
           
        }
    }
	
	public function deleteoptionajaxAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $option_value_model = new Default_Model_DbTable_Optionvalue();
		$option_value_description_model = new Default_Model_DbTable_Optionvaluedescription();
        
        if ($this->getRequest()->isPost())
        {
            $option_value_id = $_POST['option_value_id']; //Zend_Debug::dump($product_id);exit;
			
			$option_image = $option_value_model->getOptionImage($option_value_id); 
			$directory = "media/photos/option";
			//	Zend_Debug::dump($option_image);exit;
			
                unlink($directory .'/big_'.$option_image["image"]);
                unlink($directory .'/small_'.$option_image["image"]); 
         
           
            $option_value_model->deleteOptionValueRow(
														$option_value_id
													);
														
			$option_value_description_model->deleteOptionValueDescriptionRow(
																				$option_value_id
																			);
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function options_02Action()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
		$options_model = new Default_Model_DbTable_Option();
        
        $option_value_model = new Default_Model_DbTable_Optionvalue();
		
        if ($this->getRequest()->isPost())
        {
			$options = $options_model->getAllOptionsAdmin();
			
			foreach ($options as $option) {
				$option_value_data = array();
				
				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') {
					$option_values = $option_value_model->getOptionItemAdmin($option['option_id']);
					
					foreach ($option_values as $option_value) {
													
						$option_value_data[] = array(
							'option_value_id' => $option_value['option_value_id'],
							'name'            => $option_value['name'],
							'image'           => $option_value['image']					
						);
					}
					
								
				}
											
				$json[] = array(
					'option_id'    => $option['option_id'],
					'name'         => $option['name'],
					'type'         => $option['type'],
					'option_value' => $option_value_data
				);
			}
			
                          
            header('Content-Type: application/json');
            echo Zend_Json::encode(array($json));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function optionsAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $option_value_model = new Default_Model_DbTable_Optionvalue();
		
        if ($this->getRequest()->isPost())
        {
            $option_id = $this->getRequest()->getParam('option_id');
			
            $options = $option_value_model->getOptionItemAdmin( $option_id );
		//	$regions_json = array();
			foreach ($options as $option)
            {
                $option_value_id[] = $option['option_value_id'];
                $name[] = $option['name'];
            }
		//	$regions = $regions_json;
        //    Zend_Debug::dump($regions_json);exit;                        
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('option_value_id' => @$option_value_id, 'name' => @$name));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    } 
	
	public function autocompleteAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
		$json = array();
		
	
		if ($this->getRequest()->getParam('filter_name')) {
			$option_model = new Default_Model_DbTable_Option();
			$option_value_model = new Default_Model_DbTable_Optionvalue();
		
			$data = array(
					'filter_name' => $this->getRequest()->getParam('filter_name'),
					'start'       => 0,
					'limit'       => 20
				);
			$options = $option_model->getOptions( $data );
		//	 Zend_Debug::dump($options);exit; 
		 
			foreach ($options as $option) {
				$option_value_data = array();
				
				if ($option['type'] == 'select') {
					$option_values = $option_value_model->getOptionValues($option['option_id']);
				
					foreach ($option_values as $option_value) {
													
						$option_value_data[] = array(
							'option_value_id' => $option_value['option_value_id'],
							'name'            => $option_value['name']				
						);
					}
					
					$sort_order = array();

					foreach ($option_value_data as $key => $value) {
						$sort_order[$key] = $value['name'];
					}

					array_multisort($sort_order, SORT_ASC, $option_value_data);				
				}
											
				$json[] = array(
					'option_id'    => $option['option_id'],
					'name'         => $option['name'],
					'type'         => $option['type'],
					'option_value' => $option_value_data
				);
			}
		}	
            $sort_order = array();

			foreach ($json as $key => $value) {
				$sort_order[$key] = $value['name'];
			}    
			array_multisort($sort_order, SORT_ASC, $json);		
            header('Content-Type: application/json');
            echo Zend_Json::encode($json);
            exit;                        
    }
     
	
}

