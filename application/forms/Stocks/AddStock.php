<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Stocks_AddStock extends Zend_Form
{
    public function init()
    {	
		$stock_id = new Zend_Form_Element_Hidden('stock_id');
        $stock_id->addFilter('Int');
        
        $request = Zend_Controller_Front::getInstance();
        $id = $request->getRequest()->getParam('stock_id');
       
        $creation_date = new Zend_Form_Element_Text('creation_date');
        $creation_date->setLabel("Дата")
            ->setAttrib('size', '30')
            ->setAttrib('id', 'calendar-inputField')
            ->setAttrib('class', 'span4')
            ->addValidator('Date', true, array('yyyy.MM.dd'))
			->removeDecorator('label');
            
		$stock_title = new Zend_Form_Element_Text('stock_title');
        $stock_title->setLabel("Название статьи")
		    ->setAttrib('size', '120')
            ->setRequired(true)
			->setAttrib('class', 'span12')
			->setAttrib('id', 'name')
			->setAttrib('onkeyup', 'translit()')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
			->removeDecorator('label');
			
        $intro_text = new Zend_Form_Element_Textarea('intro_text');
        $intro_text->setLabel('Вступление к новости')
            ->setAttrib('cols', 120)
            ->setAttrib('rows', 5)
			->setAttrib('class', 'span12')
            ->setAttrib('id', 'text_01')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,5000))
			->removeDecorator('label');  
            
        $full_text = new Zend_Form_Element_Textarea('full_text');
        $full_text->setLabel('Текст новости')
            ->setAttrib('cols', 110)
            ->setAttrib('rows', 10)
            ->setAttrib('id', 'text_02')
            ->setAttrib('class', 'span12')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
			->removeDecorator('label');   


		$main_photo = new Zend_Form_Element_File('main_photo');
        $main_photo->setLabel("Выбрать фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif")
            ->setAttrib('size', '50')
        //  ->addValidator('Count', false, array('min' => 1, 'max' => 5))
        //  ->setMultiFile(1)
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/stocks/main_photos')
            ->setValueDisabled(true)
			->removeDecorator('label');
        //  ->setRequired(true)
        //  ->addValidator('NotEmpty', false); additional 
		
		$alt = new Zend_Form_Element_Text('alt');
        $alt->setLabel("alt:")
            ->setAttrib('class', 'span10')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');

        
        $show_photo_inside = new Zend_Form_Element_Text('show_photo_inside');
        $show_photo_inside->setLabel("Показывать рисунок внутри статьи (1/0):")
            ->setAttrib('class', 'span10')
            ->removeDecorator('label');
        
        $url_seo = new Zend_Form_Element_Text('url_seo');
        $url_seo->setLabel("SEO Url")
            ->setAttrib('size', '120')
            ->setRequired(true)
			->setAttrib('class', 'span12')
			->setAttrib('id', 'alias')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
			->removeDecorator('label')
            ->addValidator('Db_NoRecordExists', false, array(   'zend_stocks', 
                                                                'url_seo', 
                                                                'exclude' => array(
                                                                                    'field' => 'stock_id',
                                                                                    'value' => $id
                                                                                    )
                                                            )
            );  
            
        $header_tags_title = new Zend_Form_Element_Text('header_tags_title');
        $header_tags_title->setLabel("title")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->setRequired(true)
			->addValidator('NotEmpty', true)
			->removeDecorator('label');
            
        $header_tags_description = new Zend_Form_Element_Text('header_tags_description');
        $header_tags_description->setLabel("description")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->setRequired(true)
			->addValidator('NotEmpty', true)
			->removeDecorator('label');

        $header_tags_keywords = new Zend_Form_Element_Text('header_tags_keywords');
        $header_tags_keywords->setLabel("keywords")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->setRequired(true)
			->addValidator('NotEmpty', true)
			->removeDecorator('label');

        $this->addElements(array(
                                    $stock_id,
                                    $creation_date,
                                    $stock_title,
                                    $intro_text,
                                    $full_text,
                                    $url_seo,
                                    $main_photo,
                                    $alt,
                                    $show_photo_inside,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                ));
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'type' => 'submit',
		  'class' => 'btn btn-primary pull-left',
          'ignore' => true,
        ));	 
    }
}

