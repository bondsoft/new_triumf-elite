<?php

class Admin_LangController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');		
    }

    public function indexAction()
    {	
    
        $translations = new Default_Model_DbTable_Translations();
        $translations_text_ru = $translations->getTranslations('ru');
        
        $this->view->translations_text_ru = $translations_text_ru;  
       
        $translations_text_en = $translations->getTranslations('en');
        
        $this->view->translations_text_en = $translations_text_en;   
		   
    }
   
    public function ruAction()
    {   
        $lang = $this->getRequest()->getParam('lang');
        $this->view->lang = $lang;
        
        $translations = new Default_Model_DbTable_Translations();
        $translations_text = $translations->getTranslations($lang);
        
        $this->view->translations_text = $translations_text;          
    }
    
    public function enAction()
    {   
        $lang = $this->getRequest()->getParam('lang');
        $this->view->lang = $lang;
        
        $translations = new Default_Model_DbTable_Translations();
        $translations_text = $translations->getTranslations($lang);
        
        $this->view->translations_text = $translations_text;       
    }
    
    public function addAction()
    {
        $form = new Default_Form_Lang_Lang(); 
        $this->view->form = $form;
        
        $lang = $this->getRequest()->getParam('lang');
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) 
            { 
                $translations_key = $form->getValue('translations_key');
                $translations_value = $form->getValue('translations_value');
                                        
                $model_object = new Default_Model_DbTable_Translations();
               
                $model_object->addTranslation(  $lang, 
                                                $translations_key,
                                                $translations_value    
                                             );
              
                $this->_helper->redirector($lang, 'lang', 'admin', array('lang' => $lang));
            } else {
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }
	
	public function editAction()
    {
        $form = new Default_Form_Lang_EditLang(); 
        $this->view->form = $form;
        
        $lang = $this->getRequest()->getParam('lang');
        $translation_id = $this->getRequest()->getParam('translation_id');
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
          //   Zend_Debug::dump($formData);exit;
            if ($form->isValid($formData)) {
                $translations_value = $form->getValue('translations_value'); //Zend_Debug::dump($translations_value);exit;
                
               
                $model_object = new Default_Model_DbTable_Translations();
                
               
               
                $model_object->updateTranslation(   $translation_id,
                                                    $translations_value    
                                             );
                
                $this->_helper->redirector($lang, 'lang', 'admin', array('lang' => $lang));
            } else {
                $form->populate($formData);
            }
        } else {
            $translation_id = $this->_getParam('translation_id', 0);
            if ($translation_id > 0) {
                $model_object = new Default_Model_DbTable_Translations();
                
                $form->populate($model_object->getTranslation($translation_id));
            }
        }
    }
    
    
	
	
}

