<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_GalleryController extends Zend_Controller_Action
{
	 public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    public function indexAction()
    {
        $gallery_model = new Default_Model_DbTable_Gallery();
	
        $gallery = $gallery_model->getGalleryAdmin();
		//Zend_Debug::dump($gallery);exit;
		
        $paginator = Zend_Paginator::factory($gallery);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);   
    
    }
	
	public function photoAction()
    {
        $galleryphoto_model = new Default_Model_DbTable_Galleryphoto();
		$gallery_id = $this->getRequest()->getParam('gallery_id');
		$this->view->gallery_id = $gallery_id;
        $gallery = $galleryphoto_model->getGalleryAdmin( $gallery_id );
		//Zend_Debug::dump($gallery);exit;
		
        $paginator = Zend_Paginator::factory($gallery);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);   
    
    }
    
    public function addAction()
    {
        include( 'SimpleImage.php' );
			
        $form = new Default_Form_Gallery_Add(); 
        $this->view->form = $form;
        $gallery_model = new Default_Model_DbTable_Gallery();
		
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $title = $form->getValue('title');
				$text = $form->getValue('text');
                $sort_order = $form->getValue('sort_order');
				
                $gallery_model->addGallery(
                                                $title,
												$text,
												$sort_order
                                            );
				                        
                    
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editAction()
    {	
        $form = new Default_Form_Gallery_Edit(); 
        $this->view->form = $form;
        $gallery_model = new Default_Model_DbTable_Gallery();
        
        $gallery_id = $this->getRequest()->getParam('gallery_id');
	//	Zend_Debug::dump($id);exit;
       
        $this->view->gallery_id = $gallery_id; 
        
        
        $gallery = $gallery_model->getGallery($gallery_id); //Zend_Debug::dump($products_attributes);exit;
        $this->view->gallery = $gallery; //Zend_Debug::dump($product['manufacturer_id']);exit;
		
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
         //   Zend_Debug::dump($formData);exit;
            if ($form->isValid($formData)) {
                $gallery_id = $form->getValue('gallery_id');
				$title = $form->getValue('title');
				$text = $form->getValue('text');
                $sort_order = $form->getValue('sort_order');
										
 
                $gallery_model->updateGallery(
                                                    $gallery_id,
                                                    $title,
													$text,
													$sort_order
                                                );
												
				
				
                $this->_helper->redirector('index', 'gallery', 'admin', array('page' => $page));
				} else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        } else { 
            $gallery_id = $this->_getParam('gallery_id', 0);
            if ($gallery_id > 0)
            {
                $form->populate($gallery_model->getGallery($gallery_id));
            }
        }
    }
    
    public function deleteAction()
    {
		$gallery_model = new Default_Model_DbTable_Gallery();
		$galleryphoto_model = new Default_Model_DbTable_Galleryphoto();
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $gallery_id = $this->getRequest()->getPost('gallery_id');
            
                //Zend_Debug::dump($old_photo_file);exit;
				$directory = "media/photos/gallery";
				$all_photos = $galleryphoto_model->getAllPhotosForGallery($gallery_id);
				
				foreach($all_photos as $photo)
                {
                    unlink($directory .'/big_'.$photo["image"]);
                    unlink($directory .'/small_'.$photo["image"]); 
                }
               
                $gallery_model->deleteGallery(
													$gallery_id
												);
            }       
            $this->_helper->redirector('index');
        } else {
            $gallery_id = $this->_getParam('gallery_id'); // Zend_Debug::dump($nashi_zeny_id);exit;
            
            $this->view->gallery_object = $gallery_model->getGallery($gallery_id);
        }
    }
	
	public function addphotoAction()
    {
        include( 'SimpleImage.php' );
			
        $form = new Default_Form_Gallery_AddPhoto(); 
        $this->view->form = $form;
        $galleryphoto_model = new Default_Model_DbTable_Galleryphoto();
		$gallery_id = $this->getRequest()->getParam('gallery_id');
		$this->view->gallery_id = $gallery_id;
		
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $alt = $form->getValue('alt');
                $sort_order = $form->getValue('sort_order');
				
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];
					//Генерируем новое имя файла во избежании совпадения названий
                //    $name_normal = "media/photos/products/".$newname;
					$name_big = "media/photos/gallery/big_".$newname;
                    $name_small = "media/photos/gallery/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->resizeToWidth(300);
                    $image_small->save($name_small);  
					
					
                }  
                
                $galleryphoto_model->addPhoto(
													$gallery_id,
													$newname,
													$alt,
													$sort_order
												);
				                        
                unlink($_FILES['image']['tmp_name']);                            
                    
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editphotoAction()
    {
        include( 'SimpleImage.php' );
			
        $form = new Default_Form_Gallery_EditPhoto(); 
        $this->view->form = $form;
        $gallery_model = new Default_Model_DbTable_Galleryphoto();
        
        $photo_id = $this->getRequest()->getParam('photo_id');
		$gallery_id = $this->getRequest()->getParam('gallery_id');
	//	Zend_Debug::dump($id);exit;
        $image = $gallery_model->getGalleryImage($photo_id);
        $this->view->image = $image; 
        $this->view->photo_id = $photo_id; 
        
        
        $gallery = $gallery_model->getPhoto($photo_id); //Zend_Debug::dump($products_attributes);exit;
        $this->view->gallery = $gallery; //Zend_Debug::dump($product['manufacturer_id']);exit;
		
        $old_photo_file = $gallery_model->getGalleryImage($photo_id);
        $directory = "media/photos/gallery";
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $photo_id = $form->getValue('photo_id');
				$alt = $form->getValue('alt');
                $sort_order = $form->getValue('sort_order');
				$image = $form->getValue('image');
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                        //  Zend_Debug::dump($uploadedfile);exit;
                if($image)
                {       
                    if(is_file("$directory/small_$old_photo_file"))
                    {
						unlink("$directory/big_$old_photo_file");
						unlink("$directory/small_$old_photo_file");
                    }
                }
                
               if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];
					//Генерируем новое имя файла во избежании совпадения названий
                //    $name_normal = "media/photos/products/".$newname;
					$name_big = "media/photos/gallery/big_".$newname;
                    $name_small = "media/photos/gallery/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->resizeToWidth(300);
                    $image_small->save($name_small);  	
                }  
                
                if(!$image)
                {      
                    $gallery_object = $gallery_model->getPhoto($photo_id);
                    $newname = $gallery_object['image'];
                }							
 
                $gallery_model->updateGallery(
                                                    $photo_id,
                                                    $newname,
													$alt,
													$sort_order
                                                );
												
				
				
                $this->_helper->redirector('photo', 'gallery', 'admin', array('gallery_id' => $gallery_id));
				} else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        } else { 
            $photo_id = $this->_getParam('photo_id', 0);
            if ($photo_id > 0)
            {
                $form->populate($gallery_model->getPhoto($photo_id));
            }
        }
    }
    
    public function deletephotoAction()
    {
		$gallery_model = new Default_Model_DbTable_Galleryphoto();
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $photo_id = $this->getRequest()->getPost('photo_id');
            
                $old_photo_file = $gallery_model->getGalleryImage($photo_id); //Zend_Debug::dump($old_photo_file);exit;
               
                $directory = "media/photos/gallery";
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
				{
					unlink("$directory/big_$old_photo_file");
					unlink("$directory/small_$old_photo_file");
				}  
            
                $gallery_model->deletePhoto(
													$photo_id
												);
            }       
            $this->_helper->redirector('index');
        } else {
            $photo_id = $this->_getParam('photo_id'); // Zend_Debug::dump($nashi_zeny_id);exit;
            
            $this->view->gallery_object = $gallery_model->getPhoto($photo_id);
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $gallery_model = new Default_Model_DbTable_Gallery();
        
        if ($this->getRequest()->isPost())
        {
            $gallery_id = $this->getRequest()->getParam('gallery_id');
            $status = '1';
            $gallery_model->updateGalleryStatus(
                                                    $gallery_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $gallery_model = new Default_Model_DbTable_Gallery();
        
        if ($this->getRequest()->isPost())
        {
            $gallery_id = $this->getRequest()->getParam('gallery_id');
            $status = '0';
            $gallery_model->updateGalleryStatus(
                                                    $gallery_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	public function photostatusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $galleryphoto_model = new Default_Model_DbTable_Galleryphoto();
        
        if ($this->getRequest()->isPost())
        {
            $photo_id = $this->getRequest()->getParam('photo_id');
            $status = '1';
            $galleryphoto_model->updatePhotoStatus(
                                                    $photo_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function photostatusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $galleryphoto_model = new Default_Model_DbTable_Galleryphoto();
        
        if ($this->getRequest()->isPost())
        {
            $photo_id = $this->getRequest()->getParam('photo_id');
            $status = '0';
            $galleryphoto_model->updatePhotoStatus(
                                                    $photo_id,  
                                                    $status
                                                );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $gallery_model = new Default_Model_DbTable_Galleryphoto();
        $photo_id = $this->getRequest()->getParam('photo_id');
        
        $old_photo_file = $gallery_model->getGalleryImage($photo_id);
        $directory = "media/photos/gallery";
        
        if ($this->getRequest()->isPost())
        {
            $photo_id = $this->getRequest()->getParam('photo_id');
            $image = '';
           
            $gallery_model->deleteGalleryImage(
                                                    $photo_id,  
                                                    $image
                                                );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
	
	function seoAction()
    {
        $galleryseo_model = new Default_Model_DbTable_Galleryseo();
        $galleryseo = $galleryseo_model->fetchAll();
		
		$this->view->galleryseo = $galleryseo;
    }
    
    public function seoeditAction()
    {
        $form = new Default_Form_Gallery_Seo();     
        $this->view->form = $form;
        $galleryseo_model = new Default_Model_DbTable_Galleryseo();
        
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {
                $id = (int)$form->getValue('id'); 
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
 
                $galleryseo_model->updateGallerySeo(
													$id,  
													$header_tags_title,
													$header_tags_description,
													$header_tags_keywords
												);
                
                $this->_helper->redirector('seo', 'gallery', 'admin');
            } else {
                $form->populate($formData);
            }
        } else {   
            $id = $this->_getParam('id', 0);
            if ($id > 0)
            {
                $form->populate($galleryseo_model->getGallerySeo($id));
            }
        }
    }
   
}