<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Slider_SliderThree extends Zend_Form
{
    public function init()
    {   
        $uploadId = new Zend_Form_Element_Hidden('uploadId');
        $uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
            ->setAttrib('id', 'uploadId')
            ->setAttrib('value', md5(uniqid(rand())));
        
        $slider_id = new Zend_Form_Element_Hidden('slider_id');
        $slider_id->addFilter('Int');
		
		$slider_title = new Zend_Form_Element_Text('slider_title');
        $slider_title->setLabel("alt:")
            ->setAttrib('class', 'span12')
			->setAttrib('id', 'name')
            ->setAttrib('onkeyup', 'translit()')
            ->setRequired(true)
			->addValidator('NotEmpty', true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
			
		$slider_url = new Zend_Form_Element_Text('slider_url');
        $slider_url->setLabel("url:")
            ->setAttrib('class', 'span12')
			->setAttrib('id', 'alias')
         //   ->setRequired(true)
		//	->addValidator('NotEmpty', true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
			
		$slider_title_extra = new Zend_Form_Element_Text('slider_title_extra');
        $slider_title_extra->setLabel("alt:")
            ->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
			
		$slider_title_01 = new Zend_Form_Element_Text('slider_title_01');
        $slider_title_01->setLabel("alt:")
            ->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
			
		$slider_text = new Zend_Form_Element_Textarea('slider_text');
        $slider_text->setLabel('')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_02')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534))
			->removeDecorator('label');

        $slider_worktime = new Zend_Form_Element_Text('slider_worktime');
        $slider_worktime->setLabel('')
            ->setAttrib('size', '120')
            ->setAttrib('class', 'span12')
            ->setAttrib('id', 'slider_worktime')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addFilter('StripTags')
            ->removeDecorator('label');

        $department = new Zend_Form_Element_Text('department');
        $department->setLabel("")
            ->setAttrib('size', '1')
            ->setAttrib('class', array('span1'))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->removeDecorator('label');
        
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'))
					->removeDecorator('label');
          
		$alt = new Zend_Form_Element_Text('alt');
        $alt->setLabel("alt:")
            ->setAttrib('class', 'span10')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->removeDecorator('label');
		   
        $image = new Zend_Form_Element_File('image');
        $image->setLabel("Выбрать фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif, jpeg")
            ->setAttrib('size', '50')
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif,jpeg')
            ->setDestination('media/photos/slider')
            ->setValueDisabled(true)
			->removeDecorator('label');
			
		$header_tags_title = new Zend_Form_Element_Text('header_tags_title');
        $header_tags_title->setLabel("title")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->setRequired(true)
			->addValidator('NotEmpty', true)
			->removeDecorator('label');
            
        $header_tags_description = new Zend_Form_Element_Text('header_tags_description');
        $header_tags_description->setLabel("description")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->setRequired(true)
			->addValidator('NotEmpty', true)
			->removeDecorator('label');

        $header_tags_keywords = new Zend_Form_Element_Text('header_tags_keywords');
        $header_tags_keywords->setLabel("keywords")
            ->setAttrib('size', '120')
			->setAttrib('class', 'span12')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
			->setRequired(true)
			->addValidator('NotEmpty', true)
			->removeDecorator('label');
        
        $clinic_number = new Zend_Form_Element_Text('clinic_number');
        $clinic_number->setLabel("")
            ->setAttrib('size', '1')
            ->setAttrib('class', array('span1'))
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->removeDecorator('label');
        

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $uploadId,
                                    $slider_id,
									$slider_title,
									$slider_url,
									$slider_title_extra,
									$slider_title_01,
									$slider_text,
                                    $slider_worktime,
                                    $department,
                                    $alt,
                                    $image,
									$sort_order,
									$header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords,
                                    $clinic_number,
                                    $submit
                                ));
       
    }
}