<?php

/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Stocksseo extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_stocks_seo';
	
	public function getSeo($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if(!$row) {
            throw new Exception("��� ������ � id - $id");
        }
        return $row->toArray();
    }
	
	public  function updateSeo(
                                        $id,
                                        $header_tags_title,
                                        $header_tags_description,
                                        $header_tags_keywords
                                    )
    {
        $data = array( 
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords			
        );
        
        $this->update($data, 'id = ' . (int)$id);
    }

}
