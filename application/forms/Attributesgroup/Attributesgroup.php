<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Attributesgroup_Attributesgroup extends Zend_Form
{
    public function init()
    {  
        $attribute_group_id = new Zend_Form_Element_Hidden('attribute_group_id');
        $attribute_group_id->addFilter('Int');
        
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'));
        
        
           
		$attribute_group_name = new Zend_Form_Element_Text('attribute_group_name');
        $attribute_group_name->setLabel("Название:")
            ->setAttrib('class', 'span8')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true
            );

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $attribute_group_id,
                                    $attribute_group_name,
                                    $sort_order,
                                    $submit
                                ));
       
    }
}