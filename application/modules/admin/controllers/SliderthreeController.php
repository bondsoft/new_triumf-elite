<?php

class Admin_SliderthreeController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    public function indexAction()
    {
        $slider_model = new Default_Model_DbTable_Sliderthree();
        $sliders = $slider_model->getAllSliderAdmin(); //Zend_Debug::dump($products);exit;
		
        $paginator = Zend_Paginator::factory($sliders);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);   
    
    }
    
    public function addAction()
    {
        include( 'SimpleImage.php' );
		
        $form = new Default_Form_Slider_SliderThree(); 
        $this->view->form = $form;
        $slider_model = new Default_Model_DbTable_Sliderthree();
        
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $sort_order = $_POST['sort_order'];
                $slider_title = $form->getValue('slider_title');
				$slider_url = $form->getValue('slider_url');
				$slider_title_extra = $form->getValue('slider_title_extra');
				$slider_title_01 = $form->getValue('slider_title_01');
				$slider_text = $form->getValue('slider_text');
                $slider_worktime = $form->getValue('slider_worktime');
                $department = $form->getValue('department');
				$alt = $form->getValue('alt');
				$header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                $clinic_number = $form->getValue('clinic_number');
                $image = $form->getValue('image');
				$image = $_POST['image'];
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/slider/big_".$newname;
                    $name_small = "media/photos/slider/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
				//	$image_big->cutFromCenter(371, 373);
				//	$image_big->maxareafill(371, 373, 255, 255, 255); 
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
					$image_small->maxareafill(240, 300, 104, 145, 194);
                //    $image_small->resizeToWidth(300);
                    $image_small->save($name_small);  
                }  
                
                $slider_model->addSlider(
											$slider_title,
											$slider_url,
											$slider_title_extra,
											$slider_title_01,
											$slider_text,
                                            $slider_worktime,
                                            $department,
                                            $alt,
                                            $sort_order,
                                            $newname,
											$header_tags_title,
											$header_tags_description,
											$header_tags_keywords,
                                            $clinic_number
                                        );
                                            
                unlink($_FILES['image']['tmp_name']);                            
                    
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editAction()
    {
        include( 'SimpleImage.php' );
				
        $form = new Default_Form_Slider_SliderThree(); 
        $this->view->form = $form;
        $slider_model = new Default_Model_DbTable_Sliderthree();
        
        $slider_id = $this->getRequest()->getParam('slider_id');
        $page = $this->getRequest()->getParam('page');
        $this->view->page = $page; 
        $slider_image = $slider_model->getSliderImage($slider_id);
        $this->view->slider_image = $slider_image; 
        $this->view->slider_id = $slider_id; 
        
        $slider = $slider_model->getSlider($slider_id); //Zend_Debug::dump($products_attributes);exit;
        $this->view->slider = $slider; //Zend_Debug::dump($product['manufacturer_id']);exit;
        
        $old_photo_file = $slider_model->getSliderImage($slider_id);
        $directory = "media/photos/slider";
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $sort_order = $_POST['sort_order'];
                $alt = $form->getValue('alt');
				$slider_title = $form->getValue('slider_title');
				$slider_url = $form->getValue('slider_url');
				$slider_title_extra = $form->getValue('slider_title_extra');
				$slider_title_01 = $form->getValue('slider_title_01');
				$slider_text = $form->getValue('slider_text');
                $slider_worktime = $form->getValue('slider_worktime');
                $department = $form->getValue('department');
				$header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                $clinic_number = $form->getValue('clinic_number');
                $image = $form->getValue('image'); //Zend_Debug::dump($image);exit;
			
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                        //  Zend_Debug::dump($uploadedfile);exit;
                if($image)
                {       
                    if(is_file("$directory/small_$old_photo_file"))
                    {
						unlink("$directory/big_$old_photo_file");
						unlink("$directory/small_$old_photo_file");
                    }
                }
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
					//Zend_Debug::dump($newname);exit;
                    $name_big = "media/photos/slider/big_".$newname;
                    $name_small = "media/photos/slider/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
				//	$image_big->cutFromCenter(371, 373);
				//	$image_big->maxareafill(371, 373, 255, 255, 255); 
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
					$image_small->maxareafill(240, 300, 104, 145, 194);
                //    $image_small->resizeToWidth(300);
                 //   $image_small->resizeToHeight(171);
				//	$image_small->cutFromCenter(171, 161);
                    $image_small->save($name_small);  
                } 
                
                if(!$image)
                {      
                    $slider_object = $slider_model->getSlider($slider_id);
                    $newname = $slider_object['image'];
                }							
 
                $slider_model->updateSlider(
												$slider_id,
												$slider_title,
												$slider_url,
												$slider_title_extra,
												$slider_title_01,
												$slider_text,
                                                $slider_worktime,
                                                $department,
												$alt,
												$sort_order,
												$newname,
												$header_tags_title,
												$header_tags_description,
												$header_tags_keywords,
                                                $clinic_number
											);
												
                
                $this->_helper->redirector('index', 'sliderthree', 'admin', array('page' => $page));
				} else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        } else { 
            $slider_id = $this->_getParam('slider_id', 0);
            if ($slider_id > 0)
            {
                $form->populate($slider_model->getSlider($slider_id));
            }
        }
    }
    
    public function deleteAction()
    {
		$slider_model = new Default_Model_DbTable_Sliderthree();
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $slider_id = $this->getRequest()->getPost('slider_id');
            
                
                $page_slider = $slider_model->getSlider($slider_id);
                
                $old_photo_file = $slider_model->getSliderImage($slider_id); //Zend_Debug::dump($old_photo_file);exit;
               
                $directory = "media/photos/slider";
                
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
				{
					unlink("$directory/big_$old_photo_file");
					unlink("$directory/small_$old_photo_file");
				}  												
                
                $slider_model->deleteSlider(
												$slider_id
											);
            }       
            $this->_helper->redirector('index');
        } else {
            $slider_id = $this->_getParam('slider_id'); // Zend_Debug::dump($nashi_zeny_id);exit;
            
            $this->view->slider_object = $slider_model->getSlider($slider_id);
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $slider_model = new Default_Model_DbTable_Sliderthree();
        
        if ($this->getRequest()->isPost())
        {
            $slider_id = $this->getRequest()->getParam('slider_id');
            $status = '1';
            $slider_model->updateSliderStatus(
                                                $slider_id,  
                                                $status
                                             );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $slider_model = new Default_Model_DbTable_Sliderthree();
        
        if ($this->getRequest()->isPost())
        {
            $slider_id = $this->getRequest()->getParam('slider_id');
            $status = '0';
            $slider_model->updateSliderStatus(
                                                $slider_id,  
                                                $status
                                             );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $slider_model = new Default_Model_DbTable_Sliderthree();
        $slider_id = $this->getRequest()->getParam('slider_id');
        
        $old_photo_file = $slider_model->getSliderImage($slider_id);
        $directory = "media/photos/slider";
        
        if ($this->getRequest()->isPost())
        {
            $slider_id = $this->getRequest()->getParam('slider_id');
            $image = '';
           
            $slider_model->deleteSliderImage(
                                                $slider_id,  
                                                $image
                                            );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }

}

