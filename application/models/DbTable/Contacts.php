<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Contacts extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_contacts';
    
    public function getContacts($contacts_id)
    {
        $contacts_id = (int)$contacts_id;
        $row = $this->fetchRow('contacts_id = ' . $contacts_id);
        if(!$row) {
            throw new Exception("Нет записи с contacts_id - $contacts_id");
        }
        return $row->toArray();
    }
	
    public  function updateContacts(
										$contacts_id,
										$text,
										$header_tags_title,
										$header_tags_description,
										$header_tags_keywords
									)
    {
        $data = array(
            'text' => $text,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'contacts_id = ' . (int)$contacts_id);
    }
    
    
	public function getContactsText()
	{
        $_name = 'zend_contacts';
        $select = $this->select()
                    ->from($_name);
     
		$text = $this->fetchAll($select);
        return $text;
    }
   
}
