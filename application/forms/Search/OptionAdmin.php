<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Search_OptionAdmin extends Zend_Form
{
    public function init()
    {	
		$options_model = new Default_Model_DbTable_Option();
		$options = $options_model->getAllOptionsAdmin();
	//	Zend_Debug::dump($products);exit;
		$obj = array();
		foreach ($options as $option) {
				$obj[] = $option['name'];
				 
		}
		
	//	Zend_Debug::dump($asd);exit;
		
		$query_options = new ZendX_JQuery_Form_Element_AutoComplete('query_options');
        $query_options->setLabel("search_text")
		  //  ->setAttrib('size', '120')
			->setAttrib('class', array('search_option_admin', 'span3'))
			->setAttrib('id', 'query-admin-options')
			->setAttrib('placeholder', 'опции')
		//	->setAttrib('onClick', 'search();')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->removeDecorator('label')
			->addDecorator('HtmlTag',
                                   
                                    array('tag' => 'td', 'class'  => 'element'))
			
			->setJQueryParam(
                'source', $obj);
			
			
       
        $this->addElements(array(
                                    $query_options
                                ));
			 
    }
}

