<?php

/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_Lang_EditLang extends Zend_Form
{
    public function init()
    {   
    
        $this
          ->setDescription('Choose photos on your computer to add to this album.')
          ->setAttrib('id', 'lang')
          ->setAttrib('class', 'lang')
          ->setAttrib('name', 'lang')
          ->setAttrib('enctype','multipart/form-data')
         ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));
       
            
        // Set the method for the display form to POST
        $this->setMethod('post');
        
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang'); //Zend_Debug::dump($lang); exit;
              
        $translations_value = new Zend_Form_Element_Textarea('translations_value');
        $translations_value->setLabel("Текст")
            ->setRequired(false)
          //  ->setValue($lines)
            ->setAttrib('class', 'span10')
            ->setAttrib('cols', 100)
            ->setAttrib('rows', 10)
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534));
            
            
        
        
        $add = new Zend_Form_Element_Submit('add');
        $add   ->setLabel('Сохранить')
                    //->setValue($translate->translate('Search'))
                    ->setAttrib('class', 'btn btn-primary pull-left')
                    ->removeDecorator('label')
                    ->removeDecorator('DtDdWrapper');
        
                        
        $this->addElements(array(   $translations_value,
                                    $add
                                )
                            );
        
    }
}

