<?php 

/**
 * Файл с переводами на русский 
 * 
 * 
 */

return $errors = array (

    Zend_Validate_Alnum::NOT_ALNUM     => "Значение содержит запрещенные символы. Разрешены символы латиници, кирилици, цифры и пробел",
    Zend_Validate_Alnum::STRING_EMPTY  => "Поле не может быть не заполненным",
   
    Zend_Validate_Date::INVALID        => 'Неверная дата',   
    Zend_Validate_Date::FALSEFORMAT    => 'Значение не соответствует указанному формату',   
     
    Zend_Validate_EmailAddress::INVALID_FORMAT     => "'%value%' не верный формат адреса электронной почти. Введите почту в формате local-part@hostname",
    Zend_Validate_EmailAddress::INVALID_HOSTNAME   => "'%hostname%' не верный домен для адреса электронной почты '%value%'",
    Zend_Validate_EmailAddress::INVALID_MX_RECORD  => "'%hostname%' не имеет MX-записи об адресе электронной почты '%value%'",
    Zend_Validate_EmailAddress::DOT_ATOM           => "'%localPart%' не соответствует формату dot-atom",
    Zend_Validate_EmailAddress::QUOTED_STRING      => "'%localPart%' не соответствует формату quoted-string",
    Zend_Validate_EmailAddress::INVALID_LOCAL_PART => "'%localPart%' не верное имя для адреса электронной почты '%value%'",
	
	Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => "'%value%' кажется, IP-адрес, но IP-адресов не допускается",
	Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => "'%value%' оказывается местное название сети, но местные сетевые имена не допускаются",
	Zend_Validate_Hostname::UNKNOWN_TLD             => "'%value%' кажется, DNS имя хоста, но не могут сравниться TLD от известных список",

    Zend_Validate_Int::NOT_INT => 'Значение не является целочисленным значением',   
     
    Zend_Validate_NotEmpty::IS_EMPTY => 'Поле не может быть не заполненным',
     
    Zend_Validate_StringLength::TOO_SHORT => 'Длина введенного значения меньше чем %min% символов',   
    Zend_Validate_StringLength::TOO_LONG  => 'Длина введенного значения больше чем %max% символов',   

  //  App_Validate_EqualInputs::NOT_EQUAL => 'Пароли не совпадают',
    
  //  App_Validate_Password::INVALID => 'Не верный формат пароля',
  //  App_Validate_Password::INVALID_LENGTH => 'Длина пароля должна быть от 7 до 30ти символов',
    
    Zend_Validate_Db_RecordExists::ERROR_RECORD_FOUND => 'Такое значение уже присутствует в базе данных',
    
    Zend_Captcha_Word::BAD_CAPTCHA   => 'Вы ввели не верные символы',
    Zend_Captcha_Word::MISSING_VALUE => 'Поле не может быть пустым',
	
	Zend_Validate_Identical::NOT_SAME => 'Пароли не совпадают',
	
    'home' => 'Заглавная',
    'real estate' => 'Недвижимость',
    'real estate services' => 'Услуги по недвижимости',
	'services for business' => 'Услуги для бизнеса',
	'helpful information' => 'Полезная информация',
	'about' => 'О компании',
	'contact' => 'Контакты',
	'video' => 'Видео',
	
	'intro-title' => 'RU RU Lorem ipsum dolor sit amet, consectetuer adipiscing elit.',
	'company name lable' => 'Название компании:',
	'company name text' => 'RIVER, SIA',
	'reg number lable' => 'Регистрационный номер:',
	'reg number text' => '1000123456789',
	'ure adress lable' => 'Юридический адрес:',
	'ure adress text' => 'Brīvības iela 111-11/1, Rīga, LV-1011, Latvija',
	'telefon_01 lable' => 'Телефон:',
	'telefon_01 text' => '(+371) 20000000',
	'telefon_02 lable' => 'Телефон:',
	'telefon_02 text' => '(+371) 20000000',
	'email lable' => 'Е-почта:',
	'email text' => 'info@domain.lv',
	'contacts form lable' => 'Lorem ipsum',
	
// страница О компании
	'content-h1-table-full_text' => 'Регистрация компаний в Латвии',
	'content article intro' => 'Компания, зарегистрированная в Латвии, отлично выполняет функцию европейского посредника в торговых отношениях между Западом и русскоговорящими странами. 
Законодательство Латвийской республики позволяет при открытии компании нерезидентам получать все права и преимущества юридического лица зарегистрированного в Евросоюзе. Вести операции с недвижимостью, 
экспорт/импорт товаров и услуг из/в ЕС и т.д.',

// -----------------главная
    'Hot deals' => 'Горячие предложения',
	'Commercial properties' => 'Коммерческие объекты',
    'Forward' => 'Дальше',
	'all projects' => 'все проекты',
	'Show all projects' => 'Показать все проекты',
    'content-chapter-intro' => 'Этот текст редактируется в файле ru.php каталог application/languages/ru',
    'content-chapter-text' => 'Этот текст редактируется в файле ru.php каталог application/languages/ru',
    'Real Estate' => 'Недвижимость',
    'content-chapter-text Real Estate' => 'Этот текст редактируется в файле ru.php каталог application/languages/ru',
    'Investment projects' => 'Investment projects',
    'content-chapter-intro Investment projects' => 'content-chapter-text Investment prrurururururu',
    'content-chapter-text Investment projects' => 'content-chapter-text Investment prrurururururu',
//------------------------    
    
// --------------полезная информация
    'helpful information' => 'Полезная информация',
	'Read more' => 'Подробнее',
    'content information intro' => 'Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis 
lacus. Morbi magna magna, tincidunt 
a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.',
	'content information text' => 'gjkhkjhkjjdhkjashdkjashdkjahskdjahskjdhaks',
//--------------------------
// ------------------контакты	
	'content contacts intro' => 'Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.',
	
	'content contacts text' => 'Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.',
//------------------------
//-------------- футер	
	'footer about' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, biben dum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Mae cenas placerat, nisl at consequat rhoncus.',
	'footer copyright' => '© 2014. River.lv. All rights reserved. Ph.: (+371)20000000, info@river.lv',
	'facebook' => 'http://www.facebook.com/pages/Global-Finance/155098751230367',
	'twitter' => 'http://twitter.com/GlobalFinanceLV',
    'skype' => 'callto://real.lv/',
//-------------------------	
//----------- услуги по недвижимости
	'real estate services content list intro' => 'цццццццццццццццццццц',
	'real estate services content list text' => 'вввввввввввввввввввввввввв',
//-------------------		
//----------- услуги для бизнеса
	'services for business intro' => 'qqqqqqqqqqqqqqqqqqqq',
	'real estate services intro' => 'uuuuuuuuuuuuuuuuuuuu',
	'services for business content list intro' => 'aaaaaaaaaaaaqqqqqqqqqqqqqqqqqqqq',
	'services for business content list text' => 'aaaaaaaaaaaaaaaaauuuuuuuuuuuuuuuuuuuu',
//-------------------	
//----------------------страница объекта
	'Similar offers' => 'Похожие предложения',
	'lot_content_chapter_intro' => 'dasdasdasdasdasd',
	'lot_content_chapter_text' => 'qqdfssdfsdfsfrererewrewrwe',
	'to cancel' => 'Отменить',
	'View location on map' => 'Посмотреть расположение на карте',
	'Town, region' => 'Город, район',
	'City / town' => 'Город/волость',
	'Settlement' => 'Посёлок',
	'Street' => 'Улица',
	'Area, m2' => 'Площадь, м2',
	'Number of floors' => 'Количество этажей',
	'Rooms' => 'Комнат',
	'Land area, m2' => 'Площадь земли, м2',
	'Conveniences' => 'Удобства',
	'Price, €' => 'Цена, €',
	'Save a description as pdf-file' => 'Сохранить описание как pdf-файл',
	'More information' => 'Более подробная информация',
	'Contact person' => 'Контактное лицо',
	'Video' => 'Видео',
	'Photos' => 'Фотографии',
//------------------------		
// ----------форма поиска     
    'Type of deal' => 'Вид сделки',
    'Real Estate' => 'Недвижимость',
    'Region' => 'Регион',
    'Rooms' => 'Комнаты',
	'from' => 'от',
    'to' => 'до',
    'Area, m' => 'Площадь, м',
    'Price' => 'Цена',
    'Search' => 'Искать',
//----------------  	
	
	'Username:' => 'Логин:',
	'Password:' => 'Пароль:',
	'Login' => 'Войти',
	'authorization' => 'авторизация',
	'Create Account' => 'Регистрация',
	'Lost Password?' => 'Забыли пароль?',
	
	'Email Address:' => 'Електроння почта:',
	'Passwords must be at least 6 characters in length.' => 'Пароль должен быть не короче 6 символов и состоять тольки из латинських букв и цифр.',
	'Password Again:' => 'Подтверждение пароля:',
	'Enter your password again for confirmation.' => 'Введите пароль ещё раз.',
	'Enter the simbols:' => 'Введите символы:',
	'Signup' => 'Зарегистрироваться',
	
	'Choose avatar:' => 'Выбрать аватар:',
	'Valid formats: jpg, png, gif' => 'Допустимые форматы: jpg, png, gif',
	'Save' => 'Сохранить',
	'Cancel' => 'Отменить',
	
	'Subgect:' => 'Тема вопроса',
	'Email:' => 'Адрес э-почты',
	'You have been sent an email with new password.' => 'На вашу почту будет выслан новый пароль.',
	'Send Email' => 'Отправить',
	
	'Enter New Password' => 'Введите новый пароль.',
	'Confirm New Password:' => 'Подтверждение пароля:',
	'Name:' => 'Имя:',
	'Message:' => 'Сообщение:',
	'Send Message' => 'Отправить',
	
	
	
	
	'Create Account' => 'регистрация',
	'password recovery' => 'восстановление пароля',
	'Edit My Profile' => 'Редактировать профиль',
	'Exit' => 'Выход',
	
	'Edit avatar' => 'Редактировать аватар',
	'Delete avatar' => 'Удалить аватар',
	'Change your password' => 'Изменить пароль',
	'Delete Account' => 'Удалить аккаунт',
	'Yes' => 'Да',
	'No' => 'Нет',
	
	'Are you sure you want to delete your account? Any content you\'ve uploaded in the past will be permanently deleted.' => 'Вы действительно хотите удалить аккаунт ?',
	
	'Are you sure you want to delete your avatar?' => 'Вы действительно хотите удалить аватар?',
	'Enter the Password' => 'Введите пароль!',
	'Passwords did not match' => 'Пароли не совпадают!',
	'You have not selected a file!' => 'Вы не выбрали файл!',
	'You have entered an incorrect username or password.' => 'Вы ввели неверное имя пользователя или пароль',
	
	'There was an error sending the message. Please try again later.' => 'Вы ввели неверное имя пользователя или пароль',
	'Your message has been sent successfully.' => 'Ваше сообщение отправлено.',
	
	
	
 );
