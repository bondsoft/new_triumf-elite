<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Gallery extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_gallery';
	
	public function getGallery($gallery_id)
    {
        $gallery_id = (int)$gallery_id;
        $row = $this->fetchRow('gallery_id = ' . $gallery_id);
        if(!$row) {
            throw new Exception("no record gallery_id - $gallery_id");
        }
        return $row->toArray();
    }
	
	public function getGalleryAdmin()
    {
        $_name = 'zend_gallery';
        $select = $this->select()
                    ->from($_name);
     
        $photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function getGalleryFront()
    {
        $_name = 'zend_gallery';
        $select = $this->select()
                    ->from($_name)
					->where('status = ?', '1')
					->order('sort_order');
     
        $photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function getPhotoGallery( $gallery_id )
    {
		$_name = 'zend_gallery_photo';
        $select = $this->select()
					->setIntegrityCheck(false)
                    ->from($_name)
					->where('status = ?', '1')
					->where('gallery_id = ?', $gallery_id)
					->order('sort_order');
     
        $photos = $this->fetchAll($select);
        return $photos;
    }
	
	public function addGallery(
									$title,
									$text,
									$sort_order                                               
                            )
    {
        $data = array(
			'title' => $title,
			'text' => $text,
			'sort_order' => $sort_order	
        );
        $this->insert($data);
    }
	
	public  function updateGallery( 
										$gallery_id,
                                        $title,
										$text,
										$sort_order
								 )
    {
        $data = array(
            'title' => $title,
			'text' => $text,
			'sort_order' => $sort_order	
        );
        
        $this->update($data, 'gallery_id = ' . (int)$gallery_id);
    }
	
	public  function updateGalleryStatus(
                                            $gallery_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'gallery_id = ' . (int)$gallery_id);
    }
	
	public function deleteGallery($gallery_id)
    {
        $this->delete('gallery_id = ' . (int)$gallery_id);
    }
	
}
