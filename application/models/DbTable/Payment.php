<?php
/**
 * 
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    
 * @subpackage Model
 */
class Default_Model_DbTable_Payment extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_payment';
    
    public function getPayment($payment_id)
    {
        $payment_id = (int)$payment_id;
        $row = $this->fetchRow('payment_id = ' . $payment_id);
        if(!$row) {
            throw new Exception("Нет записи с payment_id - $payment_id");
        }
        return $row->toArray();
    }
	
    public  function updatePayment(
                                    $payment_id,
                                    $text,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords
                                )
    {
        $data = array(
            'text' => $text,
            'header_tags_title' => $header_tags_title,
            'header_tags_description' => $header_tags_description,
            'header_tags_keywords' => $header_tags_keywords
        );
        
        $this->update($data, 'payment_id = ' . (int)$payment_id);
    }
    
    
	public function getPaymentText()
	{
        $_name = 'zend_payment';
        $select = $this->select()
                    ->from($_name);
     
		$text = $this->fetchAll($select);
        return $text;
    }
   
}
