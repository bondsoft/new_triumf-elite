<?php

class Search_IndexController extends Zend_Controller_Action 
{
	public function init()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('category_list');
		
		$this->view->addHelperPath(
                'ZendX/JQuery/View/Helper'
                ,'ZendX_JQuery_View_Helper');
    }
	
	public function indexAction()
    {
		$sessionNamespace = new Zend_Session_Namespace('Foo');
		$priceCart = $sessionNamespace->price_cart;   //Zend_Debug::dump($priceCart);exit;
		$quantityCart = $sessionNamespace->quantity_cart;
		$this->view->priceCart = $priceCart;
		$this->view->quantityCart = $quantityCart;
		
		$sum_cart = $sessionNamespace->sum_cart;// Zend_Debug::dump($sum_cart);exit;
		$this->view->sum_cart = $sum_cart;
			
		$products_model = new Default_Model_DbTable_Products();
		$categories_model = new Default_Model_DbTable_Categories();
		$query = $this->getRequest()->getParam('query');
	//	Zend_Debug::dump($query);exit;
		$this->view->query = $query;
	
		$pieces = explode(" ", $query);
		@$query_01 = $pieces[0];
		@$query_02 = $pieces[1];
		@$query_03 = $pieces[2];
		@$query_04 = $pieces[3];
		@$query_05 = $pieces[4];
		
		$products = $products_model->getAllObjectsSearch(
															$query_01,
															$query_02,
															$query_03,
															$query_04,
															$query_05
														);
	//	Zend_Debug::dump($implode);exit;
		//}
		$product_attributes = array();
	foreach ($products as $key => $value) { 
		$product_attributes[$value->product_id] = $products_model->getProductsAttributesForFront($value->product_id);
		 
	}
	$this->view->product_attributes = $product_attributes;
	
	$product_option = array();
	foreach ($products as $key => $value) {
		$product_option[$value->product_id] = $products_model->getProductsOptionFront($value->product_id);
		 
	}
	$this->view->product_option = $product_option;
		$paginator = Zend_Paginator::factory($products);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(15);
		
		$homepage_model = new Default_Model_DbTable_Homepage();
		$page_text = $homepage_model->getPageText();
	    $this->view->page = $page_text;
    
    }
	
	
	public function updateIndexAction() {
		$this->_helper->layout()->disableLayout();
	//	$request = $this->getRequest();
    //   $helper = $this->_helper->getHelper('Layout')->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
			
		$model = new Search_Model_Search();
		$model->updateIndex();	
	}
	
	public function searchformAction()
    {
        $form = new Default_Form_Search_Search();
		$this->view->form = $form;
    } 
	
	public function searchAction() 
	{
		$model = new Search_Model_Search();
	//	$this->view->query = $this->_getParam('query');
		if ($this->getRequest()->isPost())
        { 
			$search_text = $_POST['search_text'];
			$this->view->hits = $model->search($search_text);
		}
	}
}
