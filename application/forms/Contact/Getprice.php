<?php

/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_Contact_Getprice extends Zend_Form
{
    public function init()
    {	
	    
        // Element: username
        $this->addElement('Text', 'name', array(
            'label' => '',
            'value' => '',
         //   'size'  => '30', 
         //   'maxLength' => '15',
            'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('NotEmpty', true),
            ),
         //   'attribs' => array('class' => 'form-control form-textfields', 'placeholder' => 'Представтесь...', 'required id' => 'callbackName', 'tabindex' => '1'),
            'attribs' => array('class' => '', 'id' => 'name', 'placeholder' => 'Ваше имя'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        )); 
		
		// Element: email
        $this->addElement('Text', 'email', array(
            'label' => '',
            'value' => '', 
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array('NotEmpty', true),
                array('EmailAddress', true)
            ),
        //    'attribs' => array('class' => 'form-control form-textfields form-valid', 'placeholder' => 'Е-мейл для ответа...', 'id' => 'callbackEmail', 'tabindex' => '2'),
            'attribs' => array('class' => '', 'id' => 'email', 'placeholder' => 'E-mail'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        ));
       
		// Element:    , 'onFocus' => 'callbackformContactPhone();'
        $this->addElement('Text', 'phone', array(
            'label' => '',
            'value' => '', 
         //   'size'  => '30',
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array('notEmpty', true, array(
                    'messages' => array(
                        'isEmpty'   =>  'Phone can\'t be empty'
                    )
                )),
                array('StringLength', false, array(18, 18, 'messages' => array(
                            'stringLengthInvalid'           => "Phone Length Invalid entry",
                            'stringLengthTooShort'          => "Phone Invalid Length , ex. 1234567890"
                    ))),
            ),
        //    'attribs' => array('class' => 'form-control form-textfields form-valid', 'placeholder' => 'Ваш телефон...', 'id' => 'callbackEmail', 'tabindex' => '2'),
            'attribs' => array('class' => 'phone', 'id' => 'phone', 'placeholder' => '+7 (___) ___ __ __'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        ));
       
		// Init submit
        $this->addElement('Submit', 'submit', array(
            'label' => 'ОТПРАВИТЬ',
            'type' => 'submit',
            'ignore' => true,
            'attribs' => array('class' => 'submit submit_callback_formdata'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
                            
           // 'class'  => 'ajax-validate'
        ));
		$element = $this->getElement('submit');
        $element->removeDecorator('Label');		
        
    }
}

