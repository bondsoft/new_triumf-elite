<?php

class Admin_Adminpage05Controller extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
	
	public function indexAction()
    {
        $page_model = new Default_Model_DbTable_Page05();
		$page_text = $page_model ->getPageText();
		$this->view->page_text = $page_text;
    }
	
    public function editAction()
    {
        $form = new Default_Form_Page_Page();     
        $this->view->form = $form;
        $page_model = new Default_Model_DbTable_Page05();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();
           //  Zend_Debug::dump($formData);exit;
   
            if ($form->isValid($formData)) {
         
                $page_id = (int)$form->getValue('page_id');
                $text = $form->getValue('text');
                $header_tags_title = $form->getValue('header_tags_title');
                $header_tags_description = $form->getValue('header_tags_description');
                $header_tags_keywords = $form->getValue('header_tags_keywords');
                
                $page_model->updatePage(
											$page_id,
											$text,
											$header_tags_title,
											$header_tags_description,
											$header_tags_keywords
										);

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $page_id = $this->_getParam('page_id', 0);
            if ($page_id > 0) {
                $form->populate($page_model->getPage($page_id));
            }
        }
    }
}

