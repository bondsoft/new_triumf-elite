<?php
/**
 * 
 *
 * User_Account controller.
 * 
 * 
 * 
 * 
 */
class Members_PersonalinformationController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('members');
    }
	
	public function indexAction()
    {
       // nothing to do here, index.phtml will be displayed
	   
	    $auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = @$userdata->user_id;
	   
        $users_object = new Default_Model_DbTable_Users();
		if ( $user_id != null )
		{
		    $user = $users_object->getUser($user_id);  
		} else { $this->_helper->redirector('/'); }
		//   Zend_Debug::dump($user_id);exit; 
		$this->view->user = $user;    
	
    }
    
    public function addavatarAction()
    {
        include( 'SimpleImage.php' );
	    $translate = Zend_Registry::get('Zend_Translate');
	    $lang = Zend_Registry::get('Zend_Lang');
		
        $form = new Default_Form_Members_Avatar();     
        $this->view->form = $form;
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id; // Zend_Debug::dump($user_id);exit;
	   
        $users_object = new Default_Model_DbTable_Users();
		$user = $users_object->getUser($user_id);                 // Zend_Debug::dump($user_id);exit;
		$this->view->user = $user;
		$old_avatar_file = $users_object->getAvatar_file($user_id);   // Zend_Debug::dump($old_photo_file);exit;
        
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost(); //Zend_Debug::dump($formData);exit;
		   
		   	if ($form->isValid($formData) && $form->submit->isChecked())
			{
			    $avatar_file = $form->getValue('avatar_file'); //Zend_Debug::dump($avatar_file);exit;
				
			    $directory = "media/photos/user_avatar";
		    	// 
			    if(is_file("$directory/$old_avatar_file"))
     		    {
     		        unlink("$directory/$old_avatar_file");
			    }
            
				if( empty($avatar_file) )
                {
					$this->view->errMessage = $translate->translate('You have not selected a file!');
                    return;
                }
				$users_object = new Default_Model_DbTable_Users();
				$data = $_FILES['avatar_file'];      
				$uploadedfile = $data['tmp_name'];
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = substr($data["name"], -4); 
				    $newname = date("Ymd")."_".rand(1000,9999).$ext;  
				    $name = "media/photos/user_avatar/".$newname;
					
				   /* ********************************************** */
					
				    $image = new SimpleImage();
				    $image->load($uploadedfile);
					$image->resizeToHeight(90);
				    $image->save($name);   
				} 
				
                $users_object->updateAvatar($user_id, $newname);
				
				// 
                 unlink($_FILES['photo_file']['tmp_name']);
                
				$this->_helper->redirector('index', 'personalinformation', 'members');
            }  else {
                $this->_helper->redirector('index', 'personalinformation', 'members');
            }
        }
    }
	
	public function deleteavatarAction()
    {
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id;
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'yes') {
                
                $users_object = new Default_Model_DbTable_Users();
				$avatar_file = $users_object->getAvatar_file($user_id);
				
			//	Zend_Debug::dump($avatar_file);exit;
			
				$directory = "media/photos/user_avatar";
				// 
				if(is_file("$directory/$avatar_file"))
				{
     		        unlink("$directory/$avatar_file");
				}	
             
                $users_object->updateAvatar($user_id, '');
            }
			$this->_helper->redirector('index', 'personalinformation', 'members');
         //   header('Location: http://xn--j1aelbjg7c.xn--p1ai/account');
        } else {
           
            $users_object = new Default_Model_DbTable_Users();
            
            $this->view->avatar = $users_object->getUser($user_id);
        }
    }
	
	public function updatepassAction()
    {
	    $form_passwup = new Default_Form_Auth_Passwup();     
        $this->view->form_passwup = $form_passwup;
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id; //Zend_Debug::dump($user_id);exit;
		
	    // ≈сли к нам идЄт Post запрос
        if ($this->getRequest()->isPost())
		{
		    $formData = $this->getRequest()->getPost();
			
			//$user_id = $this->getRequest()->getParam('user_id');
			$password = $this->getRequest()->getPost('password');
			$passconf = $this->getRequest()->getPost('passconf');  //  Zend_Debug::dump($password);exit;
          
		 /*   if(!$password)
			{
				$this->view->errMessage = "ѕароли не совпадают!" ;
                return;
            } */
			
			if($password !== $passconf)
			{
				$this->view->errMessage = "Пароли не совпадают!" ;
                return;
            }
			
		    $valid = new Zend_Validate_NotEmpty();
            $value  = $password;
            $result = $valid->isValid($value);
			
			if (!$result)
			{
			    $this->view->errMessage = "Введите пароль!";
                return;   
			}
             
			
			$validator = new Zend_Validate_StringLength(array('min' => 6, 'max' => 32));
            $res = $validator->isValid($password);
            if(!$res)
			{
			    $this->view->errMessage = "ƒлина парол¤ должна быть от 6 до 32 символов" ;
                return; 
			}
			
		   	if ($form_passwup->isValid($formData))
			{
                $users_object = new Default_Model_DbTable_Users();
				
                $users_object->update_password($user_id, md5($password));
            }
            $this->_helper->redirector('index', 'personalinformation', 'members');
        }
    }
	
	public function editaccountAction()
    {
	    $form = new Default_Form_Members_EditAccount();     
        $this->view->form = $form;
		$users_model = new Default_Model_DbTable_Users();
		
		$auth = Zend_Auth::getInstance();
	    $userdata = $auth->getIdentity();            
	    $user_id = $userdata->user_id; 
		
        if ($this->getRequest()->isPost())
		{
		    $formData = $this->getRequest()->getPost();
			
		   	if ($form->isValid($formData))
			{
				$realname = $form->getValue('realname');
				$surname = $form->getValue('surname');
				$town = $form->getValue('town');
			//	$email = $form->getValue('email');
				
				$users_model->updateAccount(
												$user_id,
												$realname,
												$surname,
												$town  
											);
				
				
               
            }
            $this->_helper->redirector('index', 'personalinformation', 'members');
        } else { 
          //  $token = $this->_getParam('token', 0);
            if ($user_id > 0)
            {
                $form->populate($users_model->getUser($user_id));
            }
        }
    }
	
	public function deleteAction()
    {
	    $lang = Zend_Registry::get('Zend_Lang'); 
        
        if ($this->getRequest()->isPost()) {
		
            $del = $this->getRequest()->getPost('del');
			
            // 
            if ($del == 'yes') {
                // 
                $user_id = $this->getRequest()->getParam('user_id');
		
                $users_object = new Default_Model_DbTable_Users();
				// 
				$avatar_file = $users_object->getAvatar_file($user_id);
				
			//	Zend_Debug::dump($avatar_file);exit;
			
				$directory = "images/user_avatar";
				// 
				if(is_file("$directory/$avatar_file"))
				{
     		        unlink("$directory/$avatar_file");
				}	
             
                $users_object->deleteUser($user_id);
            }
            $this->_helper->redirector('index', 'account', 'members', array('user_id' => $user_id, 'lang' => $lang));
        } else {
           
            $user_id = $this->_getParam('user_id');
            
            $users_object = new Default_Model_DbTable_Users();
            
            $this->view->avatar = $users_object->getUser($user_id);
        }
    }
    
    public function subscriptionAction()
    {
        $users_object = new Default_Model_DbTable_Users();
       
        $user_id = $this->getRequest()->getParam('user_id'); //Zend_Debug::dump($kiosk_seo);exit;
       
        $user = $users_object->getUser($user_id); //Zend_Debug::dump($page);exit;
        $this->view->user = $user;
    }
	
	public function updatesubscriptionAction()
    {
        $translate = Zend_Registry::get('Zend_Translate');
        
        $lang = Zend_Registry::get('Zend_Lang');
        $translate = Zend_Registry::get('Zend_Translate');
        
        $users_object = new Default_Model_DbTable_Users();
        $form = new Default_Form_Auth_Subscription();     
        $this->view->form = $form;
        
        $auth = Zend_Auth::getInstance();
        $userdata = $auth->getIdentity();            
        $user_id = $userdata->user_id;
        
        // 
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();
            
            $user_id = $this->getRequest()->getParam('user_id');
            $subscription = $this->getRequest()->getPost('subscription');
         
            if ($form->isValid($formData))
            {
                $users_object->update_subscription(
                                                    $user_id,
                                                    $subscription
                                                   );
            }
            $this->_helper->redirector('subscription', 'account', 'members', array('user_id' => $user_id));
        } else {       
            $user_id = $this->_getParam('user_id', 0);
            if ($user_id > 0) {  
                $form->populate($users_object->getUser($user_id));
            }
        }
    }
    
    

}


