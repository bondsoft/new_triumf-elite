<?php

/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_Contact_Contact extends Zend_Form
{
    public function init()
    {	
	    $translate = new Zend_Translate(
           array(
        'adapter' => 'array',
        'content' => '../application/languages/ua/errors.php',
        'locale'  => 'ua'
         )
        );
	    // ������ ������ ����������� ��� �����
        $this->setTranslator($translate);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		
		// Element: username
        $this->addElement('Text', 'username', array(
          'label' => iconv("windows-1251", "UTF-8", "���:"),
		  'size'  => '30', 
          'required' => true,
          'filters'    => array('StringTrim'),
		  'validators' => array(
            array('NotEmpty', true),
          ),
        ));        
    	$this->username->getValidator('NotEmpty')->setMessage(iconv("windows-1251", "UTF-8", "���� �����, ������ ��'�."), 'isEmpty');
	
		// Element: email
        $this->addElement('Text', 'email', array(
          'label' => 'Email:',
		  'size'  => '30', 
          'required' => true,
          'allowEmpty' => false,
          'validators' => array(
            array('NotEmpty', true),
            array('EmailAddress', true)
          ),
        ));
		$this->email->getDecorator('Description')->setOptions(array('placement' => 'APPEND'));
		
        // Element: textarea
        $this->addElement('textarea', 'comment', array(
            'label' => iconv("windows-1251", "UTF-8", "���������:"),
            'required' => true,
		    'cols' => '48',
		    'rows' => '10',
		    'validators' => array(
		        array('NotEmpty', true),
                array('validator' => 'StringLength', 'options' => array(0, 5000)),	
                ),
		    'filters'     => array('StringTrim'),	
        ));
				
		// Add a captcha
        $this->addElement('captcha', 'captcha', array(
          'label'      => iconv("windows-1251", "UTF-8", "������� �������:"),
		  'size'  => '30', 
		  'description' => '',
	      'captcha' => 'image',
          'required' => true,
          'tabindex' => 3,
          'captchaOptions' => array(
            'wordLen' => 4,
            'fontSize' => '30',
            'timeout' => 300,
            'imgDir' => 'images/captcha/',
            'imgUrl' => 'images/captcha',
            'font' =>  'fonts/arial.ttf'
        )));
	  	
		// Init submit
        $this->addElement('Button', 'submit', array(
          'label' => iconv("windows-1251", "UTF-8", "���������"),
          'type' => 'submit',
          'ignore' => true,
        ));
				
        // And finally add some CSRF protection
        $this->addElement('hash', 'csrf', array(
            'ignore' => true,
        ));
        
    }
}

