<?php
/**
 * Guestbook table data gateway
 *
 * @uses       Zend_Db_Table_Abstract
 * @package    QuickStart
 * @subpackage Model
 */
class Default_Model_DbTable_Videolessons extends Zend_Db_Table_Abstract
{
    /**
     * @var string Name of the database table
     */
    protected $_name = 'zend_video_lessons';
	
	public function getVideo($video_id)
    {
        $video_id = (int)$video_id;
        $row = $this->fetchRow('video_id = ' . $video_id);
        if(!$row) {
            throw new Exception("no record video_id - $video_id");
        }
        return $row->toArray();
    }
	
	public function addVideo(    
								$video_title,
								$html_code,
								$alt,
                                $sort_order,
                                $image      
                            )
    {
        $data = array(
			'video_title' => $video_title,
			'html_code' => $html_code,
			'alt' => $alt,
			'sort_order' => $sort_order,
			'image' => $image
        );
        $this->insert($data);
    }
	
	public function deleteVideo($video_id)
    {
        $this->delete('video_id = ' . (int)$video_id);
    }
	
// variant	
/*	public  function updatenashi_zeny($id, $text)
    {   $nashi_zeny = new Default_Model_DbTable_nashi_zeny();
        $data = array(
            'text' => $text,
        );
        $where = $nashi_zeny->getAdapter()->quoteInto('id = ?', $id);
        $this->update($data, $where); 
    } */
	
	public  function updateVideo( 
                                    $video_id,
									$video_title,
									$html_code,
                                    $alt,
									$sort_order,
									$image  
								 )
    {
        $data = array(
			'video_title' => $video_title,
			'html_code' => $html_code,
            'alt' => $alt,
			'sort_order' => $sort_order,
			'image' => $image
        );
        
        $this->update($data, 'video_id = ' . (int)$video_id);
    }
   
   
    public function getAllVideoAdmin()
    {  
        $_name = 'zend_video_lessons'; 
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->order('video_id DESC');
                     
        $all_video = $this->fetchAll($select);
        return $all_video;
    }
    
    public function getAllVideoFront()
    {  
        $_name = 'zend_video_lessons'; 
        $select = $this->select()
                    ->from(array('name' => $_name))
                    ->where('status = ?', '1')
                    ->order('sort_order');
                     
        $all_video = $this->fetchAll($select);
        return $all_video;
    }
	
	public  function updateVideoStatus(
                                            $video_id,
                                            $status
                                        )
    {
        $data = array( 
            'status' => $status  
        );
        
        $this->update($data, 'video_id = ' . (int)$video_id);
    }
	
	public function getVideoImage($video_id)
	{
	    $_name = 'zend_video_lessons';
        $select = $this->select()
                    ->from($_name)
					->where('video_id = ?', $video_id);

		$main_photo_file_rec = $this->fetchRow($select);	
        $main_photo_file_name = $main_photo_file_rec["image"];		
        return $main_photo_file_name;
    }
	
	public  function deleteVideoImage(
                                            $video_id,  
                                            $image
                                        )
    {
        $data = array(
            'image' => $image,
        );
        
        $this->update($data, 'video_id = ' . (int)$video_id);
    }
   
}
