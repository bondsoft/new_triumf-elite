<?php

/**
 * 
 *  
 * 
 */
class Default_Form_Lang_Lang extends Zend_Form
{
    public function init()
    {	
    
        $this
          ->setDescription('Choose photos on your computer to add to this album.')
          ->setAttrib('id', 'lang')
          ->setAttrib('class', 'lang')
          ->setAttrib('name', 'lang')
          ->setAttrib('enctype','multipart/form-data')
         ->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array()));
       
     	    
        // Set the method for the display form to POST
        $this->setMethod('post');
        
        $lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang'); //Zend_Debug::dump($lang); exit;
        	
		$translations_key = new Zend_Form_Element_Text('translations_key');
        $translations_key->setLabel("Переменная")
            ->setAttrib('size', '40')
			->setAttrib('class', 't-a')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true
            );
		
		$translations_value = new Zend_Form_Element_Textarea('translations_value');
        $translations_value->setLabel("Текст")
            ->setRequired(false)
          //  ->setValue($lines)
            ->setAttrib('cols', 130)
            ->setAttrib('rows', 22)
            ->setAttrib('class', 't-a')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534));
            
			
		
        
        $add = new Zend_Form_Element_Submit('add');
        $add   ->setLabel('Сохранить')
					//->setValue($translate->translate('Search'))
                    ->setAttrib('class', 'btn btn-midle')
                    ->removeDecorator('label')
                    ->removeDecorator('DtDdWrapper');
		
                		
        $this->addElements(array(  // $lang,
                                    $translations_key,
                                    $translations_value,
                                    $add
							    )
							);
        
    }
}

