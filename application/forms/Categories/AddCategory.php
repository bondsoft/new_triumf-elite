<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Categories_AddCategory extends Zend_Form
{
    public function init()
    {   
        $uploadId = new Zend_Form_Element_Hidden('uploadId');
        $uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
            ->setAttrib('id', 'uploadId')
            ->setAttrib('value', md5(uniqid(rand())));
        
        $category_id = new Zend_Form_Element_Hidden('category_id');
        $category_id->addFilter('Int');
        
        $request = Zend_Controller_Front::getInstance();
        $id = $request->getRequest()->getParam('category_id');
        
        // prepare menu_first_level_id
      //  $validator_menu_first_level_id = new Zend_Validate_Between(array('min' => 1, 'max' => 100));
      //  $validator_menu_first_level_id->setMessage('Не выбран родительский пункт меню');
        
        $parent_id_object = new Default_Model_DbTable_Categories();
        $parent_id = $parent_id_object->getAllCategoresDescAdmin(); 
      
        if (count($parent_id)!=0)
        {
            $parent_id_prepared['не указана']= "не указана";
            foreach ($parent_id as $parent)
            {
                $parent_id_prepared[$parent->category_id]= $parent->category_name;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $parent_id = new Zend_Form_Element_Select('parent_id');
            $parent_id ->setLabel("Категории:")
                ->addMultiOptions($parent_id_prepared)
                ->setAttrib('class', array('span4')); 
               // ->addValidator($validator_menu_first_level_id);
        }    
    
        $sort_order = new Zend_Form_Element_Text('sort_order');
        $sort_order->setLabel("Порядок сортировки:")
                    ->setAttrib('size', '1')
                    ->setAttrib('class', array('span1'));
        
        $url_seo = new Zend_Form_Element_Text('url_seo');
        $url_seo->setLabel("SEO Url:")
            ->setAttrib('class', 'span14')
            ->setAttrib('id', 'alias')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true)
            ->addValidator('Db_NoRecordExists', false, array(   'zend_category', 
                                                                'url_seo', 
                                                                'exclude' => array(
                                                                                    'field' => 'category_id',
                                                                                    'value' => $id
                                                                                    )
                                                            )
            ); 

           
		$category_name = new Zend_Form_Element_Text('category_name');
        $category_name->setLabel("Название:")
            ->setAttrib('class', 'span14')
            ->setAttrib('id', 'name')
            ->setAttrib('onkeyup', 'translit()')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true
            );
            
        $main_photo = new Zend_Form_Element_File('main_photo');
        $main_photo->setLabel("Выбрать фото:")
            ->setDescription("Допустимые форматы: jpg, png, gif")
            ->setAttrib('size', '50')
        //  ->addValidator('Count', false, array('min' => 1, 'max' => 5))
        //  ->setMultiFile(1)
            ->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '30MB'))
            ->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination('media/photos/categories')
            ->setValueDisabled(true);
        //  ->setRequired(true)
        //  ->addValidator('NotEmpty', false); 

		$title = new Zend_Form_Element_Text('title');
        $title->setLabel("Заголовок(текст):")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');
			
		$title_text = new Zend_Form_Element_Text('title_text');
        $title_text->setLabel("Заголовок(краткое описание):")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');
            
        $category_text = new Zend_Form_Element_Textarea('category_text');
        $category_text->setLabel('Краткое описание')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_01')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534));   
			
		$category_text_extra = new Zend_Form_Element_Textarea('category_text_extra');
        $category_text_extra->setLabel('Текст')
            ->setAttrib('class', 'span14')
            ->setAttrib('rows', 14)
            ->setAttrib('id', 'text_02')
         //   ->setAttrib('class', 'form-control')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534)); 
       
        $header_tags_title = new Zend_Form_Element_Text('header_tags_title');
        $header_tags_title->setLabel("title:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');
            
        $header_tags_description = new Zend_Form_Element_Text('header_tags_description');
        $header_tags_description->setLabel("description:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');

        $header_tags_keywords = new Zend_Form_Element_Text('header_tags_keywords');
        $header_tags_keywords->setLabel("keywords:")
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim'); 	
            
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', 'btn btn-primary pull-left');    

        $this->addElements(array(
                                    $uploadId,
                                    $category_id,
                                    $parent_id,
                                    $sort_order,
                                    $category_name,
                                    $url_seo,
									$title_text,
                                    $category_text,
									$title,
									$category_text_extra,
                                    $header_tags_title,
                                    $header_tags_description,
                                    $header_tags_keywords,
                                    $main_photo,
                                    $submit
                                ));
       
    }
}