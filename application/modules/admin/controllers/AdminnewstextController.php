<?php
/**
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminnewstextController extends Zend_Controller_Action
{
   public function init()
   {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
   }
	
   function indexAction()
   {
        $newstext_model = new Default_Model_DbTable_Newstext();
	
        $news_id = $this->getRequest()->getParam('news_id');
		$this->view->news_id = $news_id;
		
        $news_model = new Default_Model_DbTable_News();
		$news_object = $news_model->getNews($news_id);
		$this->view->news_object = $news_object;
		
		$page = $newstext_model->getNewspageAdmin($news_id);
		//  Zend_Debug::dump($page);exit;
		
		$paginator = Zend_Paginator::factory($page);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(1);		
   }
   
   public function addAction()
    {
		$newstext_model = new Default_Model_DbTable_Newstext();
        $form = new Default_Form_News_Newstext();
        $this->view->form = $form;
        $news_id = $this->getRequest()->getParam('news_id');
		
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $news_text = $form->getValue('news_text');
                
                $newstext_model->addNewstext(
												$news_id,
												$news_text
											);
                
                  $this->_helper->redirector('index', 'adminnewstext', 'admin', array('news_id' => $news_id));
            } else {
                $form->populate($formData);
            }
        }
    }
   
   public function editAction()
    {
        $newstext_model = new Default_Model_DbTable_Newstext();
        $form = new Default_Form_News_Newstext(); 
        $this->view->form = $form;
		
        $newstext_id = $this->getRequest()->getParam('newstext_id');
		$this->view->newstext_id = $newstext_id;
		$page = $this->getRequest()->getParam('page');
		//Zend_Debug::dump($page);exit;
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
           //  Zend_Debug::dump($formData);exit;
            if ($form->isValid($formData)) {
			    $news_id = $form->getValue('news_id');
                $news_text = $form->getValue('news_text');
               
				$news_page = $newstext_model->fetchAll();
		        $this->view->news_page = $news_page;
              
                $newstext_model->updateNewstext(
                                                    $newstext_id,
                                                    $news_id,
                                                    $news_text
                                                );
                
               
                  $this->_helper->redirector('index', 'adminnewstext', 'admin', array('news_id' => $news_id,'newstext_id' => $newstext_id, 'page' => $page));
            } else {
                $form->populate($formData);
            }
        } else {
            $newstext_id = $this->_getParam('newstext_id', 0);
            if ($newstext_id > 0) {
                
                $form->populate($newstext_model->getNewstext($newstext_id));
            }
        }
    }
	
	public function deleteAction()
    {
        $newstext_model = new Default_Model_DbTable_Newstext();
        
        if ($this->getRequest()->isPost()) {
         
            $del = $this->getRequest()->getPost('del');
            $news_id = $this->getRequest()->getParam('news_id');
			$page = $this->getRequest()->getParam('page');
			$newstext_id = $this->getRequest()->getPost('newstext_id'); //Zend_Debug::dump($newstext_id);exit;
            // Если пользователь поддтвердил своё желание удалить запись
            if ($del == 'yes') { 
                $newstext_model->deleteNewstext($newstext_id);
            }
            
            $this->_helper->redirector('index', 'adminnewstext', 'admin', array('news_id' => $news_id, 'page' => $page));
        } else {
            $newstext_id = $this->_getParam('newstext_id');
            
            $this->view->newstext = $newstext_model->getNewstext($newstext_id);
        }
    }

   
}