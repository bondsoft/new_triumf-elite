<?php

/**
 * This is the contact form.  
 * 
 * 
 */
class Default_Form_Contact_Callback extends Zend_Form
{
    public function init()
    {	
	    
        // Element: username
        $this->addElement('Text', 'callback_name', array(
            'label' => '',
        //    'value' => 'Ваше имя...',
            'size'  => '30', 
         //   'maxLength' => '15',
            'required' => true,
            'allowEmpty' => false,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('NotEmpty', true),
            ),
         //   'attribs' => array('class' => 'form-control form-textfields', 'placeholder' => 'Представтесь...', 'required id' => 'callbackName', 'tabindex' => '1'),
            'attribs' => array('class' => '', 'id' => 'callback_contact_name', 'placeholder' => 'Ваше имя', 'onFocus' => 'callbackformContactUsername();'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        )); 
       
		// Element:    , 'onFocus' => 'callbackformContactPhone();'
        $this->addElement('Text', 'callback_phone', array(
            'label' => '',
          //  'value' => 'Ваш телефон...', 
            'size'  => '30',
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array('notEmpty', true, array(
                    'messages' => array(
                        'isEmpty'   =>  'Phone can\'t be empty'
                    )
                )),
                array('StringLength', false, array(16, 16, 'messages' => array(
                            'stringLengthInvalid'           => "Phone Length Invalid entry",
                            'stringLengthTooShort'          => "Phone Invalid Length , ex. 1234567890"
                    ))),
            ),
        //    'attribs' => array('class' => 'form-control form-textfields form-valid', 'placeholder' => 'Ваш телефон...', 'id' => 'callbackEmail', 'tabindex' => '2'),
            'attribs' => array('class' => 'required phone', 'id' => 'callback_contact_phone', 'placeholder' => 'Телефон'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
            
        ));
       
		// Init submit
        $this->addElement('Submit', 'submit', array(
            'label' => 'ОТПРАВИТЬ',
            'type' => 'submit',
            'ignore' => true,
            'attribs' => array('class' => 'submit submit_callback_formdata'),
            'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
                                ),
                            
           // 'class'  => 'ajax-validate'
        ));
		$element = $this->getElement('submit');
        $element->removeDecorator('Label');		
        
    }
}

