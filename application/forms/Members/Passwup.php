<?php
/**
 * This is the guestbook form.  It is in its own directory in the application 
 * structure because it represents a "composite asset" in your application.  By 
 * "composite", it is meant that the form encompasses several aspects of the 
 * application: it handles part of the display logic (view), it also handles 
 * validation and filtering (controller and model).  
 *
 * @uses       Zend_Form
 * @package    QuickStart
 * @subpackage Form
 */
class Default_Form_Members_Passwup extends Zend_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init()
    {
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		
        $this->setName('passwup');
		
        // Element: password
        $this->addElement('Password', 'password', array(
            'label' => $translate->translate('Password:'),
		    'size'  => '30',
		    'description' => $translate->translate('Enter New Password'),
            'required' => true,
		    'validators' => array(
                array('NotEmpty', true),
                array('StringLength', false, array(6, 32)),
            )
        ));
		
		   // Init confirm password
        $this->addElement('Password', 'passconf', array(
          'label' => $translate->translate('Confirm New Password:'),
		  'size'  => '30',
          'description' => $translate->translate('Enter your password again for confirmation.'),
          'required' => true,
          'validators' => array(
            array('NotEmpty', true),
			array('identical', false, array('token' => 'password'))
        )  
        ));
	  	
		// Init submit
        $this->addElement('Button', 'submit', array(
          'label' => $translate->translate('Save'),
          'type' => 'submit',
          'ignore' => true,
        ));    
    }
}

