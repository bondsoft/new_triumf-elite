<?php

class Default_Form_Members_Login extends Zend_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init() 
    {
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
	    // Задаем объект переводчика для формы
        $this->setTranslator($adapter);
		
		// Element: email
        $this->addElement('Text', 'email', array(
			'label' => 'Username:',
		//	'size'  => '15',
			'required' => true,
			'allowEmpty' => false,
			'validators' => array(
				array('NotEmpty', true),
				array('EmailAddress', true)
			),
			'filters'    => array('StringTrim'),
			'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signin-email', 'placeholder' => 'E-mail'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        ));
		$element = $this->getElement('email');
        $element->removeDecorator('Label'); 
	

        // Element: password
        $this->addElement('Password', 'password', array(
			'label' => 'Password:',
			'required' => true,
			'attribs' => array('class' => 'full-width has-padding has-border', 'id' => 'signin-password', 'placeholder' => 'Пароль'),
			'decorators' =>array(
										'ViewHelper',
										'Label',
										array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
										array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
									)
        ));
		$element = $this->getElement('password');
        $element->removeDecorator('Label'); 
	  	
		// Init submit
        $this->addElement('Button', 'submit', array(
			'label' => 'Войти',
			'type' => 'submit',
			'ignore' => true,
			'attribs' => array('class' => 'submit_login'),
        ));
		$element = $this->getElement('submit');
        $element->removeDecorator('Label'); 
		
		
        
        
    }
}

