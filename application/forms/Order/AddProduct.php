<?php
/**
 * 
 *  
 * 
 */
class Default_Form_Order_AddProduct extends Zend_Form
{
    public function init()
    {   
        $uploadId = new Zend_Form_Element_Hidden('uploadId');
        $uploadId ->setAttrib('name', 'UPLOAD_IDENTIFIER')
            ->setAttrib('id', 'uploadId')
            ->setAttrib('value', md5(uniqid(rand())));
        
        $product_id = new Zend_Form_Element_Hidden('product_id');
        $product_id->addFilter('Int');
        
        // prepare menu_first_level_id
        $validator_products_model = new Zend_Validate_Between(array('min' => 1, 'max' => 100));
        $validator_products_model->setMessage('Не выбран продукт');
        
        
        $products_model = new Default_Model_DbTable_Products();
        $products = $products_model->fetchAll(); 
      
        if (count($products)!=0)
        {
            $product_prepared['не указана']= "не указан";
            foreach ($products as $product)
            {
                $product_prepared[$product->product_id]= $product->product_model;
            }
        //Zend_Debug::dump($regions_prepared);exit;
        
            $product_id = new Zend_Form_Element_Select('product_id');
            $product_id ->setLabel("Продукты:")
                ->addMultiOptions($product_prepared)
                ->setRequired(true)
                ->setAttrib('class', array('span4'))
			//	->removeDecorator('label'); 
                ->addValidator($validator_products_model);
        }    
    
        
           
		$quantity = new Zend_Form_Element_Text('quantity');
        $quantity->setLabel("Количество:")
            ->setAttrib('class', 'span1')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true);
		//	->removeDecorator('label');
            
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel("Сохранить");
        $submit->setAttrib('id', 'submitbutton');
        $submit->setAttrib('class', ' btn btn-primary pull-left');    

        $this->addElements(array(
                                    $product_id,
                                    $quantity,
                                    $submit
                                ));
       
    }
}