<?php
/**
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
 
class Menu_IndexController extends Zend_Controller_Action
{ 
    public function init()
    {
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('page');

    }
   
    public function indexAction()
    {
    /*   $categories_model = new Default_Model_DbTable_Menu();

        $categories_01 = $categories_model->getAllFirstLevelCategories(0); //Zend_Debug::dump($categories_01);exit;
     
        $categories = array();
        
        foreach ($categories_01 as $category) {
			$children_data = array();
			$children_lv3_data = array();
            $children = $categories_model->getAllSecondLevelCategories($category['category_id']); // Zend_Debug::dump($children); exit;
            
            foreach ($children as $child) {
                $children_data[] = array(
                            'category_name'  => $child['category_name'],
                            'url_seo'  => $child['url_seo']
                        );
				$children_lv3 = $categories_model->getAllSecondLevelCategories($child['category_id']);  //Zend_Debug::dump($children_lv3); exit;
				foreach ($children_lv3 as $child_lv3) {
					$children_lv3_data[] = array(
								'category_name'  => $child_lv3['category_name'],
								'url_seo'  => $child_lv3['url_seo']
							);
					
				}
            }
			if(@$children_lv3)
			{
				
				
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'children_lv3' => $children_lv3_data,
						'url_seo'     => $category['url_seo']
					);
			} else {
				$categories[][] = array(
						'category_name'     => $category['category_name'],
						'children' => $children_data,
						'url_seo'     => $category['url_seo']
					);
			}
			
        }
     //   Zend_Debug::dump($categories);exit;
     
        $this->view->categories_01 = $categories; */
    }
	
	public function pageAction()
    {   
		$categories_model = new Default_Model_DbTable_Menu();
		
		$url_seo = $this->getRequest()->getParam('url_seo');

        $text = $categories_model->getPageTextByUrl($url_seo); 
		$this->view->text = $text;
		$title = $categories_model->getPageTitleByUrl($url_seo); 
		$this->view->title = $title;
	//	Zend_Debug::dump($url_seo);exit;
     
        
    }
}
