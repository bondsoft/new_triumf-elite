<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_CategoryphotoController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');	
        
//Zend_Debug::dump($this->view->news_main_photo_file);exit;
    }
	
    function indexAction()
    {
        $photo_model = new Default_Model_DbTable_Categoryphoto();
        $category_id = $this->getRequest()->getParam('category_id');
		$photos = $photo_model->getAllPhotosForObject($category_id);
		//Zend_Debug::dump($photos);exit;
		
		$paginator = Zend_Paginator::factory($photos);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator; 
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(10);	

        $categories_model = new Default_Model_DbTable_Categories();
        
		$categories_object = $categories_model->getCategory($category_id);
		$this->view->categories_object = $categories_object;
	}
   
    public function addphotoAction()
    {
	    include( 'SimpleImage.php' );
		
        $form = new Default_Form_Categories_Photo();
        $this->view->form = $form;
        
        $categories_model = new Default_Model_DbTable_Categories();
        $category_id = $this->getRequest()->getParam('category_id');  //Zend_Debug::dump($sports_school_id);exit;
        $categories_object = $categories_model->getCategory($category_id);
		$this->view->categories_object = $categories_object;
		
		$values = $form->getValues();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData))
			{
				$category_id = $this->getRequest()->getParam('category_id'); 
                $photo_file = $form->getValue('photo_file'); 
				
				if( empty($photo_file) )
                {
	                echo "<b><font size=\"4\" color=\"red\" face=\"Arial\"><br><br><br>" ;
					echo "Вы не выбрали файл!";
	                echo "</font></b>" ;
                    return;
                }
			
                $photo_model = new Default_Model_DbTable_Categoryphoto();
				
				$data = $_FILES['photo_file'];
				
				foreach ($data['type'] as $key => $value) // Zend_Debug::dump($value);exit;
                {	
                    if(	$value != NULL )
                    {				
                        $uploadedfile = $data['tmp_name'][$key];   // Zend_Debug::dump($uploadedfile);exit;
                        
                        if(file_exists($uploadedfile))
                        {
                        /* ********************************************** */
                            $ext = explode(".",$data["name"][$key]); 
                            $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                         //   Zend_Debug::dump($newname);exit;
                            $name_big = "media/photos/categories/pages/big_".$newname;
                            $name_small = "media/photos/categories/pages/small_".$newname;
                            
                        /* ********************************************** */
                            
                            $image_big = new SimpleImage();
							$image_big->load($uploadedfile);
							$image_big->save($name_big);
									
							$image_small = new SimpleImage();
							$image_small->load($uploadedfile);
							$image_small->resizeToWidth(350);
						//	$image_small->cutFromCenter(130, 75);
						//	$image_small->maxareafill(290, 190, 255, 255, 255); 
						
							$image_small->save($name_small); 
									
						} 
                        
                        $photo_model->addPhoto(
												$category_id, 
												$newname
											);
                    }
				}
				// Уничтожаем файл во временном каталоге
                   unlink($_FILES['photo_file']['tmp_name']);
               
                  $this->_helper->redirector('index', 'categoryphoto', 'admin', array('category_id' => $category_id));
            } else {
                // Если форма заполнена неверно,
                $this->view->errMessage = "ффффффффффффффф" ;
            }
        }
    }
	
	public function editphotoAction()
    {
	    include( 'SimpleImage.php' );
        $form = new Default_Form_Categories_EditPhotoFile();    
        $this->view->form = $form;
        
        $categories_model = new Default_Model_DbTable_Categories();
        $category_id = $this->getRequest()->getParam('category_id'); 
        $categories_object = $categories_model->getCategory($category_id);
		$this->view->categories_object = $categories_object;
		
		$photo_id = $this->getRequest()->getParam('photo_id');// Zend_Debug::dump($photo_id);exit;
		
		$photo_model = new Default_Model_DbTable_Categoryphoto();
				
		$this->view->photo = $photo_model->getPhoto($photo_id); //Zend_Debug::dump($this->view->photo);exit;
		$old_photo_file = $photo_model->getPhotoFile($photo_id);//Zend_Debug::dump($old_photo_file);exit;
		  
        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();
		   
		   	if ($form->isValid($formData) && $form->submit->isChecked())
			{
			    $photo_file = $form->getValue('photo_file');
				
			    $directory = "media/photos/categories/pages";
		    	
            
				if( empty($photo_file) )
                {
					$this->view->errMessage = "Вы не выбрали файл!";
                    return;
                }
                
                // удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
				
				$data = $_FILES['photo_file'];
				$uploadedfile = $data['tmp_name']; //Zend_Debug::dump($uploadedfile);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name_big = "media/photos/categories/pages/big_".$newname;
                    $name_small = "media/photos/categories/pages/small_".$newname;
					
				   /* ********************************************** */
					
                    $image_big = new SimpleImage();
					$image_big->load($uploadedfile);
				//	$image_big->resizeToWidth(200); 
							
					$image_big->save($name_big);
									
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->resizeToWidth(350);
				//	$image_small->cutFromCenter(130, 75);
				//	$image_small->maxareafill(290, 190, 255, 255, 255); 
						
					$image_small->save($name_small); 
				} 
				
                $photo_model->updatePhotoFile(
												$photo_id,
												$newname
											);
				
				// Уничтожаем файл во временном каталоге
                 unlink($_FILES['photo_file']['tmp_name']);
                
                $this->_helper->redirector('index', 'categoryphoto', 'admin', array('category_id' => $category_id));
            }  else {
                $this->_helper->redirector('index', 'categoryphoto', 'admin', array('category_id' => $category_id));
            }
        } else {
            // Если мы выводим форму, то получаем id записи, которую хотим обновить
            $photo_id = $this->_getParam('photo_id', 0);
            if ($photo_id > 0) {
                
				$this->view->photo = $photo_model->getPhoto($photo_id);
                
                $form->populate($photo_model->getPhoto($photo_id));
            }
        }
    }
	
	public function deleteAction()
    {
        $photo_model = new Default_Model_DbTable_Categoryphoto();
        if ($this->getRequest()->isPost()) {

            $del = $this->getRequest()->getPost('del'); // Zend_Debug::dump($del);exit;

                $category_id = $this->getRequest()->getParam('category_id');
				$photo_id = $this->getRequest()->getParam('photo_id');
            
            // Если пользователь поддтвердил своё желание удалить запись
            if ($del == 'yes') {
                $old_photo_file = $photo_model->getPhotoFile($photo_id);//Zend_Debug::dump($old_photo_file);exit;
              
                
				$photo_file = $photo_model->getPhotoFile($photo_id);
				
			//	Zend_Debug::dump($photo_file);exit;
			
				$directory = "media/photos/categories/pages";
				// удаляем файл из каталога
                if(is_file("$directory/big_$old_photo_file"))
                {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                }
                
              //  Zend_Debug::dump($photo_file);exit;
               
                // Вызываем метод модели для удаления записи
                $photo_model->deletePhotoForObject($photo_id);
            }
            $this->_helper->redirector('index', 'categoryphoto', 'admin', array('category_id' => $category_id));
        } else {
            // Если запрос не Post, выводим сообщение для поддтвержения
            // Получаем id записи, которую хотим удалить
            $photo_id = $this->_getParam('photo_id');
           
            $this->view->photo = $photo_model->getPhoto($photo_id);
            $categories_model = new Default_Model_DbTable_Categories();
			$category_id = $this->getRequest()->getParam('category_id'); 
			$categories_object = $categories_model->getCategory($category_id);
			$this->view->categories_object = $categories_object;
        }
    }
}