<?php

/**
 * 
 *  
 * 
 */
class Default_Form_Settings_Settings extends Zend_Form
{
    public function init()
    {	
	    
        // Set the method for the display form to POST
        $this->setMethod('post');
		
	    // указываем имя формы
        $this->setName('setting');
		$setting_id = new Zend_Form_Element_Hidden('setting_id');
        $setting_id->addFilter('Int');
        
    /*    $setting_value = new Zend_Form_Element_Text('setting_value');
        $setting_value->setLabel("Ведите значение:")
            ->setAttrib('size', '120')
            ->setAttrib('class', 'span14')
            ->addFilter('StripTags')
            ->addFilter('StringTrim'); */
            
        $setting_value = new Zend_Form_Element_Textarea('setting_value');
        $setting_value->setLabel('')
            ->setAttrib('cols', 110)
            ->setAttrib('rows', 5)
            ->setAttrib('class', 'span10')
        //    ->setAttrib('id', 'article_text')
            ->addFilter('StringTrim')
            ->addValidator('StringLength', false, array(0,65534));     

        $this->addElements(array(
                                    $setting_id,
                                    $setting_value    
                                ));
		
		$this->addElement('Button', 'submit', array(
          'label' => "Сохранить",
          'type' => 'submit',
          'class' => 'btn btn-primary pull-left',
          'ignore' => true,
        ));	 
    }
}

