<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 *  
 */
class Admin_AdminmanufacturersController extends Zend_Controller_Action
{
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    function indexAction()
    {
        $manufacturers_model = new Default_Model_DbTable_Manufacturers();

        $manufacturers = $manufacturers_model->getAllManufacturersAdmin(); //Zend_Debug::dump($categories_01);exit;
     
		$paginator = Zend_Paginator::factory($manufacturers);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);
       
    }
   
    public function addmanufacturerAction()
    {
		include( 'SimpleImage.php' );
        $form = new Default_Form_Manufacturers_AddManufacturer();
        $this->view->form = $form;
      //  Zend_Debug::dump($form);exit;
        $manufacturers_model = new Default_Model_DbTable_Manufacturers();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
         //   Zend_Debug::dump($formData);exit;
            if ($form->isValid($formData)) {
                $name = $form->getValue('name');
                $sort_order = $form->getValue('sort_order');
                $image = $form->getValue('image');
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
				$uploadedfile = $data['tmp_name']; 
             			//	Zend_Debug::dump($uploadedfile);exit;
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
				    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name_big = "media/photos/manufacturers/big_".$newname;
					$name_small = "media/photos/manufacturers/small_".$newname;
					
				   /* ********************************************** */
				    $image_big = new SimpleImage();
				    $image_big->load($uploadedfile);
				//	$image_big->resizeToWidth(600);
				    $image_big->save($name_big);
					
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->resizeToWidth(200);
				    $image_small->save($name_small);  
				}  
                
                $manufacturers_model->addManufacturer(
														$name, 
														$sort_order,
														$newname
													);
				
				unlink($_FILES['image']['tmp_name']);
                    
                $this->_helper->redirector('index');
            } else { 
				$this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }
   
   
    public function editmanufacturerAction()
    {
        include( 'SimpleImage.php' );
        $form = new Default_Form_Manufacturers_AddManufacturer();
        $this->view->form = $form;
      //  Zend_Debug::dump($form);exit;
		$manufacturer_id = $this->getRequest()->getParam('manufacturer_id');
        $manufacturers_model = new Default_Model_DbTable_Manufacturers();
		$manufacturer_image = $manufacturers_model->getManufacturerImage($manufacturer_id);
        $this->view->manufacturer_image = $manufacturer_image; 
        $this->view->manufacturer_id = $manufacturer_id; 
		
		$old_photo_file = $manufacturers_model->getManufacturerImage($manufacturer_id);
        $directory = "media/photos/manufacturers";
        if ($this->getRequest()->isPost())
        {
            $formData = $this->getRequest()->getPost();  //  Zend_Debug::dump($formData);exit;
           
            if ($form->isValid($formData)) 
            {  
                $manufacturer_id = $form->getValue('manufacturer_id');
                $name = $form->getValue('name');
                $sort_order = $form->getValue('sort_order');
                $image = $form->getValue('image');
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
				$uploadedfile = $data['tmp_name']; 
             			//	Zend_Debug::dump($uploadedfile);exit;
				if($image)
				{		
					if(is_file("$directory/small_$old_photo_file"))
                    {
                    unlink("$directory/big_$old_photo_file");
                    unlink("$directory/small_$old_photo_file");
                    }
				}
				
				if(file_exists($uploadedfile))
				{
				   /* ********************************************** */
				    $ext = explode(".",$data["name"]); 
				    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
				    $name_big = "media/photos/manufacturers/big_".$newname;
					$name_small = "media/photos/manufacturers/small_".$newname;
					
				   /* ********************************************** */
				    $image_big = new SimpleImage();
				    $image_big->load($uploadedfile);
				//	$image_big->resizeToWidth(600);
				    $image_big->save($name_big);
					
					$image_small = new SimpleImage();
					$image_small->load($uploadedfile);
					$image_small->resizeToWidth(200);
				    $image_small->save($name_small);  
				} 
				
				if(!$image)
                {      
                    $manufacturers_object = $manufacturers_model->getManufacturer($manufacturer_id);
                    $newname = $manufacturers_object['image'];
                
                }
 
                $manufacturers_model->updateManufacturer(
															$manufacturer_id,
															$name,
															$sort_order,
															$newname
														);
														
                unlink($_FILES['image']['tmp_name']);
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else { 
            $manufacturer_id = $this->_getParam('manufacturer_id', 0);
            if ($manufacturer_id > 0)
            {
                $form->populate($manufacturers_model->getManufacturer($manufacturer_id));
                $manufacturers_object = $manufacturers_model->getManufacturer($manufacturer_id);
                $this->view->name = $manufacturers_object['name'];
            }
        }
    }
    
    public function deletemanufacturerAction()
    {
        $manufacturers_model = new Default_Model_DbTable_Manufacturers();
        $manufacturer_id = $this->getRequest()->getParam('manufacturer_id');
        $page = $this->getRequest()->getPost('page');
        if ($this->getRequest()->isPost())
        {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes')
            {
                $manufacturer_id = $this->getRequest()->getPost('manufacturer_id');
                $manufacturers_object = $manufacturers_model->getManufacturer($manufacturer_id);
				$old_photo_file = $manufacturers_model->getManufacturerImage($manufacturer_id);//Zend_Debug::dump($old_photo_file);exit;
                $this->view->manufacturers_object = $manufacturers_object;
                $products_model = new Default_Model_DbTable_Products();
                $checkActiv = $products_model->checkActivManufacturer($manufacturer_id);
             // Zend_Debug::dump($subcategory_second_level_name);exit;
                if($checkActiv != NULL){
                    $this->view->errMessage = "Этот производитель активнен. Удалить нельзя!";
                    return;
                } else {
                    $manufacturers_model->deleteManufacturer($manufacturer_id);
                    
                    
                    $directory = "media/photos/manufacturers";
                    if(is_file("$directory/small_$old_photo_file"))
                    {
                        unlink("$directory/big_$old_photo_file");
                        unlink("$directory/small_$old_photo_file");
                    } 
                } 
               
            }
            $this->_helper->redirector('index', 'adminmanufacturers', 'admin', array('page' => $page));
        } else {
            $manufacturer_id = $this->_getParam('manufacturer_id');
            $manufacturers_object = $manufacturers_model->getManufacturer($manufacturer_id);
            
            $this->view->manufacturers_object = $manufacturers_object;
        }
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $manufacturers_model = new Default_Model_DbTable_Manufacturers();
        $manufacturer_id = $this->getRequest()->getParam('manufacturer_id');
        
        $old_photo_file = $manufacturers_model->getManufacturerImage($manufacturer_id);
        $directory = "media/photos/manufacturers";
        
        if ($this->getRequest()->isPost())
        {
            $manufacturer_id = $this->getRequest()->getParam('manufacturer_id');
            $image = '';
           
            $manufacturers_model->deleteManufacturerImage(
                                                            $manufacturer_id,  
                                                            $image
                                                        );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
   
}