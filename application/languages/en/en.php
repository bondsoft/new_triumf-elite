<?php 

/**
 * Файл с переводами на английский
 * 
 * 
 */

return $errors = array (

    Zend_Validate_Alnum::NOT_ALNUM     => "Значение содержит запрещенные символы. Разрешены символы латиници, кирилици, цифры и пробел",
    Zend_Validate_Alnum::STRING_EMPTY  => "Поле не может быть не заполненным",
      
    Zend_Validate_Date::INVALID        => 'Неверная дата',   
    Zend_Validate_Date::FALSEFORMAT    => 'Значение не соответствует указанному формату',   
     
    Zend_Validate_EmailAddress::INVALID_FORMAT     => "'%value%' is no valid email address in the basic format local-part@hostname",
    Zend_Validate_EmailAddress::INVALID_HOSTNAME   => "'%hostname%' is no valid hostname for email address '%value%'",
    Zend_Validate_EmailAddress::INVALID_MX_RECORD  => "'%hostname%' does not appear to have a valid MX record for the email address '%value%'",
    Zend_Validate_EmailAddress::DOT_ATOM           => "'%localPart%' can not be matched against dot-atom format",
    Zend_Validate_EmailAddress::QUOTED_STRING      => "'%localPart%' can not be matched against quoted-string format",
    Zend_Validate_EmailAddress::INVALID_LOCAL_PART => "'%localPart%' is no valid local part for email address '%value%'",
	
	Zend_Validate_Hostname::IP_ADDRESS_NOT_ALLOWED  => "'%value%' appears to be an IP address, but IP addresses are not allowed",
	Zend_Validate_Hostname::LOCAL_NAME_NOT_ALLOWED  => "'%value%' appears to be a local network name but local network names are not allowed",
	Zend_Validate_Hostname::UNKNOWN_TLD             => "'%value%' appears to be a DNS hostname but cannot match TLD against known list",

    Zend_Validate_Int::NOT_INT => 'Значение не является целочисленным значением',   
     
    Zend_Validate_NotEmpty::IS_EMPTY => 'Value is required and can\'t be empty',
     
    Zend_Validate_StringLength::TOO_SHORT => 'Длина введенного значения меньше чем %min% символов',   
    Zend_Validate_StringLength::TOO_LONG  => 'Длина введенного значения больше чем %max% символов',   

  //  App_Validate_EqualInputs::NOT_EQUAL => 'Пароли не совпадают',
    
  //  App_Validate_Password::INVALID => 'Не верный формат пароля',
  //  App_Validate_Password::INVALID_LENGTH => 'Длина пароля должна быть от 7 до 30ти символов',
    
    Zend_Validate_Db_RecordExists::ERROR_RECORD_FOUND => 'Такое значение уже присутствует в базе данных',
    
    Zend_Captcha_Word::BAD_CAPTCHA   => 'You enter incorrect simbols',
    Zend_Captcha_Word::MISSING_VALUE => 'Поле не может быть пустым',
	
	Zend_Validate_Identical::NOT_SAME => 'Please make sure the ""password"" and ""password again"" fields match.',
    
	'home' => 'Заглавная',
	'real estate' => 'Real Estate',
    'real estate services' => 'Real estate services',
    'services for business' => 'Services for business',
	'helpful information' => 'Helpful information',
	'about' => 'О компании',
	'contact' => 'Contact',
	'video' => 'Video',
	
	'intro-title' => 'EN EN Lorem ipsum dolor sit amet, consectetuer adipiscing elit.',
	'company name lable' => 'Company name:',
	'company name text' => 'RIVER, SIA',
	'reg number lable' => 'Регистрационный номер:',
	'reg number text' => '1000123456789',
	'ure adress lable' => 'Юридический адрес:',
	'ure adress text' => 'Brīvības iela 111-11/1, Rīga, LV-1011, Latvija',
	'telefon_01 lable' => 'Телефон:',
	'telefon_01 text' => '(+371) 20000000',
	'telefon_02 lable' => 'Телефон:',
	'telefon_02 text' => '(+371) 20000000',
	'email lable' => 'Е-почта:',
	'email text' => 'info@domain.lv',
	'contacts form lable' => 'Lorem ipsum',
	
// страница О компании
    'content-h1-table-full_text' => 'Регистрация компаний в Латвии',
    'content article intro' => 'Компания, зарегистрированная в Латвии, отлично выполняет функцию европейского посредника в торговых отношениях между Западом и русскоговорящими странами. 
Законодательство Латвийской республики позволяет при открытии компании нерезидентам получать все права и преимущества юридического лица зарегистрированного в Евросоюзе. Вести операции с 
недвижимостью, 
экспорт/импорт товаров и услуг из/в ЕС и т.д.',

// -----------------главная
    'Hot deals' => 'Горячие предложения',
	'Commercial properties' => 'Коммерческие объекты',
    'Forward' => 'Forward',
	'all projects' => 'all projects',
	'Show all projects' => 'Show all projects',
    'content-chapter-intro' => 'Этот текст редактируется в файле en.php каталог application/languages/en',
    'content-chapter-text' => 'Этот текст редактируется в файле en.php каталог application/languages/en',
    'Real Estate' => 'Real Estate',
    'content-chapter-text Real Estate' => 'Этот текст редактируется в файле en.php каталог application/languages/en',
    'Investment projects' => 'Investment projects',
    'content-chapter-intro Investment projects' => 'content-chapter-text Investmelvlvlvlvlvl',
    'content-chapter-text Investment projects' => 'content-chapter-text Investment enenenenenene',
//------------------------    
    
// --------------полезная информация
    'helpful information' => 'Полезная информация',
	'Read more' => 'Read more',
    'content information intro' => 'Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis 
lacus. Morbi magna magna, tincidunt 
a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.',
	'content information text' => 'gjkhkjhkjjdhkjashdkjashdkjahskdjahskjdhaks',
//--------------------------
// ------------------контакты	
	'content contacts intro' => 'Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.',
	
	'content contacts text' => 'Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.',
//------------------------
//-------------- футер	
	'footer about' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, biben dum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Mae cenas placerat, nisl at consequat rhoncus.',
	'footer copyright' => '© 2014. River.lv. All rights reserved. Ph.: (+371)20000000, info@river.lv',
	'facebook' => 'http://www.facebook.com/pages/Global-Finance/155098751230367',
	'twitter' => 'http://twitter.com/GlobalFinanceLV',
    'skype' => 'callto://real.lv/',
//-------------------------
//----------- услуги по недвижимости
	'real estate services content list intro' => 'цццццццццццццццццццц',
	'real estate services content list text' => 'вввввввввввввввввввввввввв',
//-------------------		
//----------- услуги для бизнеса
	'services for business intro' => 'qqqqqqqqqqqqqqqqqqqq',
	'real estate services intro' => 'uuuuuuuuuuuuuuuuuuuu',
	'services for business content list intro' => 'aaaaaaaaaaaaqqqqqqqqqqqqqqqqqqqq',
	'services for business content list text' => 'aaaaaaaaaaaaaaaaauuuuuuuuuuuuuuuuuuuu',
//-------------------
//----------------------страница объекта
	'Similar offers' => 'Похожие предложения',
	'lot_content_chapter_intro' => 'dasdasdasdasdasd',
	'lot_content_chapter_text' => 'qqdfssdfsdfsfrererewrewrwe',
	'to cancel' => 'Отменить',
	'View location on map' => 'View location on map',
	'Town, region' => 'Town, region',
	'City / town' => 'City / town',
	'Settlement' => 'Settlement',
	'Street' => 'Street',
	'Area, m2' => 'Area, m2',
	'Number of floors' => 'Number of floors',
	'Rooms' => 'Rooms',
	'Land area, m2' => 'Land area, m2',
	'Conveniences' => 'Conveniences',
	'Price, €' => 'Price, €',
	'Save a description as pdf-file' => 'Save a description as pdf-file',
	'More information' => 'More information',
	'Contact person' => 'Contact person',
	'Video' => 'Video',
	'Photos' => 'Photos',
//------------------------			
// ----------форма поиска     
    'Type of deal' => 'Вид сделки',
    'Real Estate' => 'Недвижимость',
    'Region' => 'Регион',
    'Rooms' => 'Комнаты',
	'from' => 'от',
    'to' => 'до',
    'Area, m' => 'Площадь, м',
    'Price' => 'Цена',
    'Search' => 'Искать',
//----------------	
	'Username:' => 'Username:',
	'Password:' => 'Password:',
	'Login' => 'Login',
	'authorization' => 'authorization',
	'Create Account' => 'Create Account',
	'Lost Password?' => 'Lost Password?',
	
	'Email Address:' => 'Email Address:',
	'Passwords must be at least 6 characters in length.' => 'Passwords must be at least 6 characters in length.',
	'Password Again:' => 'Password Again:',
	'Enter your password again for confirmation.' => 'Enter your password again for confirmation.',
	'Enter the simbols:' => 'Enter the simbols:',
	'Signup' => 'Signup',
	
	'Choose avatar' => 'Choose avatar',
	'Save' => 'Save',
	'Cancel' => 'Cancel',
	
	'Subgect:' => 'Тема вопроса',
	'Email:' => 'Email:',
	'You have been sent an email with new password.' => 'You have been sent an email with new password.',
	'Send Email' => 'Send Email',
	
	'Enter New Password' => 'Enter New Password',
	'Confirm New Password:' => 'Confirm New Password:',
	'Name:' => 'Name:',
	'Message:' => 'Message:',
	'Send Message' => 'Send',
	
	
	
	
//	'home' => 'home',
	'Create Account' => 'Create Account',
	'password recovery' => 'password recovery',
	'Edit My Profile' => 'Edit My Profile',
	'Exit' => 'Exit',
	
	'Edit avatar' => 'Edit avatar',
	'Delete avatar' => 'Delete avatar',
	'Change your password' => 'Change your password',
	'Delete Account' => 'Delete Account',
	'Yes' => 'Yes',
	
	'Are you sure you want to delete your account? Any content you\'ve uploaded in the past will be permanently deleted.' => 'Are you sure you want to delete your account? Any content you\'ve uploaded in the past will be permanently deleted.',
	
	'Are you sure you want to delete your avatar?' => 'Are you sure you want to delete your avatar?',
	'Enter the Password' => 'Enter the Password',
	'Passwords did not match' => 'Passwords did not match',
	'You have not selected a file!' => 'You have not selected a file!',
	'You have entered an incorrect username or password.' => 'You have entered an incorrect username or password.',
	
	'There was an error sending the message. Please try again later.' => 'There was an error sending the message. Please try again later.',
	'Your message has been sent successfully.' => 'Your message has been sent successfully.',
	
	
 );
