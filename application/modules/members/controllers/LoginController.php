<?php
/**
 * 
 *
 * Default controller for this application.
 * 
 * 
 * 
 * 
 */
class Members_LoginController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
		$helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('layout');

    }
	
	function indexAction()
    {
       // $this->_redirect('/');
		//$this->_helper->redirector('login');
    }
	
    public function loginAction()
    {
		$request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
	    $lang = Zend_Registry::get('Zend_Lang');
		$translate = Zend_Registry::get('Zend_Translate');

        $form_login = new Default_Form_Members_Login();
    //    $this->view->form_login = $form_login;

        // 
        if ($this->getRequest()->isPost()) {
            // 
            $formData = $this->getRequest()->getPost();
            
            // 
            if ($form_login->isValid($formData)) {
                // 
                $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
                
                // указываем таблицу, где необходимо искать данные о пользователях
                // кололку, где искать имена пользователей,
                // а так же колонку, где хранятся пароли
                $authAdapter->setTableName('zend_users')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');
                
             //   $email = $this->getRequest()->getPost('email');
                $password = $this->getRequest()->getPost('password'); //Zend_Debug::dump($password);exit;
				
				$email = $_POST['email']; //Zend_Debug::dump($email);exit;
			//	$password = $_POST['password'];
                
                // подставляем полученные данные из формы
                $authAdapter->setIdentity($email)
                    ->setCredential(md5($password));
              
                // получаем экземпляр Zend_Auth
                $auth = Zend_Auth::getInstance();
                
                // делаем попытку авторизировать пользователя
                $result = $auth->authenticate($authAdapter);
                
                // если авторизация прошла успешно
                if ($result->isValid()) {
                    // используем адаптор для извлечения оставшихся данных о пользователе
                    $identity = $authAdapter->getResultRowObject();
                    
                    // получаем доступ к хранилищу данных Zend
                    $authStorage = $auth->getStorage();
                    
                    // помещаем туда информацию о пользователе,
                    // чтобы иметь к ним доступ при конфигурировании ACL
                    $authStorage->write($identity);
					
					$auth = Zend_Auth::getInstance();
	                $userdata = $auth->getIdentity();            
	                $user_id = $userdata->user_id;
					
					$users_object = new Default_Model_DbTable_Users();
					$users_object->updateLast_access($user_id);
				
				    header('Content-Type: application/json');
					echo Zend_Json::encode(array('success' => true));
					exit;
					
					// $this->_redirector = $this->_helper->getHelper('Redirector');
					// $this->_redirector->gotoUrl('/');
					
                } 
            } else {
                    $json = $form_login->getMessages();
					header('Content-Type: application/json');
					echo Zend_Json::encode($json);
					exit;
                }
        } 
 
    }
	
	public function fogotpassAction()
    {
		$request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
	    $lang = Zend_Registry::get('Zend_Lang');
		$translate = Zend_Registry::get('Zend_Translate');
		
        $form_fogotpass = new Default_Form_Members_Fogotpass();
        $this->view->form_fogotpass = $form_fogotpass;

        if ($this->getRequest()->isPost())
		{
            $formData = $this->getRequest()->getPost();
            
            if ($form_fogotpass->isValid($formData))
			{
                $email = $this->getRequest()->getPost('email');  // Zend_Debug::dump($email);exit;
				
			    $users_object = new Default_Model_DbTable_Users();
				
				$user_id = $users_object->getUser_id($email);   // Zend_Debug::dump($user_id);exit;
           
		        if (!$users_object->send_new_password_to_user($email, $user_id))
				{
				    $this->view->errMessage = $translate->translate('There was an error sending the message. Please try again later.');
				} else {
				    $this->view->errMessage = $translate->translate('You have been sent an email with new password.');
                }
				
				header('Content-Type: application/json');
				echo Zend_Json::encode(array('success' => true));
				exit;
                
            } else {
						$json = $form_fogotpass->getMessages();
						header('Content-Type: application/json');
						echo Zend_Json::encode($json);
						exit;
					}
        }
 
    }

    public function logoutAction()
    {
       // уничтожаем информацию об авторизации пользователя
        Zend_Auth::getInstance()->clearIdentity();
        
        $lang = Zend_Registry::get('Zend_Lang');
		$this->_redirector = $this->_helper->getHelper('Redirector');
		$this->_redirector->gotoUrl('/');
		
      //  $this->_helper->redirector('index', 'index', array('lang' => $lang));
    }

   
}

