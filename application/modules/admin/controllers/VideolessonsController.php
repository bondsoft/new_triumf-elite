<?php

class Admin_VideolessonsController extends Zend_Controller_Action
{
    
    public function init()
    {
        /* Initialize action controller here */
        $helper = $this->_helper->getHelper('Layout');
        $layout = $helper->getLayoutInstance();
        $layout->setLayout('adminlayout');
    }
    
    public function indexAction()
    {
        $video_model = new Default_Model_DbTable_Videolessons();
        $videos = $video_model->getAllVideoAdmin(); //Zend_Debug::dump($products);exit;
		
        $paginator = Zend_Paginator::factory($videos);
        $paginator->setCurrentPageNumber($this->_getParam('page', 1));
        $this->view->paginator = $paginator;
        $count_per_page = Zend_Paginator::setDefaultItemCountPerPage(20);   
    
    }
    
    public function addAction()
    {
        include( 'SimpleImage.php' );
		
        $form = new Default_Form_Videolessons_VideoLessons(); 
        $this->view->form = $form;
        $video_model = new Default_Model_DbTable_Videolessons();
        
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $sort_order = $_POST['sort_order'];
                $video_title = $form->getValue('video_title');
				$html_code = $form->getValue('html_code');
				$alt = $form->getValue('alt');
                $image = $form->getValue('image');
				$image = $_POST['image'];
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
                    $name_big = "media/photos/videolessons/big_".$newname;
                    $name_small = "media/photos/videolessons/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
				//	$image_big->cutFromCenter(371, 373);
				//	$image_big->maxareafill(371, 373, 255, 255, 255); 
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->maxareafill(478, 268, 0, 0, 0); 
                    $image_small->save($name_small);  
                }  
                
                $video_model->addVideo(
											$video_title,
											$html_code,
                                            $alt,
                                            $sort_order,
                                            $newname                  
                                        );
                                            
                unlink($_FILES['image']['tmp_name']);                            
                    
                $this->_helper->redirector('index');
            } else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        }
    }

    public function editAction()
    {
        include( 'SimpleImage.php' );
				
        $form = new Default_Form_Videolessons_VideoLessons(); 
        $this->view->form = $form;
        $video_model = new Default_Model_DbTable_Videolessons();
        
        $video_id = $this->getRequest()->getParam('video_id');
        $page = $this->getRequest()->getParam('page');
        $this->view->page = $page; 
        $video_image = $video_model->getVideoImage($video_id);
        $this->view->video_image = $video_image; 
        $this->view->video_id = $video_id; 
        
        $video = $video_model->getVideo($video_id); //Zend_Debug::dump($products_attributes);exit;
        $this->view->video = $video; //Zend_Debug::dump($product['manufacturer_id']);exit;
        
        $old_photo_file = $video_model->getVideoImage($video_id);
        $directory = "media/photos/videolessons";
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $sort_order = $_POST['sort_order'];
                $alt = $form->getValue('alt');
				$video_title = $form->getValue('video_title');
				$html_code = $form->getValue('html_code');
                $image = $form->getValue('image'); //Zend_Debug::dump($image);exit;
			
                $data = $_FILES['image']; // Zend_Debug::dump($data);exit;
                $uploadedfile = $_FILES['image']['tmp_name']; 
                        //  Zend_Debug::dump($uploadedfile);exit;
                if($image)
                {       
                    if(is_file("$directory/small_$old_photo_file"))
                    {
						unlink("$directory/big_$old_photo_file");
						unlink("$directory/small_$old_photo_file");
                    }
                }
                
                if(file_exists($uploadedfile))
                {
                   /* ********************************************** */
                    $ext = explode(".",$data["name"]); 
                    $newname = date("Ymd")."_".rand(1000,9999).".".$ext[1];//Генерируем новое имя файла во избежании совпадения названий
					//Zend_Debug::dump($newname);exit;
                    $name_big = "media/photos/videolessons/big_".$newname;
                    $name_small = "media/photos/videolessons/small_".$newname;
                    
                   /* ********************************************** */
                    $image_big = new SimpleImage();
                    $image_big->load($uploadedfile);
                //  $image_big->resizeToWidth(600);
				//	$image_big->cutFromCenter(371, 373);
				//	$image_big->maxareafill(371, 373, 255, 255, 255); 
                    $image_big->save($name_big);
                    
                    $image_small = new SimpleImage();
                    $image_small->load($uploadedfile);
                    $image_small->maxareafill(478, 268, 0, 0, 0); 
                    $image_small->save($name_small);  
                } 
                
                if(!$image)
                {      
                    $video_object = $video_model->getVideo($video_id);
                    $newname = $video_object['image'];
                }							
 
                $video_model->updateVideo(
												$video_id,
												$video_title,
												$html_code,
												$alt,
												$sort_order,
												$newname 
											);
												
                
                $this->_helper->redirector('index', 'videolessons', 'admin', array('page' => $page));
				} else {           
                $this->view->errMessage = "Форма заполнена не верно";
            }
        } else { 
            $video_id = $this->_getParam('video_id', 0);
            if ($video_id > 0)
            {
                $form->populate($video_model->getVideo($video_id));
            }
        }
    }
    
    public function deleteAction()
    {
		$video_model = new Default_Model_DbTable_Videolessons();
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'yes') {
                $video_id = $this->getRequest()->getPost('video_id');
            
                
                $page_video = $video_model->getVideo($video_id);
                
                $old_photo_file = $video_model->getVideoImage($video_id); //Zend_Debug::dump($old_photo_file);exit;
               
                $directory = "media/photos/videolessons";
                
                // удаляем файл из каталога
                if(is_file("$directory/small_$old_photo_file"))
				{
					unlink("$directory/big_$old_photo_file");
					unlink("$directory/small_$old_photo_file");
				}  												
                
                $video_model->deleteVideo(
												$video_id
											);
            }       
            $this->_helper->redirector('index');
        } else {
            $video_id = $this->_getParam('video_id'); // Zend_Debug::dump($nashi_zeny_id);exit;
            
            $this->view->video_object = $video_model->getVideo($video_id);
        }
    }
    
    public function statusonAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $video_model = new Default_Model_DbTable_Videolessons();
        
        if ($this->getRequest()->isPost())
        {
            $video_id = $this->getRequest()->getParam('video_id');
            $status = '1';
            $video_model->updateVideoStatus(
                                                $video_id,  
                                                $status
                                             );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function statusoffAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $video_model = new Default_Model_DbTable_Videolessons();
        
        if ($this->getRequest()->isPost())
        {
            $video_id = $this->getRequest()->getParam('video_id');
            $status = '0';
            $video_model->updateVideoStatus(
                                                $video_id,  
                                                $status
                                             );
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }
    
    public function deleteimageAction()
    {
        $request = $this->getRequest();
        $helper = $this->_helper->getHelper('Layout')->disableLayout();
        $video_model = new Default_Model_DbTable_Videolessons();
        $video_id = $this->getRequest()->getParam('video_id');
        
        $old_photo_file = $video_model->getVideoImage($video_id);
        $directory = "media/photos/videolessons";
        
        if ($this->getRequest()->isPost())
        {
            $video_id = $this->getRequest()->getParam('video_id');
            $image = '';
           
            $video_model->deleteVideoImage(
                                                $video_id,  
                                                $image
                                            );
                                                    
            if(is_file("$directory/small_$old_photo_file"))
            {
                unlink("$directory/big_$old_photo_file");
                unlink("$directory/small_$old_photo_file");
            }                                         
                                    
            header('Content-Type: application/json');
            echo Zend_Json::encode(array('success' => true));
            exit;                        
        }
       
        header('Content-Type: application/json');
        echo Zend_Json::encode(array('success' => false));
        exit;
    }

}

