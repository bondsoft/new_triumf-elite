﻿<?php

/**
 * This is the guestbook form.  It is in its own directory in the application 
 * structure because it represents a "composite asset" in your application.  By 
 * "composite", it is meant that the form encompasses several aspects of the 
 * application: it handles part of the display logic (view), it also handles 
 * validation and filtering (controller and model).  
 *
 * @uses       Zend_Form
 * @package    QuickStart
 * @subpackage Form
 */
class Default_Form_Auth_Login extends Zend_Form
{
    /**
     * init() is the initialization routine called when Zend_Form objects are 
     * created. In most cases, it make alot of sense to put definitions in this 
     * method, as you can see below.  This is not required, but suggested.  
     * There might exist other application scenarios where one might want to 
     * configure their form objects in a different way, those are best 
     * described in the manual:
     *
     * @see    http://framework.zend.com/manual/en/zend.form.html
     * @return void
     */ 
    public function init()
    {
	    $translate = Zend_Registry::get('Zend_Translate');
		$adapter = Zend_Registry::get('Zend_Adapter');
	    // Задаем объект переводчика для формы
        $this->setTranslator($adapter);
		
        // Set the method for the display form to POST
        $this->setMethod('post');
		// указываем имя формы
        $this->setName('login');
		
		// Element: email
        $this->addElement('Text', 'username', array(
          'label' => $translate->translate('Username:'),
		//  'size'  => '20',
          'required' => true,
          'filters'    => array('StringTrim'),
          'attribs' => array('class' => 'span4'),
          'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
        ));
        $element = $this->getElement('username');
        $element->removeDecorator('Label'); 
        
        // Element: password
        $this->addElement('Password', 'password', array(
          'label' => $translate->translate('Password:'),
		//  'size'  => '20',
          'required' => true,
          'attribs' => array('class' => 'span4'),
          'decorators' =>array(
                                    'ViewHelper',
                                    'Label',
                                    array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class'  => 'element')),
                                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                                )
        ));
        $element = $this->getElement('password');
        $element->removeDecorator('Label'); 
       
       
		// Init submit
        $this->addElement('Button', 'submit', array(
          'label' => $translate->translate('Login'),
          'type' => 'submit',
          'attribs' => array('class' => 'btn btn-primary'),
          'ignore' => true,
        ));
		
		
        
        
    }
}

